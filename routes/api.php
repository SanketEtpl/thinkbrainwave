<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('apiGet', 'ApiController@index');
Route::post('SignUp', 'ApiController@SignUp');
Route::get('SignUpSocial/{name}/{email}/{token}', 'ApiController@SignUpSocial');
Route::post('SignIn', 'ApiController@SignIn');
Route::get('ShowProfile/{id}/{role}', 'ApiController@ShowProfile');
Route::post('StudentProfileSave', 'ApiController@StudentProfileSave');
Route::post('UpdatePassword', 'ApiController@UpdatePassword');

Route::post('PostDemo', 'ApiController@PostDemo');


Route::post('TutorProfileSave', 'ApiController@TutorProfileSave');

Route::post('ParentProfileSave', 'ApiController@ParentProfileSave');

// cronJob url
Route::get('OneHourCron', 'CronJobController@OneHourCron');
//end cronJob url


//notify user by sms gateway
Route::post('Sms/{number}','NotifyController@sendSMS');