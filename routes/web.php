<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/website', function () {
    return view('welcome');
});


Route::get('/', function () {
    return view('Website.index');
})->name('home');
Route::get('Home', function () {
    return view('Website.index');
})->name('home');
Route::get('About', function () {
    return view('Website.about');
})->name('about');
Route::get('ContactUs', function () {
    return view('Website.contact_us');
})->name('contact_us');
Route::get('Signin', 'SignupController@Signin')->name('SigninSignup');
Route::resource('SignupView','SignupController');
Route::get('Signup','SignupController@index')->name('SigninSignup');
Route::post('SignupSave','SignupController@SignupSave');
Route::post('SignInSave','SignupController@SignInSave');

Route::get('logout','SignupController@logout');
Route::get('logoutTutor','SignupController@logoutTutor');

Route::get('Faq', function () {
    return view('Website.faq');
});

Route::get('PrivacyPolicy', function () {
    return view('Website.privacy_policy');
});
Route::get('TermsAndService', function () {
    return view('Website.terms_and_service');
});

Route::post('ResetPassLink', 'SignupController@resetPassLink');

Route::get('ResetPassView', function () {
    return view('Website.reset_password');
});

//end website
// email

Route::get('EmailDemo', 'MailController@index');
//Route::get('SendEmail/{to_name}/{to_email}/{body}/{text_subject}', 'MailController@SendEmail');

// end emil

//student
Route::get('Dashboard', 'StudentController@Dashboard')->name('dashboard');

Route::get('MyAccount', 'StudentController@MyAccount')->name('my_account');

Route::get('MyPlan', 'StudentController@MyPlan')->name('my_plan');

Route::get('MyProfile', 'StudentController@MyProfile');
Route::get('EditProfile', 'StudentController@EditProfile');

Route::get('Notification', 'StudentController@Notification');

Route::get('ViewNotificationDetails/{id}', 'StudentController@ViewNotificationDetails');

Route::get('BookSession', 'StudentController@BookSession');

Route::get('BoostingPlans', function () {
    return view('Student.boosting_plans');
});
Route::get('VideoStreaming', 'StudentController@VideoStreaming');

Route::get('studentRecord', 'StudentController@index');

Route::post('GradeFetch','StudentController@GradeFetch');

Route::post('ProfileSave','StudentController@ProfileSave');

Route::post('UpdatePassword', 'SignupController@UpdatePassword');

Route::get('LoginWithFacebook/{id}', 'SignupController@LoginWithFacebook');


Route::get('/callbackFB', 'SignupController@callbackFB');
Route::get('/redirectFB/{id}', 'SignupController@redirectFB');

Route::get('/callback', 'SignupController@callback');
Route::get('/redirect/{id}', 'SignupController@redirect');

Route::get('PostDemo', 'StudentController@PostDemo');

Route::post('FetchSubject','StudentController@FetchSubject');

Route::post('FetchTopic','StudentController@FetchTopic');

Route::post('BookSessionSave','StudentController@BookSessionSave');

Route::post('BookSessionConfirm','StudentController@BookSessionConfirm');

Route::get('StudentSessionCancleSave/{id}/{reason}','StudentController@StudentSessionCancleSave');

Route::get('StartSessionSave/{id}','StudentController@StartSessionSave');

Route::get('StopSessionSave/{id}','StudentController@StopSessionSave');


Route::post('StudentAddRating','StudentController@StudentAddRating');

Route::post('CodeShareWithParentSms','StudentController@CodeShareWithParentSms');

Route::post('CodeShareWithParentEmail','StudentController@CodeShareWithParentEmail');


Route::get('SessionAssignToTutorAlgo','AllCommonDataController@SessionAssignToTutorAlgo');

Route::get('GetLatLongDemo','AllCommonDataController@GetLatLongDemo');

Route::get('SessionAssignToTutorAlgoFinal/{id2}/{id3}/{id4}/{id5}/{id6}/{id7}/{id8}/{id9}/{id10}/{id11}/{id12}/{id13}','AllCommonDataController@SessionAssignToTutorAlgoFinal');

//end student

//tutor

Route::get('TutorDashboard', 'TutorController@index')->name('tutor_dashboard');

Route::get('TutorAccount', 'TutorController@TutorAccount')->name('tutor_account');

Route::get('TutorBoostingPlans', 'TutorController@TutorBoostingPlans')->name('tutor_boosting_plans');

Route::get('TutorEditProfile', 'TutorController@TutorEditProfile')->name('tutor_edit_profile');

Route::post('TurorProfileSave','TutorController@TurorProfileSave');

Route::get('TutorMyPlan', 'TutorController@TutorMyPlan')->name('tutor_my_plan');

Route::get('TutorSessionCancleSave/{id}/{reason}','TutorController@TutorSessionCancleSave');

Route::get('TutorMyProfile', 'TutorController@TutorMyProfile')->name('tutor_my_profile');

Route::get('TutorNotification', 'TutorController@TutorNotification')->name('tutor_notification');

Route::get('TutorViewNotificationDetails/{id}', 'TutorController@TutorViewNotificationDetails');

Route::get('TutorVideoStreaming', 'TutorController@TutorVideoStreaming')->name('tutor_video_streaming');

Route::get('TutorStartSessionSave/{id}','TutorController@TutorStartSessionSave');

Route::get('TutorStopSessionSave/{id}','TutorController@TutorStopSessionSave');

Route::get('TutorSessionDataLastweek/{id}','TutorController@TutorSessionDataLastweek');
Route::get('TutorSessionDataLastMonth/{id}','TutorController@TutorSessionDataLastMonth');
Route::get('TutorSessionDataLastYear/{id}','TutorController@TutorSessionDataLastYear');

Route::post('startSessionUsingOtp','TutorController@startSessionUsingOtp');

Route::post('TutorAddRating','TutorController@TutorAddRating');

Route::post('TutorAvailabilitySave','TutorController@TutorAvailabilitySave');

//end tutor


// Parent
Route::get('ParentDashboard', 'ParentController@index');

Route::get('ParentMyProfile', 'ParentController@parentMyProfile')->name('my_profile');

Route::get('ParentEditProfile', 'ParentController@parentEditProfile');

Route::post('ParentProfileSave','ParentController@parentProfileSave');

Route::get('ParentStudentProfile','ParentController@parentStudentProfile');

Route::get('ParStudEditProfile/{id}/{role_id}', 'ParentController@parStudEditProfile');

Route::post('ParStudProfileSave','ParentController@parStudProfileSave');

Route::post('SaveNewPass','ParentController@saveNewPass');

Route::get('ChildCalender/{id}','ParentController@index');

Route::get('ChildProfile/{id}','ParentController@parentStudentProfile');

Route::get('ParentMyPlan','ParentController@parentMyPlan')->name('my_plan');

Route::get('StudentMyPlan/{id}','ParentController@parentMyPlan');

Route::get('ParentMyAccount','ParentController@parentMyAccount')->name('my_account');

Route::get('getSessionByChild/{id}','ParentController@parentMyAccount');

Route::post('AddChild','ParentController@addChild');

Route::get('ParentBookSession/{id}','ParentController@parentBookSession');

Route::post('FetchSubjectForStudent','ParentController@fetchSubjectForStudent');

Route::post('FetchTopicForStudent','ParentController@fetchTopicForStudent');

Route::post('BookStudentSessionSave','ParentController@bookSessionSave');

Route::post('BookStudentSessionConfirm','ParentController@bookSessionConfirm');

Route::get('SessionCancleSaveByParent/{id}/{reason}','ParentController@studentSessionCancleSave');

Route::get('AllNotification', 'ParentController@allNotification');

Route::get('ViewNotification/{id}', 'ParentController@viewNotification');

//stripe route

//end stripe route
Route::get('stripePayment', 'AddMoneyController@stripePayment');
Route::post('stripepay', 'AddMoneyController@postPaymentWithStripe');
// end Parent


//ADMIN Backend

Route::get('admin', function () {
    return view('Backend.index');
});

Route::get('admin/dashboard', function () {
    return view('Backend.dashboard');
});

Route::post('forgotPassword','AdminProfileController@ForgotPassword');

// Myprofile
Route::post('admin/adminlogin','AdminProfileController@AdminLogin');
Route::get('admin/logout','AdminProfileController@logout');
Route::get('admin/myprofile','AdminProfileController@index');
Route::post('admin/myprofile/updateprofile','AdminProfileController@updateprofile');
Route::post('admin/myprofile/updatebankdetails','AdminProfileController@updatebankdetails');
Route::post('admin/myprofile/FetchState','AdminProfileController@FetchState');
Route::post('admin/myprofile/FetchCity','AdminProfileController@FetchCity');



//Grade
Route::get('admin/grade','GradeController@index');
Route::post('admin/grade/addGrade','GradeController@addGrade');
Route::get('admin/grade/editGrade/{id}','GradeController@index');
Route::get('admin/grade/deleteGrade/{id}','GradeController@deleteGrade');
Route::get('admin/grade/changeStatus/{id}/{status}','GradeController@changeStatus');



//Topics
Route::get('admin/topics','TopicController@index');
Route::post('admin/topics/addTopic','TopicController@addTopic');
Route::get('admin/topics/editTopic/{id}','TopicController@index');
Route::get('admin/topics/deleteTopic/{id}','TopicController@deleteTopic');
Route::post('admin/topics/FetchCourse','TopicController@FetchCourse');
Route::get('admin/topics/changeStatus/{id}/{status}','TopicController@changeStatus');

//Resolution Center
Route::get('admin/resolution','ResolutionController@index');
Route::get('admin/resolution/deleteResolution/{id}','ResolutionController@deleteResolution');

//Notify
Route::get('admin/notify','NotifyController@index');
Route::get('AdminAllNotification', 'NotifyController@adminAllNotification');
Route::get('ViewAdminNotification/{id}', 'NotifyController@viewAdminNotification');

//Reports 
Route::get('admin/reports','ReportController@index');

//Subscription Plan
Route::get('admin/subscription','SubscriptionController@index');
Route::post('admin/subscription/addsubscription','SubscriptionController@addsubscription');
Route::get('admin/subscription/editsubscription/{id}','SubscriptionController@index');
Route::get('admin/subscription/deletesubscription/{id}','SubscriptionController@deletesubscription');
Route::get('admin/subscription/changeStatus/{id}/{status}','SubscriptionController@changeStatus');

// users 
Route::get('admin/users','UserController@index');
Route::post('admin/users/addUser','UserController@addUser');
Route::get('admin/users/acceptUser/{user_id}','UserController@acceptUser');
Route::get('admin/users/rejectUser/{user_id}','UserController@rejectUser');
Route::get('admin/users/deleteUser/{user_id}','UserController@deleteUser');
Route::get('admin/users/editUser/{user_id}/{role_id}','UserController@index');
Route::post('EmailValid','UserController@EmailValid');
Route::get('admin/users/changeStatus/{id}/{status}','UserController@changeStatus');

// courses
Route::get('admin/courses','CourseController@index');
Route::post('admin/courses/addCourse','CourseController@addCourse');
Route::get('admin/courses/deleteCourse/{subject_id}','CourseController@deleteCourse');
Route::get('admin/courses/editCourse/{subject_id}','CourseController@index');
Route::get('admin/courses/changeStatus/{id}/{status}','CourseController@changeStatus');

// Pricing
Route::get('admin/price','PriceController@index');
Route::post('admin/price/addPrice','PriceController@addPrice');
Route::get('admin/price/deletePrice/{price_id}','PriceController@deletePrice');
Route::get('admin/price/editPrice/{price_id}','PriceController@index');
Route::get('admin/price/changeStatus/{id}/{status}','PriceController@changeStatus');


//Content management
Route::get('admin/contentmanagement','ContentManagementController@index');
Route::post('admin/contentmanagement/addContent','ContentManagementController@addContent');
Route::get('admin/contentmanagement/deleteContent/{id}','ContentManagementController@deleteContent');
Route::get('admin/contentmanagement/changeStatus/{id}/{status}','ContentManagementController@changeStatus');
Route::get('admin/contentmanagement/editContent/{id}','ContentManagementController@index');


Route::get('chat', function () {
    return view('laravel_chat');
});
 
