DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sign_in`(IN `input_username` VARCHAR(200), IN `input_password` VARCHAR(200), OUT `result` JSON)
    NO SQL
BEGIN
	DECLARE tmp varchar(500);
    DECLARE last_id int(11);
    DECLARE rowcount int(11);   
    
    SET rowcount = (SELECT COUNT(id) FROM user_master_ut WHERE username=input_username AND status='1' AND is_deleted='0');
        
    IF(rowcount = 0)
            THEN
       SET result = JSON_OBJECT('status','exists');
       
    ELSE  
   		
        
        SET rowcount = (SELECT COUNT(id) FROM user_master_ut WHERE username = input_username AND password = input_password);
        
        IF(rowcount = 0)
            THEN
       		SET result = JSON_OBJECT('status','no data');
        ELSE
   			SET result = (SELECT JSON_OBJECT('id',id,'role_id',role_id,'username',username,'password',password) FROM user_master_ut WHERE username = input_username AND password = input_password);
        
        END IF;
            
    END IF;
END$$
DELIMITER ;


-----------------------------------------------------------------------------------------------------------------------------------------------------------------------


DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sign_up_parent`(IN `key_cond` VARCHAR(100), IN `get_id` INT(11), IN `input_username` VARCHAR(200), IN `input_password` TEXT, IN `input_name` VARCHAR(200), IN `input_dob` VARCHAR(200), IN `input_gender` VARCHAR(200), IN `input_phone_number` VARCHAR(200), IN `input_email` VARCHAR(200), IN `input_address` TEXT, IN `input_city_id` INT(11), IN `input_state_id` INT(11), IN `input_country_id` INT(11), OUT `result` JSON)
    NO SQL
BEGIN
  DECLARE tmp varchar(500);
    DECLARE last_id int(11);
    DECLARE rowcount int(11);
    
   IF(key_cond = 'insert') 
    THEN
  SET rowcount = (SELECT COUNT(id) as tot_rec FROM user_master_ut WHERE username = input_username AND status='1' AND is_deleted='0');
       IF(rowcount > 0)
            THEN
            SET result = JSON_OBJECT('status','exists');
          ELSE
          INSERT INTO user_master_ut (`role_id`, `username`, `password`) VALUES ('4', input_username, input_password); 
            
            SET last_id = (SELECT LAST_INSERT_ID());
                
            INSERT INTO parent_details_ut (`user_id`,`name`, `email`,`phone_number`) VALUES (last_id, input_name, input_username, input_phone_number);
            
            SET rowcount = (SELECT ROW_COUNT());
            IF(rowcount > 0)
                THEN
                
                
                SET result = JSON_OBJECT('status','success','id',last_id);
            ELSE
                SET result = JSON_OBJECT('status','error');
            END IF;
        
         END IF;
  END IF;
    IF(key_cond = 'email_verifi') 
      THEN
    SET rowcount = (SELECT COUNT(id) FROM user_master_ut WHERE id=get_id AND status='1' AND is_deleted='0');
        IF(rowcount = 0)
            THEN
            SET result = JSON_OBJECT('status','exists');
            
        ELSE  
        UPDATE `user_master_ut` set `verify_status` = '1' WHERE `id` = get_id AND is_deleted='0';
   
        SET rowcount = (SELECT ROW_COUNT());
        IF(rowcount > 0)
          THEN 
          SET result = JSON_OBJECT('status','success');
        ELSE
          SET result = JSON_OBJECT('status','error');
        END IF;
    
    END IF;
  END IF;
    IF(key_cond = 'password_update') 
      THEN
    SET rowcount = (SELECT COUNT(id) FROM user_master_ut WHERE id=get_id AND status='1' AND is_deleted='0');
        IF(rowcount = 0)
            THEN
            SET result = JSON_OBJECT('status','exists');
            
        ELSE  
        UPDATE `user_master_ut` set `password` = input_password WHERE `id` = get_id AND is_deleted='0';
   
        SET rowcount = (SELECT ROW_COUNT());
        IF(rowcount > 0)
          THEN 
          SET result = JSON_OBJECT('status','success');
        ELSE
          SET result = JSON_OBJECT('status','error');
        END IF;
    
    END IF;
  END IF;
    IF(key_cond = 'profile_update') 
      THEN
    SET rowcount = (SELECT COUNT(id) FROM tutor_details_ut WHERE user_id=get_id);
        IF(rowcount = 0)
            THEN
            SET result = JSON_OBJECT('status','exists');
            
        ELSE  
        UPDATE `parent_details_ut` set `name` = input_name, `dob` = input_dob, `gender` = input_gender, `phone_number` = input_phone_number, `email` = input_email, `address` = input_address, `city_id` = input_city_id, `state_id` = input_state_id, `country_id` = input_country_id WHERE `user_id` = get_id;
   
        SET rowcount = (SELECT ROW_COUNT());
        IF(rowcount > 0)
          THEN 
          SET result = JSON_OBJECT('status','success');
        ELSE
          SET result = JSON_OBJECT('status','error');
        END IF;
    
    END IF;
  END IF;
    IF(key_cond = 'select') 
      THEN
    
         SET result =  (SELECT JSON_OBJECT('id',PT.id,'user_id',PT.user_id,'name',PT.name,'dob',PT.dob,'gender',PT.gender,'phone_number',PT.phone_number,'email',PT.email,'address',PT.address,'city_id',PT.city_id,'state_id',PT.state_id,'country_id',PT.country_id,'city_name',CT.name,'state_name',ST.name,'country_name',COT.name) FROM parent_details_ut PT

LEFT JOIN cities_master_ut CT ON PT.city_id = CT.id 
LEFT JOIN states_master_ut ST  ON PT.state_id = ST.id 
LEFT JOIN countries_master_ut COT  ON PT.country_id = COT.id 
WHERE user_id = get_id);
  END IF;
    IF(key_cond = 'delete')
        THEN 
    
        SET rowcount = (SELECT COUNT(id) FROM user_master_ut WHERE id=get_id);
        IF(rowcount = 0)
            THEN
            SET result = JSON_OBJECT('status','exists');
        ELSE
            UPDATE user_master_ut SET `is_deleted`=1 WHERE id = get_id;  
            SET rowcount = (SELECT ROW_COUNT());
            IF(rowcount > 0)
                THEN
                SET result = JSON_OBJECT('status','success');
            ELSE
                SET result = JSON_OBJECT('status','error');
            END IF;
        END IF;

    END IF;
END$$
DELIMITER ;

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sign_up_student`(IN `key_cond` VARCHAR(200), IN `get_id` INT(11), IN `input_username` VARCHAR(200), IN `input_password` TEXT, IN `input_first_name` VARCHAR(200), IN `input_middle_name` VARCHAR(200), IN `input_last_name` VARCHAR(200), IN `input_gender` VARCHAR(200), IN `input_dob` VARCHAR(200), IN `input_address_line1` TEXT, IN `input_address_line2` TEXT, IN `input_pincode` VARCHAR(200), IN `input_city_id` INT(11), IN `input_state_id` INT(11), IN `input_country_id` INT(11), IN `input_max_distance_travel_kms` INT(11), IN `input_primary_phone` VARCHAR(200), IN `input_alternate_phone` VARCHAR(200), IN `input_profile_file_path` TEXT, OUT `result` JSON)
    NO SQL
BEGIN
  DECLARE tmp varchar(500);
    DECLARE last_id int(11);
    DECLARE rowcount int(11);
    
    IF(key_cond = 'insert') 
    THEN
  SET rowcount = (SELECT COUNT(id) as tot_rec FROM user_master_ut WHERE username = input_username AND status='1' AND is_deleted='0');
       IF(rowcount > 0)
            THEN
            SET result = JSON_OBJECT('status','exists');
          ELSE
          INSERT INTO user_master_ut (`role_id`, `username`,`password`) VALUES ('3', input_username, input_password); 
            SET last_id = (SELECT LAST_INSERT_ID()); 
                INSERT INTO student_master_ut (`user_id`,`first_name`, `primary_phone`) VALUES (last_id, input_first_name, input_primary_phone); 
            SET rowcount = (SELECT ROW_COUNT());
            IF(rowcount > 0)
                THEN
                SET result = JSON_OBJECT('status','success','id',last_id);
            ELSE
                SET result = JSON_OBJECT('status','error');
            END IF;
        
         END IF;
  END IF;
    IF(key_cond = 'email_verifi') 
      THEN
    SET rowcount = (SELECT COUNT(id) FROM user_master_ut WHERE id=get_id AND status='1' AND is_deleted='0');
        IF(rowcount = 0)
            THEN
            SET result = JSON_OBJECT('status','exists');
            
        ELSE  
        UPDATE `user_master_ut` set `verify_status` = '1' WHERE `id` = get_id AND is_deleted='0';
   
        SET rowcount = (SELECT ROW_COUNT());
        IF(rowcount > 0)
          THEN 
          SET result = JSON_OBJECT('status','success');
        ELSE
          SET result = JSON_OBJECT('status','error');
        END IF;
    
    END IF;
  END IF;
    IF(key_cond = 'password_update') 
      THEN
    SET rowcount = (SELECT COUNT(id) FROM user_master_ut WHERE id=get_id AND status='1' AND is_deleted='0');
        IF(rowcount = 0)
            THEN
            SET result = JSON_OBJECT('status','exists');
            
        ELSE  
        UPDATE `user_master_ut` set `password` = input_password WHERE `id` = get_id AND is_deleted='0';
   
        SET rowcount = (SELECT ROW_COUNT());
        IF(rowcount > 0)
          THEN 
          SET result = JSON_OBJECT('status','success');
        ELSE
          SET result = JSON_OBJECT('status','error');
        END IF;
    
    END IF;
  END IF;
    IF(key_cond = 'delete')
        THEN 
    
        SET rowcount = (SELECT COUNT(id) FROM user_master_ut WHERE id=get_id);
        IF(rowcount = 0)
            THEN
            SET result = JSON_OBJECT('status','exists');
        ELSE
            UPDATE user_master_ut SET `is_deleted`=1 WHERE id = get_id;  
            SET rowcount = (SELECT ROW_COUNT());
            IF(rowcount > 0)
                THEN
                SET result = JSON_OBJECT('status','success');
            ELSE
                SET result = JSON_OBJECT('status','error');
            END IF;
        END IF;

    END IF;
    
    IF(key_cond = 'select') 
      THEN
        SET result = (SELECT JSON_OBJECT('id', GROUP_CONCAT( DISTINCT ST.id), 'user_id', GROUP_CONCAT( DISTINCT IFNULL(ST.user_id,'')), 'first_name', GROUP_CONCAT( DISTINCT IFNULL(ST.first_name,'')), 'middle_name', GROUP_CONCAT( DISTINCT IFNULL(ST.middle_name,'')), 'last_name', GROUP_CONCAT( DISTINCT IFNULL(ST.last_name,'')), 'gender', GROUP_CONCAT( DISTINCT IFNULL(ST.gender,'')), 'dob', GROUP_CONCAT( DISTINCT IFNULL(ST.dob,'')), 'address_line1', GROUP_CONCAT( DISTINCT IFNULL(ST.address_line1,'')), 'address_line2', GROUP_CONCAT( DISTINCT IFNULL(ST.address_line2,'')), 'pincode', GROUP_CONCAT(DISTINCT IFNULL(ST.pincode,'')), 'city_id', GROUP_CONCAT( DISTINCT IFNULL(ST.city_id,'')), 'state_id', GROUP_CONCAT( DISTINCT IFNULL(ST.state_id,'')), 'country_id', GROUP_CONCAT( DISTINCT IFNULL(ST.country_id,'')), 'max_distance_travel_kms', GROUP_CONCAT( DISTINCT IFNULL(ST.max_distance_travel_kms,'')), 'primary_phone', GROUP_CONCAT( DISTINCT IFNULL(ST.primary_phone,'')), 'alternate_phone', GROUP_CONCAT( DISTINCT IFNULL(ST.alternate_phone,'')), 'profile_file_path', GROUP_CONCAT( DISTINCT IFNULL(ST.profile_file_path,'')), 'city_name', GROUP_CONCAT( DISTINCT IFNULL(CT.name,'')), 'state_name', GROUP_CONCAT( DISTINCT IFNULL(StT.name,'')),'country_name', GROUP_CONCAT( DISTINCT IFNULL(COT.name,'')),'grade_name', GROUP_CONCAT( DISTINCT IFNULL(GM.name,'')),'grade_id', GROUP_CONCAT( DISTINCT IFNULL(GM.id,'')),  'subject_name', GROUP_CONCAT(IFNULL(SUBM.name,''))) FROM student_master_ut ST
LEFT JOIN cities_master_ut CT ON ST.city_id = CT.id  
LEFT JOIN states_master_ut StT  ON ST.state_id = StT.id 
LEFT JOIN countries_master_ut COT  ON ST.country_id = COT.id 
LEFT JOIN student_grade_ut STG  ON ST.user_id = STG.student_id 
LEFT JOIN grade_master_ut GM  ON STG.grade_id = GM.id
LEFT JOIN subject_master_ut SUBM  ON GM.id = SUBM.grade_id
WHERE user_id = get_id
GROUP BY GM.id); 
  
  END IF;
    
    
    
    IF(key_cond = 'profile_update') 
      THEN
    SET rowcount = (SELECT COUNT(id) FROM student_master_ut WHERE user_id=get_id);
        IF(rowcount = 0)
            THEN
            SET result = JSON_OBJECT('status','exists');
            
        ELSE  
        UPDATE `student_master_ut` set `first_name` = input_first_name, `middle_name` = input_middle_name, `last_name` = input_last_name, `gender` = input_gender, `dob` = input_dob, `address_line1` = input_address_line1, `address_line2` = input_address_line2, `pincode` = input_pincode, `city_id` = input_city_id, `state_id` = input_state_id, `country_id` = input_country_id, `max_distance_travel_kms` = input_max_distance_travel_kms, `primary_phone` = input_primary_phone, `alternate_phone` = input_alternate_phone, `profile_file_path` = input_profile_file_path WHERE `user_id` = get_id;
   
        SET rowcount = (SELECT ROW_COUNT());
        IF(rowcount > 0)
          THEN 
          SET result = JSON_OBJECT('status','success');
        ELSE
          SET result = JSON_OBJECT('status','error');
        END IF;
    
    END IF;
  END IF;
    
 END$$
DELIMITER ;

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------


DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sign_up_tutor`(IN `key_cond` VARCHAR(100), IN `get_id` INT(11), IN `input_username` VARCHAR(200), IN `input_password` TEXT, IN `input_name` VARCHAR(200), IN `input_dob` VARCHAR(200), IN `input_gender` VARCHAR(200), IN `input_profile_file_path` TEXT, IN `input_address` TEXT, IN `input_city_id` INT, IN `input_state_id` INT, IN `input_country_id` INT, IN `input_pincode` VARCHAR(20), IN `input_primary_phone` VARCHAR(200), IN `input_alternate_phone` VARCHAR(200), IN `input_email_id` VARCHAR(200), IN `input_max_distance_travel` VARCHAR(200), IN `input_year_exp` VARCHAR(200), OUT `result` JSON)
    NO SQL
BEGIN
  DECLARE tmp varchar(500);
    DECLARE last_id int(11);
    DECLARE rowcount int(11);
    
   IF(key_cond = 'insert') 
    THEN
  SET rowcount = (SELECT COUNT(id) as tot_rec FROM user_master_ut WHERE username = input_username AND status='1' AND is_deleted='0');
       IF(rowcount > 0)
            THEN
            SET result = JSON_OBJECT('status','exists');
          ELSE
          INSERT INTO user_master_ut (`role_id`, `username`, `password`) VALUES ('2', input_username, input_password); 
            
            SET rowcount = (SELECT ROW_COUNT());
            IF(rowcount > 0)
                THEN
                SET last_id = (SELECT LAST_INSERT_ID()); 
                INSERT INTO tutor_details_ut (`user_id`,`name`, `email_id`,`primary_phone`) VALUES (last_id, input_name, input_username, input_primary_phone);
                
                SET result = JSON_OBJECT('status','success','id',last_id);
            ELSE
                SET result = JSON_OBJECT('status','error');
            END IF;
        
         END IF;
  END IF;
    IF(key_cond = 'email_verifi') 
      THEN
    SET rowcount = (SELECT COUNT(id) FROM user_master_ut WHERE id=get_id AND status='1' AND is_deleted='0');
        IF(rowcount = 0)
            THEN
            SET result = JSON_OBJECT('status','exists');
            
        ELSE  
        UPDATE `user_master_ut` set `verify_status` = '1' WHERE `id` = get_id AND is_deleted='0';
   
        SET rowcount = (SELECT ROW_COUNT());
        IF(rowcount > 0)
          THEN 
          SET result = JSON_OBJECT('status','success');
        ELSE
          SET result = JSON_OBJECT('status','error');
        END IF;
    
    END IF;
  END IF;
    IF(key_cond = 'password_update') 
      THEN
    SET rowcount = (SELECT COUNT(id) FROM user_master_ut WHERE id=get_id AND status='1' AND is_deleted='0');
        IF(rowcount = 0)
            THEN
            SET result = JSON_OBJECT('status','exists');
            
        ELSE  
        UPDATE `user_master_ut` set `password` = input_password WHERE `id` = get_id AND is_deleted='0';
   
        SET rowcount = (SELECT ROW_COUNT());
        IF(rowcount > 0)
          THEN 
          SET result = JSON_OBJECT('status','success');
        ELSE
          SET result = JSON_OBJECT('status','error');
        END IF;
    
    END IF;
  END IF;
    IF(key_cond = 'profile_update') 
      THEN
    SET rowcount = (SELECT COUNT(id) FROM tutor_details_ut WHERE user_id=get_id);
        IF(rowcount = 0)
            THEN
            SET result = JSON_OBJECT('status','exists');
            
        ELSE  
        UPDATE `tutor_details_ut` set `name` = input_name, `dob` = input_dob, `gender` = input_gender, `profile_file_path` = input_profile_file_path, `address` = input_address, `city_id` = input_city_id, `state_id` = input_state_id, `country_id` = input_country_id, `pincode` = input_pincode, `primary_phone` = input_primary_phone, `alternate_phone` = input_alternate_phone, `email_id` = input_email_id, `max_distance_travel` = input_max_distance_travel, `year_exp` = input_year_exp WHERE `user_id` = get_id;
   
        SET rowcount = (SELECT ROW_COUNT());
        IF(rowcount > 0)
          THEN 
          SET result = JSON_OBJECT('status','success');
        ELSE
          SET result = JSON_OBJECT('status','error');
        END IF;
    
    END IF;
  END IF;
    IF(key_cond = 'select') 
      THEN
    
       SET result = (SELECT JSON_OBJECT('id',TT.id,'user_id',TT.user_id,'name',TT.name,'dob',TT.dob,'gender',TT.gender,'level',TT.level,'profile_file_path',TT.profile_file_path,'address',TT.address,'city_id',TT.city_id,'state_id',TT.state_id,'country_id',TT.country_id,'pincode',TT.pincode,'primary_phone',TT.primary_phone,'alternate_phone',TT.alternate_phone,'email_id',TT.email_id,'max_distance_travel',TT.max_distance_travel,'year_exp',TT.year_exp,'city_name',CT.name,'state_name',StT.name,'country_name',COT.name) FROM tutor_details_ut TT

LEFT JOIN cities_master_ut CT ON TT.city_id = CT.id 
LEFT JOIN states_master_ut StT  ON TT.state_id = StT.id 
LEFT JOIN countries_master_ut COT  ON TT.country_id = COT.id 
WHERE user_id = get_id);
  END IF;
    IF(key_cond = 'delete')
        THEN 
    
        SET rowcount = (SELECT COUNT(id) FROM user_master_ut WHERE id=get_id);
        IF(rowcount = 0)
            THEN
            SET result = JSON_OBJECT('status','exists');
        ELSE
            UPDATE user_master_ut SET `is_deleted`=1 WHERE id = get_id;  
            SET rowcount = (SELECT ROW_COUNT());
            IF(rowcount > 0)
                THEN
                SET result = JSON_OBJECT('status','success');
            ELSE
                SET result = JSON_OBJECT('status','error');
            END IF;
        END IF;

    END IF;
END$$
DELIMITER ;

-------------------------------------------------------------------------------------------------------------------------------------------------------------


ADMIN SECTION 


DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_admin_profile`(IN `key_cond` VARCHAR(200), IN `get_id` INT(11), IN `name` VARCHAR(200), IN `phone` VARCHAR(200), IN `email` VARCHAR(200), IN `address` TEXT, IN `city_id` INT(11), IN `state_id` INT(11), IN `country_id` INT(11), IN `bank_name` VARCHAR(200), IN `branch_name` VARCHAR(200), IN `Ifsc` VARCHAR(200), IN `swift_code` BIGINT(100), IN `micr_code` BIGINT(100), IN `account_no` BIGINT(100), OUT `result` JSON)
    NO SQL
BEGIN
    DECLARE last_id int(11);
    DECLARE rowcount int(11);
    IF(key_cond = 'select')
    
    THEN
    
      SET result =  (SELECT JSON_OBJECT('id',IFNULL(AD.id,''),'user_id',IFNULL(AD.user_id,''),'name',IFNULL(AD.name,''),'contact_number',IFNULL(AD.contact_number,''),'email',IFNULL(AD.email,''),'address',IFNULL(AD.address,''),'city_id',IFNULL(AD.city_id,''),'state_id',IFNULL(AD.state_id,''),'country_id',IFNULL(AD.country_id,''),'city_name',IFNULL(CT.name,''),'state_name',IFNULL(ST.name,''),'country_name',IFNULL(COT.name,''),'bank_name',IFNULL(ADB.bank_name,''),'branch_name',IFNULL(ADB.branch_name,''),'Ifsc',IFNULL(ADB.Ifsc,''),'swift_code',IFNULL(ADB.swift_code,''),'micr_code',IFNULL(ADB.micr_code,''),'account_no',IFNULL(ADB.account_no,'')) FROM  admin_details_ut AD

LEFT JOIN admin_bank_details_ut ADB ON AD.user_id = ADB.user_id 
LEFT JOIN cities_master_ut CT ON AD.city_id = CT.id 
LEFT JOIN states_master_ut ST  ON AD.state_id = ST.id 
LEFT JOIN countries_master_ut COT  ON AD.country_id = COT.id 
WHERE AD.user_id = get_id);
  END IF;
 
   IF(key_cond = 'profile_update') 
    THEN
    
    SET rowcount = (SELECT COUNT(id) FROM admin_details_ut WHERE user_id=get_id);
      
       IF(rowcount = 0)
            THEN
            SET result = JSON_OBJECT('status','user does not exists');
        ELSE  
      
        UPDATE `admin_details_ut` set `name` = name, `contact_number` = phone,`email` = email,`address` = address,`city_id`= city_id,`state_id` = state_id,`country_id` = country_id WHERE `user_id` = get_id;
        
        SET rowcount = (SELECT ROW_COUNT());
        IF(rowcount > 0)
          THEN 
          SET result = JSON_OBJECT('status','Profile updated successfully');
        ELSE
            
          SET result = JSON_OBJECT('status','error');
             END IF; 
      END IF;       
   END IF;
    
        
   IF(key_cond = 'bank_detail_update') 
    THEN
    
    SET rowcount = (SELECT COUNT(id) FROM admin_bank_details_ut WHERE user_id=get_id);
      
       IF(rowcount = 0)
            THEN
            INSERT INTO admin_bank_details_ut (`user_id`, `bank_name`,`branch_name`,`Ifsc`,`swift_code`,`micr_code`,`account_no`) VALUES (get_id, bank_name, branch_name,Ifsc,swift_code,micr_code,account_no); 
            
            
            SET rowcount = (SELECT ROW_COUNT());
        IF(rowcount > 0)
          THEN 
          SET result = JSON_OBJECT('status','Back details added successfully');
        ELSE
            
          SET result = JSON_OBJECT('status','error');
             END IF; 
            
            
        ELSE  
      
        UPDATE `admin_bank_details_ut` set `bank_name` = bank_name, `branch_name` = branch_name,`Ifsc` = Ifsc,`swift_code` = swift_code,`micr_code`= micr_code,`account_no` = account_no WHERE `user_id` = get_id;
        
        SET rowcount = (SELECT ROW_COUNT());
        IF(rowcount > 0)
          THEN 
          SET result = JSON_OBJECT('status','Bank details updated successfully');
        ELSE
            
          SET result = JSON_OBJECT('status','error');
             END IF; 
      END IF;       
   END IF;
    
END$$
DELIMITER ;

----------------------------------------------------------------------------------------------------------------------------------------------------------------------

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_grade`(IN `key_cond` VARCHAR(200), IN `get_id` INT(11), IN `grade` VARCHAR(200), IN `active_status` INT(11), OUT `result` JSON)
    NO SQL
BEGIN

DECLARE rowcount int(11);

IF(key_cond = 'select')
THEN
SET result = (SELECT CONCAT('[',GROUP_CONCAT(Json_object('name',name,'status',status,id,'id')),']') FROM grade_master_ut WHERE is_deleted = 0); 

END IF;

IF(key_cond = 'insert')

THEN

 INSERT INTO grade_master_ut (`name`,`status`) VALUES
 (grade, active_status); 
 
   
  SET rowcount = (SELECT ROW_COUNT());
  
   IF(rowcount > 0)
      THEN 
      SET result = JSON_OBJECT('status','Grade added successfully');
        ELSE
            
   SET result = JSON_OBJECT('status','error');
    END IF; 
  END IF; 
 
 IF(key_cond = 'update')
 THEN
 SET rowcount = (SELECT COUNT(id) FROM grade_master_ut WHERE id = get_id);
   
       IF(rowcount = 0)
            THEN
            SET result = JSON_OBJECT('status','grade does not exists');
        ELSE  
      
        UPDATE `grade_master_ut` set `name` = grade, `status` = active_status WHERE `id` = get_id;
        
        SET rowcount = (SELECT ROW_COUNT());
        IF(rowcount > 0)
          THEN 
          SET result = JSON_OBJECT('status','Update successfully');
        ELSE
            
          SET result = JSON_OBJECT('status','error');
             END IF; 
      END IF;       
 END IF;
 
  IF(key_cond = 'delete')
        THEN 
    
        SET rowcount = (SELECT COUNT(id) FROM grade_master_ut WHERE id=get_id);
        IF(rowcount = 0)
            THEN
            SET result = JSON_OBJECT('status','does not exist');
        ELSE
            UPDATE grade_master_ut SET `is_deleted`=1 WHERE id = get_id;  
            SET rowcount = (SELECT ROW_COUNT());
            IF(rowcount > 0)
                THEN
                SET result = JSON_OBJECT('status','success');
            ELSE
                SET result = JSON_OBJECT('status','error');
            END IF;
        END IF;

    END IF;
END$$
DELIMITER ;


------------------------------------------------------------------------------------------------------------------------------------------------------------------

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_course`(IN `key_cond` VARCHAR(200), IN `get_id` INT(11), IN `grade_id` INT(11), IN `name` VARCHAR(200), IN `description` TEXT, IN `active_status` INT(11), OUT `result` JSON)
    NO SQL
BEGIN

DECLARE rowcount int(11);

IF(key_cond = 'select')
THEN
SET result = (SELECT CONCAT('[',GROUP_CONCAT(Json_object('name',sm.name,'status',sm.status,'id',sm.id,'description',sm.description,'grade_id',sm.grade_id,'grade_name',gm.name)),']') FROM subject_master_ut sm 
              
LEFT JOIN  grade_master_ut  gm ON gm.id = sm.grade_id  
              
WHERE sm.is_deleted = 0); 

END IF;

IF(key_cond = 'insert')

THEN

 INSERT INTO subject_master_ut (`name`,`status`,`grade_id`,`description`) VALUES
 (name, active_status,grade_id,description); 
 
   
  SET rowcount = (SELECT ROW_COUNT());
  
   IF(rowcount > 0)
      THEN 
      SET result = JSON_OBJECT('status','add successfully');
        ELSE
            
   SET result = JSON_OBJECT('status','error');
    END IF; 
  END IF; 
 
 IF(key_cond = 'update')
 THEN
 SET rowcount = (SELECT COUNT(id) FROM subject_master_ut WHERE id = get_id);
   
       IF(rowcount = 0)
            THEN
            SET result = JSON_OBJECT('status','does not exist');
        ELSE  
      
        UPDATE `subject_master_ut` set `name` = name, `status` = active_status,`description` = description,`grade_id` = grade_id WHERE `id` = get_id;
        
        SET rowcount = (SELECT ROW_COUNT());
        IF(rowcount > 0)
          THEN 
          SET result = JSON_OBJECT('status','Update successfully');
        ELSE
            
          SET result = JSON_OBJECT('status','error');
             END IF; 
      END IF;       
 END IF;
 
  IF(key_cond = 'delete')
        THEN 
    
        SET rowcount = (SELECT COUNT(id) FROM subject_master_ut WHERE id=get_id);
        IF(rowcount = 0)
            THEN
            SET result = JSON_OBJECT('status','does not exist');
        ELSE
            UPDATE subject_master_ut SET `is_deleted`=1 WHERE id = get_id;  
            SET rowcount = (SELECT ROW_COUNT());
            IF(rowcount > 0)
                THEN
                SET result = JSON_OBJECT('status','success');
            ELSE
                SET result = JSON_OBJECT('status','error');
            END IF;
        END IF;

    END IF;
END$$
DELIMITER ;








------------------------------------------------------------------------------------------------------------------------------------------------------