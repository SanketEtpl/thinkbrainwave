'use strict';
//  Author: AdminDesigns.com
// 
//  This file is reserved for changes made by the use. 
//  Always seperate your work from the theme. It makes
//  modifications, and future theme updates much easier 
// 

(function($) {

    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    $("#country_id").click(function(e){
            var country_id = $("select[name=country_id]").val();
            $.ajax({
                    type:'POST',
                    url:'myprofile/FetchState',
                    data:{country_id:country_id},
                    success:function(data){
                        
                        $('select[name="state_id"]').empty();
                        $('select[name="state_id"]').append('<option value="">Select State</option>');
                        $('select[name="city_id"]').empty();
                        $('select[name="city_id"]').append('<option value="">Select City</option>');
				        $.each(data, function(key, value){
				             $('select[name="state_id"]').append('<option value="'+ key +'">' + value + '</option>');
				           });
                        
                    }
                });
        });
        $("#state_id").click(function(e){
            var state_id = $("select[name=state_id]").val();
            
            $.ajax({
                    type:'POST',
                    url:'myprofile/FetchCity',
                    data:{state_id:state_id},
                    success:function(data){
                        
                        $('select[name="city_id"]').empty();
                        $('select[name="city_id"]').append('<option value="">Select City</option>');
        $.each(data, function(key, value){

             $('select[name="city_id"]').append('<option value="'+ key +'">' + value + '</option>');
        });
                        
                    }
                });
        });


        $("#grade_id").click(function(e){
            var grade = $("select[name=grade]").val();
            var url = $("#baseurl").val();
            $.ajax({
                    type:'POST',
                    url:url,
                    data:{grade:grade},
                    success:function(data){
                        
                        $('select[name="course"]').empty();
                        $('select[name="course"]').append('<option value="">Select Course</option>');
        $.each(data, function(key, value){

             $('select[name="course"]').append('<option value="'+ key +'">' + value + '</option>');
        });
                        
                    }
                });
        });


        $(".showpopup").click(function(e){           
         var id = $(this).data('id');            
         $('#showcontent').html(id);             
         $('#myModal').modal('show');
     });    

})(jQuery);

