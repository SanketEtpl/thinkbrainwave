// navbar scroll remove class js 

	$(window).scroll(function() {
	  if ($(document).scrollTop() > 100) {
		$('.header_section_new').addClass('navbar-fixed-top');
		$('.header_section').addClass('navbar-fixed-top');
		  } else {
		$('.header_section_new').removeClass('navbar-fixed-top');
		$('.header_section').removeClass('navbar-fixed-top');
		  }
	});
	
// bottom to top button js 
	
 $(document).ready(function() {
  /******************************
      BOTTOM SCROLL TOP BUTTON
   ******************************/

  // declare variable
  var scrollTop = $(".scrollTop");

  $(window).scroll(function() {
    // declare variable
    var topPos = $(this).scrollTop();

    // if user scrolls down - show scroll to top button
    if (topPos > 100) {
      $(scrollTop).css("opacity", "1");

    } else {
      $(scrollTop).css("opacity", "0");
    }

  }); // scroll END

  //Click event to scroll to top
  $(scrollTop).click(function() {
    $('html, body').animate({
      scrollTop: 0
    }, 800);
    return false;

  }); // click() scroll top EMD

  /*************************************
    LEFT MENU SMOOTH SCROLL ANIMATION
   *************************************/
  // declare variable
  var h1 = $("#h1").position();
  var h2 = $("#h2").position();
  var h3 = $("#h3").position();

  $('.link1').click(function() {
    $('html, body').animate({
      scrollTop: h1.top
    }, 500);
    return false;

  }); // left menu link2 click() scroll END

  $('.link2').click(function() {
    $('html, body').animate({
      scrollTop: h2.top
    }, 500);
    return false;

  }); // left menu link2 click() scroll END

  $('.link3').click(function() {
    $('html, body').animate({
      scrollTop: h3.top
    }, 500);
    return false;

  }); // left menu link3 click() scroll END

}); // ready() END
	
//faq collapse icon js
	
  function toggleIcon(e) {
	$(e.target)
		.prev('.panel-heading')
		.find(".more-less")
		.toggleClass('glyphicon-plus glyphicon-minus');
	}
	$('.panel-group').on('hidden.bs.collapse', toggleIcon);
	$('.panel-group').on('shown.bs.collapse', toggleIcon);
	

 // multiple img slider js start here

	var base_carousel = $('.features_owl_multiple_img_slider');
		if (base_carousel.length) {
			base_carousel.owlCarousel({
				loop:true,
				margin:50,
				autoplay:true,
				autoplayTimeout:2000,
				autoplayHoverPause:true,
				nav:false,
				dots: true,
				responsive:{
					0:{
						items:1
					},
					640:{
						items:2
					},
					767:{
						items:2
					},
					1000:{
						items:3
					}
				}
			});
		} 
		
	
// multiple img slider js start here

	var base_carousel = $('.our_teachers_owl_multiple_img_slider');
		if (base_carousel.length) {
			base_carousel.owlCarousel({
				loop:true,
				margin:50,
				autoplay:true,
				autoplayTimeout:2000,
				autoplayHoverPause:true,
				nav:false,
				dots: true,
				responsive:{
					0:{
						items:1
					},
					600:{
						items:2
					},
					767:{
						items:3
					},
					1000:{
						items:3
					}
				}
			});
		}
	
// datepicker js here

	$('.form_datetime').datetimepicker({
		//language:  'fr',
		weekStart: 1,
		todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
		showMeridian: 1
	});
	$('.form_date').datetimepicker({
		language:  'fr',
		weekStart: 1,
		todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
	});
	$('.form_time').datetimepicker({
		language:  'fr',
		weekStart: 1,
		todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 1,
		minView: 0,
		maxView: 1,
		forceParse: 0
	});
	
//scrollbar js here//

	jQuery(document).ready(function(){
		jQuery('.scrollbar-inner').scrollbar();
	});

//upload input js here//

	$(document).ready(function(){
		$('.custom-file-input input[type="file"]').change(function(e){
			$(this).siblings('input[type="text"]').val(e.target.files[0].name);
		});
	});


