jQuery(document).ready(function(){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});


		jQuery('.scrollbar-inner').scrollbar();

		$("#div_from_date").hide();
		$("#div_to_date").hide();
		$("#div_availability_week").hide();
		
		$("#div_from_month").hide();
		$("#div_to_month").hide();
		$("#div_availability_month").hide();

		$("#repeat").click(function(){

				var repeat = $("select[name=repeat]").val();

				if(repeat=='Never')
				{
					$("#div_from_date").hide();
					$("#div_to_date").hide();
					$("#div_availability_date").show();
					$("#div_availability_week").hide();
					$("#div_from_month").hide();
					$("#div_to_month").hide();
					$("#div_availability_month").hide();

				}
				if(repeat=='EveryDay')
				{
					$("#div_from_date").show();
					$("#div_to_date").show();
					$("#div_availability_date").hide();
					$("#div_availability_week").hide();

					$("#div_from_month").hide();
					$("#div_to_month").hide();
					$("#div_availability_month").hide();

				}
				if(repeat=='EveryWeek')
				{
					$("#div_from_date").show();
					$("#div_to_date").show();
					$("#div_availability_date").hide();
					$("#div_availability_week").show();

					$("#div_from_month").hide();
					$("#div_to_month").hide();
					$("#div_availability_month").hide();

				}
				if(repeat=='EveryMonth')
				{
					$("#div_from_date").hide();
					$("#div_to_date").hide();
					$("#div_availability_date").hide();
					$("#div_availability_week").hide();

					$("#div_from_month").show();
					$("#div_to_month").show();
					$("#div_availability_month").show();

				}
		});
		$("#saveAvailability").click(function(){
			var add_title = $("input[name=add_title]").val();
			var description = $("input[name=description]").val();
			var repeat = $("select[name=repeat]").val();
			var from_date = $("input[name=from_date]").val();
			var to_date = $("input[name=to_date]").val();
			var availability_date = $("input[name=availability_date]").val();
			var availability_from_time = $("input[name=availability_from_time]").val();
			var availability_to_time = $("input[name=availability_to_time]").val();

			var from_month = $("input[name=from_month]").val();
			var to_month = $("input[name=to_month]").val();
		
			var availability_days= [];

			$("input:checkbox[name=availability_days]:checked").each(function(){
			availability_days.push($(this).val());
			});

			availability_days_json= JSON.stringify(availability_days);


			var availability_month= [];

			$("input:checkbox[name=availability_month]:checked").each(function(){
			availability_month.push($(this).val());
			});

			availability_month_json= JSON.stringify(availability_month);

			var URL = "TutorAvailabilitySave";	
			$.ajax({
				type:'POST',
				url:URL,
				data:{add_title:add_title, description:description, repeat:repeat, from_date:from_date, to_date:to_date, availability_date:availability_date, availability_from_time:availability_from_time, availability_to_time:availability_to_time,availability_days:availability_days_json,  from_month:from_month,to_month:to_month, availability_month:availability_month_json},
				success:function(data){
					//alert(data.success);
					console.log(data);
					if(data.success=='success')
					{
						$('#successfilly_session_popup').modal('show');
					}
					else
					{
					}
				}
			});

			//$('#successfilly_session_popup').modal('show');
		});
});