
$(document).ready(function(){
    
   
    $.validator.setDefaults({
       
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
            //$(element).closest('.form-group').addClass('help-block');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
            //$(element).closest('.form-group').removeClass('help-block');
            
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
                
            } else {
                error.insertAfter(element);
            }
        }
    });

    jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z\s&]+$/i.test(value);
}, "Please enter only letters."); 


    $('.form1').validate({
        rules: {
        }
    });
    $('.form2').validate({
        rules: {
        }
    });
    $('.form3').validate({
        rules: {
        }
    });
    
   
    $.validator.addClassRules("required_check", {
        required: true
    });

    $.validator.addClassRules("required_date_check",{
        required: true,
        date: true
    });
    $.validator.addClassRules("required_date_check2",{
        required: true,
        date: true
    });

    $.validator.addClassRules("required_time_check",{
        required: true
    });
    $.validator.addClassRules("required_time_check2",{
        required: true
    });

    // $(".required_time_check").each(function() {
    //     $(this).rules( "add",{ required: true,
    //         messages : { required : '' } } ); 
    // });

    $.validator.addClassRules("required_letter",{
        required: true,
        lettersonly : true
    });

      $.validator.addClassRules("required_number",{
        required: true,
        digits: true,
        minlength :5,
        maxlength: 20
    });


    $.validator.addClassRules("required_email", {
        required: true,
        email: true
    });
    $.validator.addClassRules("required_mobile", {
        required: true,
        number: true,
        minlength :10,
        maxlength: 12
    });
    $.validator.addClassRules("required_confirm_password", {
        required: true,
        equalTo: "#password"
    });

   
   
    
    // $(".requred_check").rules("add", { 
    //     required:true,  
    //     minlength:3
    //   });
});
function resetform()
{
   
   document.getElementById("form1").reset();
   document.getElementById("form2").reset();
}
function resetformnew()
{
   
   document.getElementById("form1").reset();
}


