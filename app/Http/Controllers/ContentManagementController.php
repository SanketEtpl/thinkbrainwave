<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route ;



use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Cookie; 

use Socialite;

class ContentManagementController extends BaseController
{
    
    public function index(Request $request, $id= NULL)
    {

        $content = DB::select('select * from admin_manage_content_ut where is_deleted = 0 order by id DESC ');
        

       
        $ContentEditData = NULL;
            if($id!=NULL)
            {
                $ContentEditData = DB::table('admin_manage_content_ut')->where('id', $id)->first();
            }

        return view('Backend.contentmanage',['ContentEditData' => $ContentEditData,'content'=>$content]);

    }

    public function addContent(Request $request)
    {
            $input = request()->all();

            $name = $request->input('title');
            $status = $request->input('status');
            $description = $request->input('description');
            
            $id = $request->input('contentID');

            if(isset($id))
            {
            
                $data = array('title' =>$name,'description' => $description,  'status' => $status);
                DB::table('admin_manage_content_ut')->where('id', $id)->update($data);  
            }
            else
            {
                
                $data = array('title' =>$name,'description' => $description,  'status' => $status);
                DB::table('admin_manage_content_ut')->insert($data);
 
            }

            
       
            return redirect()->to('admin/contentmanagement');
            
    }
    public function deleteContent($id)
    {
        $data = array('is_deleted'=>'1');                    
        DB::table('admin_manage_content_ut')->where('id', $id)->update($data);        
        return redirect('admin/contentmanagement');
    }

    public function changeStatus($id, $status) {

        if($status == 0) {
            $data =array('status' =>1);
        } else {
            $data =array('status' =>0);
        }

        DB::table('admin_manage_content_ut')->where('id', $id)->update($data);        
        return redirect('admin/contentmanagement');
        
    }


}

?>