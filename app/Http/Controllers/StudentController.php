<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route ;


use Illuminate\Support\Facades\Input;
use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;


class StudentController extends BaseController
{
    
        
        public function __construct()
        {
            $this->middleware(function ($request, $next) {
                if($request->session()->has('loginSession'))
                {
                     return $next($request);
                }
                else
                {
                    return redirect()->to('Signin');
                }
            });
        }
    
    public function index()
    {
        
    }
    
    public function Dashboard(Request $request)
    {
        $loginSession = $request->session()->get('loginSession');
        
        $id = $loginSession['session_user_id'];
        $role = $loginSession['session_user_role_id'];

        $ShowProfileData = \App\Http\Controllers\AllProfileDataController::ShowProfileData($id, $role);

        $tasks = $this->StudentPlanTask($id);

        $StudentCancletasks = $this->StudentCancletasks($id);
        
        $TutorCancletasks = $this->TutorCancletasks($id);
        
        $Completedtasks = $this->Completedtasks($id);

        $Booktasks = $this->Booktasks($id);

        $StudentRatingCount = \App\Http\Controllers\AllCommonDataController::StudentRatingCount($id);


		return view('Student.dashboard',['users' => $ShowProfileData,'tasks' => $tasks,'StudentCancletasks' => $StudentCancletasks, 'TutorCancletasks' => $TutorCancletasks, 'Completedtasks' => $Completedtasks, 'Booktasks' => $Booktasks, 'StudentRatingCount' => $StudentRatingCount]);
    }

    
    //fetch calender data
    public static function StudentPlanTask($session_user_id)
    {       
        $tasks = DB::table('sessions_master_ut as sesM')
                    ->leftJoin('grade_master_ut as gm', 'sesM.grade_id', '=', 'gm.id')
                    ->leftJoin('subject_master_ut as subm', 'sesM.subject_id', '=', 'subm.id')
                    ->leftJoin('topic_master_ut as tm', 'sesM.topic_id', '=', 'tm.id')
                    ->leftJoin('student_master_ut as sm', 'sesM.student_id', '=', 'sm.user_id')

                    ->select('sesM.*', 'gm.name as grade_name', 'subm.name as subject_name', 'tm.name as topic_name', 'sm.first_name as student_name', 'sm.gender as student_gender')

                    ->where('sesM.student_id', '=', $session_user_id)
                    ->where('sesM.status', '!=', 3)
                    ->Where('sesM.status', '!=', 4)
                    ->Where('sesM.status', '!=', 6)
                    ->Where('sesM.status', '!=', 2)
                ->get();

        return $tasks;
    }
    public static function StudentCancletasks($session_user_id)
    {       
        $StudentCancletasks = DB::table('sessions_master_ut as sesM')
                    ->leftJoin('grade_master_ut as gm', 'sesM.grade_id', '=', 'gm.id')
                    ->leftJoin('subject_master_ut as subm', 'sesM.subject_id', '=', 'subm.id')
                    ->leftJoin('topic_master_ut as tm', 'sesM.topic_id', '=', 'tm.id')
                    ->leftJoin('student_master_ut as sm', 'sesM.student_id', '=', 'sm.user_id')

                    ->select('sesM.*', 'gm.name as grade_name', 'subm.name as subject_name', 'tm.name as topic_name', 'sm.first_name as student_name', 'sm.gender as student_gender')
                ->where('sesM.student_id', '=', $session_user_id)
                ->where('sesM.status', '=', 3)
                ->get();

        return $StudentCancletasks;
    }

    public static function TutorCancletasks($session_user_id)
    {       
        $TutorCancletasks = DB::table('sessions_master_ut as sesM')
                    ->leftJoin('grade_master_ut as gm', 'sesM.grade_id', '=', 'gm.id')
                    ->leftJoin('subject_master_ut as subm', 'sesM.subject_id', '=', 'subm.id')
                    ->leftJoin('topic_master_ut as tm', 'sesM.topic_id', '=', 'tm.id')
                    ->leftJoin('student_master_ut as sm', 'sesM.student_id', '=', 'sm.user_id')

                    ->select('sesM.*', 'gm.name as grade_name', 'subm.name as subject_name', 'tm.name as topic_name', 'sm.first_name as student_name', 'sm.gender as student_gender')
                    ->where('sesM.student_id', '=', $session_user_id)
                    ->Where('sesM.status', '=', 4)
                    ->get();

        return $TutorCancletasks;
    }

    public static function Completedtasks($session_user_id)
    {       
        $Completedtasks = DB::table('sessions_master_ut as sesM')
                    ->leftJoin('grade_master_ut as gm', 'sesM.grade_id', '=', 'gm.id')
                    ->leftJoin('subject_master_ut as subm', 'sesM.subject_id', '=', 'subm.id')
                    ->leftJoin('topic_master_ut as tm', 'sesM.topic_id', '=', 'tm.id')
                    ->leftJoin('student_master_ut as sm', 'sesM.student_id', '=', 'sm.user_id')

                    ->select('sesM.*', 'gm.name as grade_name', 'subm.name as subject_name', 'tm.name as topic_name', 'sm.first_name as student_name', 'sm.gender as student_gender')
                    ->where('sesM.student_id', '=', $session_user_id)
                    ->Where('sesM.status', '=', 6)
                    ->get();

        return $Completedtasks;
    }
    public static function Booktasks($session_user_id)
    {       
        $Booktasks = DB::table('sessions_master_ut as sesM')
                ->leftJoin('grade_master_ut as gm', 'sesM.grade_id', '=', 'gm.id')
                    ->leftJoin('subject_master_ut as subm', 'sesM.subject_id', '=', 'subm.id')
                    ->leftJoin('topic_master_ut as tm', 'sesM.topic_id', '=', 'tm.id')
                    ->leftJoin('student_master_ut as sm', 'sesM.student_id', '=', 'sm.user_id')

                    ->select('sesM.*', 'gm.name as grade_name', 'subm.name as subject_name', 'tm.name as topic_name', 'sm.first_name as student_name', 'sm.gender as student_gender')
                    ->where('sesM.student_id', '=', $session_user_id)
                    ->Where('sesM.status', '=', 2)
                    ->get();

        return $Booktasks;
    }

    // end fetch calender data

    //get all grade
    public static function GradeAllData()
    {
        $grades = DB::table('grade_master_ut')->get();

        return $grades;
    }

    //end get all grade

    //get all country
    public function CountriesAllData()
    {
        $countries = DB::table('countries_master_ut')->get();

        return $countries;
    }

    //end get all country


    public function MyProfile(Request $request)
    {

        $loginSession = $request->session()->get('loginSession');
        
        $id = $loginSession['session_user_id'];
        $role = $loginSession['session_user_role_id'];

        $ShowProfileData = \App\Http\Controllers\AllProfileDataController::ShowProfileData($id, $role);

        //print_r($result1);
      

        $tasks = $this->StudentPlanTask($id);

        $StudentCancletasks = $this->StudentCancletasks($id);
        
        $TutorCancletasks = $this->TutorCancletasks($id);
        
        $Completedtasks = $this->Completedtasks($id);

        $Booktasks = $this->Booktasks($id);

        $StudentRatingCount = \App\Http\Controllers\AllCommonDataController::StudentRatingCount($id);
        
        return view('Student.my_profile',['users' => $ShowProfileData, 'tasks' => $tasks,'StudentCancletasks' => $StudentCancletasks, 'TutorCancletasks' => $TutorCancletasks, 'Completedtasks' => $Completedtasks, 'Booktasks' => $Booktasks, 'StudentRatingCount' => $StudentRatingCount]);
        
    }
    public function EditProfile(Request $request)
    {

        $loginSession = $request->session()->get('loginSession');
        
        $id = $loginSession['session_user_id'];
        $role = $loginSession['session_user_role_id'];

        $ShowProfileData = \App\Http\Controllers\AllProfileDataController::ShowProfileData($id, $role);

        //print_r($result1);
        $countries = $this->CountriesAllData();
        
        $grades = $this->GradeAllData();

        $tasks = $this->StudentPlanTask($id);

        $StudentCancletasks = $this->StudentCancletasks($id);
        
        $TutorCancletasks = $this->TutorCancletasks($id);
        
        $Completedtasks = $this->Completedtasks($id);

        $Booktasks = $this->Booktasks($id);

        $StudentRatingCount = \App\Http\Controllers\AllCommonDataController::StudentRatingCount($id);

        return view('Student.edit_profile',['users' => $ShowProfileData,'countries' => $countries,'grades' => $grades, 'tasks' => $tasks,'StudentCancletasks' => $StudentCancletasks, 'TutorCancletasks' => $TutorCancletasks, 'Completedtasks' => $Completedtasks, 'Booktasks' => $Booktasks, 'StudentRatingCount' => $StudentRatingCount]);
        
    }
    public function GradeFetch(Request $request)
    {
        $input = $request->all();
        $grade = $request->grade;
        // $gradeData = DB::table('subject_master_ut')
        //              ->select('name')
        //              ->where('grade_id', '=', $grade)
        //              ->groupBy('status')
        //              ->get();

        $gradeData = DB::table('subject_master_ut')
                     ->select(DB::raw('group_concat(name) as subject_name'))
                     ->where('grade_id', '=', $grade )
                     ->first();
        $subject_name = $gradeData->subject_name;
        return response()->json(['success'=>$subject_name]);
    }

    public function FetchSubject(Request $request)
    {
        $input = $request->all();
        $grade_id = $request->grade_id;


        $gradeData = DB::table("subject_master_ut")->where("grade_id",$grade_id)->pluck("name","id");

        return response()->json($gradeData);
    }

    public function FetchTopic(Request $request)
    {
        $input = $request->all();
        $grade_id = $request->grade_id;
        $subject_id = $request->subject_id;


        $gradeData = DB::table("topic_master_ut")
                            ->where("grade_id",$grade_id)
                            ->where("subject_id",$subject_id)
                            ->pluck("name","id");

        return response()->json($gradeData);
    }

    public function ProfileSave(Request $request)
    {
        $user_id = $request->input('user_id');
        $name = $request->input('name');
        $grade = $request->input('grade');
        $dob = $request->input('dob');
        $gender = $request->input('gender');
        $tutor_type = $request->input('tutor_type');
        $address_line1 = $request->input('address_line1');
        $country_id = $request->input('country_id');
        $max_distance_travel_kms = $request->input('max_distance_travel_kms');
        $primary_phone = $request->input('primary_phone');
        $email = $request->input('email');

        $user_profile = $request->file('profile_file_path');

        //get old session value
            $temp_session= $request->session()->get('loginSession');
            $temp_session_user_role_id = $temp_session['session_user_role_id'];
            $temp_session_user_id = $temp_session['session_user_id'];
            $temp_session_user_username = $temp_session['session_user_username'];
            $temp_session_user_password = $temp_session['session_user_password'];
            $temp_session_user_name = $temp_session['session_user_name'];
            $temp_session_user_type = $temp_session['session_user_type'];
        
            $temp_session_user_role = $temp_session['session_user_role'];
            if ($request->hasFile('profile_file_path')){
                $image = $request->file('profile_file_path');
                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                
            
                $temp_session_user_profile_file_path = 'student/'.$input['imagename'];
                echo "insert";
            }
            else{
                $temp_session_user_profile_file_path = $temp_session['session_user_profile_file_path'];
                echo "not insert";
            }
        //end get old session value
        
        
        $args = array(
            'first_name' => $name,
            'dob' => $dob,
            'gender' => $gender,
            'tutor_type' => $tutor_type,
            'address_line1' => $address_line1,
            'country_id' => $country_id,
            'max_distance_travel_kms' => $max_distance_travel_kms,
            'email' => $email,
            'primary_phone' => $primary_phone
        );

        
        $request = Request::create('/api/StudentProfileSave', 'POST', $args);
        $instance = json_decode(Route::dispatch($request)->getContent());
        //print_r($instance[0]->result);

        if($grade!='')
        {
            $today = date('Y-m-d');
            $data = array('student_id'=> $user_id, 'grade_id' => $grade, 'form_date' => $today, 'to_date' => $today);

            $update_data = array('status'=> '0');
        
            DB::table('student_grade_ut')
            ->where('student_id', $user_id)
            ->update($update_data);

            DB::table('student_grade_ut')->insert(array($data));
        }

        //print_r($instance);
        

       
        //set new session value
            $sessionArray = array(
                'session_user_role_id' => $temp_session_user_role_id,
                'session_user_id' => $temp_session_user_id,
                'session_user_type' => $temp_session_user_type,
                'session_user_username' => $temp_session_user_username,
                'session_user_password' => $temp_session_user_password,
                'session_user_name' => $name,
                'session_user_profile_file_path' => $temp_session_user_profile_file_path,
                'session_user_role' => $temp_session_user_role);
            
            $request->session()->put('loginSession', $sessionArray);
        //end set new session value

        //update lat long
            list($latitude, $longitude)  = \App\Http\Controllers\AllCommonDataController::GetLatLong($address_line1);
            $update_data = array('latitude'=> $latitude, 'longitude'=> $longitude);
                
            $update = DB::table('student_master_ut')
                ->where('user_id', $user_id)
                ->update($update_data);
        //end update lat long

        return redirect()->to('MyProfile');
        
        
    }

    public function BookSession(Request $request)
    {
        $loginSession = $request->session()->get('loginSession');
        
        $id = $loginSession['session_user_id'];
        $role = $loginSession['session_user_role_id'];

        $ShowProfileData = \App\Http\Controllers\AllProfileDataController::ShowProfileData($id, $role);

        //print_r($result1);
        $grades = $this->GradeAllData();

        return view('Student.book_session',['users' => $ShowProfileData,'grades' => $grades]);
        
        
    }

    public function BookSessionSave(Request $request)
    {
        $loginSession = $request->session()->get('loginSession');
        
        $session_user_id = $loginSession['session_user_id'];
        $session_user_name = $loginSession['session_user_name'];

        $input = $request->all();

        $grade_id = $request->grade_id;
        $subject_id = $request->subject_id;
        $topic_id = $request->topic_id;

        $start_date = date('Y-m-d', strtotime($request->start_date));
        $start_time = $request->start_time;
        
        $timestamp = strtotime($start_time) + 60*60;

        $end_time = date('H:i', $timestamp);


        $combinedDT = date('Y-m-d H:i', strtotime("$start_date $start_time"));

        $date = new \DateTime($combinedDT);
        $date->add(new \DateInterval('PT1H'));
        $end_date = $date->format('Y-m-d');

        $tutor_type = $request->tutor_type;
        $visit_type = $request->visit_type;
        $session_type = $request->session_type;
        $venue = $request->venue;
        // $rand = Str::random(60);
        
        $otp = mt_rand(100000, 999999);

        $tutor_id = $request->tutor_id;

        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $distance = $request->max_distance_travel_kms;

            if($session_type=="1")
            {
                if($latitude!="")
                {
                    $lat = $latitude;
                    $long = $longitude;
                }
                elseif($tutor_id!='')
                {
                    $lat = $latitude;
                    $long = $longitude;
                }
                elseif($tutor_id=='')
                {
                    list($lat, $long)  = \App\Http\Controllers\AllCommonDataController::GetLatLong($venue);

                    if($lat=="REQUEST_DENIED")
                    {
                        $error = "REQUEST_DENIED";
                        return response()->json(['error'=>$error]);
                    }
                }
            }
            else
            {
                $lat = $latitude;
                    $long = $longitude;
            }

        $SessionAssignToTutorAlgo = \App\Http\Controllers\AllCommonDataController::SessionAssignToTutorAlgoFinal($session_user_id, $grade_id, $subject_id, $topic_id, $start_date, $start_time, $end_time, $tutor_type, $visit_type, $session_type, $lat, $long, $distance);
        if($SessionAssignToTutorAlgo!="TUTOR_NOT_FOUND")
        {
            $tutor_id_add = $SessionAssignToTutorAlgo['tutor_id'];
        }
        elseif($tutor_id!='')
        {
            $tutor_id_add = $tutor_id;
        }
        else
        {
            $tutor_id_add = '';
        }

            $data = array('student_id'=> $session_user_id, 'grade_id'=> $grade_id, 'subject_id' => $subject_id, 'topic_id' => $topic_id, 'start_date' => $start_date, 'start_time' => $start_time, 'end_date' => $end_date, 'end_time' => $end_time, 'tutor_type' => $tutor_type, 'visit_type' => $visit_type, 'session_type' => $session_type, 'venue' => $venue, 'tutor_id' => $tutor_id_add, 'status' => '2', 'otp' => $otp, 'latitude' => $lat, 'longitude' => $long);

        DB::table('sessions_master_ut')->insert(array($data));

        $id = DB::getPdo()->lastInsertId();

        if($id)
        {
            $notification_type ="SessionBook";
            $session_type_val="";
            if($session_type==1)
            {
                $session_type_val="face to face";
            }
            if($session_type==2)
            {
                $session_type_val="video to video";
            }

            
            $message_student = $session_type_val." session book by ".$session_user_name." on ".$start_date." at ".$start_time." .OTP for your session id ".$id." is ".$otp;

            $message_tutor = $session_type_val." session assign to you, on ".$start_date." at ".$start_time.". Session Booked by ".$session_user_name;

            $message_admin = $session_type_val." session book by ".$session_user_name." on ".$start_date." at ".$start_time;

            $InsertNotificationData_student = \App\Http\Controllers\AllCommonDataController::InsertNotificationData($session_user_id, $notification_type, $message_student);

            
            $InsertNotificationData_tutor = \App\Http\Controllers\AllCommonDataController::InsertNotificationData($tutor_id_add, $notification_type, $message_tutor);

            $InsertNotificationData_admin = \App\Http\Controllers\AllCommonDataController::InsertNotificationData("1", $notification_type, $message_admin);

            $SessionData = DB::table('sessions_master_ut')
                        ->where('id', '=', $id)
                        ->first();
        
            return response()->json(['success'=>$SessionAssignToTutorAlgo]);
        }
        else
        {
            $error = "REQUEST_DENIED";
            return response()->json(['error'=>$error]);
        }
    }

    public function BookSessionConfirm(Request $request)
    {
        $error = "";

        $loginSession = $request->session()->get('loginSession');
        
        $session_user_id = $loginSession['session_user_id'];

        $input = $request->all();

        $grade_id = $request->grade_id;
        $subject_id = $request->subject_id;
        $topic_id = $request->topic_id;
        $start_date = date('Y-m-d', strtotime($request->start_date));
        $start_time = $request->start_time;
        
        $timestamp = strtotime($start_time) + 60*60;

        $end_time = date('H:i', $timestamp);


        $combinedDT = date('Y-m-d H:i', strtotime("$start_date $start_time"));

        $date = new \DateTime($combinedDT);
        $date->add(new \DateInterval('PT1H'));
        $end_date = $date->format('Y-m-d');

        $tutor_type = $request->tutor_type;
        $visit_type = $request->visit_type;
        $session_type = $request->session_type;
        $venue = $request->venue;
        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $distance = $request->max_distance_travel_kms;

        if($session_type=="1")
        {
            if($latitude!="")
            {
                $lat = $latitude;
                $long = $longitude;
            }
            {
                list($lat, $long)  = \App\Http\Controllers\AllCommonDataController::GetLatLong($venue);

               
                if($lat=="REQUEST_DENIED")
                {
                    $error = "REQUEST_DENIED";
                    return response()->json(['error'=>$error]);
                }
                
            }
        }
        else
        {
            $lat = $latitude;
            $long = $longitude;
        }
        //$SessionAssignToTutorAlgo='';
        $SessionAssignToTutorAlgo = \App\Http\Controllers\AllCommonDataController::SessionAssignToTutorAlgoFinal($session_user_id, $grade_id, $subject_id, $topic_id, $start_date, $start_time, $end_time, $tutor_type, $visit_type, $session_type, $lat, $long, $distance);
        
        
        $topic_name= DB::table('topic_master_ut')
                        ->where('id', '=', $topic_id)
                        ->select('topic_master_ut.name as topic_name')
                        ->first();
        $grade_name= DB::table('grade_master_ut')
                        ->where('id', '=', $grade_id)
                        ->select('grade_master_ut.name as grade_name')
                        ->first();
        $subject_name= DB::table('subject_master_ut')
                        ->where('id', '=', $subject_id)
                        ->select('subject_master_ut.name as subject_name')
                        ->first();
                        
        if($SessionAssignToTutorAlgo!="TUTOR_NOT_FOUND")
        {
            //$tutor_id = $SessionAssignToTutorAlgo->tutor_id;
            $successArray = array('topic_name'=>$topic_name->topic_name,
                                'start_date'=>$start_date,
                                'end_date'=>$end_date,
                                'start_time'=>$start_time,
                                'end_time'=>$end_time,
                                'grade_name'=>$grade_name->grade_name,
                                'subject_name'=>$subject_name->subject_name);

            return response()->json(['success'=>$SessionAssignToTutorAlgo, 'successArray'=>$successArray]);
        }
        else
        {
            //$error = "TUTOR_NOT_FOUND";
            return response()->json(['error'=>$SessionAssignToTutorAlgo]);
        }
    }

   

    public function PostDemo(Request $request)
    {
        echo "hi";
        $args = array(
            'name' => 'yogita',
        );

        
        $request = Request::create('/api/PostDemo', 'POST', $args);
        $instance = json_decode(Route::dispatch($request)->getContent());
        print_r($instance);
    }

    public function VideoStreaming(Request $request)
    {

        $loginSession = $request->session()->get('loginSession');
        
        $id = $loginSession['session_user_id'];
        $role = $loginSession['session_user_role_id'];

        $ShowProfileData = \App\Http\Controllers\AllProfileDataController::ShowProfileData($id, $role);

        $StudentRatingCount = \App\Http\Controllers\AllCommonDataController::StudentRatingCount($id);

        return view('Student.video_streaming',['users' => $ShowProfileData, 'StudentRatingCount' => $StudentRatingCount]);
        
    }

    public function MyAccount(Request $request)
    {
        $loginSession = $request->session()->get('loginSession');
        
        $session_user_id = $loginSession['session_user_id'];
        
        
        $PastSessions = $this->FetchAllSesionDataOrder($session_user_id,'=','desc');

        $FutureSessions = $this->FetchAllSesionDataOrder($session_user_id,'!=','ASC');

        $SessionsStartFirst = $this->FetchUpcomingSesionFirst($session_user_id);

        $grades = $this->GradeAllData(); 

        return view('Student.my_account',['PastSessions' => $PastSessions,'FutureSessions' => $FutureSessions,'grades' => $grades,'SessionsStartFirst' => $SessionsStartFirst]);
        

    }
     public static function FetchUpcomingSesionFirst($studentId)
    {
        $SessionsStartFirst = DB::table('sessions_master_ut as sesM')
                    ->select('sesM.id as session_start_first_id')
                    ->where('sesM.student_id', '=', $studentId)
                    ->where('sesM.status', '!=', 6)
                    ->orderBy('sesM.id', 'ASC')
                    ->first();
        return $SessionsStartFirst;
    }

    public static function FetchAllSesionDataOrder($studentId, $condition, $oderType)
    {
        $SessionsData = DB::table('sessions_master_ut as sesM')
                    ->leftJoin('tutor_details_ut as tDet', 'sesM.tutor_id', '=', 'tDet.user_id')
                    ->leftJoin('grade_master_ut as gradeM', 'sesM.grade_id', '=', 'gradeM.id')
                    ->leftJoin('subject_master_ut as subM', 'sesM.subject_id', '=', 'subM.id')
                    ->leftJoin('topic_master_ut  as topM', 'sesM.topic_id', '=', 'topM.id')

                    ->select('gradeM.name as grade_name', 'subM.name as subject_name', 'topM.name as topic_name', 'tDet.user_id as tutor_id',  'tDet.gender as tutor_gender', 'tDet.name as tutor_name', 'tDet.level as tutor_level', 'sesM.*')

                    ->where('sesM.student_id', '=', $studentId)
                    ->where('sesM.status', $condition, 6)
                    ->orderBy('sesM.id', $oderType)
                    ->get();
        return $SessionsData;

    }

    public function MyPlan(Request $request)
    {
        $loginSession = $request->session()->get('loginSession');
        
        $session_user_id = $loginSession['session_user_id'];
        $tasks = $this->StudentPlanTask($session_user_id);

        $StudentCancletasks = $this->StudentCancletasks($session_user_id);
        
        $TutorCancletasks = $this->TutorCancletasks($session_user_id);
        
        $Completedtasks = $this->Completedtasks($session_user_id);

        $Booktasks = $this->Booktasks($session_user_id);

		return view('Student.my_plan',['tasks' => $tasks,'StudentCancletasks' => $StudentCancletasks, 'TutorCancletasks' => $TutorCancletasks, 'Completedtasks' => $Completedtasks, 'Booktasks' => $Booktasks]);
    }
    public function StudentSessionCancleSave($id ,$reason ,Request $request)
    {
        $loginSession = $request->session()->get('loginSession');
        
        $session_user_id = $loginSession['session_user_id'];
        $session_user_name = $loginSession['session_user_name'];

        $update_data = array('student_cancel_reason'=> $reason,'status'=> '3');
        
            DB::table('sessions_master_ut')
            ->where('id', $id)
            ->update($update_data);

        $SingleSessionData = \App\Http\Controllers\AllCommonDataController::SingleSessionData($id);
        $tutor_id = $SingleSessionData->tutor_id;
        $student_id = $SingleSessionData->student_id;

        $notification_type ="SessionStudentCancel";

            $session_type_val="";
            if($SingleSessionData->session_type==1)
            {
                $session_type_val="face to face";
            }
            if($SingleSessionData->session_type==2)
            {
                $session_type_val="video to video";
            }
            
            $message = $session_type_val." session no ".$id." cancel by ".$session_user_name." . Session date ".$SingleSessionData->start_date.", session time ".$SingleSessionData->start_time;

            if($tutor_id!='')
            {
                $InsertNotificationData_tutor = \App\Http\Controllers\AllCommonDataController::InsertNotificationData($tutor_id, $notification_type, $message);
            }
            if($student_id!='')
            {
                $InsertNotificationData_student = \App\Http\Controllers\AllCommonDataController::InsertNotificationData($student_id, $notification_type, $message);
            }
            
            $InsertNotificationData_admin = \App\Http\Controllers\AllCommonDataController::InsertNotificationData("1", $notification_type, $message);


        return redirect()->to('MyPlan');
    }
    public function StartSessionSave($id)
    {
        
        $update_data = array('status'=> '5');
        
            DB::table('sessions_master_ut')
            ->where('id', $id)
            ->update($update_data);
        return redirect()->to('MyAccount');
    }
    public function StopSessionSave($id)
    {
        
        $update_data = array('status'=> '6');
        
            DB::table('sessions_master_ut')
            ->where('id', $id)
            ->update($update_data);
        return redirect()->to('MyAccount');
    }
    public function Notification(Request $request)
    {
        $loginSession = $request->session()->get('loginSession');
        
        $id = $loginSession['session_user_id'];
        $role = $loginSession['session_user_role_id'];

        $AllNotificationData = \App\Http\Controllers\AllCommonDataController::AllNotificationData($id);
        
        return view('Student.notification',['AllNotificationData' => $AllNotificationData]);
        

    }
    public function ViewNotificationDetails($NotificatinId,Request $request)
    {
        
        
        $UpdateNotificationData = \App\Http\Controllers\AllCommonDataController::UpdateNotificationData($NotificatinId);

        $SingleNotificationData = \App\Http\Controllers\AllCommonDataController::SingleNotificationData($NotificatinId);
        
        
        return view('Student.view_notification_details',['SingleNotificationData' => $SingleNotificationData]);
        

    }
    public static function StudentAddRating(Request $request)
    {
        
        $input = $request->all();
        $id = $request->id;
        $rating = $request->rating;
        $update_data = array('tutor_rating'=> $rating);
        
        $update = DB::table('sessions_master_ut')
                ->where('id', $id)
                ->update($update_data);

        return $update;

    }
    public static function CodeShareWithParentSms(Request $request)
    {
        $loginSession = $request->session()->get('loginSession');
        $session_user_name = $loginSession['session_user_name'];

        $input = $request->all();
        $user_id = $request->user_id;
        $child_code = $request->child_code;
        $phone_number = $request->phone_number;

        $message = "Your child ".$session_user_name." profile code : ".$child_code." .Please add this code in your profile.";

        //$sendSMS = \App\Http\Controllers\NotifyController::sendSMS($phone_number, $message);
        
        return response()->json(['success'=>'success']);

    }
    public static function CodeShareWithParentEmail(Request $request)
    {
        $loginSession = $request->session()->get('loginSession');
        $session_user_name = $loginSession['session_user_name'];
        $input = $request->all();
        $user_id = $request->user_id;
        $child_code = $request->child_code;
        $to_email = $request->email_id;

        $to_name = "";

        $subject = $session_user_name." profile code";

        $body = "Your child ".$session_user_name." profile code : ".$child_code." .Please add this code in your profile.";

        

        $sendEmail = \App\Http\Controllers\MailController::SendEmail($to_name, $to_email, $body, $subject);

        return response()->json(['success'=>'success']);
    }
    

}
