<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route ;



use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Cookie; 

use Socialite;

class SubscriptionController extends BaseController
{
    
    public function index(Request $request, $id= NULL)
    {
    	$planEditData = NULL;
        if($id != NULL)
        {
        	 $planEditData = $plandata = DB::select('SELECT *,plan_master_ut.id as Id FROM `plan_master_ut` left JOIN role_master_ut on plan_master_ut.user_type = role_master_ut.id left join subscription_validperiod_ut on subscription_validperiod_ut.id= plan_master_ut.valid_period WHERE is_deleted = 0 and plan_master_ut.id='.$id);
        	 // print_r($planEditData);
        	 // die;
        }
        

        $plandata = DB::select('SELECT *,plan_master_ut.id as Id FROM `plan_master_ut` left JOIN role_master_ut on plan_master_ut.user_type = role_master_ut.id left join subscription_validperiod_ut on subscription_validperiod_ut.id= plan_master_ut.valid_period WHERE is_deleted = 0');

        $validperiod = DB::table("subscription_validperiod_ut")->get();

        $userdata = DB::table("role_master_ut")->where("id","!=",1)->where("id","!=",4)->get();

        return view('Backend.Subscription',['planEditData' => $planEditData,'plandata'=>$plandata,'validperiod'=> $validperiod,'userdata'=>$userdata]);
        
    }

    function addsubscription(Request $request)
    {
    	 $input = request()->all();

         $plan_name = $request->input('plan');
         $price_per_year = $request->input('price');
         $price_per_month = $request->input('priceyear');
         $valid_period = $request->input('period');
         $valid_period_unit = $request->input('unit');
         $user_type = $request->input('user');
         $status = $request->input('status');
         $description = $request->input('description');
         $id = $request->input('planid');

         $data = array('plan_name' => $plan_name,'price_per_year' => $price_per_year, 'price_per_month' => $price_per_month, 'valid_period' => $valid_period,'user_type'=>$user_type,'status'=>$status,'description'=>$description,'valid_period_unit'=>$valid_period_unit);

             

            if(isset($id))
            {
                DB::table('plan_master_ut')->where('id', $id)->update($data);  
            }
            else
            {
                DB::table('plan_master_ut')->insert($data);
                
            }
            return redirect()->to('admin/subscription');
    }

    public function deletesubscription($id)
    {
        $data = array('is_deleted'=>'1');                    
            DB::table('plan_master_ut')            
            ->where('id', $id)            
            ->update($data);        
        return redirect('admin/subscription');
    }

    public function changeStatus($id, $status) {

        if($status == 0) {
            $data =array('status' =>1);
        } else {
            $data =array('status' =>0);
        }

            DB::table('plan_master_ut')            
            ->where('id', $id)            
            ->update($data);        
        return redirect('admin/subscription');
        
    }

    
   

}

?>