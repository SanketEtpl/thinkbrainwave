<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route ;


use Illuminate\Support\Facades\Input;
use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;


class AllCommonDataController extends BaseController
{
    
    

    // show notifaction unread count data 
    public static function NotificationUnreadCount($id, $role)
    {
        $CountData = DB::table('notification_ut')
                ->where('status', '=','0')
                ->where('receiver_id', '=', $id)
                ->count();
        return $CountData;

    }
    // end show notifaction count unread data 

    // show all notifaction data 
    public static function AllNotificationData($id)
    {
        $CountData = DB::table('notification_ut')
                ->where('receiver_id', '=', $id)
                ->orderBy('id', 'desc')
                ->get();
        return $CountData;

    }
    // end show all notifaction  data 

    // show Single notifaction data 
    public static function SingleNotificationData($NotificatinId)
    {
        $SingleNotificationData = DB::table('notification_ut')
                ->where('id', '=', $NotificatinId)
                ->first();
        return $SingleNotificationData;

    }
    // end show Single notifaction  data 
    // Update notifaction data 
    public static function UpdateNotificationData($NotificatinId)
    {
        

        $update_data = array('status'=> '1');
        
        $update = DB::table('notification_ut')
                ->where('id', $NotificatinId)
                ->update($update_data);

        return $update;

    }
    // end Update notifaction  data 

    // insert notifaction data 
    public static function InsertNotificationData($receiver_id, $notification_type, $message)
    {
        
        $nowDateTime = date('Y-m-d H:i:s');
        $insert_data = array('sender_id'=> '1','receiver_id'=> $receiver_id,'notification_type'=> $notification_type,'message'=> $message,'created_at'=> $nowDateTime);
        
        $insert = DB::table('notification_ut')->insert(array($insert_data));

        return $insert;

    }
    // end insert notifaction  data 
    
    // show Single session data 
    public static function SingleSessionData($id)
    {
        $SingleSessionData = DB::table('sessions_master_ut')
                ->where('id', '=', $id)
                ->first();
        return $SingleSessionData;

    }
    // end show session notifaction  data

    // get tutor rating count
    public static function TutorRatingCount($id)
    {
        $TutorRatingCount = DB::table('sessions_master_ut as sesM')
                    ->where('sesM.tutor_id', '=', $id)
                    ->where('tutor_rating', '!=', '0.0')
                    ->where('sesM.status', '=', '6')
                    ->avg('tutor_rating');
        return $TutorRatingCount;

    }
    // end get tutor rating count

    // get student rating count
    public static function StudentRatingCount($id)
    {
        $StudentRatingCount = DB::table('sessions_master_ut as sesM')
                    ->where('sesM.student_id', '=', $id)
                    ->where('student_rating', '!=', '0.0')
                    ->where('sesM.status', '=', '6')
                    ->avg('student_rating');
        return $StudentRatingCount;

    }
    // end get student rating count

    // show Single session data 
    public static function GetLatLong($address)
    {
        $REQUEST_DENIED = 'REQUEST_DENIED';
        if($address!='')
        {
            $geocodeFromAddr =  file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCY2buDtbYIot8Llm_FkQXHW36f0Cme6TI&address=".urlencode($address));

            $output = json_decode($geocodeFromAddr);
            
            if($output->status=='OK')
            {
                $latitude = $output->results[0]->geometry->location->lat;
                
                $longitude = $output->results[0]->geometry->location->lng;

                return [$latitude, $longitude]; 
            }
            if($output->status=='REQUEST_DENIED')
            { 
                return [$REQUEST_DENIED, $REQUEST_DENIED];
            }
        }
        else
        {
            return [$REQUEST_DENIED, $REQUEST_DENIED]; 
        }
    }

  
    // end show session notifaction  data
    // session auto assign algo
    public static function SessionAssignToTutorAlgoFinal($session_user_id, $grade_id, $subject_id, $topic_id, $start_date, $start_time, $end_time, $tutor_type, $visit_type, $session_type, $latitude, $longitude, $distance)
    {    

        $session_start_time = date('H:i', strtotime($start_time) - 59*60);

        //$session_end_time = date('H:i', strtotime($end_time) + 60*60);
        $session_end_time = $end_time;
        
        $query = DB::table('tutor_subject_grade_ut as tsub')
                        ->join('tutor_details_ut as tDet', 'tsub.user_id', '=', 'tDet.user_id')
                        ->leftJoin('grade_master_ut as gm', 'tsub.grade_id', '=', 'gm.id')
                        ->leftJoin('subject_master_ut as subm', 'tsub.subject_id', '=', 'subm.id')
                        ->leftJoin('tutor_availablity_ut as tavai', 'tDet.user_id', '=', 'tavai.user_id')

                        ->leftJoin('sessions_master_ut as smaster', 'tsub.user_id', '=', 'smaster.tutor_id');                       
                        if($session_type=="1")
                        {
                            $query =  $query->select(DB::raw("(COUNT(smaster.id)) as total_sesion,GROUP_CONCAT( DISTINCT tsub.id) as id, GROUP_CONCAT( DISTINCT gm.name) as grade_name,  GROUP_CONCAT( DISTINCT subm.name) as subject_name,GROUP_CONCAT( DISTINCT tDet.user_id )as tutor_id, GROUP_CONCAT( DISTINCT tDet.name )as tutor_name, GROUP_CONCAT( DISTINCT IFNULL(tDet.latitude,'') )as user_lat, GROUP_CONCAT( DISTINCT IFNULL(tDet.longitude,'') )as user_long, GROUP_CONCAT( DISTINCT IFNULL(tDet.max_distance_travel,'') )as tutor_max_distance, ROUND(GROUP_CONCAT( DISTINCT 111.111 *  DEGREES(ACOS(LEAST(COS(RADIANS( '$latitude')) * COS(RADIANS(tDet.latitude)) * COS(RADIANS('$longitude' - tDet.longitude)) + SIN(RADIANS( '$latitude')) * SIN(RADIANS(tDet.latitude)), 1.0))) ),3) as distance_in_km"));

                        }
                        elseif($session_type=="2")
                        {
                            $query =  $query->select(DB::raw("(COUNT(smaster.id)) as total_sesion,GROUP_CONCAT( DISTINCT tsub.id) as id, GROUP_CONCAT( DISTINCT gm.name) as grade_name,  GROUP_CONCAT( DISTINCT subm.name) as subject_name,GROUP_CONCAT( DISTINCT tDet.user_id )as tutor_id, GROUP_CONCAT( DISTINCT tDet.name )as tutor_name, GROUP_CONCAT( DISTINCT IFNULL(tDet.latitude,'') )as user_lat, GROUP_CONCAT( DISTINCT IFNULL(tDet.longitude,'') )as user_long, GROUP_CONCAT( DISTINCT IFNULL(tDet.max_distance_travel,'') )as tutor_max_distance"));

                        }
                        $query =  $query->where('tsub.grade_id', '=', $grade_id)
                        ->where('tsub.subject_id', '=', $subject_id);

                        
                            
                        if($tutor_type!="Any")
                        {   
                            $query =  $query->where('tDet.gender', '=', $tutor_type);
                        }
                    
                        $query =  $query->where('tavai.date', '=', $start_date);
                        
                        if($start_time!="")
                        {
                            $query =  $query->where('tavai.from_time', '<=', $start_time)
                                            ->where('tavai.to_time', '>=', $end_time);
                        }

                        
                                      
                        $query =  $query->groupBy(DB::raw("tDet.id"));                        
                        
                        
                        if($session_type=="1")
                        {
                            
                            if($visit_type=="2")
                            {
                                $query =  $query->having('distance_in_km', '<=', $distance);
                            }
                            elseif($visit_type=="1")
                            {
                                
                              
                                $query =  $query->havingRaw('tutor_max_distance >= distance_in_km');
                            }
                        }
                        $query =  $query->orderBy('total_sesion', 'ASC')
                        ->orderBy('tDet.name', 'ASC')
                        ->get();
            //print_r($query);
            if(isset($query[0])) 
            {               
                 
                foreach($query as $key)
                {
                            $queryTutor = DB::table('sessions_master_ut as sm')
                            ->select(DB::raw("tutor_id"))
                            ->whereBetween('sm.start_date', array($session_start_time, $end_time))
                            ->orWhereRaw("sm.end_time BETWEEN '$session_start_time' AND '$end_time'")

                            ->where('sm.tutor_id', '=', $key->tutor_id)
                            ->where('sm.start_date', '=', $start_date)

                            ->get();
                            
                            if(isset($queryTutor[0]))
                            {
                                //print_r($queryTutor);
                                foreach($queryTutor as $key2)
                                {
                                    if($key2->tutor_id!=$key->tutor_id)
                                    {
                                        $tutor_avai = array('tutor_id'=>$key->tutor_id);
                                        return $tutor_avai;
                                    }
                                }
                            }
                            else
                            {
                                $tutor_avai = array('tutor_id'=>$key->tutor_id);
                                return $tutor_avai;
                            }
                    
                } 
                
            }
            else
            {
                return "TUTOR_NOT_FOUND";
            } 
    }
    // end session auto assign algo

    // demo :  session auto assign algo
    public static function SessionAssignToTutorAlgo(Request $request)
    {
        
        $loginSession = $request->session()->get('loginSession');
        
        $session_user_id = $loginSession['session_user_id'];

        $input = $request->all();

        $type = "1";
        $grade_id = "1";
        $subject_id = "3";
        $topic_id = "1";
        $start_date = '2018-12-12';
        $start_time = '09:05';
        $end_date = '2018-12-12';
        $end_time = '10:00';
        $tutor_type = "Any";
        $visit_type = "1";
        $session_type = "1";
        $venue = "Pune";

        $latitude = "18.555876";
        $longitude = "73.769167";
        $distance = "5.7";
        

        //$session_start_time = date('H:i', strtotime($start_time) - 60*60);
        $session_start_time = $start_time;

        //$session_end_time = date('H:i', strtotime($end_time) + 60*60);
        $session_end_time = $end_time;

        
        $query = DB::table('tutor_subject_grade_ut as tsub')
                        ->join('tutor_details_ut as tDet', 'tsub.user_id', '=', 'tDet.user_id')
                        ->leftJoin('grade_master_ut as gm', 'tsub.grade_id', '=', 'gm.id')
                        ->leftJoin('subject_master_ut as subm', 'tsub.subject_id', '=', 'subm.id')
                        ->leftJoin('tutor_availablity_ut as tavai', 'tDet.user_id', '=', 'tavai.user_id')

                        ->leftJoin('sessions_master_ut as smaster', 'tsub.user_id', '=', 'smaster.tutor_id')                        

                        ->select(DB::raw("(COUNT(smaster.id)) as total_sesion,GROUP_CONCAT( DISTINCT tsub.id) as id, GROUP_CONCAT( DISTINCT gm.name) as grade_name,  GROUP_CONCAT( DISTINCT subm.name) as subject_name,GROUP_CONCAT( DISTINCT tDet.user_id )as tutor_id, GROUP_CONCAT( DISTINCT tDet.name )as tutor_name, GROUP_CONCAT( DISTINCT IFNULL(tDet.latitude,'') )as user_lat, GROUP_CONCAT( DISTINCT IFNULL(tDet.longitude,'') )as user_long, GROUP_CONCAT( DISTINCT IFNULL(tDet.max_distance_travel,'') )as tutor_max_distance, ROUND(GROUP_CONCAT( DISTINCT 111.111 *  DEGREES(ACOS(LEAST(COS(RADIANS( '$latitude')) * COS(RADIANS(tDet.latitude)) * COS(RADIANS('$longitude' - tDet.longitude)) + SIN(RADIANS( '$latitude')) * SIN(RADIANS(tDet.latitude)), 1.0))) ),3) as distance_in_km"))


                        ->where('tsub.grade_id', '=', $grade_id)

                        ->where('tsub.subject_id', '=', $subject_id);

                        
                       
                            
                        if($tutor_type!="Any")
                        {   
                            $query =  $query->where('tDet.gender', '=', $tutor_type);
                        }
                    
                            $query =  $query->where('tavai.date', '=', $start_date);
                        if($start_time!="")
                        {
                            //$query =  $query->whereBetween('tavai.from_time', array($start_time, $end_time));
                            $query =  $query->where('tavai.from_time', '<=', $start_time)
                                            ->where('tavai.to_time', '>=', $end_time);
                        }


                        $query =  $query->groupBy(DB::raw("tDet.id"));                        
                        
                        
                        if($session_type=="1")
                        {
                            
                            if($visit_type=="2")
                            {
                                $query =  $query->having('distance_in_km', '<=', $distance);
                            }
                            elseif($visit_type=="1")
                            {
                                
                               // $query =  $query->having('tutor_max_distance', '>=', 'distance_in_km');
                                $query =  $query->havingRaw('tutor_max_distance >= distance_in_km');
                            }
                        }
                        $query =  $query->orderBy('total_sesion', 'ASC')
                        ->orderBy('tDet.name', 'ASC')
                        ->get();
              
                        
            foreach($query as $key)
            {
                
                $queryTutorDate = DB::table('sessions_master_ut as sm')
                ->select(DB::raw("tutor_id"))
                ->where('sm.tutor_id', '=', $key->tutor_id)
                ->where('sm.start_date', '=', $start_date)
                ->first();

                if(!empty($queryTutorDate))
                {
                    
                    if($queryTutorDate->tutor_id ==$key->tutor_id)
                    {
                        $queryTutor = DB::table('sessions_master_ut as sm')
                        ->select(DB::raw("tutor_id"))
                        ->whereBetween('sm.start_date', array($start_time, $end_time))
                        ->orWhereRaw("sm.end_time BETWEEN '$start_time' AND '$end_time'")

                        ->where('sm.tutor_id', '=', $key->tutor_id)
                        ->where('sm.start_date', '=', $start_date)
                        //->whereBetween('sm.end_time', array($start_time, $end_time))
                        ->get();
                        print_r($queryTutor);
                        if($queryTutor=='')
                        {
                            foreach($queryTutor as $key2)
                            {
                                if($key2->tutor_id!=$key->tutor_id)
                                {
                                    echo " --- ".$key->tutor_id."<br>";
                                }
                            }
                        }
                        else
                        {
                            echo " - ".$key->tutor_id."<br>";
                        }
                    }   
                }
                else
                {
                    echo " - ".$key->tutor_id."<br>";
                }
            }     
    }

    //end demo :  session auto assign algo

}
