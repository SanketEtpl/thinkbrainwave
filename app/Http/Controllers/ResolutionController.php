<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route ;



use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Cookie; 

use Socialite;

class ResolutionController extends BaseController
{
    
    public function index(Request $request)
    {
       
        $resolution =  DB::select('SELECT rm.* , com.name as country_name FROM resolution_master_ut rm LEFT JOIN  countries_master_ut  com ON com.id = rm.country_id WHERE rm.is_deleted = 0');
        
         if(!isset($resolution))
         {
            $resolution = array();
         }
         else
         {
             $resolution = $resolution;
         }
       
      
        return view('Backend.resolution',['resolution'=>$resolution]);

    }

    
    public function deleteResolution($id)
    {
        
        $data = array('is_deleted'=>'1');                    
            DB::table('resolution_master_ut')            
            ->where('id', $id)            
            ->update($data);        
        return redirect('admin/resolution');
    }

   

}

?>