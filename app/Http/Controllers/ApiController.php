<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;


class ApiController extends BaseController
{
    public function index()
    {

        $users = DB::table('user_master_ut')
        ->orderByRaw('id DESC')
        ->get(); 

        return response()->json([
          'status' => 'success',
          'data' => $users,
          ]);
    }
    public function SignUp(Request $request)
    {
        $users ="";
        $role = $request->input('role');
        $name = $request->input('name');
        $contact = $request->input('contact');
        $email = $request->input('email');

        //$password = Hash::make($request->input('password'));
        $password = md5($request->input('password'));
        //$password = $request->input('password');
        $status ='insert';
        if($role=='2')
        {
            DB::select('CALL `sp_sign_up_tutor`("'.$status.'", 1, "'.$email.'", "'.$password.'", "'.$name.'", 1, 1, 1, 1, 1, 1, 1, 1, "'.$contact.'", 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  @result)');
            $users = DB::select('SELECT @result AS `result`;');
        } 
        if($role=='3')
        {
            DB::select('CALL `sp_sign_up_student`("'.$status.'", 1, "'.$email.'", "'.$password.'", "'.$name.'", 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "'.$contact.'", 1, 1, 1, 1, @result)');
            $users = DB::select('SELECT @result AS `result`;');
        }
        if($role=='4')
        {
            DB::select('CALL `sp_sign_up_parent`("'.$status.'", 1, "'.$email.'", "'.$password.'", "'.$name.'", 1, 1, "'.$contact.'", 1, 1, 1, 1, 1, 1, @result)');
            $users = DB::select('SELECT @result AS `result`;');
        }
        return $users;
    }
    public function SignUpSocial($name, $email,$token)
    {
        

        
        $data = array('role_id'=> '3', 'user_type' => '2', 'token' => $token, 'username' => $email, 'password' => '1');

        DB::table('user_master_ut')->insert(array($data));
        $id = DB::getPdo()->lastInsertId();
        return $id;
    }
    public function SignIn(Request $request)
    {
        
       
        $email = $request->input('email');
        //$password = Hash::make($request->input('password'));
        $password = md5($request->input('password'));
        //$password = $request->input('password');
        
        DB::select('CALL `sp_sign_in`("'.$email.'", "'.$password.'", @result);');
        $users = DB::select('SELECT @result AS `result`;');
        
        return $users;
        
    }
    public function ShowProfile($user_id,$role)
    {
        
        $users = '';

        $status = "select";

        if($role=='2')
        {
            DB::select('CALL `sp_sign_up_tutor`("'.$status.'", "'.$user_id.'", 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  @result)');
            $users = DB::select('SELECT @result AS `result`;');
        }
        if($role=='3')
        {
            DB::select('CALL `sp_sign_up_student`("'.$status.'", "'.$user_id.'", 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, @result)');
            $users = DB::select('SELECT @result AS `result`;');
        
        }
        if($role=='4')
        {
            DB::select('CALL `sp_sign_up_parent`("'.$status.'", "'.$user_id.'", 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, @result)');
            $users = DB::select('SELECT @result AS `result`;');
        }
        return $users;
        
    }
    public function StudentProfileSave(Request $request)
    {
        
        $users = '';

        $status = "profile_update";

        $user_id = $request->input('user_id');
        $first_name = $request->input('name');
        $dob = $request->input('dob');
        $gender = $request->input('gender');
        $tutor_type = $request->input('tutor_type');
        $address_line1 = $request->input('address_line1');
        $country_id = $request->input('country_id');
        $max_distance_travel_kms = $request->input('max_distance_travel_kms');
        $primary_phone = $request->input('primary_phone');
        $profile_file_path = $request->file('profile_file_path');
        $email = $request->input('email');

        $user_profile = $request->file('profile_file_path');
        if ($request->hasFile('profile_file_path')){
			$image = $request->file('profile_file_path');
			$input['imagename'] = time().'.'.$image->getClientOriginalExtension();
			$destinationPath = public_path('/images/student');
			$image->move($destinationPath, $input['imagename']);
			$profile_file_name = 'student/'.$input['imagename'];
        }
        else{
            $profile_file_name =  $request->input('old_image');
        }
        
            DB::select('CALL `sp_sign_up_student`("'.$status.'", "'.$user_id.'", 1, 1, "'.$first_name.'", 1, 1, "'.$gender.'", "'.$dob.'", "'.$address_line1.'", 1, 1, 1,1, "'.$country_id.'", "'.$max_distance_travel_kms.'", "'.$primary_phone.'",  1, "'.$profile_file_name.'", "'.$tutor_type.'", "'.$email.'", @result)');
            $users = DB::select('SELECT @result AS `result`;');
       
        return $users;
        //return $profile_file_name;
    }
    public function TutorProfileSave(Request $request)
    {
        
        $users = '';

        $status = "profile_update";
        $user_id = $request->input('user_id');
        $grade_id = $request->input('grade_id');
        $grade_id2 = $request->input('grade_id2');
        $name = $request->input('name');
        $dob = $request->input('dob');
        $gender = $request->input('gender');
        $address = $request->input('address');
        $country_id = $request->input('country_id');
        $primary_phone = $request->input('primary_phone');
        $email_id = $request->input('email_id');
        $max_distance_travel = $request->input('max_distance_travel');
        $qualification = $request->input('qualification');
        $year_exp = $request->input('year_exp');
        $tutor_since = $request->input('tutor_since');

        $bank_name = $request->input('bank_name');
        $branch_name = $request->input('branch_name');
        $Ifsc = $request->input('Ifsc');
        $swift_code = $request->input('swift_code');
        $account_no = $request->input('account_no');
       
        $user_profile = $request->file('profile_file_path');

        if ($request->hasFile('profile_file_path')){
			$image = $request->file('profile_file_path');
			$input['imagename'] = time().'.'.$image->getClientOriginalExtension();
			$destinationPath = public_path('/images/tutor');
			$image->move($destinationPath, $input['imagename']);
			$profile_file_name = 'tutor/'.$input['imagename'];
        }
        else{
            $profile_file_name =  $request->input('old_image');
        }
        
        $profile_file_path = $request->file('profile_file_path');

            DB::select('CALL `sp_sign_up_tutor`("'.$status.'", "'.$user_id.'", 1, 1, "'.$name.'", "'.$dob.'", "'.$gender.'", "'.$profile_file_name.'", "'.$address.'", 1, 1, "'.$country_id.'", 1, "'.$primary_phone.'", 1, "'.$email_id.'",  "'.$max_distance_travel.'", "'.$year_exp.'", "'.$qualification.'", 1, "'.$tutor_since.'", "'.$bank_name.'", "'.$branch_name.'", "'.$Ifsc.'", "'.$swift_code.'", "'.$account_no.'", "'.$grade_id.'", "'.$grade_id2.'", @result)');
            $users = DB::select('SELECT @result AS `result`;');
       
       // return $users;
        return $users;
    }

    public function ParentProfileSave(Request $request)
    {
        
        $users = '';

        $status = "profile_update";
        $user_id = $request->input('user_id');
        $name = $request->input('name');
        $dob = $request->input('dob');
        $gender = $request->input('gender');
        $address = $request->input('address');
        $country_id = $request->input('country_id');
        $primary_phone = $request->input('primary_phone');
        $email_id = $request->input('email');
        $profile = $request->input('profile');
        $old_image = $request->input('old_image');
        

        if ($request->hasFile('profile')){
            $image = $request->file('profile');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/parent');
            $image->move($destinationPath, $input['imagename']);
            $profile_file_name = 'parent/'.$input['imagename'];

            DB::select('CALL `sp_sign_up_parent`("'.$status.'", "'.$user_id.'", 1, 1, "'.$name.'", "'.$dob.'", "'.$gender.'", "'.$primary_phone.'", "'.$email_id.'", "'.$address.'", 1, 1, "'.$country_id.'", "'.$profile_file_name.'", @result)');
            $users = DB::select('SELECT @result AS `result`;');
        }
        else{

            if($old_image != '') {

                DB::select('CALL `sp_sign_up_parent`("'.$status.'", "'.$user_id.'", 1, 1, "'.$name.'", "'.$dob.'", "'.$gender.'", "'.$primary_phone.'", "'.$email_id.'", "'.$address.'", 1, 1, "'.$country_id.'", "'.$profile_file_name.'", @result)');
                $users = DB::select('SELECT @result AS `result`;');
            } else {
                DB::select('CALL `sp_sign_up_parent`("'.$status.'", "'.$user_id.'", 1, 1, "'.$name.'", "'.$dob.'", "'.$gender.'", "'.$primary_phone.'", "'.$email_id.'", "'.$address.'", 1, 1, "'.$country_id.'", 1, @result)');
                $users = DB::select('SELECT @result AS `result`;');
            }
            
        }
       
       return $users;
        // return $request->file();
    }

    public function UpdatePassword(Request $request)
    {
        
        $user_id = $request->input('user_id');
        $new_password = $request->input('new_password');
        $password = md5($request->input('new_password'));
        $data = array('password' => $password);

        DB::table('user_master_ut')
		->where('id', $user_id)
		->update($data);
        $profile_update = "1"; 
        return $profile_update;
    }
    public function PostDemo(Request $request)
    { 
        $put = "2";
        return $request->input('name');
    }
   
}
 
    
