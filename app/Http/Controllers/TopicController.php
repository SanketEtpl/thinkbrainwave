<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route ;



use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Cookie; 

use Socialite;

class TopicController extends BaseController
{
    
    public function index(Request $request, $id = NULL)
    {
        $select = 'select';
        DB::select('CALL `sp_topic`("'.$select.'",1,1,1,1,1,1, @result)');
        $data = DB::select('SELECT @result AS `result`;');
        $topicdata = json_decode($data[0]->result);
        
        if(isset($topicdata->status))
         {
            $topicdata = array();
         }
         else
         {
             $topicdata = $topicdata;
         }


        $select = 'select';
        DB::select('CALL `sp_grade`("'.$select.'",1,1,1, @result)');
        $data = DB::select('SELECT @result AS `result`;');
        $gradedata = json_decode($data[0]->result);

        if(isset($gradedata->status))
         {
            $gradedata = array();
         }
         else
         {
             $gradedata = $gradedata;
         }


        $topicEditData = NULL;
        $couresedata = NULL;
            if($id!=NULL)
            {
                $topicEditData = DB::table('topic_master_ut')->where('id', $id)->first();
                $couresedata = DB::table('subject_master_ut')->where('grade_id', $topicEditData->grade_id)->where('status',1)->where('is_deleted',0)->get();
                  
            }

       return view('Backend.topics',['topic'=>$topicdata,'topiceditData' => $topicEditData, 'grade' => $gradedata, 'course' => $couresedata]);

    }

    public function addTopic(Request $request)
    {
            $input = request()->all();

            $grade_id = $request->input('grade');
            $subject_id = $request->input('course');
            $name = $request->input('topic');
            $description = $request->input('description');
            $status = $request->input('status');
            $id = $request->input('topicid');

            if(isset($id))
            {
                $update = 'update';
                DB::select('CALL `sp_topic`("'.$update.'","'.$id.'","'.$grade_id.'","'.$subject_id.'","'.$name.'","'.$description.'","'.$status.'", @result)');
            }
            else
            {
                $insert = 'insert';
                DB::select('CALL `sp_topic`("'.$insert.'",1,"'.$grade_id.'","'.$subject_id.'","'.$name.'","'.$description.'","'.$status.'", @result)');
            }

            
       
            return redirect()->to('admin/topics');
            
    }
    public function deleteTopic($id)
    {
        $delete = 'delete';
        DB::select('CALL `sp_topic`("'.$delete.'","'.$id.'",1,1,1,1,1, @result)');
        return redirect()->to('admin/topics');
    }

    public function FetchCourse(Request $request)
    {
         $input = $request->all();
         $grade_id = $request->grade;
         $Data = DB::table("subject_master_ut")->where("grade_id",$grade_id )->where("status",1)->where("is_deleted",0)->pluck("name","id");
         return response()->json($Data); 
    }

    public function changeStatus($id, $status) {

        if($status == 0) {
            $data =array('status' =>1);
        } else {
            $data =array('status' =>0);
        }

            DB::table('topic_master_ut')            
            ->where('id', $id)            
            ->update($data);        
        return redirect('admin/topics');
        
    }


}

?>