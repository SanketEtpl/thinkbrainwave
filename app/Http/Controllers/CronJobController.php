<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;

use DateTime;
use Carbon\Carbon;

use Illuminate\Support\Str;

class CronJobController extends BaseController
{
    public function index()
    {

        
    }
    public function OneHourCron(Request $request)
    {
        
        //CONSTANTS_EXAMPLE
          //  echo config('constants.CONSTANTS_EXAMPLE'); 
        //end CONSTANTS_EXAMPLE
        $nowDateTime = date('Y-m-d H:i:s');
        $nowDate = date('Y-m-d');
        $nowTime = date("H:i");
        $timestamp = strtotime($nowTime) + 60*60;
        $oneHourAfterNowTime = date('H:i', $timestamp);

        $SessionData = DB::table('sessions_master_ut as sesM')
                ->select('sesM.*')
                ->where('sesM.start_date', '=', $nowDate)
                ->where('sesM.start_time', '=', $oneHourAfterNowTime)
                ->get();
        
        
        
        foreach($SessionData as $key)
        {
            $id = $key->id;
            $tutor_id = $key->tutor_id;
            $student_id = $key->student_id;

            $message ="your session No :".$id." will start in 1 hour";

            $Tutordata = array('sender_id'=> '1', 'receiver_id' => $tutor_id, 'notification_type' => 'Session_1_Hour_Left', 'message' => $message, 'created_at' => $nowDateTime);

            $Studentdata = array('sender_id'=> '1', 'receiver_id' => $student_id, 'notification_type' => 'Session_1_Hour_Left', 'message' => $message, 'created_at' => $nowDateTime);

            DB::table('notification_ut')->insert(array($Tutordata));
            DB::table('notification_ut')->insert(array($Studentdata));
            
        }   
        //echo $rand = Str::random(60);
        echo $number = mt_rand(1000000000, 9999999999);        
    }
    
}
 
    
