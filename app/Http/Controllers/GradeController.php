<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route ;



use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Cookie; 

use Socialite;

class GradeController extends BaseController
{
    
    public function index(Request $request, $id= NULL)
    {
        $select = 'select';
        DB::select('CALL `sp_grade`("'.$select.'",1,1,1, @result)');
        $data = DB::select('SELECT @result AS `result`;');
        $gradedata = json_decode($data[0]->result);
        $gradeEditData = NULL;
            if($id!=NULL)
            {
                $gradeEditData = DB::table('grade_master_ut')->where('id', $id)->first();
            }

        
         if(isset($gradedata->status))
         {
            $gradedata = array();
         }
         else
         {
             $gradedata = $gradedata;
         }

        return view('Backend.grade',['grade'=>$gradedata,'gradeeditData' => $gradeEditData]);

    }

    public function addGrade(Request $request)
    {
            $input = request()->all();

            $name = $request->input('grade');
            $status = $request->input('status');
            $id = $request->input('gradeid');

            if(isset($id))
            {
                $update = 'update';
                DB::select('CALL `sp_grade`("'.$update.'","'.$id.'","'.$name.'","'.$status.'", @result)');
            }
            else
            {
                $insert = 'insert';
                DB::select('CALL `sp_grade`("'.$insert.'",1,"'.$name.'","'.$status.'", @result)');
            }

            
       
            return redirect()->to('admin/grade');
            
    }
    public function deleteGrade($id)
    {
        $delete = 'delete';
        DB::select('CALL `sp_grade`("'.$delete.'","'.$id.'",1,1, @result)');
        return redirect()->to('admin/grade');
    }

    public function changeStatus($id, $status) {

        if($status == 0) {
            $data =array('status' =>1);
        } else {
            $data =array('status' =>0);
        }

            DB::table('grade_master_ut')            
            ->where('id', $id)            
            ->update($data);        
        return redirect('admin/grade');
        
    }


}

?>