<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route ;



use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Cookie; 

use Socialite;

class AdminProfileController extends BaseController
{
    public function index(Request $request)
    {
    	$sessionData = $request->session()->get('AdminloginSession');

         $countries = DB::table('countries_master_ut')->get();
         $state = DB::table('states_master_ut')->get();
         $city = DB::table('cities_master_ut')->get();
         
    	$select = 'select';

		DB::select('CALL `sp_admin_profile`("'.$select.'", "'.$sessionData['admin_session_user_id'].'",1,1,1,1,1,1,1,1,1,1,1,1,1, @result);');

		 $Profiledata = DB::select('SELECT @result AS `result`;');
		 $adminData = json_decode($Profiledata[0]->result);

		 

    	return view('Backend.myprofile',['profileData' => $adminData,'countries' => $countries, 'state' =>$state, 'city' => $city ]);

    }

    public function updateprofile(Request $request)
    {
             $input = request()->all();

             $updateTable = 'profile_update';
             $select = 'select';

    	     $sessionData = $request->session()->get('AdminloginSession');
             $user_id = $sessionData['admin_session_user_id'];
             $name = $request->input('name');
             $phone = $request->input('contact');
             $email = $request->input('email');
             $address = $request->input('address');
             $city_id = $request->input('city_id');
             $state_id = $request->input('state_id');
             $country_id = $request->input('country_id');
             
            


            DB::select('CALL `sp_admin_profile`("'.$updateTable.'", "'.$user_id.'", "'.$name.'","'.$phone.'", "'.$email.'", "'.$address.'","'.$city_id.'", "'.$state_id.'", "'.$country_id.'",1,1,1,1,1,1, @result)');
            
            DB::select('CALL `sp_admin_profile`("'.$select.'", "'.$sessionData['admin_session_user_id'].'",1,1,1,1,1,1,1,1,1,1,1,1,1, @result);');

             $data = DB::select('SELECT @result AS `result`;');
			 $admindata = json_decode($data[0]->result);
             $sessionArray = array(
		            'admin_session_user_id' => $admindata->id,
		            'admin_session_username' => $admindata->username,
		            'admin_session_user_password' => $admindata->password,
		            'admin_session_user_name' => $admindata->name,
		            );

		        
		      $request->session()->put('AdminloginSession', $sessionArray);

             return redirect()->to('admin/myprofile');

    }


    public function updatebankdetails(Request $request)
    {
             $input = request()->all();

             $updateTable = 'bank_detail_update';
    	     $sessionData = $request->session()->get('AdminloginSession');
             $user_id = $sessionData['admin_session_user_id'];
             $bname = $request->input('bankname');
             $brname = $request->input('branchname');
             $ifsc = $request->input('ifsc');
             $swift = $request->input('swiftcode');
             $micr = $request->input('micr');
             $account = $request->input('accountnumber');

            DB::select('CALL `sp_admin_profile`("'.$updateTable.'", "'.$user_id.'",1,1,1,1,1,1,1,"'.$bname.'","'.$brname.'", "'.$ifsc.'", "'.$swift.'","'.$micr.'", "'.$account.'", @result)');
            
       
            return redirect()->to('admin/myprofile');

    }
    

    public function AdminLogin(Request $request)
    {


        $input = request()->all();
    	$username = $request->input('username');
        $password1 = $request->input('password');
        $remember = $request->input('remember');
        
        $password = md5($password1);

        // if($remember=='on')
        // {
           
        //      $response = new \Illuminate\Http\Response();
        //      $response->withCookie(cookie('username', $username, 60));
        //       $response->withCookie(cookie('password', $password, 60));
        //     // echo Cookie::get('password');
        //      //die;
             
        // }

        DB::select('CALL `sp_sign_in`("'.$username.'", "'.$password.'", @result);');
        $users = DB::select('SELECT @result AS `result`;');
        $result = json_decode($users[0]->result);
          if(!isset($result->status))
	        {
                if($result->role_id==1)
                {
                  

					 $select = 'select';
			        DB::select('CALL `sp_admin_profile`("'.$select.'", "'.$result->id.'",1,1,1,1,1,1,1,1,1,1,1,1,1, @result);');
			        $data = DB::select('SELECT @result AS `result`;');
                    $admindata = json_decode($data[0]->result);
                    if($admindata->user_id==1)
                    {
                        $sessionArray = array(
                        'admin_session_user_id' => $result->id,
                        'admin_session_username' => $result->username,
                        'admin_session_user_password' => $result->password,
                        'admin_session_user_name' => $admindata->name,
                        );
                }

                // $notCount =  DB::table('notification_ut')
                //             ->where('receiver_id', '=', '1')
                //             ->get();
                // echo $countNotification = count($notCount);
		        
		        $request->session()->put('AdminloginSession', $sessionArray);
	            return redirect()->to('admin/dashboard');		
             }
                else
                {
                    return redirect()->to('admin')
                        ->with('error','Incorrect email or password');
                } 
            }
	    	else
	    	{
	    		return redirect()->to('admin')
			        ->with('error','Incorrect email or password');
	    	} 
	        
          
    }

    public function FetchState(Request $request)
    {
    	$input = $request->all();
         $country_id = $request->country_id;
        $Data = DB::table("states_master_ut")->where("country_id",$country_id)->pluck("name","id");
        return response()->json($Data);
    }

    public function FetchCity(Request $request)
    {
    	$input = $request->all();
         $state_id = $request->state_id;
        $Data = DB::table("cities_master_ut")->where("state_id",$state_id)->pluck("name","id");
        return response()->json($Data);
    }

    public function logout(Request $request) {
        Session::forget('AdminloginSession');
		// $request->session()->flush('AdminloginSession');
		return redirect()->to('admin');
    }

    function ForgotPassword(Request $request)
    {
        $input = $request->all();
        $email = $request->email;
        $Data = DB::select('select * from user_master_ut as u where u.username = "'.$email.'" ');
        //print_r($Data[0]->username);
       
        if(count($Data)>0)
        {
            $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890');
            $passwordEmail = substr($random, 0, 10);
            $password = md5($passwordEmail);

            $data = array('password'=>$password);                    
            DB::table('user_master_ut')            
            ->where('id', $Data[0]->id)            
            ->update($data);  

            $receiveName = 'Admin';
            $mailto = $Data[0]->username;
            $body = $passwordEmail;
            $subject = 'Updated Password';

           $sendMail =  \App\Http\Controllers\MailController::SendEmail($receiveName, $mailto, $body, $subject);
           return response()->json(['status'=>'1']);
            
        }
        else{
            return response()->json(['status'=>'0']);
        }
       
    }
}

?>