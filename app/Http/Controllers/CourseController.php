<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route ;



use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\Http\Requests;
// use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
// use Cookie; 

use Socialite;


class CourseController extends BaseController
{

	public function index($id=NULL)
    {
        $select = 'select';        
        DB::select('CALL `sp_grade`("'.$select.'",1,1,1, @result)');        
        $data = DB::select('SELECT @result AS `result`;');        
        $gradedata = json_decode($data[0]->result);

        if(isset($gradedata->status))
         {
            $gradedata = array();
         }
         else
         {
             $gradedata = $gradedata;
         }

        $cousesData = DB::select('SELECT *,subject_master_ut.id as Sid,subject_master_ut.status as Status, subject_master_ut.name as Name FROM `subject_master_ut` LEFT JOIN grade_master_ut on grade_master_ut.id = subject_master_ut.grade_id order by subject_master_ut.id DESC ');

            $singleCourse = NULL;
            if($id!=NULL) {

                $course =  DB::table('subject_master_ut')
                                ->where('id', '=', $id)
                                ->get();
                $singleCourse['subject_id'] = $course[0]->id;
                $singleCourse['grade'] = $course[0]->grade_id;
                $singleCourse['name'] = $course[0]->name;
                $singleCourse['description'] = $course[0]->description;
                $singleCourse['status'] = $course[0]->status;
                
            }
        return view('Backend.courses', ['grade' => $gradedata, 'couses' => $cousesData, 'singleCourse' => $singleCourse]);
    }
    
    public function addCourse(Request $request)
    {
    	
    	$subject_id = $request->input('subject_id');
        $grade = $request->input('grade');
        $name = $request->input('name');
        $description = $request->input('description');
        $status = $request->input('status');
 
        if($subject_id == '') {

            $data = array('grade_id' => $grade,'name' => $name, 'description' => $description, 'status' => $status);
            DB::table('subject_master_ut')->insert($data);
                
        } else {

            $data = array('grade_id' => $grade,'name' => $name, 'description' => $description, 'status' => $status);                 
            DB::table('subject_master_ut')            
            ->where('id', $subject_id)            
            ->update($data); 

        }

        return redirect('admin/courses');
    }

    public function changeStatus($id, $status) {

        if($status == 0) {
            $data =array('status' =>1);
        } else {
            $data =array('status' =>0);
        }

            DB::table('subject_master_ut')            
            ->where('id', $id)            
            ->update($data);        
        return redirect('admin/courses');
        
    }

    public function deleteCourse($id) {

        $data = array('is_deleted'=>'1');                    
            DB::table('subject_master_ut')            
            ->where('id', $id)            
            ->update($data);        
        return redirect('admin/courses');
        
    }




}

?>