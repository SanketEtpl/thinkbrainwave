<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route ;


use Illuminate\Support\Facades\Input;
use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;


class AllProfileDataController extends BaseController
{
    
    // show profile data from api
    public static function ShowProfileData($id, $role)
    {
        $request1 = Request::create('/api/ShowProfile/'.$id.'/'.$role, 'GET');
        $response1 = Route::dispatch($request1)->getContent();
        $instance1 = json_decode($response1,true);
        $jsonNew1 = $instance1[0]['result'];
        $result1 = json_decode($jsonNew1,true);

        return $result1;

    }
    // end show profile data from api

    


}
