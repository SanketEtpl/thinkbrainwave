<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route ;



use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Cookie; 

use Socialite;

class SignupController extends BaseController
{
    public function index()
    {
        $roles = DB::table('role_master_ut')
                    ->where('id','!=','1')
                    ->get();
        return view('Website.signup',['roles' => $roles]);
    }
    public function Signin()
    {
        $roles = DB::table('role_master_ut')
                    ->where('id','!=','1')
                    ->get();
        return view('Website.signin',['roles' => $roles]);
    }
    
    public function SignupSave(Request $request)
    {
        $input = $request->all();
        $role = $request->role;
        $name = $request->name;
        $email = $request->email;
        $contact = $request->contact;
        $password = $request->password;
        
        $args = array(
            'role' => $role,
            'name' => $name,
            'email' => $email,
            'contact' => $contact,
            'password' => $password
        );
        $request = Request::create('/api/SignUp', 'POST', $args);
        $instance = json_decode(Route::dispatch($request)->getContent());
        // print_r($instance[0]->result);
        // echo $instance[0]->result['status'];
        // print_r($instance[0]);
        if($instance[0]->result!='{"status": "exist"}')
        {
            if($role == 2) {
                $signup =  'Tutor';
            } else if($role == 3) {
                $signup =  'Student';
            } else if($role == 4) {
                $signup =  'Parent';
            }

            $adminId = '1';
            $message = " $name sign up on platform as $signup " ;
            $notification_type = "SignUpUser";
            
            $InsertNotificationData_admin = \App\Http\Controllers\AllCommonDataController::InsertNotificationData($adminId, $notification_type, $message);
        
            return response()->json(['success'=>'success']);
        }
        else
        {
            return response()->json(['success'=>'error']);
        }
    }
    public function SignInSave(Request $request)
    {
        //$input = $request->all();
        $email = $request->input('email');
        $password = $request->input('password');
        $check1 = $request->input('check1');
        if($check1=='on')
        {
            
            $response = new \Illuminate\Http\Response();
            $response->withCookie(cookie('email', $email, 60));
            $response->withCookie(cookie('password', $password, 60));
           
           //echo  $value = Cookie::get('email');
        }
        
       // $email = "student@gmail.com";
        //$password = "student";
        
        $argsLogin = array(
            'emailsasa' => $email,
            'password' => $password
        );

        $request = Request::create('/api/SignIn', 'POST');
        $response = Route::dispatch($request)->getContent();
        $instance = json_decode($response,true);
        $jsonNew = $instance[0]['result'];
        $result = json_decode($jsonNew,true);

       


        if(!isset($result['status']))
        {
            $id = $result['id'];
            $role = $result['role_id'];

                $roleStaus ='';
                if($result['role_id']=='2')
                {
                    $roleStaus ='Tutor';
                }
                if($result['role_id']=='3')
                {   
                    $roleStaus ='Student';  
                }   
                if($result['role_id']=='4')
                {
                    $roleStaus ='Parent';
                }
        
           
                
                
                if($result['role_id']=='3')
                {
                    $request1 = Request::create('/api/ShowProfile/'.$id.'/'.$role, 'GET');
                    $response1 = Route::dispatch($request1)->getContent();
                    $instance1 = json_decode($response1,true);
                    $jsonNew1 = $instance1[0]['result'];
                    $result1 = json_decode($jsonNew1,true);

                    if($result1['profile_file_path']=='')
                    {
                    
                        $session_image = "no-image.png";
                    }
                    else{
                        $session_image = $result1['profile_file_path'];
                    }
                    $sessionArray = array(
                        'session_user_role_id' => $result['role_id'],
                        'session_user_id' => $result['id'],
                        'session_user_type' => $result['user_type'],
                        'session_user_username' => $result['username'],
                        'session_user_password' => $result['password'],
                        'session_user_name' => $result1['first_name'],
                        'session_user_profile_file_path' => $session_image,
                        'session_user_role' => $roleStaus);
                    
                        $request->session()->put('loginSession', $sessionArray);
                    $value = $request->session()->get('loginSession');

                    //print_r($value);
               
                    return redirect()->to('Dashboard'); 
                } 
                if($result['role_id']=='2')
                {
                    $request_tu = Request::create('/api/ShowProfile/'.$id.'/'.$role, 'GET');
                    $response_tu = Route::dispatch($request_tu)->getContent();
                    $instance_tu = json_decode($response_tu,true);
                    $jsonNew_tu = $instance_tu[0]['result'];
                    $result_tu = json_decode($jsonNew_tu,true);
                    //print_r($result_tu);

                    if($result_tu['profile_file_path']=='')
                    {
                    
                        $session_image = "no-image.png";
                    }
                    else{
                        $session_image = $result_tu['profile_file_path'];
                    }
                    $sessionArray = array(
                        'session_tutor_role_id' => $result['role_id'],
                        'session_tutor_id' => $result['id'],
                        'session_tutor_type' => $result['user_type'],
                        'session_tutor_username' => $result['username'],
                        'session_tutor_password' => $result['password'],
                        'session_tutor_name' => $result_tu['name'],
                        'session_tutor_profile_file_path' => $session_image,
                        'session_tutor_role' => $roleStaus);
                    
                    $request->session()->put('loginSessionTutor', $sessionArray);
                    $value = $request->session()->get('loginSessionTutor');
                   // print_r($value);
                    //die();
                    return redirect()->to('TutorDashboard'); 
                }
                if($result['role_id']=='4')
                {
                    $request_pa = Request::create('/api/ShowProfile/'.$id.'/'.$role, 'GET');
                    $response_pa = Route::dispatch($request_pa)->getContent();
                    $instance_pa = json_decode($response_pa,true);
                    $jsonNew_pa = $instance_pa[0]['result'];
                    $result_pa = json_decode($jsonNew_pa,true);
                    
                    if($result_pa['profile_file_path']=='')
                    {
                    
                        $session_image = "public/images/no-image.png";
                    }
                    else{
                        $session_image = 'public/images/'.$result_pa['profile_file_path'];
                    }
                    $sessionParArray = array(
                        'sessRoleId' => $result['role_id'],
                        'sessId' => $result['id'],
                        'sessType' => $result['user_type'],
                        'sessUsername' => $result['username'],
                        'sessPassword' => $result['password'],
                        'sessName' => $result_pa['name'],
                        'sessImage' => $session_image,
                        'sessRole' => $roleStaus
                    );
                    
                    $request->session()->put('loginSessionParent', $sessionParArray);
                    $value = $request->session()->get('loginSessionParent');
                   // print_r($value);
                    // die();
                    return redirect()->to('ParentDashboard'); 
                } 
                else{
                    return redirect()->to('Signin')
			        ->with('error','Detail not match');
                }
           
        }
        else
        {
            return redirect()->to('Signin')
			->with('error','incorrect email or password');
        }
    }
    public function logout(Request $request) {

		// $request->session()->flush('loginSession');
        Session::forget('loginSession');
        Session::forget('loginSessionTutor');
        Session::forget('loginSessionParent');
		return redirect()->to('Home');
    }
    public function logoutTutor(Request $request) {

		// $request->session()->flush('loginSession');
        Session::forget('loginSessionTutor');
		return redirect()->to('Home');
    }
    public function UpdatePassword(Request $request)
    {
        
        $input = $request->all();
        $user_id = $request->user_id;
        $old_password = $request->old_password;
        $new_password = $request->new_password;
        $retype_password = $request->retype_password;
        
        $password = md5($request->old_password);

        $UserData = DB::table('user_master_ut')
                     ->where('id', '=', $user_id)
                     ->where('password', '=', $password)
                     ->first();
        if(!empty($UserData ))
        {
            $args = array(
                'user_id' => $user_id,
                'new_password' => $new_password
            );
            $request = Request::create('/api/UpdatePassword', 'POST', $args);
            $instance = json_decode(Route::dispatch($request)->getContent());
            //print_r($instance[0]->result);
            
            return response()->json(['success'=>'password updated']);
        }
        else
        {
       
            return response()->json(['error'=>'password not same']);
        }
    }
    public function LoginWithFacebook($role)
    {
        echo "hello";
        echo $role;
    }
    public function callbackFB(Request $request)
    {
        $user = Socialite::driver('facebook')->user();

        $temp_session= $request->session()->get('FacebookSession');
        $temp_session_role_id = $temp_session['session_role_id'];
       
        $data = array('role_id'=>$temp_session_role_id, 'user_type' => '2', 'token' => $user->token, 'username' => $user->email);

        $Studentdata = array('first_name'=>  $user->name, 'profile_file_path' => $user->avatar_original);

        $Tutordata = array('name'=>  $user->name, 'profile_file_path' => $user->avatar_original, 'email_id'=>  $user->email);


        $Parentdata = array('name'=>  $user->name, 'profile_file_path' => $user->avatar_original, 'email'=>  $user->email);


        $UserData = DB::table('user_master_ut')
                     ->where('username', '=', $user->email)
                     ->first();
        if(!empty($UserData ))
        {
            DB::table('user_master_ut')
            ->where('username', $user->email)
            ->update($data);
            $id = $UserData->id;

           

            if($temp_session_role_id=='2')
            {
                DB::table('tutor_details_ut')
                ->where('user_id', $id)
                ->update($Tutordata);
            }
            if($temp_session_role_id=='3')
            {
                DB::table('student_master_ut')
                ->where('user_id', $id)
                ->update($Studentdata);
            }
            if($temp_session_role_id=='4')
            {
                DB::table('parent_details_ut')
                ->where('user_id', $id)
                ->update($Parentdata);
            }
        }
        else{
            DB::table('user_master_ut')->insert(array($data));
            $id = DB::getPdo()->lastInsertId();
            if($temp_session_role_id=='2')
            {
                $Tutordata['user_id'] = $id;
                DB::table('tutor_details_ut')->insert(array($Tutordata));
            }
            if($temp_session_role_id=='3')
            {
                $Studentdata['user_id'] = $id;
                DB::table('student_master_ut')->insert(array($Studentdata));
            }
            if($temp_session_role_id=='4')
            {
                $Parentdata['user_id'] = $id;
                DB::table('parent_details_ut')->insert(array($Parentdata));
            }
            
        }
        if($temp_session_role_id=='2')
        {
            $roleStaus ='Tutor';
        }
        if($temp_session_role_id=='3')
        {   
            $roleStaus ='Student';  
        }   
        if($temp_session_role_id=='4')
        {
            $roleStaus ='Parent';
        }
        $result = DB::table('user_master_ut')
                     ->where('username', '=', $user->email)
                     ->first();

        if($user->avatar_original=='')
        {
        
            $session_image = "no-image.png";
        }
        else{
            $session_image = $user->avatar_original;
        }

        
        if($temp_session_role_id=='3')
        {
            $sessionArray = array(
                'session_user_role_id' => $result->role_id,
                'session_user_id' => $result->id,
                'session_user_type' => $result->user_type,
                'session_user_username' => $result->username,
                'session_user_password' => $result->password,
                'session_user_name' => $user->name,
                'session_user_profile_file_path' => $session_image,
                'session_user_role' => $roleStaus);
            
            $request->session()->put('loginSession', $sessionArray);
            $value = $request->session()->get('loginSession');
            return redirect()->to('Dashboard');

        } 
        if($temp_session_role_id=='2')
        {
            $sessionArray = array(
                'session_tutor_role_id' => $result->role_id,
                'session_tutor_id' => $result->id,
                'session_tutor_type' => $result->user_type,
                'session_tutor_username' => $result->username,
                'session_tutor_password' => $result->password,
                'session_tutor_name' => $user->name,
                'session_tutor_profile_file_path' => $session_image,
                'session_tutor_role' => $roleStaus);
            $request->session()->put('loginSessionTutor', $sessionArray);
            $value = $request->session()->get('loginSession');
            return redirect()->to('TutorDashboard'); 

        }
        if($temp_session_role_id=='4')
        {
            $sessionParArray = array(
                'sessRoleId' => $result->role_id,
                'sessId' => $result->id,
                'sessUsername' => $result->username,
                'sessPassword' => $result->password,
                'sessName' => $user->name,
                'sessImage' => $session_image,
                'sessRole' => $roleStaus);
            
            
            $request->session()->put('loginSessionParent', $sessionParArray);
            $value = $request->session()->get('loginSessionParent');
           // print_r($value);
            // die();
            return redirect()->to('ParentDashboard'); 

        }
        else{
            return redirect()->to('Signin')
            ->with('error','Tutor and parent dashbord under construction');
        }

    }
 
    public function redirectFB(Request $request,$role)
    {
        $sessionFacebookArray = array('session_role_id' => $role);
        
        $request->session()->put('FacebookSession', $sessionFacebookArray);

        return Socialite::driver('facebook')->redirect();
        
    }

    public function callback(Request $request)
    {
        $user = Socialite::driver('google')->user();
       
        $temp_session= $request->session()->get('GoogleSession');
        $temp_session_role_id = $temp_session['session_role_id'];
       
        $data = array('role_id'=>$temp_session_role_id, 'user_type' => '3', 'token' => $user->token, 'username' => $user->email);

        $Studentdata = array('first_name'=>  $user->name, 'profile_file_path' => $user->avatar_original);

        $Tutordata = array('name'=>  $user->name, 'profile_file_path' => $user->avatar_original, 'email_id'=>  $user->email);


        $Parentdata = array('name'=>  $user->name, 'profile_file_path' => $user->avatar_original, 'email'=>  $user->email);


        $UserData = DB::table('user_master_ut')
                     ->where('username', '=', $user->email)
                     ->first();
        if(!empty($UserData ))
        {
            DB::table('user_master_ut')
            ->where('username', $user->email)
            ->update($data);
            $id = $UserData->id;

           

            if($temp_session_role_id=='2')
            {
                DB::table('tutor_details_ut')
                ->where('user_id', $id)
                ->update($Tutordata);
            }
            if($temp_session_role_id=='3')
            {
                DB::table('student_master_ut')
                ->where('user_id', $id)
                ->update($Studentdata);
            }
            if($temp_session_role_id=='4')
            {
                DB::table('parent_details_ut')
                ->where('user_id', $id)
                ->update($Parentdata);
            }
        }
        else{
            DB::table('user_master_ut')->insert(array($data));
            $id = DB::getPdo()->lastInsertId();
            if($temp_session_role_id=='2')
            {
                $Tutordata['user_id'] = $id;
                DB::table('tutor_details_ut')->insert(array($Tutordata));
            }
            if($temp_session_role_id=='3')
            {
                $Studentdata['user_id'] = $id;
                DB::table('student_master_ut')->insert(array($Studentdata));
            }
            if($temp_session_role_id=='4')
            {
                $Parentdata['user_id'] = $id;
                DB::table('parent_details_ut')->insert(array($Parentdata));
            }
            
        }
        if($temp_session_role_id=='2')
        {
            $roleStaus ='Tutor';
        }
        if($temp_session_role_id=='3')
        {   
            $roleStaus ='Student';  
        }   
        if($temp_session_role_id=='4')
        {
            $roleStaus ='Parent';
        }
        $result = DB::table('user_master_ut')
                     ->where('username', '=', $user->email)
                     ->first();

        if($user->avatar_original=='')
        {
        
            $session_image = "public/images/no-image.png";
        }
        else{
            $session_image = $user->avatar_original;
        }

       
        
        
        if($temp_session_role_id=='3')
        {
            $sessionArray = array(
                'session_user_role_id' => $result->role_id,
                'session_user_id' => $result->id,
                'session_user_type' => $result->user_type,
                'session_user_username' => $result->username,
                'session_user_password' => $result->password,
                'session_user_name' => $user->name,
                'session_user_profile_file_path' => $session_image,
                'session_user_role' => $roleStaus);
            $request->session()->put('loginSession', $sessionArray);
            $value = $request->session()->get('loginSession');
            return redirect()->to('Dashboard');

        } 
        if($temp_session_role_id=='2')
        {
            $sessionArray = array(
                'session_tutor_role_id' => $result->role_id,
                'session_tutor_id' => $result->id,
                'session_tutor_type' => $result->user_type,
                'session_tutor_username' => $result->username,
                'session_tutor_password' => $result->password,
                'session_tutor_name' => $user->name,
                'session_tutor_profile_file_path' => $session_image,
                'session_tutor_role' => $roleStaus);
            $request->session()->put('loginSessionTutor', $sessionArray);
            $value = $request->session()->get('loginSession');
            return redirect()->to('TutorDashboard'); 

        }
        if($temp_session_role_id=='4')
        {
            $sessionParArray = array(
                'sessRoleId' => $result->role_id,
                'sessId' => $result->id,
                'sessUsername' => $result->username,
                'sessPassword' => $result->password,
                'sessName' => $user->name,
                'sessImage' => $session_image,
                'sessRole' => $roleStaus);
            
            
            $request->session()->put('loginSessionParent', $sessionParArray);
            $value = $request->session()->get('loginSessionParent');
           // print_r($value);
            // die();
            return redirect()->to('ParentDashboard'); 

        }
        else{
            return redirect()->to('Signin')
            ->with('error','Tutor and parent dashbord under construction');
        }
    }
 
    public function redirect(Request $request,$role)
    {
        $sessionGoogleArray = array('session_role_id' => $role);
        
        $request->session()->put('GoogleSession', $sessionGoogleArray);
        
        return Socialite::driver('google')->redirect();
        
    }

    public function resetPassLink(Request $request)
    {
        $emailAddress = $request->email_address;
        
        $getUser = DB::Table('user_master_ut')->select('id','role_id','username')->where('username',$emailAddress)->get(); 
        
        if(count($getUser) > 0) {
            
            $userid = $getUser[0]->id;
            if($getUser[0]->role_id == 2) {
                $table = 'tutor_details_ut';
                $search = 'name';
            } else if($getUser[0]->role_id == 3) {
                $table = 'student_master_ut';
                $search = 'first_name';
            } else if($getUser[0]->role_id == 4) {
                $table = 'parent_details_ut';
                $search = 'name';
            }
            
            $getRecevicer = DB::Table($table)->select("$search AS name")->where('user_id',$userid)->get();
            $receiveName = $getRecevicer[0]->name;
            
            $mailto = 'priya@exceptionaire.co';
            $subject = 'Reset Password Link';

            $body = "Test";
            
            $sendMail = \App\Http\Controllers\MailController::SendResetEmail($receiveName, $mailto, $body, $subject);

            return response()->json(['status' => '1', 'msg' => 'Verification link has been sent to your registered email address. Kindly verify to proceed further.']);
            
        } else {
            return response()->json(['status' => '0', 'msg' => 'Email address not exists.']);
        }
        
    }

}
