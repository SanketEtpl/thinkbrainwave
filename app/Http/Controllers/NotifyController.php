<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route ;



use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Cookie; 

use Socialite;

class NotifyController extends BaseController
{
    
    public function index(Request $request)
    {
        
      
        return view('Backend.notify');

    }

    public static function sendSMS($number,$message)
    {
        
        $username = "priya@exceptionaire.co";
        $hash = "6bfb9c7e2ebbd0a5e493e917400999e9eed36bf2edd8d76844ff0ec69c4ff829";

        // Config variables. Consult http://api.textlocal.in/docs for more info.
        $test = "0";
        $sender = "TXTLCL"; 
        //$message = "BrainWave test message";
        $message = urlencode($message);
        $data = "username=".$username."&hash=".$hash."&message=".$message."&sender=".$sender."&numbers=".$number."&test=".$test;
        $ch = curl_init('http://api.textlocal.in/send/?');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch); // This is the result from the API
        // print_r($result);
        curl_close($ch);
       
    }


    public function adminAllNotification(Request $request)
    {
        $id = '1';
        $role = '1';

        $AllNotificationData = \App\Http\Controllers\AllCommonDataController::AllNotificationData($id);
        
        return view('Backend.notification',['AllNotificationData' => $AllNotificationData]);
    }


    public function viewAdminNotification($NotificatinId,Request $request)
    {
        
        $UpdateNotificationData = \App\Http\Controllers\AllCommonDataController::UpdateNotificationData($NotificatinId);

        $SingleNotificationData = \App\Http\Controllers\AllCommonDataController::SingleNotificationData($NotificatinId);
        
        return view('Backend.view_notification_details',['SingleNotificationData' => $SingleNotificationData]);
    }   
   

}

?>