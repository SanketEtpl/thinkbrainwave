<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route ;



use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\Http\Requests;
// use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
// use Cookie; 

use Socialite;


class UserController extends BaseController
{
    
    public function index($user_id=NULL, $role_id=NULL)
    {
         $userdata = DB::select("SELECT um.* ,CONCAT(IFNULL(tm.name,''), IFNULL(sm.first_name,''), IFNULL(pm.name,'')) AS Fname, 
         CONCAT(IFNULL(tm.email_id,''), IFNULL(sm.email_id,''), IFNULL(pm.email,'')) AS Email,
         CONCAT(IFNULL(tm.address,''), IFNULL(sm.address_line1,''), IFNULL(pm.address,'')) AS Address,
         CONCAT(IFNULL(tm.primary_phone,''), IFNULL(sm.primary_phone,''), IFNULL(pm.phone_number,'')) AS Contact
         from user_master_ut as um 
         left join role_master_ut as rm on um.role_id = rm.id 
         left join tutor_details_ut as tm on um.id = tm.user_id
         left join student_master_ut as sm on um.id = sm.user_id
         left join parent_details_ut as pm on um.id = pm.user_id where um.is_deleted=0
         ORDER BY um.id  DESC");
         
         $userSingle = NULL;

        if($user_id!=NULL && $role_id!=NULL) {
                if($role_id == 2) {

                    $tutor = DB::select('select u.id as user_id, t.name, t.address, t.primary_phone, t.email_id, t.level, u.role_id,u.status from user_master_ut as u LEFT JOIN tutor_details_ut as t ON u.id = t.user_id where u.id = "'.$user_id.'" ');

                    $userSingle['user_id'] = $tutor[0]->user_id;
                    $userSingle['user'] = 2;
                    $userSingle['tutor_level'] = $tutor[0]->level;
                    $userSingle['name'] = $tutor[0]->name;
                    $userSingle['contact'] = $tutor[0]->primary_phone;
                    $userSingle['email'] = $tutor[0]->email_id;
                    $userSingle['address'] = $tutor[0]->address;
                    $userSingle['status'] = $tutor[0]->status;

                } elseif($role_id == 3) {
                   
                    $student = DB::select('select u.id as user_id, t.first_name as name, t.address_line1 as address, t.primary_phone, t.email_id, u.role_id,u.status from user_master_ut as u LEFT JOIN student_master_ut as t ON u.id = t.user_id where u.id = "'.$user_id.'" ');

                    $userSingle['user_id'] = $student[0]->user_id;
                    $userSingle['user'] = 3;
                    $userSingle['tutor_level'] = '';
                    $userSingle['name'] = $student[0]->name;
                    $userSingle['contact'] = $student[0]->primary_phone;
                    $userSingle['email'] = $student[0]->email_id;
                    $userSingle['address'] = $student[0]->address;
                    $userSingle['status'] = $student[0]->status;
                }
                else{
                    $parent = DB::select('select u.id as user_id, t.name as name, t.address as address, t.phone_number as primary_phone , t.email as email_id, u.role_id,u.status from user_master_ut as u LEFT JOIN parent_details_ut as t ON u.id = t.user_id where u.id = "'.$user_id.'" ');
                    
                    $userSingle['user_id'] = $parent[0]->user_id;
                    $userSingle['user'] = 3;
                    $userSingle['tutor_level'] = '';
                    $userSingle['name'] = $parent[0]->name;
                    $userSingle['contact'] = $parent[0]->primary_phone;
                    $userSingle['email'] = $parent[0]->email_id;
                    $userSingle['address'] = $parent[0]->address;
                    $userSingle['status'] = $parent[0]->status;
                }
            }

        return view('Backend.users', ['users' => $userdata, 'singleData' => $userSingle]);
     
    }

	
    
    public function addUser(Request $request)
    {
    	
    	$user_id = $request->input('user_id');
        $user = $request->input('user');
        $tutor_level = $request->input('tutor_level');
        $name = $request->input('name');
        $contact = $request->input('contact');
        $email = $request->input('email');
        $address = $request->input('address');
        $status = $request->input('status');
        $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890');
        $passwordN = substr($random, 0, 10);
        $password = md5($passwordN);
        
        if($user_id == '') {

            if($user=='2')
            {
                $data = array('role_id' => '2','username' => $email, 'password' => $password, 'status' => $status);
                DB::table('user_master_ut')->insert($data);
                $userId = DB::getPdo()->lastInsertId();

                    if($userId > 0) {
                        $tutorData = array('user_id' => $userId,'name' => $name, 'level' => $tutor_level, 'address' => $address, 'primary_phone' => $contact, 'email_id' => $email);
                        DB::table('tutor_details_ut')->insert($tutorData);
                        
                    }
            
            } 
            if($user=='3')
            {
               $data = array('role_id' => '3','username' => $email, 'password' => $password, 'status' => $status);
                DB::table('user_master_ut')->insert($data);
                $userId = DB::getPdo()->lastInsertId();

                    if($userId > 0) {
                        $studentData = array('user_id' => $userId,'first_name' => $name, 'address_line1' => $address, 'primary_phone' => $contact);
                        DB::table('student_master_ut')->insert($studentData);
                    
                    }
            }

        } else {

            if($user == '2') {

                $tutdata = array('level'=>$tutor_level, 'name'=>$name, 'address'=> $address, 'primary_phone'=> $contact,'email_id'=>$email);                    
                DB::table('tutor_details_ut')            
                ->where('user_id', $user_id)            
                ->update($tutdata); 

                $tutUdata = array('status'=>$status);                    
                DB::table('user_master_ut')            
                ->where('id', $user_id)            
                ->update($tutUdata);  

            }

            if($user == '3') {
                $studata = array('first_name'=>$name, 'address_line1'=> $address, 'primary_phone'=> $contact,'email_id'=>$email);                    
                DB::table('student_master_ut')            
                ->where('user_id', $user_id)            
                ->update($studata); 

                $stuUdata = array('status'=>$status);                    
                DB::table('user_master_ut')            
                ->where('id', $user_id)            
                ->update($stuUdata); 
            }

        }

        return redirect('admin/users');
    }

    public function acceptUser($user_id) {

        $update_data = array('status'=>'1');                    
            DB::table('user_master_ut')            
            ->where('id', $user_id)            
            ->update($update_data);        
        return redirect('admin/users');
        
    }

    public function rejectUser($user_id) {

        $update_data = array('status'=>'2');                    
            DB::table('user_master_ut')            
            ->where('id', $user_id)            
            ->update($update_data);        
        return redirect('admin/users');
        
    }

    public function changeStatus($user_id, $status) {

        if($status == 0) {
            $data =array('status' =>1);
        } else {
            $data =array('status' =>0);
        }

            DB::table('user_master_ut')            
            ->where('id', $user_id)            
            ->update($data);        
        return redirect('admin/users');
        
    }

    public function deleteUser($user_id) {

        $data = array('is_deleted'=>'1');                    
            DB::table('user_master_ut')            
            ->where('id', $user_id)            
            ->update($data);        
        return redirect('admin/users');
        
    }

    public function EmailValid(Request $request)
    {
        $email = $request->email;
        $validemail = DB::select('select * from user_master_ut as u where u.username = "'.$email.'" ');
        if(count($validemail)>0)
        {
            return response()->json(['error' => '1', 'msg' => 'Email already registered.']);
        }
        else{
            return response()->json(['error' => '0', 'msg' => '']); 
        }
    }



}

?>