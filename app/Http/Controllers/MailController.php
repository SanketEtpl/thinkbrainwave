<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route ;


use Illuminate\Support\Facades\Input;
use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;
use Mail;


class MailController extends BaseController
{
    
    public function index(Request $request)
    {
       $to_name = 'Pratiksha';
       $to_email = 'yogita.p@exceptionaire.co';
      $data = array('name'=> $to_name, "body" => "Test mail");
          
      Mail::send('Email.mail', $data, function($message) use ($to_name, $to_email) {
          $message->to($to_email, $to_name)
                  ->subject('Think BrainWave Testing Mail');
          $message->from(config('constants.EMAIL_FROM'),'Think BrainWave');
      });
    // $subject = $to_name." profile code";

    // $body = "Your child ".$to_name." profile code : ".$to_name." .Please add this code in your profile.";
    //     $SendEmail = $this->SendEmail($to_name, $to_email, $body, $subject);

        //print_r($SendEmail);
    }
    
    public static function SendEmail($to_name, $to_email, $body, $subject)
    {
        
      $data = array('name'=>$to_name, "body" => $body);
      Mail::send('Email.mail', $data, function($message) use ($to_name, $to_email,$subject) {
            $message->to($to_email, $to_name);
            $message->subject($subject);
            $message->from(config('constants.EMAIL_FROM'),config('constants.EMAIL_NAME'));
      });
    }


    public static function SendResetEmail($to_name, $to_email, $body, $subject)
    {
        
        $data = array('name'=>$to_name, "body" => $body);
        Mail::send('Email.resetmail', $data, function($message) use ($to_name, $to_email,$subject) {
              $message->to($to_email, $to_name);
              $message->subject($subject);
              $message->from(config('constants.EMAIL_FROM'),config('constants.EMAIL_NAME'));
        });

    }

    

}
