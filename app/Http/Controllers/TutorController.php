<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route ;


use Illuminate\Support\Facades\Input;
use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;

use Carbon\Carbon;

class TutorController extends BaseController
{
        public function __construct()
        {
            $this->middleware(function ($request, $next) {
                if($request->session()->has('loginSessionTutor'))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->to('Signin');
                }
            });
        }
   
    public function index(Request $request)
    {
        $loginSessionTutor = $request->session()->get('loginSessionTutor');
        
        $id = $loginSessionTutor['session_tutor_id'];
        $role = $loginSessionTutor['session_tutor_role_id'];

        $ShowProfileData = \App\Http\Controllers\AllProfileDataController::ShowProfileData($id, $role);

        // print_r($result1);
        // die();
        $Tasks = $this->Tasks($id);

        $StudentCancletasks = $this->StudentCancletasks($id);
        
        $TutorCancletasks = $this->TutorCancletasks($id);
        
        $Completedtasks = $this->Completedtasks($id);

        $Booktasks = $this->Booktasks($id);


        $TutorSessionCount = DB::table('sessions_master_ut as sesM')                    
                    ->where('sesM.tutor_id', '=', $id)
                    ->where('sesM.status', '=', '6')
                    ->count();

        $TutorRatingCount = \App\Http\Controllers\AllCommonDataController::TutorRatingCount($id);
    //    print_r($TutorRatingCount);
    //    die();
        
        $TutorSessionData = $this->TutorSessionDataLastweek($id);
        $TutorSessionDataLastYear = $this->TutorSessionDataLastYear($id);

        
        return view('Tutor.tutor_dashboard',['users' => $ShowProfileData, 'Tasks' => $Tasks, 'StudentCancletasks' => $StudentCancletasks, 'TutorCancletasks' => $TutorCancletasks, 'Completedtasks' => $Completedtasks, 'Booktasks' => $Booktasks, 'TutorSessionCount' => $TutorSessionCount, 'TutorSessionData' => $TutorSessionData, 'TutorRatingCount' => $TutorRatingCount]);
        
    }
    public function TutorSessionDataLastweek_new($id)
    {
        $a = "(select curdate()as dy union select DATE_SUB(curdate(), INTERVAL 1 day) as dy union select DATE_SUB(curdate(), INTERVAL 2 day) as dy union select DATE_SUB(curdate(), INTERVAL 3 day) as dy union select DATE_SUB(curdate(), INTERVAL 4 day) as dy union select DATE_SUB(curdate(), INTERVAL 5 day) as dy union select DATE_SUB(curdate(), INTERVAL 6 day) as dy ) as qb ";

        $a2 = "qa.start_date = qb.dy and qa.start_date > DATE_SUB(curdate(), INTERVAL 7 day)";

        $date = \Carbon\Carbon::today()->subDays(7);
        $TutorSessionData = DB::table('sessions_master_ut as qa')   
                    ->rightJoin($a,$a2)
                    ->select(DB::raw('qb.dy as yourday, COALESCE(count(id), 0) as yourcount'))
                    ->groupBy(DB::raw("qb.dy"))
                    ->orderBy('qb.dy', 'ASC')
                    ->get();

        return $TutorSessionData;
    }
    public function TutorSessionDataLastweek($id)
    {

        $date = \Carbon\Carbon::today()->subDays(7);
        $TutorSessionData = DB::table('sessions_master_ut')   
                    ->select(DB::raw('(COUNT(*)) as total_click, start_date,  DATE_FORMAT(start_date, "%a") as day_d'))
                    //->where( 'start_date', '>=', 'DATE_SUB(CURDATE(),INTERVAL 7 DAY)')
                    ->where( 'start_date', '>', Carbon::now()->subDays(7))  
                    ->where('tutor_id', '=', $id)                  
                    ->groupBy(DB::raw("start_date"))
                    ->get();

        return $TutorSessionData;
    }
    public function TutorSessionDataLastMonth($id)
    {
       

        $date = \Carbon\Carbon::today()->subDays(30);
        $TutorSessionData = DB::table('sessions_master_ut')   
                    ->select(DB::raw('(COUNT(*)) as total_click, start_date,  DATE_FORMAT(start_date, "%a") as day_d'))
                    ->where( 'start_date', '>', Carbon::now()->subDays(30))
                    ->where('tutor_id', '=', $id)   
                    ->groupBy(DB::raw("start_date"))
                    ->get();

        return $TutorSessionData;
    }
    public function TutorSessionDataLastYear($id)
    {

        $TutorSessionData = DB::table('sessions_master_ut')   
        ->select(DB::raw('(COUNT(*)) as total_click, GROUP_CONCAT( DISTINCT IFNULL( start_date,"")) as start_date, GROUP_CONCAT( DISTINCT IFNULL( DATE_FORMAT(start_date, "%M") ,"")) as day_d'))
        ->where('tutor_id', '=', $id)   
        ->groupBy(DB::raw("MONTH(start_date)"))
        ->get();

        return $TutorSessionData;
    }
    public function TutorSessionDataLastYear_OLD($id)
    {
       

        $TutorSessionData = DB::table('sessions_master_ut')   
        //->select(DB::raw("(COUNT(*)) as total_click, GROUP_CONCAT( DISTINCT IFNULL( CONCAT(MONTH(start_date),'-',YEAR(start_date)),'')) as session_date"))
        ->select(DB::raw('(COUNT(*)) as total_click, GROUP_CONCAT( DISTINCT IFNULL( DATE_FORMAT(start_date, "%M %Y"),"")) as session_date, GROUP_CONCAT( DISTINCT IFNULL( FLOOR((DayOfMonth(start_date)-1)/7)+1 ,"")) as session_week'))
        // ->groupBy(DB::raw("MONTH(start_date)"))
        // ->groupBy(DB::raw("YEAR(start_date)"))
        ->groupBy(DB::raw("WEEK(start_date)"))
        ->get();
    }
    
    public function TutorMyProfile(Request $request)
    {
        $loginSessionTutor = $request->session()->get('loginSessionTutor');
        
        $id = $loginSessionTutor['session_tutor_id'];
        $role = $loginSessionTutor['session_tutor_role_id'];

        $ShowProfileData = \App\Http\Controllers\AllProfileDataController::ShowProfileData($id, $role);

        $Tasks = $this->Tasks($id);

        $StudentCancletasks = $this->StudentCancletasks($id);
        
        $TutorCancletasks = $this->TutorCancletasks($id);
        
        $Completedtasks = $this->Completedtasks($id);

        $Booktasks = $this->Booktasks($id);

        $TutorRatingCount = \App\Http\Controllers\AllCommonDataController::TutorRatingCount($id);

        return view('Tutor.tutor_my_profile',['users' => $ShowProfileData, 'Tasks' => $Tasks, 'StudentCancletasks' => $StudentCancletasks, 'TutorCancletasks' => $TutorCancletasks, 'Completedtasks' => $Completedtasks, 'Booktasks' => $Booktasks, 'TutorRatingCount' => $TutorRatingCount]);
        
    }

    public function TutorEditProfile(Request $request)
    {
        $loginSessionTutor = $request->session()->get('loginSessionTutor');
        
        $id = $loginSessionTutor['session_tutor_id'];
        $role = $loginSessionTutor['session_tutor_role_id'];

        $ShowProfileData = \App\Http\Controllers\AllProfileDataController::ShowProfileData($id, $role);

        $countries = $this->CountriesAllData();

        $grades = $this->GradeAllData();
        
        $subjects = $this->SubjectAllData();

        $Tasks = $this->Tasks($id);

        $StudentCancletasks = $this->StudentCancletasks($id);
        
        $TutorCancletasks = $this->TutorCancletasks($id);
        
        $Completedtasks = $this->Completedtasks($id);

        $Booktasks = $this->Booktasks($id);

        $TutorRatingCount = \App\Http\Controllers\AllCommonDataController::TutorRatingCount($id);

        
        return view('Tutor.tutor_edit_profile',['users' => $ShowProfileData,'countries' => $countries,'grades' => $grades,'subjects' => $subjects, 'Tasks' => $Tasks, 'StudentCancletasks' => $StudentCancletasks, 'TutorCancletasks' => $TutorCancletasks, 'Completedtasks' => $Completedtasks, 'Booktasks' => $Booktasks, 'TutorRatingCount' => $TutorRatingCount]);
        
    }
    public function TurorProfileSave(Request $request)
    {
        
        $user_id = $request->input('user_id');
        $old_image = $request->input('old_image');
        $grade_id = $request->input('grade_id');
        $grade_id2 = $request->input('grade_id2');
        $name = $request->input('name');
        $dob = $request->input('dob');
        $gender = $request->input('gender');
        $address = $request->input('address');
        $country_id = $request->input('country_id');
        $primary_phone = $request->input('primary_phone');
        $email_id = $request->input('email_id');
        $max_distance_travel = $request->input('max_distance_travel');
        $qualification = $request->input('qualification');
        $year_exp = $request->input('year_exp');
        $tutor_since = $request->input('tutor_since');

        $bank_name = $request->input('bank_name');
        $branch_name = $request->input('branch_name');
        $Ifsc = $request->input('Ifsc');
        $swift_code = $request->input('swift_code');
        $account_no = $request->input('account_no');

        $subject_id = $request->input('subject_id');

        
        

        $profile_file_path = $request->file('profile_file_path');

            //get old session value
                $temp_session= $request->session()->get('loginSessionTutor');
                $temp_session_tutor_role_id = $temp_session['session_tutor_role_id'];
                $temp_session_tutor_id = $temp_session['session_tutor_id'];
                $temp_session_tutor_username = $temp_session['session_tutor_username'];
                $temp_session_tutor_password = $temp_session['session_tutor_password'];
                $temp_session_tutor_name = $temp_session['session_tutor_name'];
                $temp_session_tutor_type = $temp_session['session_tutor_type'];
            
                $temp_session_tutor_role = $temp_session['session_tutor_role'];

                if ($request->hasFile('profile_file_path')){
                    $image = $request->file('profile_file_path');
                    $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                    
                
                    $temp_session_tutor_profile_file_path = 'tutor/'.$input['imagename'];
                    //echo "insert";
                }
                else{
                    $temp_session_tutor_profile_file_path = $temp_session['session_tutor_profile_file_path'];
                    //echo $old_image;
                }
            //end get old session value
            
            $args = array(
                'name' => $name,
                'grade_id' => $grade_id,
                'grade_id2' => $grade_id2,
                'old_image' => $old_image,
                'user_id' => $user_id,
                'dob' => $dob,
                'gender' => $gender,
                'address' => $address,
                'country_id' => $country_id,
                'primary_phone' => $primary_phone,
                'email_id' => $email_id,
                'max_distance_travel' => $max_distance_travel,
                'qualification' => $qualification,
                'year_exp' => $max_distance_travel,
                'tutor_since' => $tutor_since,

                'bank_name' => $bank_name,
                'branch_name' => $branch_name,
                'Ifsc' => $Ifsc,
                'swift_code' => $swift_code,
                'account_no' => $account_no,
            );
    
            
            $request = Request::create('/api/TutorProfileSave', 'POST', $args);
            $instance = json_decode(Route::dispatch($request)->getContent());
            //print_r($instance);

            //set new session value
            $sessionArray = array(
                'session_tutor_role_id' => $temp_session_tutor_role_id,
                'session_tutor_id' => $temp_session_tutor_id,
                'session_tutor_type' => $temp_session_tutor_type,
                'session_tutor_username' => $temp_session_tutor_username,
                'session_tutor_password' => $temp_session_tutor_password,
                'session_tutor_name' => $name,
                'session_tutor_profile_file_path' => $temp_session_tutor_profile_file_path,
                'session_tutor_role' => $temp_session_tutor_role);
            
            $request->session()->put('loginSessionTutor', $sessionArray);
        //end set new session value
        //print_r($subject_id);
        $i=0;
        DB::table('tutor_subject_grade_ut')->where('user_id', '=', $user_id)->delete();
        foreach($subject_id as $val)
        {
            
            $subject_data = explode("-", $subject_id[$i]);
            $data = array('user_id'=> $user_id, 'subject_id' => $subject_data[1], 'grade_id' => $subject_data[0]);
           
            DB::table('tutor_subject_grade_ut')->insert(array($data));
            $i++;
        }

        list($latitude, $longitude)  = \App\Http\Controllers\AllCommonDataController::GetLatLong($address);
        //update lat long   
            $update_data = array('latitude'=> $latitude, 'longitude'=> $longitude);
            
            $update = DB::table('tutor_details_ut')
                    ->where('user_id', $user_id)
                    ->update($update_data);

        //end update lat long
        return redirect()->to('TutorMyProfile');

    }
    public function TutorAccount(Request $request)
    {
        $loginSessionTutor = $request->session()->get('loginSessionTutor');
        
        $session_tutor_id = $loginSessionTutor['session_tutor_id'];                   
        
        $PastSessions = $this->FetchAllSesionDataOrder($session_tutor_id,'=','desc');

        $FutureSessions = $this->FetchAllSesionDataOrder($session_tutor_id,'!=','ASC');

        $SessionsStartFirst = $this->FetchUpcomingSesionFirst($session_tutor_id);

        return view('Tutor.tutor_account',['PastSessions' => $PastSessions,'FutureSessions' => $FutureSessions,'SessionsStartFirst' => $SessionsStartFirst]);
        
    }
    
    public function TutorBoostingPlans(Request $request)
    {
        return view('Tutor.tutor_boosting_plans');
        
    }
    public function TutorNotification(Request $request)
    {
        $loginSessionTutor = $request->session()->get('loginSessionTutor');
        
        $session_tutor_id = $loginSessionTutor['session_tutor_id']; 

        $AllNotificationData = \App\Http\Controllers\AllCommonDataController::AllNotificationData($session_tutor_id);
        
        return view('Tutor.tutor_notification',['AllNotificationData' => $AllNotificationData]);
        
    }
    public function TutorViewNotificationDetails($NotificatinId,Request $request)
    {
        
        
        $UpdateNotificationData = \App\Http\Controllers\AllCommonDataController::UpdateNotificationData($NotificatinId);

        $SingleNotificationData = \App\Http\Controllers\AllCommonDataController::SingleNotificationData($NotificatinId);
        
        
        return view('Tutor.tutor_view_notification_details',['SingleNotificationData' => $SingleNotificationData]);
        

    }
    public function TutorVideoStreaming(Request $request)
    {
        return view('Tutor.tutor_video_streaming');
        
    }

   
    

//start common  data
    
    //get all subject
    public function SubjectAllData()
    {
        $subjects = DB::table('subject_master_ut as sm')
                    ->leftJoin('grade_master_ut as gm', 'sm.grade_id', '=', 'gm.id')
                    ->select('sm.id as subject_id', 'sm.name as subject_name', 'gm.id as grade_id',  'gm.name as grade_name')
                    ->get();

        return $subjects;
    }

    //end get all subject
    //get all grade
    public function GradeAllData()
    {
        $grades = DB::table('grade_master_ut')->get();

        return $grades;
    }

    //end get all grade

    //get all country
    public function CountriesAllData()
    {
        $countries = DB::table('countries_master_ut')->get();

        return $countries;
    }

    //end get all country
    public function FetchUpcomingSesionFirst($tutorId)
    {
        $SessionsStartFirst = DB::table('sessions_master_ut as sesM')
                    ->select('sesM.id as session_start_first_id')
                    ->where('sesM.tutor_id', '=', $tutorId)
                    ->where('sesM.status', '!=', 6)
                    ->orderBy('sesM.id', 'ASC')
                    ->first();
        return $SessionsStartFirst;
    }
    public function FetchAllSesionDataOrder($tutorId, $condition, $oderType)
    {
        $SessionsData = DB::table('sessions_master_ut as sesM')
                    ->leftJoin('student_master_ut as sm', 'sesM.student_id', '=', 'sm.user_id')
                    ->leftJoin('tutor_details_ut as tDet', 'sesM.tutor_id', '=', 'tDet.user_id')
                    ->leftJoin('grade_master_ut as gradeM', 'sesM.grade_id', '=', 'gradeM.id')
                    ->leftJoin('subject_master_ut as subM', 'sesM.subject_id', '=', 'subM.id')
                    ->leftJoin('topic_master_ut  as topM', 'sesM.topic_id', '=', 'topM.id')

                    ->select('gradeM.name as grade_name', 'subM.name as subject_name', 'topM.name as topic_name', 'tDet.user_id as tutor_id',  'tDet.gender as tutor_gender', 'tDet.name as tutor_name', 'tDet.level as tutor_level', 'sm.user_id as student_id',  'sm.gender as student_gender', 'sm.first_name as student_name', 'sesM.*')

                    ->where('sesM.tutor_id', '=', $tutorId)
                    ->where('sesM.status', $condition, 6)
                    ->orderBy('sesM.id', $oderType)
                    ->get();
        return $SessionsData;

    }
    //fetch calender data
    public function Tasks($session_user_id)
    {       
        $tasks = DB::table('sessions_master_ut as sesM')
                    ->leftJoin('grade_master_ut as gm', 'sesM.grade_id', '=', 'gm.id')
                    ->leftJoin('subject_master_ut as subm', 'sesM.subject_id', '=', 'subm.id')
                    ->leftJoin('topic_master_ut as tm', 'sesM.topic_id', '=', 'tm.id')
                    ->leftJoin('student_master_ut as sm', 'sesM.student_id', '=', 'sm.user_id')

                    ->select('sesM.*', 'gm.name as grade_name', 'subm.name as subject_name', 'tm.name as topic_name', 'sm.first_name as student_name', 'sm.gender as student_gender')
                    ->where('sesM.tutor_id', '=', $session_user_id)
                    ->where('sesM.status', '!=', 3)
                    ->Where('sesM.status', '!=', 4)
                    ->Where('sesM.status', '!=', 6)
                    ->Where('sesM.status', '!=', 2)
                ->get();

        return $tasks;
    }
    public function StudentCancletasks($session_user_id)
    {       
        $StudentCancletasks = DB::table('sessions_master_ut as sesM')
                ->leftJoin('grade_master_ut as gm', 'sesM.grade_id', '=', 'gm.id')
                ->leftJoin('subject_master_ut as subm', 'sesM.subject_id', '=', 'subm.id')
                ->leftJoin('topic_master_ut as tm', 'sesM.topic_id', '=', 'tm.id')
                ->leftJoin('student_master_ut as sm', 'sesM.student_id', '=', 'sm.user_id')

                ->select('sesM.*', 'gm.name as grade_name', 'subm.name as subject_name', 'tm.name as topic_name', 'sm.first_name as student_name', 'sm.gender as student_gender')
                ->where('sesM.tutor_id', '=', $session_user_id)
                ->where('sesM.status', '=', 3)
                ->get();

        return $StudentCancletasks;
    }

    public function TutorCancletasks($session_user_id)
    {       
        $TutorCancletasks = DB::table('sessions_master_ut as sesM')
                ->leftJoin('grade_master_ut as gm', 'sesM.grade_id', '=', 'gm.id')
                ->leftJoin('subject_master_ut as subm', 'sesM.subject_id', '=', 'subm.id')
                ->leftJoin('topic_master_ut as tm', 'sesM.topic_id', '=', 'tm.id')
                ->leftJoin('student_master_ut as sm', 'sesM.student_id', '=', 'sm.user_id')

                ->select('sesM.*', 'gm.name as grade_name', 'subm.name as subject_name', 'tm.name as topic_name', 'sm.first_name as student_name', 'sm.gender as student_gender')
                ->where('sesM.tutor_id', '=', $session_user_id)
                ->Where('sesM.status', '=', 4)
                ->get();

        return $TutorCancletasks;
    }

    public function Completedtasks($session_user_id)
    {       
        $Completedtasks = DB::table('sessions_master_ut as sesM')
                    ->leftJoin('grade_master_ut as gm', 'sesM.grade_id', '=', 'gm.id')
                    ->leftJoin('subject_master_ut as subm', 'sesM.subject_id', '=', 'subm.id')
                    ->leftJoin('topic_master_ut as tm', 'sesM.topic_id', '=', 'tm.id')
                    ->leftJoin('student_master_ut as sm', 'sesM.student_id', '=', 'sm.user_id')

                    ->select('sesM.*', 'gm.name as grade_name', 'subm.name as subject_name', 'tm.name as topic_name', 'sm.first_name as student_name', 'sm.gender as student_gender')
                    ->where('sesM.tutor_id', '=', $session_user_id)
                    ->Where('sesM.status', '=', 6)
                    ->get();

        return $Completedtasks;
    }
    public function Booktasks($session_user_id)
    {       
        $Booktasks = DB::table('sessions_master_ut as sesM')
                    ->leftJoin('grade_master_ut as gm', 'sesM.grade_id', '=', 'gm.id')
                    ->leftJoin('subject_master_ut as subm', 'sesM.subject_id', '=', 'subm.id')
                    ->leftJoin('topic_master_ut as tm', 'sesM.topic_id', '=', 'tm.id')
                    ->leftJoin('student_master_ut as sm', 'sesM.student_id', '=', 'sm.user_id')

                    ->select('sesM.*', 'gm.name as grade_name', 'subm.name as subject_name', 'tm.name as topic_name', 'sm.first_name as student_name', 'sm.gender as student_gender')
                    ->where('sesM.tutor_id', '=', $session_user_id)
                    ->Where('sesM.status', '=', 2)
                    ->get();

        return $Booktasks;
    }

    // end fetch calender data

    public function TutorMyPlan(Request $request)
    {
        $loginSessionTutor = $request->session()->get('loginSessionTutor');
        
        $session_tutor_id = $loginSessionTutor['session_tutor_id'];

        $Tasks = $this->Tasks($session_tutor_id);

        $StudentCancletasks = $this->StudentCancletasks($session_tutor_id);
        
        $TutorCancletasks = $this->TutorCancletasks($session_tutor_id);
        
        $Completedtasks = $this->Completedtasks($session_tutor_id);

        $Booktasks = $this->Booktasks($session_tutor_id);

		return view('Tutor.tutor_my_plan',['Tasks' => $Tasks, 'StudentCancletasks' => $StudentCancletasks, 'TutorCancletasks' => $TutorCancletasks, 'Completedtasks' => $Completedtasks, 'Booktasks' => $Booktasks]);
    
        
    }
//end common  data
    public function TutorSessionCancleSave($id ,$reason, Request $request)
    {
        
        $loginSessionTutor = $request->session()->get('loginSessionTutor');
        
        $session_tutor_name = $loginSessionTutor['session_tutor_name'];

        $update_data = array('tutor_cancel_reason'=> $reason,'status'=> '4');
        
            DB::table('sessions_master_ut')
            ->where('id', $id)
            ->update($update_data);


            $SingleSessionData = \App\Http\Controllers\AllCommonDataController::SingleSessionData($id);
            $tutor_id = $SingleSessionData->tutor_id;
            $student_id = $SingleSessionData->student_id;
    
            $notification_type ="SessionTutorCancel";
    
            $session_type_val="";
            if($SingleSessionData->session_type==1)
            {
                $session_type_val="face to face";
            }
            if($SingleSessionData->session_type==2)
            {
                $session_type_val="video to video";
            }
            
            $message = $session_type_val." session no ".$id." cancel by ".$session_tutor_name." . Session date ".$SingleSessionData->start_date.", session time ".$SingleSessionData->start_time;


            if($tutor_id!='')
            {
                $InsertNotificationData_tutor = \App\Http\Controllers\AllCommonDataController::InsertNotificationData($tutor_id, $notification_type, $message);
            }
            if($student_id!='')
            {
                $InsertNotificationData_student = \App\Http\Controllers\AllCommonDataController::InsertNotificationData($student_id, $notification_type, $message);
            }

            $InsertNotificationData_admin = \App\Http\Controllers\AllCommonDataController::InsertNotificationData('1', $notification_type, $message);


        return redirect()->to('TutorMyPlan');
    }
    public static function TutorAddRating(Request $request)
    {
        
        $input = $request->all();
        $id = $request->id;
        $rating = $request->rating;
        $update_data = array('student_rating'=> $rating);
        
        $update = DB::table('sessions_master_ut')
                ->where('id', $id)
                ->update($update_data);

        return $update;

    }
    public function TutorStopSessionSave($id, Request $request)
    {
        $loginSessionTutor = $request->session()->get('loginSessionTutor');
        
        $session_tutor_name = $loginSessionTutor['session_tutor_name'];

        $SingleSessionData = \App\Http\Controllers\AllCommonDataController::SingleSessionData($id);
            $tutor_id = $SingleSessionData->tutor_id;
            $student_id = $SingleSessionData->student_id;


        $update_data = array('status'=> '6');
        
            DB::table('sessions_master_ut')
            ->where('id', $id)
            ->update($update_data);

            $notification_type ="SessionComplete";
    
            $session_type_val="";
            if($SingleSessionData->session_type==1)
            {
                $session_type_val="face to face";
            }
            if($SingleSessionData->session_type==2)
            {
                $session_type_val="video to video";
            }
            
            $message = $session_type_val." session no ".$id." complete by ".$session_tutor_name." on ".$SingleSessionData->start_date." at ".$SingleSessionData->start_time;
            
            if($tutor_id!='')
            {
                $InsertNotificationData_tutor = \App\Http\Controllers\AllCommonDataController::InsertNotificationData($tutor_id, $notification_type, $message);
            }
            if($student_id!='')
            {
                $InsertNotificationData_student = \App\Http\Controllers\AllCommonDataController::InsertNotificationData($student_id, $notification_type, $message);
            }

            $InsertNotificationData_admin = \App\Http\Controllers\AllCommonDataController::InsertNotificationData("1", $notification_type, $message);

        return redirect()->to('TutorAccount');
    }
    public function startSessionUsingOtp(Request $request)
    {

        $loginSessionTutor = $request->session()->get('loginSessionTutor');
        
        $session_tutor_name = $loginSessionTutor['session_tutor_name'];
        
        $input = $request->all();
        $id = $request->session_id;
        $session_otp = $request->session_otp;

        $SingleSessionData = DB::table('sessions_master_ut')
                ->where('id', '=', $id)
                ->where('otp', '=', $session_otp)
                ->first();
        
        if($SingleSessionData)
        {
            $update_data = array('status'=> '5');
        
            DB::table('sessions_master_ut')
            ->where('id', $id)
            ->update($update_data);


            $SingleSessionData = \App\Http\Controllers\AllCommonDataController::SingleSessionData($id);
            $tutor_id = $SingleSessionData->tutor_id;
            $student_id = $SingleSessionData->student_id;
    
            $notification_type ="SessionStart";

            $session_type_val="";
            if($SingleSessionData->session_type==1)
            {
                $session_type_val="face to face";
            }
            if($SingleSessionData->session_type==2)
            {
                $session_type_val="video to video";
            }
            
            $message = $session_type_val." session no ".$id." start by ".$session_tutor_name."  on ".$SingleSessionData->start_date." at ".$SingleSessionData->start_time;

            if($tutor_id!='')
            {
                $InsertNotificationData_tutor = \App\Http\Controllers\AllCommonDataController::InsertNotificationData($tutor_id, $notification_type, $message);
            }
            if($student_id!='')
            {
                $InsertNotificationData_student = \App\Http\Controllers\AllCommonDataController::InsertNotificationData($student_id, $notification_type, $message);
            }

            $InsertNotificationData_admin = \App\Http\Controllers\AllCommonDataController::InsertNotificationData("1", $notification_type, $message);

            return response()->json(['success'=>'success']);
        }
        else
        {
            return response()->json(['error'=>'otp not match']);
        }
        
    }
    public function TutorAvailabilitySave(Request $request)
    {
        $loginSessionTutor = $request->session()->get('loginSessionTutor');
        
        $id = $loginSessionTutor['session_tutor_id'];

        $input = $request->all();
        $add_title = $request->add_title;
        $description = $request->description;
        $repeat = $request->repeat;
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $availability_date = $request->availability_date;
        $availability_from_time = $request->availability_from_time;
        $availability_to_time = $request->availability_to_time;

        $availability_days = json_decode($request->availability_days,true);

        $from_month = $request->from_month;
        $to_month = $request->to_month;

        $availability_month = json_decode($request->availability_month,true);

        if($repeat=='Never')
        {
            $insert_data = array('user_id'=> $id,'date'=> $availability_date,'from_time'=> $availability_from_time,'to_time'=> $availability_to_time,'title'=> $add_title,'description'=> $description);
        
             $InsertVailability = $this->InsertVailability($insert_data);
            return response()->json(['success'=>'success']);

        }
        if($repeat=='EveryDay')
        {
            $dates = [];

            $date_from = strtotime($from_date);  
            
            $date_to = strtotime($to_date);
                
            for ($i=$date_from; $i<=$date_to; $i+=86400) {  
                //$dates[] =  date("Y-m-d", $i);  
                $dates =  date("Y-m-d", $i);  

                $insert_data = array('user_id'=> $id,'date'=> $dates,'from_time'=> $availability_from_time,'to_time'=> $availability_to_time,'title'=> $add_title,'description'=> $description);
        
                $InsertVailability = $this->InsertVailability($insert_data);
            }
            return response()->json(['success'=>'success']);
        }
        if($repeat=='EveryWeek')
        {
            $aviDay = [];
            $aviDate = [];
            $count = count($availability_days);
            for($i=0;$i<$count;$i++)
            {
                //$aviDay[] =  $availability_days[$i];
                $endDate = strtotime($to_date);
           
                for($j = strtotime($availability_days[$i], strtotime($from_date)); $j <= $endDate; $j = strtotime('+1 week', $j))
                {
                    //$aviDate[] =   date('Y-m-d', $j);
                    $DayDate =   date('Y-m-d', $j);

                    $insert_data = array('user_id'=> $id,'date'=> $DayDate,'from_time'=> $availability_from_time,'to_time'=> $availability_to_time,'title'=> $add_title,'description'=> $description);
        
                    $InsertVailability = $this->InsertVailability($insert_data);
                }
            }
            
            return response()->json(['success'=>'success']);
        }
        if($repeat=='EveryMonth')
        {
            $aviDay = [];

            $count = count($availability_month);
            for($i=0;$i<$count;$i++)
            {
                //$aviDay[] =  $availability_month[$i];
                $start    = (new \DateTime($from_month))->modify('first day of this month');
                $end      = (new \DateTime($to_month))->modify('first day of next month');
                $interval = \DateInterval::createFromDateString('1 month');
                $period   = new \DatePeriod($start, $interval, $end);

                foreach ($period as $dt) {
                    $year = $dt->format("Y");
                    $month = $dt->format("m");
                    $date = $availability_month[$i];
                    //$aviDay[] = $year."-".$month."-".$date;
                    $aviDate = $year."-".$month."-".$date;

                    $insert_data = array('user_id'=> $id,'date'=> $aviDate,'from_time'=> $availability_from_time,'to_time'=> $availability_to_time,'title'=> $add_title,'description'=> $description);
        
                    $InsertVailability = $this->InsertVailability($insert_data);
                }
                
            }         
            return response()->json(['success'=>'success']);
        }
        
    }
    
    public function InsertVailability($insert_data)
    {
        $insert = DB::table('tutor_availablity_ut')->insert(array($insert_data));
        return $insert;
    }
}
