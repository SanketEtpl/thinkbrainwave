<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route ;



use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\Http\Requests;
// use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
// use Cookie; 

use Socialite;


class PriceController extends BaseController
{

	public function index($id=NULL)
    {
        $select = 'select';        
        DB::select('CALL `sp_pricing`("'.$select.'",1,1,1,1, @result,1)');        
        $data = DB::select('SELECT @result AS `result`;');        
        $pricedata = json_decode($data[0]->result);

        if(isset($pricedata->status))
         {
            $pricedata = array();
         }
         else
         {
             $pricedata = $pricedata;
         }


            $singlePrice = NULL;
            if($id!=NULL) {

                $course =  DB::table('pricing_master_ut')
                            ->where('id', '=', $id)
                            ->get();
                $singlePrice['price_id'] = $course[0]->id;
                $singlePrice['session_type'] = $course[0]->session_type;
                $singlePrice['duration'] = $course[0]->duration;
                $singlePrice['price'] = $course[0]->price;
                $singlePrice['status'] = $course[0]->status;
                
            }
        return view('Backend.price', ['price' => $pricedata, 'singlePrice' => $singlePrice]);
    }
    
    public function addPrice(Request $request)
    {
    	
    	$price_id = $request->input('price_id');
        $session_type = $request->input('session_type');
        $duration = $request->input('duration');
        $price = $request->input('price');
        $status = $request->input('status');
 
        if($price_id == '') {

            $data = array('session_type' => $session_type,'duration' => $duration, 'price' => $price, 'status' => $status);
            DB::table('pricing_master_ut')->insert($data);
                
        } else {

            $data = array('session_type' => $session_type,'duration' => $duration, 'price' => $price, 'status' => $status);              
            DB::table('pricing_master_ut')            
            ->where('id', $price_id)            
            ->update($data); 

        }

        return redirect('admin/price');
    }

    public function changeStatus($id, $status) {

        if($status == 0) {
            $data =array('status' =>1);
        } else {
            $data =array('status' =>0);
        }

            DB::table('pricing_master_ut')            
            ->where('id', $id)            
            ->update($data);        
        return redirect('admin/price');
        
    }

    public function deletePrice($id) {

        $data = array('is_deleted'=>'1');                    
            DB::table('pricing_master_ut')            
            ->where('id', $id)            
            ->update($data);        
        return redirect('admin/price');
        
    }




}

?>