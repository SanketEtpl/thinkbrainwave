<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\registration;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Http\Resources\Json\Resource;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route ;


use Illuminate\Support\Facades\Input;
use Laravel\Socialite\Contracts\Provider;
use App\User;
use App\Http\Requests;
use Session;
use App\Http\Controllers\Controller;

//stripe
use Stripe\Error\Card;
use Cartalyst\Stripe\Stripe;

class ParentController extends BaseController
{

    public function __construct()
        {
            $this->middleware(function ($request, $next) {
                if($request->session()->has('loginSessionParent'))
                {
                     return $next($request);
                }
                else
                {
                    return redirect()->to('Signin');
                }
            });
        }

    public function index(Request $request, $childId = NULL)
    {
        $loginSessionParent = $request->session()->get('loginSessionParent');
        
        $id = $loginSessionParent['sessId'];
        $role = $loginSessionParent['sessRoleId'];

        $request1 = Request::create('/api/ShowProfile/'.$id.'/'.$role, 'GET');
        $response1 = Route::dispatch($request1)->getContent();
        $instance1 = json_decode($response1,true);
        $jsonNew1 = $instance1[0]['result'];
        $resultDash = json_decode($jsonNew1,true);

        if($resultDash['profile_file_path'] == '') {

            $resultDash['profile_file_path'] = 'public/images/no-image.png';
        } else {
            $resultDash['profile_file_path'] = 'public/images/'.$resultDash['profile_file_path'];
        }

        $child = DB::table('parent_chlid_relation_ut as p')
                ->join('student_master_ut as s', 'p.student_id', '=','s.user_id')
                ->join('user_master_ut as u', 's.user_id', '=','u.id')
                ->select('p.student_id', 's.first_name','u.role_id')
                ->where('p.parent_id', '=', $id)
                ->where('p.is_deleted', '=', '0')
                ->get();

        if(count($child) > 0) {

            if($childId == NULL) {
                $childId = $child[0]->student_id;
                $childName = $child[0]->first_name;
            } else {
                $childId = $childId;
    
                $getName = DB::Table('student_master_ut')->select('first_name')->where('user_id',$childId)->get();   
                $childName = $getName[0]->first_name;
            }
            
            $tasks = \App\Http\Controllers\StudentController::StudentPlanTask($childId);
            
            $StudentCancletasks = \App\Http\Controllers\StudentController::StudentCancletasks($childId);
            
            $TutorCancletasks = \App\Http\Controllers\StudentController::TutorCancletasks($childId);
            
            $Completedtasks = \App\Http\Controllers\StudentController::Completedtasks($childId);
    
            $Booktasks = \App\Http\Controllers\StudentController::Booktasks($childId);

        } else {
            $childId = '0';
            $childName = '';
            $tasks = array();
            $StudentCancletasks = array();
            $TutorCancletasks = array();
            $Completedtasks = array();
            $Booktasks = array();
        }

        return view('Parent.dashboard', ['profDash' => $resultDash, 'child' => $child, 'childId' => $childId,'childName' => $childName, 'tasks' => $tasks,'StudentCancletasks' => $StudentCancletasks, 'TutorCancletasks' => $TutorCancletasks, 'Completedtasks' => $Completedtasks, 'Booktasks' => $Booktasks]);
        
    }

    public function parentMyProfile(Request $request)
    {

        $loginSessionParent = $request->session()->get('loginSessionParent');
        
        $id = $loginSessionParent['sessId'];
        $role = $loginSessionParent['sessRoleId'];

        $request1 = Request::create('/api/ShowProfile/'.$id.'/'.$role, 'GET');
        $response1 = Route::dispatch($request1)->getContent();
        $instance1 = json_decode($response1,true);
        $jsonNew1 = $instance1[0]['result'];
        $resultParent= json_decode($jsonNew1,true);

        if($resultParent['profile_file_path'] == '') {

            $resultParent['profile_file_path'] = 'public/images/no-image.png';
        } else {
            $resultParent['profile_file_path'] = 'public/images/'.$resultParent['profile_file_path'];
        }

        return view('Parent.my_profile',['parentData' => $resultParent]);
        
    }

    public function parentEditProfile(Request $request)
    {
        $loginSessionParent = $request->session()->get('loginSessionParent');
        
        $id = $loginSessionParent['sessId'];
        $role = $loginSessionParent['sessRoleId'];

        $request1 = Request::create('/api/ShowProfile/'.$id.'/'.$role, 'GET');
        $response1 = Route::dispatch($request1)->getContent();
        $instance1 = json_decode($response1,true);
        $jsonNew1 = $instance1[0]['result'];
        $resultEdit = json_decode($jsonNew1,true);
        
        if($resultEdit['profile_file_path'] == '') {

            $resultEdit['profile_file_path'] = 'public/images/no-image.png';
        } else {
            $resultEdit['profile_file_path'] = 'public/images/'.$resultEdit['profile_file_path'];
        }

        $countries = DB::table('countries_master_ut')->get();
        // print_r($countries);
        return view('Parent.edit_profile',['editData' => $resultEdit,'countries' => $countries]);
        
    }
    public function parentProfileSave(Request $request)
    {
        
        $user_id = $request->input('user_id');
        $name = $request->input('name');
        $dob = $request->input('dob');
        $gender = $request->input('gender');
        $address = $request->input('address');
        $country_id = $request->input('country_id');
        $primary_phone = $request->input('primary_phone');
        $email = $request->input('email');
        $old_image = $request->input('old_image');
        $profile = $request->input('profile');
        
        $updateImage = '';
        if($request->hasfile('profile')) 
        { 
          $file = $request->file('profile');
          $extension = $file->getClientOriginalExtension(); // getting image extension
          $filename  = time().'.'.$extension;
          $sessionImage = 'public/images/parent/'.$filename;

        } else {
            if($old_image != '') {
                $filename  = $old_image;
                $sessionImage = $old_image;
            } else {
                $filename = '';
                $sessionImage = '';
            }
        }
        
        $data = array('name' => $name,'dob' => $dob,'gender' => $gender,'address' => $address,'country_id' => $country_id,'primary_phone' => $primary_phone,'email' => $email,'profile' => $filename );

        // print_r($data); die();

        $request = Request::create('/api/ParentProfileSave', 'POST', $data);
        $response1 = Route::dispatch($request)->getContent();
        $instance1 = json_decode($response1,true);

            $sessionData = $request->session()->get('loginSessionParent');
            //set new session value
            $sessionArray = array(
                'sessRoleId' => $sessionData['sessRoleId'],
                'sessId' => $sessionData['sessId'],
                'sessUsername' => $sessionData['sessUsername'],
                'sessPassword' => $sessionData['sessPassword'],
                'sessName' => $name,
                'sessImage' => $sessionImage,
                'sessRole' => $sessionData['sessRole']
            );

            $request->session()->put('loginSessionParent', $sessionArray);
            // $ses = $request->session()->get('loginSessionParent');
            // print_r($ses);
            
        return redirect()->to('ParentMyProfile');

    }

    public function parentStudentProfile(Request $request, $childId = NULL)
    {

        $loginSessionParent = $request->session()->get('loginSessionParent');
        
        $id = $loginSessionParent['sessId'];
        $role = $loginSessionParent['sessRoleId'];

        $request1 = Request::create('/api/ShowProfile/'.$id.'/'.$role, 'GET');
        $response1 = Route::dispatch($request1)->getContent();
        $instance1 = json_decode($response1,true);
        $jsonNew1 = $instance1[0]['result'];
        $parentPro = json_decode($jsonNew1,true);
        
        if($parentPro['profile_file_path'] == '') {

            $parentPro['profile_file_path'] = 'no-image.png';
        } else {
            $parentPro['profile_file_path'] = $parentPro['profile_file_path'];
        }

        
        $child = DB::table('parent_chlid_relation_ut as p')
                ->join('student_master_ut as s', 'p.student_id', '=','s.user_id')
                ->join('user_master_ut as u', 's.user_id', '=','u.id')
                ->select('p.student_id', 's.first_name', 'u.role_id')
                ->where('p.parent_id', '=', $id)
                ->where('p.is_deleted', '=', '0')
                ->get();

        if(count($child) > 0) {
             
            $childRoleId = $child[0]->role_id; 

            if($childId == NULL) {
                $childId = $child[0]->student_id;
            } else {
                $childId = $childId;
            }
            
            $request1 = Request::create('/api/ShowProfile/'.$childId.'/'.$childRoleId, 'GET');
            $response1 = Route::dispatch($request1)->getContent();
            $instance1 = json_decode($response1,true);
            $jsonNew1 = $instance1[0]['result'];
            $result1 = json_decode($jsonNew1,true);

            if($result1['profile_file_path'] == '') {

                $result1['profile_file_path'] = 'no-image.png';
            } else {
                $result1['profile_file_path'] = $result1['profile_file_path'];
            }

            $tasks = \App\Http\Controllers\StudentController::StudentPlanTask($childId);
            
            $StudentCancletasks = \App\Http\Controllers\StudentController::StudentCancletasks($childId);
            
            $TutorCancletasks = \App\Http\Controllers\StudentController::TutorCancletasks($childId);
            
            $Completedtasks = \App\Http\Controllers\StudentController::Completedtasks($childId);

            $Booktasks = \App\Http\Controllers\StudentController::Booktasks($childId);

        } else {
            $childRoleId = '';
            $childId = '0';
            $result1 = array();
            $tasks = array();
            $StudentCancletasks = array();
            $TutorCancletasks = array();
            $Completedtasks = array();
            $Booktasks = array();
        }

        return view('Parent.student_profile',['parent' => $parentPro,'child' => $child, 'childData' => $result1, 'childRoleId' => $childRoleId, 'tasks' => $tasks,'StudentCancletasks' => $StudentCancletasks, 'TutorCancletasks' => $TutorCancletasks, 'Completedtasks' => $Completedtasks, 'Booktasks' => $Booktasks]);
        
    }

    public function parStudEditProfile(Request $request, $stuid, $roleId)
    {
        $loginSessionParent = $request->session()->get('loginSessionParent');
        
        $request1 = Request::create('/api/ShowProfile/'.$stuid.'/'.$roleId, 'GET');
        $response1 = Route::dispatch($request1)->getContent();
        $instance1 = json_decode($response1,true);
        $jsonNew1 = $instance1[0]['result'];
        $result1 = json_decode($jsonNew1,true);

        if($result1['profile_file_path'] == '') {
            $result1['profile_file_path'] = 'no-image.png';
        } else {
            $result1['profile_file_path'] = $result1['profile_file_path'];
        }
       
        $studentRating = \App\Http\Controllers\AllCommonDataController::StudentRatingCount($stuid);
        $result1['rating'] =  $studentRating;   
        
        $countries = DB::table('countries_master_ut')->get();
        $grade = DB::table('grade_master_ut')
                 ->where('status', '=', '0')
                 ->where('is_deleted','=','0')
                 ->get();
        
        return view('Parent.student_edit_profile',['studEdit' => $result1,'countries' => $countries,'grades' => $grade]);
    }

    public function parStudProfileSave(Request $request)
    {
        // print_r($request->input());
        // die();
        $user_id = $request->input('user_id');
        $name = $request->input('name');
        $grade = $request->input('grade');
        $dob = $request->input('dob');
        $gender = $request->input('gender');
        $tutor_type = $request->input('tutor_type');
        $address_line1 = $request->input('address_line1');
        $country_id = $request->input('country_id');
        $max_distance_travel_kms = $request->input('max_distance_travel_kms');
        $primary_phone = $request->input('primary_phone');
        $email = $request->input('email');

        $user_profile = $request->file('profile_file_path');

        if ($request->hasFile('profile_file_path')){
            $image = $request->file('profile_file_path');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
        } 
       
        $args = array(
            'first_name' => $name,
            'dob' => $dob,
            'gender' => $gender,
            'tutor_type' => $tutor_type,
            'address_line1' => $address_line1,
            'country_id' => $country_id,
            'max_distance_travel_kms' => $max_distance_travel_kms,
            'email' => $email,
            'primary_phone' => $primary_phone
        );

        $request = Request::create('/api/StudentProfileSave', 'POST', $args);
        $instance = json_decode(Route::dispatch($request)->getContent());
        //print_r($instance[0]->result);

        if($grade!='')
        {
            $today = date('Y-m-d');
            $data = array('student_id'=> $user_id, 'grade_id' => $grade, 'form_date' => $today, 'to_date' => $today);

            $update_data = array('status'=> '0');
        
            DB::table('student_grade_ut')
            ->where('student_id', $user_id)
            ->update($update_data);

            DB::table('student_grade_ut')->insert(array($data));
        }

        return redirect()->to('ParentStudentProfile');

    }
    
    public function saveNewPass(Request $request)
    {
        $user_id = $request->user_id;
        $oldPass = MD5($request->old_password);
        $newPass = $request->new_password;
        $confirmPass = $request->retype_password;
        
        $getUser = DB::Table('user_master_ut')->select('password')->where('id',$user_id)->get();   
        $userpass = $getUser[0]->password;
        if($oldPass == $userpass) {

            $updateData = MD5($newPass);
            $result = "Update user_master_ut SET password = '".$updateData."' where id= '".$user_id."' ";
            $result2 = DB::statement(DB::raw($result));
            $getRole = DB::table('user_master_ut')
                        ->where('id', '=', $user_id)
                        ->get();
            $role = $getRole[0]->role_id;
            
            return response()->json(['status' => '1', 'msg' => 'update successfully.', 'role' => $role]);
        } else {
            return response()->json(['status' => '0', 'msg' => 'Wrong old Password.']);
        }
    } 
 
    public function parentMyPlan(Request $request, $childId = NULL)
    {
        $loginSessionParent = $request->session()->get('loginSessionParent');
        $id = $loginSessionParent['sessId'];
        
        $child = DB::table('parent_chlid_relation_ut as p')
                ->join('student_master_ut as s', 'p.student_id', '=','s.user_id')
                ->join('user_master_ut as u', 's.user_id', '=','u.id')
                ->select('p.student_id', 's.first_name','u.role_id')
                ->where('p.parent_id', '=', $id)
                ->where('p.is_deleted', '=', '0')
                ->get();

        if($childId == NULL) {
            $currentchildId = NULL;
            $childName = '';
            $tasks = array();
            $StudentCancletasks = array();
            $TutorCancletasks = array();
            $Completedtasks = array();
            $Booktasks = array();
        } else {
            $currentchildId = $childId;
            $getName = DB::Table('student_master_ut')->select('first_name')->where('user_id',$childId)->get();   
            $childName = $getName[0]->first_name. ' Plan';

            $tasks = \App\Http\Controllers\StudentController::StudentPlanTask($childId);
        
            $StudentCancletasks = \App\Http\Controllers\StudentController::StudentCancletasks($childId);
            
            $TutorCancletasks = \App\Http\Controllers\StudentController::TutorCancletasks($childId);
            
            $Completedtasks = \App\Http\Controllers\StudentController::Completedtasks($childId);

            $Booktasks = \App\Http\Controllers\StudentController::Booktasks($childId);
        }
        
        return view('Parent.my_plan', ['childs' => $child, 'childid' => $currentchildId, 'childName' => $childName, 'tasks' => $tasks,'StudentCancletasks' => $StudentCancletasks, 'TutorCancletasks' => $TutorCancletasks, 'Completedtasks' => $Completedtasks, 'Booktasks' => $Booktasks]);
    }     
    

    public function parentMyAccount(Request $request, $childId = NULL)
    {
        $loginSessionParent = $request->session()->get('loginSessionParent');
        $id = $loginSessionParent['sessId'];
        
        $child = DB::table('parent_chlid_relation_ut as p')
                ->join('student_master_ut as s', 'p.student_id', '=','s.user_id')
                ->join('user_master_ut as u', 's.user_id', '=','u.id')
                ->select('p.student_id', 's.first_name','u.role_id')
                ->where('p.parent_id', '=', $id)
                ->where('p.is_deleted', '=', '0')
                ->get();

        if(count($child) > 0) {
            if($childId == NULL) {

                $childId = $child[0]->student_id;
                $childName = $child[0]->first_name;
            } else {
                $childId = $childId;
                $getName = DB::Table('student_master_ut')->select('first_name')->where('user_id',$childId)->get();   
                $childName = $getName[0]->first_name. ' SESSIONS';
            }

            $PastSessions = \App\Http\Controllers\StudentController::FetchAllSesionDataOrder($childId,'=','desc');

            $FutureSessions = \App\Http\Controllers\StudentController::FetchAllSesionDataOrder($childId,'=','ASC');

            $SessionsStartFirst = \App\Http\Controllers\StudentController::FetchUpcomingSesionFirst($childId);

            $grades = \App\Http\Controllers\StudentController::GradeAllData(); 

        } else {
            $childId = '0';
            $childName = '';
            $PastSessions = array();
            $FutureSessions = array();
            $SessionsStartFirst = array();
            $grades = array();
        }

        return view('Parent.my_account',['child' => $child, 'childId' => $childId, 'childName' => $childName, 'PastSessions' => $PastSessions,'FutureSessions' => $FutureSessions,'grades' => $grades,'SessionsStartFirst' => $SessionsStartFirst]);

    }      


    public function addChild(Request $request)
    {

        $childCode = $request->childcode;
        $parent_id = $request->user_id;

        $checkCodeExists = DB::table('student_master_ut')
                            ->where('child_code', '=', $childCode)
                            ->get();
            
        if(count($checkCodeExists) > 0) {

            $student_id = $checkCodeExists[0]->user_id;

            $values = array('parent_id' => $parent_id,'student_id' => $student_id);
            DB::table('parent_chlid_relation_ut')->insert($values);
            
            return response()->json(['status' => '1', 'msg' => 'Child add successfully.', 'role' => '0']);
        } else {
            return response()->json(['status' => '0', 'msg' => 'Wrong child code.']);
        }
    }    


    public function parentBookSession(Request $request, $id)
    {
        $childdata = DB::table('student_master_ut')
                            ->where('user_id', '=', $id)
                            ->get();
        $student_id = $childdata[0]->user_id;
        $studentRating = \App\Http\Controllers\AllCommonDataController::StudentRatingCount($student_id);
        $childdata[0]->rating =  $studentRating;  

        $grades = \App\Http\Controllers\StudentController::GradeAllData(); 
                                 
        return view('Parent.book_session',['child' => $childdata[0], 'grades' => $grades]);

    } 

    public function bookSessionSave(Request $request)
    {

        $loginSessionParent = $request->session()->get('loginSessionParent');
        
        $session_user_id = $loginSessionParent['sessId'];
        $session_user_name = $loginSessionParent['sessName'];
       
        $input = $request->all();

        $studentId = $request->studentId;
        $grade_id = $request->grade_id;
        $subject_id = $request->subject_id;
        $topic_id = $request->topic_id;

        $start_date = date('Y-m-d', strtotime($request->start_date));
        $start_time = $request->start_time;
        
        $timestamp = strtotime($start_time) + 60*60;

        $end_time = date('H:i', $timestamp);


        $combinedDT = date('Y-m-d H:i', strtotime("$start_date $start_time"));

        $date = new \DateTime($combinedDT);
        $date->add(new \DateInterval('PT1H'));
        $end_date = $date->format('Y-m-d');

        $tutor_type = $request->tutor_type;
        $visit_type = $request->visit_type;
        $session_type = $request->session_type;
        $venue = $request->venue;
        // $rand = Str::random(60);
        
        $otp = mt_rand(100000, 999999);

        $tutor_id = $request->tutor_id;

        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $distance = $request->max_distance_travel_kms;

            if($session_type=="1")
            {
                $sessionName = "F2F";
                if($latitude!="")
                {
                    $lat = $latitude;
                    $long = $longitude;
                }
                elseif($tutor_id!='')
                {
                    $lat = $latitude;
                    $long = $longitude;
                }
                elseif($tutor_id=='')
                {
                    list($lat, $long)  = \App\Http\Controllers\AllCommonDataController::GetLatLong($venue);

                    if($lat=="REQUEST_DENIED")
                    {
                        $error = "REQUEST_DENIED";
                        return response()->json(['error'=>$error]);
                    }
                }
            }
            else
            {
                $sessionName = "V2V";
                $lat = $latitude;
                $long = $longitude;
            }

        $SessionAssignToTutorAlgo = \App\Http\Controllers\AllCommonDataController::SessionAssignToTutorAlgoFinal($studentId, $grade_id, $subject_id, $topic_id, $start_date, $start_time, $end_time, $tutor_type, $visit_type, $session_type, $lat, $long, $distance);
        if($SessionAssignToTutorAlgo!="TUTOR_NOT_FOUND")
        {
            $tutor_id_add = $SessionAssignToTutorAlgo['tutor_id'];
            // $tutor_id_add = '1';

        }
        elseif($tutor_id!='')
        {
            $tutor_id_add = $tutor_id;
        }
        else
        {
            $tutor_id_add = '';
        }

            $data = array('student_id'=> $studentId, 'parent_id'=> $session_user_id, 'grade_id'=> $grade_id, 'subject_id' => $subject_id, 'topic_id' => $topic_id, 'start_date' => $start_date, 'start_time' => $start_time, 'end_date' => $end_date, 'end_time' => $end_time, 'tutor_type' => $tutor_type, 'visit_type' => $visit_type, 'session_type' => $session_type, 'venue' => $venue, 'tutor_id' => $tutor_id_add, 'status' => '2', 'otp' => $otp, 'latitude' => $lat, 'longitude' => $long);

        DB::table('sessions_master_ut')->insert(array($data));

        $id = DB::getPdo()->lastInsertId();

        $childData =  DB::table('student_master_ut')
                    ->where('user_id', '=', $studentId)
                    ->get();
        $childName = $childData[0]->first_name;

        if($id)
        {
            $notification_type ="SessionBook";
            $message = "Session book sccessfully. OTP for your session id ".$id." is ".$otp;
            $InsertNotificationData_student = \App\Http\Controllers\AllCommonDataController::InsertNotificationData($session_user_id, $notification_type, $message);

            $adminId = '1';
            $messageA = " ".$sessionName." session book by parent ".$session_user_name." for child ".$childName." on ".$start_date." at ".$start_time."  ";
            $InsertNotificationData_admin = \App\Http\Controllers\AllCommonDataController::InsertNotificationData($adminId, $notification_type, $messageA);

            $SessionData = DB::table('sessions_master_ut')
                        ->where('id', '=', $id)
                        ->first();
        
            return response()->json(['success'=>$SessionAssignToTutorAlgo]);
        }
        else
        {
            $error = "REQUEST_DENIED";
            return response()->json(['error'=>$error]);
        }
        
        
      // return response()->json(['error'=>$SessionAssignToTutorAlgo['tutor_id']]);

    }

    public function bookSessionConfirm(Request $request)
    {

        $error = "";

        $loginSessionParent = $request->session()->get('loginSessionParent');
        
        $session_user_id = $loginSessionParent['sessId'];

        $input = $request->all();

        $grade_id = $request->grade_id;
        $subject_id = $request->subject_id;
        $topic_id = $request->topic_id;
        $start_date = date('Y-m-d', strtotime($request->start_date));
        $start_time = $request->start_time;
        
        $timestamp = strtotime($start_time) + 60*60;

        $end_time = date('H:i', $timestamp);


        $combinedDT = date('Y-m-d H:i', strtotime("$start_date $start_time"));

        $date = new \DateTime($combinedDT);
        $date->add(new \DateInterval('PT1H'));
        $end_date = $date->format('Y-m-d');

        $tutor_type = $request->tutor_type;
        $visit_type = $request->visit_type;
        $session_type = $request->session_type;
        $venue = $request->venue;
        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $distance = $request->max_distance_travel_kms;

        if($session_type=="1")
        {
            if($latitude!="")
            {
                $lat = $latitude;
                $long = $longitude;
            }
            {
                list($lat, $long)  = \App\Http\Controllers\AllCommonDataController::GetLatLong($venue);

               
                if($lat=="REQUEST_DENIED")
                {
                    $error = "REQUEST_DENIED";
                    return response()->json(['error'=>$error]);
                }
                
            }
        }
        else
        {
            $lat = $latitude;
            $long = $longitude;
        }
        //$SessionAssignToTutorAlgo='';
        $SessionAssignToTutorAlgo = \App\Http\Controllers\AllCommonDataController::SessionAssignToTutorAlgoFinal($session_user_id, $grade_id, $subject_id, $topic_id, $start_date, $start_time, $end_time, $tutor_type, $visit_type, $session_type, $lat, $long, $distance);
        
        
        $topic_name= DB::table('topic_master_ut')
                        ->where('id', '=', $topic_id)
                        ->select('topic_master_ut.name as topic_name')
                        ->first();
        $grade_name= DB::table('grade_master_ut')
                        ->where('id', '=', $grade_id)
                        ->select('grade_master_ut.name as grade_name')
                        ->first();
        $subject_name= DB::table('subject_master_ut')
                        ->where('id', '=', $subject_id)
                        ->select('subject_master_ut.name as subject_name')
                        ->first();
                        
        if($SessionAssignToTutorAlgo!="TUTOR_NOT_FOUND")
        {
            //$tutor_id = $SessionAssignToTutorAlgo->tutor_id;
            $successArray = array('topic_name'=>$topic_name->topic_name,
                                'start_date'=>$start_date,
                                'end_date'=>$end_date,
                                'start_time'=>$start_time,
                                'end_time'=>$end_time,
                                'grade_name'=>$grade_name->grade_name,
                                'subject_name'=>$subject_name->subject_name);

            return response()->json(['success'=>$SessionAssignToTutorAlgo, 'successArray'=>$successArray]);
        }
        else
        {
            //$error = "TUTOR_NOT_FOUND";
            return response()->json(['error'=>$SessionAssignToTutorAlgo]);
        }

    }

    public function fetchSubjectForStudent(Request $request)
    {
        $input = $request->all();
        $grade_id = $request->grade_id;

        $gradeData = DB::table("subject_master_ut")->where("grade_id",$grade_id)->pluck("name","id");
        return response()->json($gradeData);
    }


    public function fetchTopicForStudent(Request $request)
    {
        $input = $request->all();
        $grade_id = $request->grade_id;
        $subject_id = $request->subject_id;

        $gradeData = DB::table("topic_master_ut")
                            ->where("grade_id",$grade_id)
                            ->where("subject_id",$subject_id)
                            ->pluck("name","id");

        return response()->json($gradeData);
    }


    public function studentSessionCancleSave($id ,$reason,Request $request)
    {
        
        $update_data = array('student_cancel_reason'=> $reason,'status'=> '3');
        
            DB::table('sessions_master_ut')
            ->where('id', $id)
            ->update($update_data);

        $SingleSessionData = \App\Http\Controllers\AllCommonDataController::SingleSessionData($id);
        $tutor_id = $SingleSessionData->tutor_id;
        $student_id = $SingleSessionData->student_id;

        $loginSessionParent = $request->session()->get('loginSessionParent');
        
        $parentId = $loginSessionParent['sessId'];
        $parentName = $loginSessionParent['sessName'];

        $notification_type ="SessionParentCancel";

        if($tutor_id!='')
            {
                $message = "Session No : ".$id." cancel by parent ".$parentName." ";
                $InsertNotificationData_tutor = \App\Http\Controllers\AllCommonDataController::InsertNotificationData($tutor_id, $notification_type, $message);

                $adminId = '1';
                $InsertNotificationData_admin = \App\Http\Controllers\AllCommonDataController::InsertNotificationData($adminId, $notification_type, $message);
            }
            if($parentId!='')
            {
                $message = "Session No : ".$id." cancel by you";
                $InsertNotificationData_student = \App\Http\Controllers\AllCommonDataController::InsertNotificationData($parentId, $notification_type, $message);
            }
        
        return redirect()->to('ParentMyPlan');
    }   
    
    public function stripePayment() {

        return view('Parent.stripeForm');
    }

    public function postPaymentWithStripe(Request $request)
    {
        $input = $request->all();
        
            $input = array_except($input,array('_token'));            
            $stripe = Stripe::make('sk_test_JfwbRwhqFNpL4S2IJtt8S3pc');
            
            try {
                $token = $stripe->tokens()->create([
                    'card' => [
                        'number'    => $request->input('card_number'),
                        'exp_month' => $request->input('ccExpiryMonth'),
                        'exp_year'  => $request->input('ccExpiryYear'),
                        'cvc'       => $request->input('cvvNumber'),
                    ],
                ]); 
                            
                if (!isset($token['id'])) {
                    echo "The Stripe Token was not generated correctly";
                    //\Session::put('error','The Stripe Token was not generated correctly');
                   // return redirect()->to('home');
                }
                $charge = $stripe->charges()->create([
                    'card' => $token['id'],
                    'currency' => 'USD',
                    'amount'   => $request->input('amount'),
                    'description' => 'Add in wallet',
                ]);
                
                if($charge['status'] == 'succeeded') {

                    $data = array('user_id' => $request->input('user_id'), 'name' => $request->input('name'), 'payment_token' => $token['card']['id'], 'amount' => $request->input('amount'), 'description' => 'Add in wallet' );
                    DB::table('payment_ut')->insert($data);
                    
                    \Session::put('success','Money add successfully in wallet');
                    return redirect()->to('home');
                } else {
                    echo "Money not add in wallet!!";
                    //\Session::put('error','Money not add in wallet!!');
                    // return redirect()->to('home');
                }
            } catch (Exception $e) {
                echo $e->getMessage();
                //\Session::put('error',$e->getMessage());
                // return redirect()->to('home');
            } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
                echo $e->getMessage();
                // \Session::put('error',$e->getMessage());
                // return redirect()->to('home');
            } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
                echo $e->getMessage();
                // \Session::put('error',$e->getMessage());
                // return redirect()->to('home');
            }
    } 


    public function allNotification(Request $request)
    {
        $loginSessionParent = $request->session()->get('loginSessionParent');
        
        $id = $loginSessionParent['sessId'];
        $role = $loginSessionParent['sessRoleId'];

        $AllNotificationData = \App\Http\Controllers\AllCommonDataController::AllNotificationData($id);
        
        return view('Parent.notification',['AllNotificationData' => $AllNotificationData]);
    }


    public function viewNotification($NotificatinId,Request $request)
    {
        
        $UpdateNotificationData = \App\Http\Controllers\AllCommonDataController::UpdateNotificationData($NotificatinId);

        $SingleNotificationData = \App\Http\Controllers\AllCommonDataController::SingleNotificationData($NotificatinId);
        
        return view('Parent.view_notification_details',['SingleNotificationData' => $SingleNotificationData]);
    }    



}
