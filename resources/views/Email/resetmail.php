<!DOCTYPE html>
<html lang="en">
<head>
		<title>Mail Templates</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<!--css link start here-->
		<link rel="icon" type="imge/png" sizes="16x16" href="{{ url('public/assets/img/favicon.png')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/bootstrap.min.css') }}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/font-awesome-4.7.0/css/font-awesome.min.css') }}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/style.css') }}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/media.css') }}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/fonts/stylesheet.css') }}"/>
		<link rel="stylesheet"  href="{{ url('public/assets/css/owl.carousel.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/jquery.scrollbar.css')}}"/>

		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/bootstrap-datetimepicker.min.css')}}"/>

		<link rel="stylesheet"  href="{{ url('public/assets/css/owl.carousel.min.css') }}"/>
		<!--css link end here-->
		<meta name="csrf-token" content="{{ csrf_token() }}" />

		<style>	
			.help-block
			{
				color:red;
			}
		</style>

		<script src="{{ url('public/assets/js/jquery.min.js')}}"></script>
		<script src="{{ url('public/js/validation.js')}}"></script>
		
	</head>
		
<body style="padding:0px; margin:0px;background-color:#fff;">
	<div class="mail_templet" style="background-color:none; padding:5px 0px; margin:0% 0 0 0; float:left; width:100%;">
	<center>
	<table style="width:650px; border:2px solid #cccccc;" cellpadding="0" cellspacing="0" class="mail_temp_table">


	<tr> 
	<td style="background-color:#ffffff;padding:0px 20px;">
	<hr style="border: 0;color: #ededeb;background-color:#ededeb;height:1px;width:100%;text-align: left; padding:0px; margin:6px 0 0 0px;">
	<div>
		<body>
		Hello {{ $name }} ,
		<br/><br/><p style='font-family: 'Calibri'; font-size: 13px;'>This mail is regarding with your reset password request.<br/>To reset your password <a href="{{url('ResetPassView')}}">click here...</a><br/>
		
		</p><br/><p style='font-family: 'Calibri'; font-size: 13px;'>Please let us know any questions you have.</p>
                                    
				
				
<hr style="border: 0;color: #ededeb;background-color:#ededeb;height:1px;width:100%;text-align: left;margin:10px 0 0;">
	</td>
	</tr>				
	<tr><td height="40" style="background:#fff; font-size: 16px; text-align: left; color: #000000;padding-left:20px;font-family:Calibri"> Regards,</td></tr>
	
	<tr><td height="25" style="background:#fff; font-size:16px; text-align: left; color: #000000;padding-left:20px;font-family:Calibri;">Think BrinWave</td></tr>
	<tr><td height="25" style="background:#fff; font-size:16px; text-align: left; color: #000000;padding-left:20px;font-family:Calibri;">Office : +852 3689 6860</td></tr>
	<tr><td height="25" style="background:#fff; font-size:16px; text-align: left; color: #000000;padding-left:20px;font-family: Calibri;">tbw@exceptionaire.com</td></tr>

	
	
	</table>
	</center>
	</div>
<!--js link start here-->
			<script src="{{ url('public/assets/js/bootstrap.min.js')}}"></script>
			<script src="{{ url('public/assets/js/bootstrap-datetimepicker.min.js')}}"></script>
			
			<script src="{{ url('public/assets/js/owl.carousel.min.js')}}"></script>
			<script src="{{ url('public/assets/js/custom.js')}}"></script>
			<!--js link end here-->
		</body>
</html>