<!DOCTYPE html>
<html lang="en">
<head>
		<title>Homepage</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<!--css link start here-->
		<link rel="icon" type="imge/png" sizes="16x16" href="{{ url('public/assets/img/favicon.png')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/bootstrap.min.css') }}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/font-awesome-4.7.0/css/font-awesome.min.css') }}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/style.css') }}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/media.css') }}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/fonts/stylesheet.css') }}"/>
		<link rel="stylesheet"  href="{{ url('public/assets/css/owl.carousel.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/jquery.scrollbar.css')}}"/>

		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/bootstrap-datetimepicker.min.css')}}"/>

		<link rel="stylesheet"  href="{{ url('public/assets/css/owl.carousel.min.css') }}"/>
		<!--css link end here-->
		<meta name="csrf-token" content="{{ csrf_token() }}" />

		<style>	
			.help-block
			{
				color:red;
			}
		</style>

		<script src="{{ url('public/assets/js/jquery.min.js')}}"></script>
		<script src="{{ url('public/js/validation.js')}}"></script>
		
	</head>
		<body>
			<!-- Page wrapper Start here-->
			<div class="wrapper">
				<!-- header section Start here-->
				<header class="header_section_new">
					
				</header>
				<!-- header section end here-->
				<section class="banner_section" style="background-image:url({{ url('public/assets/img/mid-bg.jpg')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<p>Hello {{ $name }}</p>
								<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $body }}
								</p>
								<p>Thanks & Regards</p>
								<p>Think BrinWave</p>
							</div>
						</div>
					</div>
				</section>
				
				
				<!-- footer section start Here -->
				<footer class="footer-section">
					
				</footer>
				<!-- footer section end Here -->
				
			</div>
			<!--js link start here-->
			<script src="{{ url('public/assets/js/bootstrap.min.js')}}"></script>
			<script src="{{ url('public/assets/js/bootstrap-datetimepicker.min.js')}}"></script>
			
			<script src="{{ url('public/assets/js/owl.carousel.min.js')}}"></script>
			<script src="{{ url('public/assets/js/custom.js')}}"></script>
			<!--js link end here-->
		</body>
</html>
