@include('Backend.header')
<script type="text/javascript" src="http://192.168.100.14/BrainWave/public/js/checkplus.js"></script>
    <!-- Start: Main -->
    <div id="main" class="animated fadeIn">

        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- begin canvas animation bg -->
            <div id="canvas-wrapper">
                <canvas id="demo-canvas"></canvas>
            </div>

            <!-- Begin: Content -->
            <section id="content">
<!--admin-form-->
                <div class="admin-form theme-info" id="login1">

                   
                    <div class="panel panel-info mt10 br-n">

                        <!-- end .form-header section -->
                        <form>
                       
                            <div class="panel-body bg-light p30">
                                <div class="row">
                                    <div class="col-sm-7 pr30">
                                        <div class="section">
                                            <label for="username" class="field-label text-muted fs18 mb10">Username</label>
                                            <label for="username" class="field prepend-icon">
                                                <input type="text" id="username" class="gui-input" placeholder="Enter username">
                                                <label for="username" class="field-icon"><i class="fa fa-user"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- end section -->

                                        <div class="section">
                                            <label for="email" class="field-label text-muted fs18 mb10">Email</label>
                                            <label for="email" class="field prepend-icon">
                                                <input type="email" id="email" class="gui-input" placeholder="Enter email">
                                                <label for="email" class="field-icon"><i class="fa fa-lock"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- end section -->

                                         <div class="section">
                                            <label for="url" class="field-label text-muted fs18 mb10">Url</label>
                                            <label for="url" class="field prepend-icon">
                                                <input type="text" id="url" class="gui-input" placeholder="Enter url">
                                                <label for="url" class="field-icon"><i class="fa fa-lock"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- end section -->

                                    </div>
                                    
                                </div>
                            </div>
                            <!-- end .form-body section -->
                            <div class="panel-footer clearfix p10 ph15">
                                 <label>
                                      <button type="button" class="button btn-primary mr10 pull-right" id="addnew" onclick="ShowNextForm();">Add</button>
                                </label> 
                            </div>
                            <!-- end .form-footer section -->
                        </form>
                         <!-- end .form-header section -->
                        <form style="display:none" id="newForm">
                       
                       <div class="panel-body bg-light p30">
                           <div class="row">
                               <div class="col-sm-7 pr30">
                                   <div class="section">
                                       <label for="username" class="field-label text-muted fs18 mb10">Username</label>
                                       <label for="username" class="field prepend-icon">
                                           <input type="text" id="username_new" class="gui-input" placeholder="Enter username">
                                           <label for="username" class="field-icon"><i class="fa fa-user"></i>
                                           </label>
                                       </label>
                                   </div>
                                   <!-- end section -->

                                   <div class="section">
                                       <label for="email" class="field-label text-muted fs18 mb10">Email</label>
                                       <label for="email" class="field prepend-icon">
                                           <input type="email" id="email_new" class="gui-input" placeholder="Enter password">
                                           <label for="email" class="field-icon"><i class="fa fa-lock"></i>
                                           </label>
                                       </label>
                                   </div>
                                   <!-- end section -->

                                    <div class="section">
                                       <label for="url" class="field-label text-muted fs18 mb10">Url</label>
                                       <label for="url" class="field prepend-icon">
                                           <input type="text"  id="url_new" class="gui-input" placeholder="Enter password">
                                           <label for="url" class="field-icon"><i class="fa fa-lock"></i>
                                           </label>
                                       </label>
                                   </div>
                                   <!-- end section -->

                               </div>
                               
                           </div>
                       </div>
                       <!-- end .form-body section -->
                       <div class="panel-footer clearfix p10 ph15">
                            <label>
                                 <button type="submit"   class="button btn-primary mr10 pull-right">Save</button>
                           </label> 
                       </div>
                       <!-- end .form-footer section -->
                   </form>


                    </div>
                </div>

            </section>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->

    </div>
    <!-- End: Main -->



@include('Backend.footer')
