<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">

            <form accept-charset="UTF-8" action="{{url('stripepay')}}" class="require-validation"
            data-cc-on-file="false"
            data-stripe-publishable-key="pk_test_ZOhMLptzJqPi6Ibo96wWowmN"
            id="payment-form" method="post" onsubmit="return validate();">
            {{ csrf_field() }}
            <div class='form-row form-group'>
                <div class='col-xs-12 form-group required'>
                    <label class='control-label'>Name on Card</label> <input
                        class='form-control' type='text' name="name" placeholder ="Enter user name">
                </div>
            </div>
            <div class='form-row form-group'>
                <div class='col-xs-12 form-group card required'>
                    <label class='control-label'>Card Number</label> <input
                        autocomplete='off' class='form-control card-number'
                        type='text' name="card_number" placeholder="Enter card number">
                </div>
            </div>
            <div class='form-row form-group'>
                <div class='col-xs-4 form-group cvc required'>
                    <label class='control-label'>CVC</label> <input autocomplete='off'
                        class='form-control card-cvc' placeholder='Enter CVV' size='4'
                        type='text' name="cvvNumber">
                </div>
                <div class='col-xs-4 form-group expiration required'>
                    <label class='control-label'>Expiration Month</label> <input
                        class='form-control card-expiry-month' placeholder='MM' size='2'
                        type='text' name="ccExpiryMonth">
                </div>
                <div class='col-xs-4 form-group expiration required'>
                    <label class='control-label'>Expiration Year </label> 
                    <input
                        class='form-control card-expiry-year' placeholder='YYYY' size='4'
                        type='text' name="ccExpiryYear">
                </div>
            </div>

            <div class='form-row form-group'>
                <div class='col-xs-12 form-group required'>
                    <label class='control-label'>Amount</label>
                     <input class='form-control' type='text' name="amount" placeholder="Amount">
                </div>
            </div>

            <div class='form-row  form-group'>
                <div class='col-md-12 form-group'>
                    <button class='btn btn-primary submit-button'
                        type='submit' style="margin-top: 10px;">Pay »</button>
                </div>
            </div>
        </form>

        </div>
    </body>
</html>

<script>
    // $(function() {
    //     $('form.require-validation').bind('submit', function(e) {
    //         var $form         = $(e.target).closest('form'),
    //             inputSelector = ['input[type=email]', 'input[type=password]',
    //                             'input[type=text]', 'input[type=file]',
    //                             'textarea'].join(', '),
    //             $inputs       = $form.find('.required').find(inputSelector),
    //             $errorMessage = $form.find('div.error'),
    //             valid         = true;
        
    //         $errorMessage.addClass('hide');
    //         $('.has-error').removeClass('has-error');
    //         $inputs.each(function(i, el) {
    //         var $input = $(el);
    //         if ($input.val() === '') {
    //             $input.parent().addClass('has-error');
    //             $errorMessage.removeClass('hide');
    //             e.preventDefault(); // cancel on first error
    //         }
    //         });
    //     });
    // });
</script>
<script src="https://js.stripe.com/v3/"></script>