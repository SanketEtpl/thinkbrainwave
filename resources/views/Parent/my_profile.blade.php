@include('Template.header_parent')

				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url(./img/mid-bg.jpg);">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>My Profile</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				
				<!--account user details info start here-->
				<section class="my_account_all_us_details">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="my_account_data">
									<div class="row">
										<div class="col-md-5 col-sm-6 col-xs-6 dash_dta_border_right">
											<div class="dash_bora_profil_user_sec">
												<span class="dash_img_profile_img">
													
													<img src="{{$parentData['profile_file_path']}}" class="img-responsive" alt="profile"/>
													
												</span>
												<div class="dash_board_details_sec">
													<h4>{{$parentData['name']}}</h4>
													<span>Parent</span>
													<div class="star_ratting_table_views">
														
													</div>
													<div class="refer_frds_btn_new">
														<a href="{{url('ParentEditProfile')}}" class="btn small_comm_btn">Edit Profile</a>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-6">
											<div class="user-privat-details">
												<div class="user_prvi_det_lists">
													<span><img src="{{url('public/assets/ParentAssets/img/gender.png')}}" class="img-responsive" alt="gender"/></span>
													<div>{{$parentData['gender']}}</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{url('public/assets/ParentAssets/img/age.png')}}" class="img-responsive" alt="age"/></span>
													<div>
														@php
														$dob = $parentData['dob'];
														$today = date('Y-m-d');
														$d1 = new DateTime($today);
														$d2 = new DateTime($dob);

														$diff = $d2->diff($d1);

														echo $diff->y;
														@endphp
														Years
													</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{url('public/assets/ParentAssets/img/map-marker.png')}}" class="img-responsive" alt="gender"/></span>
													<div>{{$parentData['address']}}</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--account user details info end here-->
				<!-- profile details section start here-->
				<section class="common_section new_rspo_common_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="acc_secti_tabing">
										<ul class="nav nav-tabs">
										  <li class="active"><a data-toggle="tab" href="{{url ('ParentMyProfile')}}">PERSONAL</a></li>
										  <li><a href="{{url ('ParentStudentProfile')}}">CHILD</a></li>
										</ul>
									</div>
									<div class="tab_account_content">
										<div class="tab-content">
											<div class="common_section_heading">
												<h4>PROFILE DETAILS</h4>
											</div>
											<div class="common_section_content">
												<div class="row">
													<div class="col-md-7">
														<div class="profile_user_details">
															<div class="profile_personal_details">
																<ul>
																	<li><span>NAME:</span><div class="profile_details_content">{{$parentData['name']}}</div></li>
																	<li><span>DATE OF BIRTH:</span><div class="profile_details_content">{{$parentData['dob']}}</div></li>
																	<li><span>GENDER</span><div class="profile_details_content">{{$parentData['gender']}}</div></li>
																	<li><span>ADDRESS</span><div class="profile_details_content">{{$parentData['address']}}</div></li>
																	<li><span>COUNTRY</span><div class="profile_details_content">{{$parentData['country_name']}}</div></li>
																	<li><span>PHONE NUMBER:</span><div class="profile_details_content">{{$parentData['phone_number']}}</div></li>
																	<li><span>EMAIL ID:</span><div class="profile_details_content">{{$parentData['email']}}</div></li>
																	<li><span>PASSWORD:</span><div class="profile_details_content_link">**********</div><a href="#." class="info-link-details"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
																</ul>
															</div>
															<div class="common_btn_div">
																<button
																 onclick="getUserIdModal({{$parentData['user_id']}})" class="btn small_comm_btn" >Change Password</button>
															</div>
														</div>
													</div>
													<div class="col-md-5">
														
													</div>
												</div>
											</div>
										</div>
									</div>
							</div>
						</div>
					</div>
				</section>
				<!-- profile details section end here-->


@include('Template.footer_parent')