@include('Template.header_parent')

				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url(./img/mid-bg.jpg);">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>Edit Profile</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<form action = "{{url('ParentProfileSave')}}" id="formEdit" class="form1" method = "post" enctype="multipart/form-data">
					@csrf

					<input type="hidden" name="user_id" id="user_id" value ="{{ $editData['user_id'] }}">
					@if($editData['profile_file_path'] == 'public/images/no-image.png')
					<input type="hidden" name="old_image" value =" ">
					@else
					<input type="hidden" name="old_image" value ="{{$editData['profile_file_path']}}">
					@endif
				<!--edit profile start here-->
				<section class="my_account_all_us_details">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="my_account_data">
									<div class="row">
										<div class="col-md-5 col-sm-6 col-xs-6 dash_dta_border_right">
											<div class="dash_bora_profil_user_sec">
												
												<span class="dash_img_profile_img">
												 <div class="circle">
												       
														<img src="{{$editData['profile_file_path']}}" class="img-responsive profile-pic" alt="profile"/>
														
												     </div>
												     
												     <div class="file btn edit_profile_photo">
														<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
														<input type="file" id="profile-img" name="profile"/>
													</div>
												</span>
												
												<div class="dash_board_details_sec">
													<h4>{{$editData['name']}}</h4>
													<span>Parent</span>
													<div class="star_ratting_table_views">
														<ul>
															<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
															<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
															<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
															<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
															<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
														</ul>
														<small>3.56/5</small>
													</div>
													<!-- <div class="refer_frds_btn_new">
														<a href="my_profile.html" class="btn small_comm_btn">View Profile</a>
													</div>-->
													<!-- <div class="refer_frds_btn_new">
														<a href="my_profile.html" class="btn small_comm_btn same_width_btn">Save</a>
														<a href="#." class="btn small_comm_btn same_width_btn">Cancel</a>
													</div> -->
												</div>
											</div>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-6">
											<div class="user-privat-details">
												<div class="user_prvi_det_lists">
													<span><img src="{{url('public/assets/ParentAssets/img/gender.png')}}" class="img-responsive" alt="gender"/></span>
													<div>{{$editData['gender']}}</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{url('public/assets/ParentAssets/img/age.png')}}" class="img-responsive" alt="age"/></span>
													<div>
														@php
														$dob = $editData['dob'];
														$today = date('Y-m-d');
														$d1 = new DateTime($today);
														$d2 = new DateTime($dob);

														$diff = $d2->diff($d1);

														echo $diff->y;
														@endphp
														Years
													</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{url('public/assets/ParentAssets/img/map-marker.png')}}" class="img-responsive" alt="address"/></span>
													<div>{{$editData['address']}}</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--edit profile details end here-->
				<!-- profile details section start here-->

				
					<section class="common_section new_rspo_common_section">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<div class="tab_account_content">
										<div class="common_section_heading comm_sec_new_heading">
											<h4>PROFILE DETAILS</h4>
											<div class="brainwav_plns_back_btn">
												<a href="{{url('ParentMyProfile')}}" class="btn back_btn_new">Back</a>
											</div>
										</div>
										<div class="common_section_content">
											<div class="row">
												<div class="col-md-7">
													<div class="profile_user_details">
														<div class="profile_personal_details">
															<ul>
																<li><span>Name:</span><div class="input_boxes_set"><input type="text" class="form-control required_check" name="name" id="name" value="{{$editData['name']}}" / ></div></li>
																<li>
																	<span>DATE OF BIRTH:</span>
																	<div class="input_boxes_set">
																		<div class="form-group calend_div_new">
																			<div class="input-group date pastDateParent ">
																				<input class="form-control calender_set required_check" type="text" name="dob" value="{{$editData['dob']}}" >
																				<div class="input-group-addon custom_input_calend"><i class="fa fa-calendar" aria-hidden="true"></i></div>
																			</div>
																			<input type="hidden" id="dtp_input2" >
																		</div>
																	</div>
																	<a href="#." class="info-link-details_sec tooltip_new"><span class="tooltiptext_new">Parent age should be greater than or equal to 20</span><i class="fa fa-info-circle" aria-hidden="true"></i></a>
																</li>
																<li>
																	<span>GENDER</span>
																	<div class="profile_details_content">
																	  <div class="radio_button">
																		<input type="radio" id="test1" name="gender" value="Male" {{ $editData['gender'] == 'Male'? 'checked="checked"' : '' }}>
																		<label for="test1">Male</label>
																	  </div>
																	  <div class="radio_button">
																		<input type="radio" id="test2" name="gender" value="Female" {{ $editData['gender'] == 'Female'? 'checked="checked"' : '' }}>
																		<label for="test2">Female</label>
																	  </div>
																	</div>
																</li>
																<li><span>PHONE NUMBER:</span><div class="input_boxes_set "><input type="text" class="form-control required_number" name="primary_phone" id="primary_phone" value="{{$editData['phone_number']}}" /></div></li>
																<li><span>EMAIL ID:</span><div class="input_boxes_set"><input type="text" class="form-control required_email" name="email" id="email" value="{{$editData['email']}}" /></div></li>
																
																<li>
																	<span>COUNTRY</span>
																	<div class="input_boxes_set ">
																		<select name="country_id" class="form-control select_tag_box required_check">
																			<option value="">Select Country</option>
																			@foreach($countries as $country)
																			<option value="{{$country->id}}" {{ $editData['country_id'] == $country->id? 'selected="selected"' : '' }}>{{$country->name}}</option>
																			@endforeach
																		</select>
																	</div>
																</li>
																<li><span>ADDRESS</span><div class="input_boxes_set"><input type="text" class="form-control required_check" name="address" id="address" value="{{$editData['address']}}" /></div></li>
															<li><span>PASSWORD:</span><div class="input_boxes_set"><input type="text" class="form-control" id="phon_number" value="******" /></div><a href="#." class="info-link-details_sec"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
															</ul>
														</div>
														<div class="common_btn_div">
															<a href="#." class="btn small_comm_btn"  data-toggle="modal" data-target="#change_password_popup">Change Password</a>
														</div>
													</div>
												</div>
												<div class="col-md-5">
													
												</div>
												<div class="col-md-12">
													<div class="save_adn_cancel_btn_grp">
														<input type="submit" class="btn common_btn_new" value="Save">
														<button type="button" class="btn common_btn_new">Cancel</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</form>
				<!-- profile details section end here-->
@include('Template.footer_parent')