@include('Template.header_parent')

				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url(./img/mid-bg.jpg);">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>Child Edit Profile</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!--edit profile start here-->

				<form action = "{{url('ParStudProfileSave')}}" id="formEdit" class="form1" method = "post" enctype="multipart/form-data">
					@csrf

					<input type="hidden" name="user_id" id="user_id" value ="{{ $studEdit['user_id'] }}">

					<input type="hidden" name="tutor_type" id="tutor_type" value ="{{ $studEdit['tutor_type'] }}">
					
					<input type="hidden" name="old_image" value ="{{$studEdit['profile_file_path']}}">
					
					<section class="my_account_all_us_details">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<div class="my_account_data">
										<div class="row">
											<div class="col-md-5 col-sm-6 col-xs-6 dash_dta_border_right">
												<div class="dash_bora_profil_user_sec">
													<!-- <span class="dash_img_profile_img">
														<img src="img/parent-profile.png" class="img-responsive" alt="profile"/>
														<a href="#." class="edit_profile_photo"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
													</span> -->
													<span class="dash_img_profile_img">
													 <div class="circle">
													       
															<img src="{{ URL::to('/') }}/public/images/{{ $studEdit['profile_file_path'] }}" class="img-responsive profile-pic" alt="profile"/>
															
													     </div>
													     
													     <div class="file btn edit_profile_photo">
															<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
															<input type="file" id="profile-img" name="profile_file_path"/>
														</div>
													</span>
													<div class="dash_board_details_sec">
														<h4>{{ $studEdit['first_name'] }}</h4>
														<span>Child</span>
														<div class="star_ratting_table_views">
															<ul>
																@php $rating = $studEdit['rating'];  @endphp
																@foreach(range(1,5) as $i)
																		@if($rating >0)
																			@if($rating >0.5)
																				<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																			@elseif($rating <=0.5)
																				<li><a href="#."><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>
																			@endif	
																		@else
																			<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																		@endif
																		@php $rating--; @endphp
																	
																@endforeach
															</ul>
															<small>@if($studEdit['rating'] != ""){{ $studEdit['rating'] }}/5 @endif</small>
														</div>
														<!-- <div class="refer_frds_btn_new">
															<a href="my_profile.html" class="btn small_comm_btn">View Profile</a>
														</div>-->
														<!-- <div class="refer_frds_btn_new">
															<a href="my_profile.html" class="btn small_comm_btn same_width_btn">Save</a>
															<a href="#." class="btn small_comm_btn same_width_btn">Cancel</a>
														</div> -->
													</div>
												</div>
											</div>
											<div class="col-md-4 col-sm-6 col-xs-6">
												<div class="user-privat-details">
													<div class="user_prvi_det_lists">
														<span><img src="{{url('public/assets/ParentAssets/img/gender.png')}}" class="img-responsive" alt="gender"/></span>
														<div>{{ $studEdit['gender'] }}</div>
													</div>
													<div class="user_prvi_det_lists">
														<span><img src="{{url('public/assets/ParentAssets/img/age.png')}}" class="img-responsive" alt="age"/></span>
														<div>
															@php
															$dob = $studEdit['dob'];
															$today = date('Y-m-d');
															$d1 = new DateTime($today);
															$d2 = new DateTime($dob);

															$diff = $d2->diff($d1);

															echo $diff->y;
															@endphp
															Years
														</div>
													</div>
													<div class="user_prvi_det_lists">
														<span><img src="{{url('public/assets/ParentAssets/img/map-marker.png')}}" class="img-responsive" alt="gender"/></span>
														<div>{{ $studEdit['address_line1'] }}</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!--edit profile details end here-->
					<!-- profile details section start here-->
					<section class="common_section new_rspo_common_section">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<div class="tab_account_content">
											<div class="common_section_heading comm_sec_new_heading">
												<h4>CHILD DETAILS</h4>
												<div class="brainwav_plns_back_btn">
													<a href="{{url('ParentStudentProfile')}}" class="btn back_btn_new">Back</a>
												</div>
											</div>
											<div class="common_section_content">
												<div class="row">
													<div class="col-md-7">
														<div class="profile_user_details">
															<div class="profile_personal_details">
																<ul>
																	<li><span>Name:</span><div class="input_boxes_set"><input type="text" class="form-control required_check" name="name" id="name" value="{{ $studEdit['first_name'] }}" /></div></li>
																	<li><span>Grade:</span><div class="input_boxes_set">
																		<select name="grade" class="form-control select_tag_box required_check">
																				<option value="">Select Grade</option>
																				@foreach($grades as $grade)
																				<option value="{{$grade->id}}" {{ $studEdit['grade_id'] == $grade->id? 'selected="selected"' : '' }}>{{$grade->name}}</option>
																				@endforeach
																			</select>

																		</div></li>
																	<li>
																		<span>DATE OF BIRTH:</span>
																		<div class="input_boxes_set">
																			<div class="form-group calend_div_new">
																				<div class="input-group date pastDateStudent">
																					<input class="form-control calender_set required_check" type="datetime" name="dob" value="{{ $studEdit['dob'] }}">
																					<div class="input-group-addon custom_input_calend"><i class="fa fa-calendar" aria-hidden="true"></i></div>
																				</div>
																				<input type="hidden" id="dtp_input2" value="">
																			</div>
																		</div>
																		<a href="#." class="info-link-details_sec tooltip_new"><span class="tooltiptext_new">Student age should be greater than or equal to 5</span><i class="fa fa-info-circle" aria-hidden="true"></i></a>
																	</li>
																	<li>
																		<span>GENDER</span>
																		<div class="profile_details_content">
																		  <div class="radio_button">
																		<input type="radio" id="test1" name="gender" value="Male" {{ $studEdit['gender'] == 'Male'? 'checked="checked"' : '' }}>
																		<label for="test1">Male</label>
																	  </div>
																	  <div class="radio_button">
																		<input type="radio" id="test2" name="gender" value="Female" {{ $studEdit['gender'] == 'Female'? 'checked="checked"' : '' }}>
																		<label for="test2">Female</label>
																	  </div>
																		</div>
																	</li>
																	<li><span>ADDRESS</span><div class="input_boxes_set"><input type="text" class="form-control required_check" name="address_line1" id="address" value="{{ $studEdit['address_line1'] }}" /></div></li>
																	<li>
																		<span>COUNTRY</span>
																		<div class="input_boxes_set">
																			<select name="country_id" class="form-control select_tag_box required_check">
																				<option value="">Select Country</option>
																				@foreach($countries as $country)
																				<option value="{{$country->id}}" {{ $studEdit['country_id'] == $country->id? 'selected="selected"' : '' }}>{{$country->name}}</option>
																				@endforeach
																			</select>
																		</div>
																	</li>
																	<li><span>MAX DISTANCE TRAVEL:</span><div class="input_boxes_set"><input type="text" class="form-control required_check" name="max_distance_travel_kms" value="{{$studEdit['max_distance_travel_kms'] }}" id="distan_travel"></div><a href="#." class="info-link-details_sec tooltip_new"><span class="tooltiptext_new">Maximum Distance You Can Travel to Tutor</span><i class="fa fa-info-circle" aria-hidden="true"></i></a></li>
																	<li><span>PHONE NUMBER:</span><div class="input_boxes_set"><input type="text" class="form-control required_number" id="phon_number" name="primary_phone" value="{{$studEdit['primary_phone'] }}" /></div></li>

																	<li><span>EMAIL:</span><div class="input_boxes_set"><input type="text" class="form-control required_email" id="email" name="email" value="{{$studEdit['email_id'] }}" /></div></li>
																	
																	<li><span>PASSWORD:</span><div class="input_boxes_set"><input type="text" class="form-control" id="phon_number" value="******" /></div><a href="#." class="info-link-details_sec"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
																</ul>
															</div>
															<!-- <div class="common_btn_div">
																<button class="btn small_comm_btn" >Change Password</button>
															</div> -->
														</div>
													</div>
													<!-- <div class="col-md-5">
														<div class="profile_details_shar_pa_div">
															<div class="profile_calender">
																<div class="calender_form_set_new">
																	<div class="input_new_set_search">
																		<input type="text" class="form-control" placeholder="Search">
																	</div>
																</div>
																<div class="pie_chart_heading">
																	<h4>MY PLAN</h4>
																</div>
																<div class="piechart-demo">
																	<img src="img/calender-img.jpg" class="img-responsive" alt="piechart">
																</div>
															</div>
															<div class="profile_shar_refer_add_links">
																
																<div class="profile_share_add_link_here">
																	<span><img src="img/refer-freind-icon.png" class="img-responsive" alt="refer-friend"/></span>
																	<div class="blog_btn_set">
																		<a href="book_session.html" class="btn common_btn">Add Session</a>
																	</div>
																</div>
															</div>
														</div>
													</div> -->
													<div class="col-md-12">
														<div class="save_adn_cancel_btn_grp">
															<input type="submit" class="btn common_btn_new" value="Save">
															<button type="button" class="btn common_btn_new">Cancel</button>
														</div>
													</div>
												</div>
											</div>
										  </div>
										</div>
									</div>
								</div>
					</section>

				</form>
				<!-- profile details section end here-->
@include('Template.footer_parent')

