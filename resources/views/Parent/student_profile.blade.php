@include('Template.header_parent')

				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url(./img/mid-bg.jpg);">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">

									<h4>MY Profile</h4>
									
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!--account user details info start here-->
				<section class="my_account_all_us_details">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="my_account_data">
									<div class="row">
										<div class="col-md-5 col-sm-6 col-xs-6 dash_dta_border_right">
											<div class="dash_bora_profil_user_sec">
												<span class="dash_img_profile_img">
													<img src="{{ URL::to('/') }}/public/images/{{ $parent['profile_file_path'] }}
													" class="img-responsive" alt="profile"/>
												</span>
												<div class="dash_board_details_sec">
													<h4>{{$parent['name']}}</h4>
													<span>Parent</span>
													<div class="star_ratting_table_views">
														
													</div>
													<div class="refer_frds_btn_new">
														<a href="{{url('ParentEditProfile')}}" class="btn small_comm_btn">Edit Profile</a>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-6">
											<div class="user-privat-details">
												<div class="user_prvi_det_lists">
													<span><img src="{{url('public/assets/ParentAssets/img/gender.png')}}" class="img-responsive" alt="gender"/></span>
													<div>{{$parent['gender']}}</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{url('public/assets/ParentAssets/img/age.png')}}" class="img-responsive" alt="age"/></span>
													<div>
														@php
														$dob = $parent['dob'];
														$today = date('Y-m-d');
														$d1 = new DateTime($today);
														$d2 = new DateTime($dob);

														$diff = $d2->diff($d1);

														echo $diff->y;
														@endphp
														Years
													</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{url('public/assets/ParentAssets/img/map-marker.png')}}" class="img-responsive" alt="gender"/></span>
													<div>{{$parent['address']}}</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--account user details info end here-->
				<!-- profile details section start here-->
				<section class="common_section new_rspo_common_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="acc_secti_tabing">
										<ul class="nav nav-tabs">
										  <li ><a href="{{url ('ParentMyProfile')}}">PERSONAL</a></li>
										  <li class="active"><a href="{{url ('ParentStudentProfile')}}">CHILD</a></li>
										</ul>
									</div>
									<div class="tab_account_content">
										<div class="tab-content">
											<div class="common_section_heading">
												<h4>CHILD'S DETAILS</h4>
											</div>
											<div class="common_section_content">
												<div class="row">
													<div class="col-md-7">
														<div class="profile_user_details">
															<div class="profile_personal_details">
																<ul>
																	<li>
																		<span>SELECT STUDENT</span>
																		<div class="input_boxes_set">
																			<select id="child_select"class="form-control select_tag_box">
																				@foreach($child as $childs)
															<option value="{{$childs->student_id}}" {{$childData['user_id'] == $childs->student_id? 'selected="selected"' : "" }}>{{$childs->first_name}}</option>
																				@endforeach
																			</select>
																		</div>
																	</li>
																	<li>
																		<span>Name:</span>
																		<div class="profile_details_content_best">
																			<div>{{$childData['first_name']}}</div>
																			<a href="{{url('ParStudEditProfile')}}/{{$childData['user_id']}}/{{$childRoleId}}" class="btn edit_text_profiles">Edit</a>
																		</div>
																	</li>
																	<li><span>GRADE:</span><div class="profile_details_content">{{$childData['grade_name']}}</div></li>
																	<li><span>SUBJECT/COURSES:</span><div class="profile_details_content">{{$childData['subject_name']}}</div></li>
																	<li><span>DATE OF BIRTH:</span><div class="profile_details_content">{{$childData['dob']}}</div></li>
																	<li><span>GENDER</span><div class="profile_details_content">{{$childData['gender']}}</div></li>
																	<li><span>ADDRESS</span><div class="profile_details_content">{{$childData['address_line1']}}</div></li>
																	<li><span>COUNTRY</span><div class="profile_details_content">{{$childData['country_name']}}</div></li>
																	<li><span>PHONE NUMBER:</span><div class="profile_details_content">{{$childData['primary_phone']}}</div></li>
																	<li><span>Email:</span><div class="profile_details_content">{{$childData['email_id']}}</div></li>
																	<li><span>PASSWORD:</span><div class="profile_details_content_link">**********</div><a href="#." class="info-link-details"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
																</ul>
															</div>
															<div class="common_btn_div">
																<button onclick="getUserIdModal({{$childData['user_id']}})" class="btn small_comm_btn" >Change Password</button>
															</div>
														</div>
													</div>
													<div class="col-md-5">
													<div class="pie_chart_details">
												<div class="pie_chart_heading">
													<h4>{{$childData['first_name']}} PLAN</h4>
												</div>
												<div class="calender_form_set_new">
													<div class="row">
														<div class="col-md-8">
															<!-- <div class="input_new_set_search">
																<input type="text" class="form-control" placeholder="Search"/>
															</div> -->
														</div>
														<div class="col-md-4">
															<!-- <div class="input_new_set_select">
																<select class="form-control select_tag_new">
																	<option>Daily</option>
																	<option>Monthly</option>
																	<option>Yearly</option>
																</select>
															</div> -->
														</div>
													</div>
												</div>
												<div class="piechart-demo">
													<!-- <img src="{{ url('public/assets/img/calender-img.jpg')}}" class="img-responsive" alt="piechart"/> -->
												</div>
													<div class='calendar1'></div>
												</div>
												<div class="calnder-btn_set">
													<a href="{{url('ParentMyPlan')}}" class="btn common_btn">Add Plan</a>
												</div>
											</div>
													</div>
												</div>
											</div>
										</div>
									</div>
							</div>
						</div>
					</div>
				</section>
				<!-- profile details section end here-->
				<!-- Calender script on -->
				@include('Student.calendar_code') 
				<!-- Calender script on -->

<script>
    $(function(){
      $('#child_select').on('change', function () {
		  var id = $(this).val(); // get selected value
		  var APP_URL = {!! json_encode(url('/ChildProfile')) !!} + '/' + id;
		//   alert(APP_URL);
		  
          if (id) { 
              window.location = APP_URL; 
          }
        
      });
    });
</script>				
@include('Template.footer_parent')