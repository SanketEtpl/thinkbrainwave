@include('Template.header_parent')

				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url(./img/mid-bg.jpg);">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>My Plan</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!-- add session section start here-->
				<section class="common_section_new">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="common_section_details_here_new">
									<div class="calender_form_set_new_set">
										<div class="calender_setn_calen">
											<div class="calend_plan_label">
												<span>SELECT CHILD :</span>
											</div>
											<div class="input_boxes_set">
												<select id="selectChild" class="form-control select_tag_box">
													<option value="0">Select Child:</option>
													@foreach($childs as $children)
													<option value="{{$children->student_id}}" {{$childid == $children->student_id? 'selected="selected"' : "" }}>{{$children->first_name}}</option>
													
													@endforeach
												</select>
											</div>
										</div>
									</div>
									<div class="calender_set_form">
									<div class="calender_form_set">
											<div class="row">
												<div class="col-md-6 col-sm-6 col-xs-6">
													<!-- <div class="input_boxes_set">
														<select class="form-control select_tag_box">
															<option selected="">Select Status:</option>
															<option>Daily</option>
															<option>Weekly</option>
															<option>Yearly</option>
														</select>
													</div> -->
												</div>
												<div class="col-md-6 col-sm-6 col-xs-6">
													<!-- <div class="form_set">
														<input class="form-control" id="search" placeholder="Search">
													</div> -->
												</div>
											</div>
										</div>
									</div>
									<div class="calender_set_form">
										<div class="calender_heading_new">
											<h4>{{$childName}}</h4>
										</div>
										<div class="calend_btn_group">
											@if($childName != '')
											<a href="{{url('ParentBookSession')}}/{{$childid}}" class="btn calender_btn">Add Session</a>
											@endif
										</div>
										<div class="calender_form_set">
											<div class="row">
												<div class="col-md-6 col-sm-6 col-xs-6">
													<div class="input_boxes_set">
														
													</div>
												</div>
												<div class="col-md-6 col-sm-6 col-xs-6">
													<div class="form_set">
														
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="calender_paln_set_here">
										<!--<a href="#." data-toggle="modal" data-target="#cancel_session_popup">
											<img src="img/may_plan_calender.png" class="img-responsive" alt="plan"/>
										</a>-->
										<div class="responsive_calender" >
											<div class="calender" >
											<div id='editable_calendar'></div>
												<ul class="days" data-toggle="modal" data-target="#cancel_session_popup">

												</ul>
											</div>
											<div class="booking_status">
												<div><span class="plan_deails_sign"></span><h4>Plan Details</h4></div>
												<div><span class="not_avialable_sign"></span><h4>Not Available</h4></div>
												<div><span class="book_cancle_sign"></span><h4>Book</h4></div>
												<div><span class="student_cancle_sign"></span><h4>Student Cancel</h4></div>
												<div><span class="tutor_cancle_sign"></span><h4>Tutor Cancel</h4></div>
												<div><span class="completed_cancle_sign"></span><h4>Complete</h4></div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- add session section end here-->

				
				
				<!--popup start here-->
				
				<div class="modal fade" id="account_session_details" role="dialog">
						<div class="modal-dialog session_details_popup_width">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="model_details">
									
									<div class="model_content">
										
										<div class="my_sessiondetails">
											<div class="my_session_heading">
												<h4>SESSION DETAILS</h4>
											</div>
											<div class="session_details_list_new">
												<ul>
													<li><label >Student Name:</label><span id="open_student_name"></span></li>
													<li><label id="">Date:</label><span id="open_date"></span></li>
													<li><label>Time:</label><span  id="open_time"></span></li>
													<li><label>Location:</label><span  id="open_location"></span></li>
													<li><label>Grade:</label><span  id="open_grade"></span></li>
													<li><label>Suject:</label><span  id="open_subject"></span></li>
													<li><label>Topic:</label><span  id="open_topic"></span></li>
												<ul>
											</div>
										</div>
										<div class="pop_btn_sect">
											<a href="#." id="id_cancel" class="btn save_cancel_btn" data-dismiss="modal" data-toggle="modal">Cancel</a>
											<button type="button" class="btn save_cancel_btn" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- view session details popup end Here -->
					  
					  <!-- cancel session popup Start Here -->
					  <div class="modal fade" id="cancel_session_popup" role="dialog">
						<div class="modal-dialog session_cancel_pass_popup">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="moel_details">
									<span class="model_header model_head_new_set">
										<h4>ARE YOU SURE <br> YOU WANT TO CANCEL SESSION ?</h4>
									</span>
									<div class="model_content">
										<div class="pop_btn_sect">
											<a href="#." class="btn save_cancel_btn " data-dismiss="modal" data-toggle="modal" data-target="#reason_comment_popup">Yes</a>
											<a href="#." data-dismiss="modal" class="btn save_cancel_btn ">No</a>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- cancel session popup end Here -->
					  <!-- cancel session display popup Start Here -->
					  <div class="modal fade" id="cancel_display_session_popup" role="dialog">
						<div class="modal-dialog session_cancel_pass_popup">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="moel_details">
									
									<div class="model_content">
										<div class="my_sessiondetails">
											<div class="my_session_heading">
												<h4>CANCEL REASON</h4>
											</div>
											<div class="session_details_list_new">
												<ul>
													<li><span id="cancel_reason"></span></li>
													
												<ul>
											</div>
										</div>
										<div class="pop_btn_sect">
											<a href="#." data-dismiss="modal" class="btn save_cancel_btn ">Close</a>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- cancel session display popup end Here -->
					  
					  <!-- reason comment popup Start Here -->
					  <div class="modal fade" id="reason_comment_popup" role="dialog">
						<div class="modal-dialog session_cancel_pass_popup">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="moel_details">
									<div class="section_heading_sec">
										<h4>REASON</h4>
									</div>
									<div class="model_content">
										<div class="model_comment_box">
											<textarea id="reason" class="form-control" placeholder="Enter Comments"></textarea>
											<input type="hidden" name="cancleId" id="cancleId">
										</div>
										<div class="pop_btn_sect">
											<a href="#." id="cancleSaveData" class="btn save_cancel_btn ">Submit</a>
											<a href="#." data-dismiss="modal" class="btn save_cancel_btn ">Cancel</a>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- reason comment session popup end Here -->
						
					</div>
					  
				<!--popup end here-->
			</div>
			<!--js link start here-->
			
			<script>
				jQuery(document).ready(function(){
					jQuery('.scrollbar-inner').scrollbar();
				});
		    </script>
			
			<!--js link end here-->
		
			<!-- Claender script on -->
				@include('Student.calendar_code') 
			<!-- Claender script on -->
<script>
    $(function(){
      $('#selectChild').on('change', function () {
		  var id = $(this).val(); // get selected value
		  var APP_URL = {!! json_encode(url('/StudentMyPlan')) !!} + '/' + id;
		  
          if (id == '0') { 
			window.location = {!! json_encode(url('/ParentMyPlan')) !!};
             
          } else{
			window.location = APP_URL; 
		  }
        
      });

			$("#cancleSaveData").click(function(e){

			var cancleId = $('#cancleId').val();
			var reason = $('#reason').val();
			var reason_val;
				
			if(reason=='')
			{
				reason_val="-";
			}
			else
			{
				reason_val=reason;
			}
			var url ='{{ url("SessionCancleSaveByParent")}}/'+cancleId+"/"+reason_val;
			window.location.replace(url);
			});
    });
</script>
@include('Template.footer_parent')