@include('Template.header_parent')

				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url(./img/mid-bg.jpg);">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>My Account</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!-- my account section start here-->
				<section class="my_accoun_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12 shwstudent">
								<div class="my_accoun_section_form">
									<div class="my_accoun_student_form">
										<span>SELECT CHILD :</span>
										<select id="getChildSession" class="form-control select_tag_box">
											@if(count($child) > 0)
												@foreach($child as $childData)
													<option value="{{$childData->student_id}}" {{$childId == $childData->student_id? 'selected="selected"' : "" }}>{{$childData->first_name}}</option>
												@endforeach
											@else
											<option value="0">Select Child</option>
											@endif
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="account_section_deta">
									<div class="account_heading_new_set">
										<h4>SESSION DETAILS</h4>
									</div>
									<div class="acc_secti_tabing">
										<ul class="nav nav-tabs">
										  <li class="active"><a data-toggle="tab" href="#home">UPCOMING</a></li>
										  <li><a data-toggle="tab" href="#menu1">PAST</a></li>
										</ul>
										<div class="accoun_ad_session_btn">
											<a href="{{url('ParentBookSession')}}/{{$childId}}" class="btn common_btn">Add Session</a>
										</div>
									</div>
									<div class="tab_account_content">
										<div class="tab-content">
										  <div id="home" class="tab-pane fade in active">
											   <div class="tab_content_account_deta">
													<div class="common_section_heading_new">
														<h4>{{$childName}}</h4>
													</div>
													<div class="may_account_session_info">

														@foreach ($FutureSessions as $FutureSessions)
															<div class="my_accoundetails">
																<div class="my_sesion_id_head">
																	<div class="my_session_id_details">
																		<a href="#." id="open_account_session_details" class="open_account_session_details" data-id ="{{ $FutureSessions->id }}" data-tutor_name ="{{ $FutureSessions->tutor_name }}" data-grade_name ="{{ $FutureSessions->grade_name }}" data-subject_name ="{{ $FutureSessions->subject_name }}" data-topic_name ="{{ $FutureSessions->topic_name }}" data-tutor_level ="{{ $FutureSessions->tutor_level }}" data-session_type ="{{ $FutureSessions->session_type }}" data-start_date ="{{ $FutureSessions->start_date }}" data-start_time ="{{ $FutureSessions->start_time }}" data-venue ="{{ $FutureSessions->venue }}" >
																		  
																			<span><img src="{{ url('public/assets/img/session-icon.png')}}" class="img-responsive" alt="session"/></span>
																			<h4>SESSION ID: {{ $FutureSessions->id }}</h4>
																		</a>
																	</div>
																	<div class="my_session_date">
																	{{ date('d M Y', strtotime($FutureSessions->start_date)) }}, {{ date('h:i A ', strtotime($FutureSessions->start_time)) }}
																	</div>
																</div>
																<div class="my_session_info">
																	<div class="my_session_info_icon">
																		<span><img src="@if($FutureSessions->session_type==1) {{ url('public/assets/img/session_symonds_icon.png')}} @else {{ url('public/assets/img/session_video_icon.png')}} @endif" class="img-responsive" alt="session"/></span>
																	</div>
																	<div class="my_session_details_here">
																		<div class="my_session_deta_set my_session_new_data">
																			<ul>
																				<li>
																					<label>Tutor Name :</label>
																					<span><a href="#.">@if($FutureSessions->tutor_id=='') Tutor not assigned yet @else {{ $FutureSessions->tutor_name }} @endif </a></span>
																				</li>
																				<li>
																					<label>Tutor Subject :</label>
																					<span>{{ $FutureSessions->subject_name }}</span>
																				</li>
																				<li>
																					<label>Standard :</label>
																					<span>{{ $FutureSessions->grade_name }}</span>
																				</li>
																				<li>
																					<label>Topic Name :</label>
																					<span>{{ $FutureSessions->topic_name }}</span>
																				</li>
																				
																				<li>
																					<label>Session Name :</label>
																					<small>@if($FutureSessions->session_type==1) F2F Session @else V2V Session @endif</small>
																				</li>
																			</ul>
																		</div>
																		<div class="my_session_btn">
																			@if($FutureSessions->id==$SessionsStartFirst->session_start_first_id && $FutureSessions->status!=5)
																				<a href="@if($FutureSessions->session_type==1) {{ url('StartSessionSave/'.$FutureSessions->id )}} @else {{ url('VideoStreaming')}}  @endif" class="btn session_comm_btn">Start</a>
																			
																			@elseif($FutureSessions->id==$SessionsStartFirst->session_start_first_id && $FutureSessions->status==5)
																				<a href="@if($FutureSessions->session_type==1) {{ url('StopSessionSave/'.$FutureSessions->id )}} @else {{ url('VideoStreaming')}} @endif" class="btn session_comm_btn">Stop</a>
																			@endif
																		</div>
																	</div>
																	<div class="my_session_status">
																		@if($FutureSessions->status==5) <h2>LIVE</h2> @else  @endif
																		<span>Booked Hours: 
																		
																			@php
																				$d1 = new DateTime($FutureSessions->start_time);
																				$d2 = new DateTime($FutureSessions->end_time);

																				$diff = $d2->diff($d1);

																				echo $diff->h;
																				echo ".".$diff->i;
																			@endphp
																		</span>
																	</div>
																</div>
															</div>
														@endforeach

													</div>
											   </div>
										  </div>
										  <div id="menu1" class="tab-pane fade">
										  <div class="tab_content_account_deta">
													<div class="common_section_heading_new">
														<h4>{{$childName}}</h4>
													</div>
													<div class="may_account_session_info">
														@foreach ($PastSessions as $PastSessions)
															<div class="my_accoundetails">
																<div class="my_sesion_id_head">
																	<div class="my_session_id_details">
																		<a href="#." data-toggle="modal" data-target="#account_past_session_details"> 
																			<h4>SESSION ID: {{ $PastSessions->id }}</h4>
																		</a>
																	</div>
																	<div class="my_session_date">
																	{{ date('d M Y', strtotime($PastSessions->start_date)) }}, {{ date('h:i A ', strtotime($PastSessions->start_time)) }}
																	</div>
																</div>
																<div class="my_session_info">
																	<div class="my_session_info_icon">
																		<span><img src="@if($PastSessions->session_type==1) {{ url('public/assets/img/session_symonds_icon.png')}} @else {{ url('public/assets/img/session_video_icon.png')}} @endif" class="img-responsive" alt="session"/></span>
																	</div>
																	<div class="my_session_details_here">
																		<div class="my_session_deta_set my_session_new_data">
																			<ul>
																				<li>
																					<label>Tutor Name :</label>
																					<span><a>{{ $PastSessions->tutor_name }}</a></span>
																				</li>
																				<li>
																					<label>Subject Name :</label>
																					<span>{{ $PastSessions->subject_name }}</span>
																				</li>
																				<li>
																					<label>Standard :</label>
																					<span>{{ $PastSessions->grade_name }} </span>
																				</li>
																				<li>
																					<label>Topic Name :</label>
																					<span>{{ $PastSessions->topic_name }}</span>
																				</li>
																				<li>
																					<label>Session Name :</label>
																					<small>@if($PastSessions->session_type==1) F2F Session @else V2V Session @endif</small>
																				</li>
																			</ul>
																		</div>
																		<!-- <div class="my_session_btn">
																			<a href="#." class="btn session_comm_btn rebook" id="rebook"  data-id ="{{ $PastSessions->id }}" data-tutor_id ="{{ $PastSessions->tutor_id }}"  data-tutor_name ="{{ $PastSessions->tutor_name }}"  data-tutor_gender ="{{ $PastSessions->tutor_gender }}" data-student_id ="{{ $PastSessions->student_id }}" data-session_type ="{{ $PastSessions->session_type }}" data-venue ="{{ $PastSessions->venue }}" data-visit_type ="{{ $PastSessions->visit_type }}" data-tutor_type ="{{ $PastSessions->tutor_type }}" data-grade_name ="{{ $PastSessions->grade_name }}" data-subject_name ="{{ $PastSessions->subject_name }}" data-topic_name ="{{ $PastSessions->topic_name }}" data-grade_id ="{{ $PastSessions->grade_id }}" data-subject_id ="{{ $PastSessions->subject_id }}" data-topic_id ="{{ $PastSessions->topic_id }}">Rebook Tutor</a>
																		</div> -->
																	</div>
																	<div class="my_session_status">
																		<h2> @php
																				$d1 = new DateTime($PastSessions->start_time);
																				$d2 = new DateTime($PastSessions->end_time);

																				$diff = $d2->diff($d1);

																				echo $diff->h;
																				echo ".".$diff->i;
																			@endphp</h2>
																		<span>Available Hours: 23</span>
																	</div>
																</div>
															</div>
														@endforeach
														
														
													</div>
											   </div>
										  </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- my account section end here-->
				<!-- profile details section end here-->


				
				<!--popup start here-->
				
					
					  
					<!-- view session details Start Here -->
					<div class="modal fade" id="account_session_details" role="dialog">
						<div class="modal-dialog session_details_popup_width">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="model_details">
									<div class="section_heading">
										<h4 id="open_id"></h4>
									</div>
									<div class="model_content">
										<div class="my_session_info_sec">
											<div class="my_session_info_icon_popups">
												<div><span><img src="{{ url('public/assets/img/session_symonds_icon.png')}}" class="img-responsive" alt="session"></span></div>
											</div>
											<div class="my_session_details_here_popups">
												<div class="my_session_deta_set">
													<ul>
														<li>
															<label>Tutor Name:</label>
															<span id="open_tutor_name"></span>
														</li>
														<li>
															<label>Gender:</label>
															<span id="open_tutor_gender">Male</span>
														</li>
														<li>
															<label>Tutor Level:</label>
															<span id="open_tutor_level"></span>
														</li>
														<li>
															<label>Standard:</label>
															<span id="open_standerd"></span>
														</li>
														<li>
															<label>Subject:</label>
															<span id="open_subject"></span>
														</li>
														<li>
															<label>Topic Name:</label>
															<span id="open_topic"></span>
														</li>
													</ul>
												</div>
												<!-- <div class="my_session_btn_new">
													<a href="#." id="open_start" class="btn session_comm_btn">Start</a>
												</div> -->
												<div class="calls_icon_popups">
													<!-- <a href="#.">
														<span>
															<img src="{{ url('public/assets/img/popup-call-icon-active.png')}}" class="img-responsive" alt="call"/>
														</span>
													</a> -->
												</div>
											</div>
										</div>
										<div class="my_sessiondetails">
											<div class="my_session_heading">
												<h4>SESSION DETAILS</h4>
											</div>
											<div class="session_details_list_new">
												<ul>
													<li><label >Session Type:</label><span id="open_session_type">Face 2 Face Session</span></li>
													<li><label id="">Date:</label><span id="open_date"></span></li>
													<li><label>Time:</label><span  id="open_time"></span></li>
													<li><label>Location:</label><span  id="open_location"></span></li>
													<li><label>Subject:</label><span  id="open_subject2"></span></li>
													<li><label>Topic:</label><span  id="open_topic2"></span></li>
												<ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- view session details popup end Here -->
					  
					  <!-- view past session details Start Here -->
					  <div class="modal fade" id="account_past_session_details" role="dialog">
						<div class="modal-dialog session_details_popup_width">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="model_details">
									<div class="section_heading">
										<h4>SESSION ID: 890</h4>
									</div>
									<div class="model_content">
										<div class="my_session_info_sec">
											<div class="my_session_info_icon_popups">
												<div><span><img src="{{ url('public/assets/img/session_symonds_icon.png')}}" class="img-responsive" alt="session"></span></div>
											</div>
											<div class="my_session_details_here_popups">
												<div class="my_session_deta_set set_past_sesio_user new_width_set_popups_name">
													<h4>Roland Claven</h4>
												</div>
												<div class="past_user_session_hrs">
													<h4>2 Hours</h4>
												</div>
											</div>
										</div>
										<div class="my_sessiondetails">
											<div class="my_session_heading">
												<h4>SESSION DETAILS</h4>
											</div>
											<div class="session_details_list_new">
												<ul>
													<li><label>Session Type:</label><span>Face 2 Face Session</span></li>
													<li><label>Date:</label><span>12.04.2018</span></li>
													<li><label>Time:</label><span>03.30 pm(70 minutes to go</span></li>
													<li><label>Location:</label><span>Roman Hills,Plot No. 344 Circle</span></li>
													<li><label>Topic:</label><span>English - Grammer</span></li>
												<ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- view past session details popup end Here -->
					  
					  <!-- view session details Start Here -->
					  <div class="modal fade" id="balance_details_popups" role="dialog">
						<div class="modal-dialog session_details_popup_width_new">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="model_details">
									<div class="section_heading">
										<h4>BALANCE DETAILS</h4>
									</div>
									<div class="model_content">
										<div class="my_session_info_sec">
											<div class="my_session_info_icon_popups_sec ">
												<div><span><img src="{{ url('public/assets/img/balance_detail_icon_pop.png')}}" class="img-responsive" alt="session"></span></div>
											</div>
											<div class="my_session_details_here_popups my_session_details_here_popups_new">
												<div class="my_session_deta_set set_past_sesio_user">
													<h2>Plan : GOLD</h2>
												</div>
												<div class="past_user_session_hrs_new ">
													<h4>Upto 09-09-2019</h4>
												</div>
											</div>
										</div>
										<div class="my_sessiondetails">
											<div class="my_session_heading">
												<h4>SUBSCRIPTION BALANCE :</h4>
											</div>
											<div class="session_details_list_new_set">
												<ul>
													<li><span>F2F Sessions:</span><div class="session_det_no">56</div></li>
													<li><span>V2V Sessions:</span><div class="session_det_no">66</div></li>
												</ul>
											</div>
										</div>
										<div class="my_sessiondetails">
											<div class="my_session_heading">
												<h4>OTHER BALANCE (FREE) :</h4>
											</div>
											<div class="session_details_list_new_set session_details_listup">
												<ul>
													<li><span>F2F Sessions (15 mins) :</span><div class="session_det_no">1</div></li>
													<li><span>V2V Sessions (30 mins) :</span><div class="session_det_no">1</div></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- view session details popup end Here -->
					  
					  <!-- booking details Start Here -->
					  <div class="modal fade" id="booking_details_popups" role="dialog">
						<div class="modal-dialog booking_details_popup_width">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="model_details">
									<div class="section_heading">
										<h4>BOOKING DETAILS</h4>
									</div>
									<div class="model_content">
										<div class="my_session_info_sec">
											<div class="booking_info_form">
												<div class="row">
													<div class="col-md-4">
														<div class="form_set">
															<label>TUTOR</label>
															<input type="text" class="form-control" id="tutor"/>
															<input type="hidden" id="tutor_id_fk" name="tutor_id_fk"/>
															<input type="hidden" id="student_id_fk" name="student_id_fk"/>
															<input type="hidden" id="tutor_type" name="tutor_type"/>
															<input type="hidden" id="visit_type" name="visit_type"/>
															<input type="hidden" id="venue" name="venue"/>
															<input type="hidden" id="session_type" name="session_type"/>

															<input type="hidden" id="grade_id" name="grade_id"/>

															<input type="hidden" id="subject_id" name="subject_id"/>

															<input type="hidden" id="topic_id" name="topic_id"/>
														</div>
													</div>
													<div class="col-md-4">
														<div class="form_set">
															<label>DATE</label>
															<div class="input_boxes_set">
																<div class="form-group calend_div_new">
																	<div class="input-group date form_date">
																		<input class="form-control calender_set" type="datetime" name="start_date" id="start_date">
																		<div class="input-group-addon custom_input_calend"><i class="fa fa-calendar" aria-hidden="true"></i></div>
																	</div>
																	<input type="hidden" id="dtp_input2" value="">
																</div>
															</div>
														</div>
													</div>
													<div class="col-md-2">
														<div class="form_set_sec">
															<span>START TIME</span>
															<div class="input_boxes_set">
																<div class="form-group calend_div_new">
																	<div class="input-group date form_time" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
																		<input class="form-control calender_set" size="16" type="text" value="" id="start_time" name="start_time"  readonly="">
																		<div class="input-group-addon custom_input_calend"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
																	</div>
																	<input type="hidden" id="dtp_input3" value="">
																</div>
															</div>
														</div>
													</div>
													<div class="col-md-2">
														<div class="form_set_sec">
															<span>END TIME</span>
															<div class="input_boxes_set">
																<div class="form-group calend_div_new">
																	<div class="input-group date form_time" data-date="" data-date-format="hh:ii" data-link-field="dtp_input4" data-link-format="hh:ii">
																		<input class="form-control calender_set" size="16" type="text" value="" id="end_time" name="end_time" readonly="">
																		<div class="input-group-addon custom_input_calend"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
																	</div>
																	<input type="hidden" id="dtp_input4" value="">
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
														<div class="form_set">
															<label>GRADE</label>
															<div class="input_boxes_set">
															<input type="text" class="form-control" id="grade_id_name"/>
															</div>
														</div>
													</div>
													<div class="col-md-4">
														<div class="form_set">
															<label>SUBJECT</label>
															<div class="input_boxes_set">
																
															<input type="text" class="form-control" id="subject_id_name"/>
															</div>
														</div>
													</div>
													<div class="col-md-4">
														<div class="form_set">
															<label>TOPIC</label>
															<div class="input_boxes_set">
															<input type="text" class="form-control" id="topic_id_name"/>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="main_profil_btn">
											<a href="#." class="btn save_cancel_btn" ID="payment_getway_confirm">BOOK</a>
											<a href="#." class="btn save_cancel_btn" data-dismiss="modal">RESET</a>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- booking details popup end Here -->
					  
					  <!-- payment getway popup Start Here -->
					  <div class="modal fade" id="payment_getway_popup" role="dialog">
						<div class="modal-dialog model_forgot_pass_popup">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="moel_details">
									<div class="section_heading">
										<h4>PAYMENT GETWAY</h4>
									</div>
									<div class="model_content">
										<span class="model_header">
											<img src="{{ url('public/assets/img/payment_getway.png')}}" class="img-responsive" alt="forgotpassword">
										</span>
										<div class="payment_gat_info">
											<h1>$9</h1>
										</div>
										<div class="pop_btn_sect">
											<a href="#." data-dismiss="modal" id="confirm_payment" class="btn popup_coom_btn">OK</a>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- payment getway popup end Here -->
					  
				<!--popup end here-->
			<script>
				$(document).ready(function() {
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
				$("#grade_id").click(function(e){
						var grade_id = $("select[name=grade_id]").val();
						//alert(grade_id);
						$.ajax({
								type:'POST',
								url:'{{ url("FetchSubject")}}',
								data:{grade_id:grade_id},
								success:function(data){
									console.log(data);
									$('select[name="subject_id"]').empty();
									$('select[name="subject_id"]').append('<option value=""> --- Select Subject---</option>');
									$.each(data, function(key, value){

										$('select[name="subject_id"]').append('<option value="'+ key +'">' + value + '</option>');
																//console.log(key);
																//console.log(value);
									});
									
								}
							});
					});
					$("#subject_id").click(function(e){
						var grade_id = $("select[name=grade_id]").val();
						var subject_id = $("select[name=subject_id]").val();
						
						$.ajax({
								type:'POST',
								url:'{{ url("FetchTopic")}}',
								data:{grade_id:grade_id,subject_id:subject_id},
								success:function(data){
									console.log(data);
									$('select[name="topic_id"]').empty();
									$('select[name="topic_id"]').append('<option value=""> --- Select Topic---</option>');
									$.each(data, function(key, value){

										$('select[name="topic_id"]').append('<option value="'+ key +'">' + value + '</option>');
									});
									
								}
							});
					});
					$(".rebook").click(function(e){

						var id = $(this).data('id');
						var tutor_name = $(this).data('tutor_name');
						var tutor_id = $(this).data('tutor_id');
						var student_id = $(this).data('student_id');

						var tutor_type = $(this).data('tutor_type');
						var visit_type = $(this).data('visit_type');
						var venue = $(this).data('venue');
						var session_type = $(this).data('session_type');

						var grade_name = $(this).data('grade_name');
						var subject_name = $(this).data('subject_name');
						var topic_name = $(this).data('topic_name');
						var grade_id = $(this).data('grade_id');
						var subject_id = $(this).data('subject_id');
						var topic_id = $(this).data('topic_id');

						$('#tutor').val(tutor_name);
						$('#tutor_id_fk').val(tutor_id);
						$('#student_id_fk').val(student_id);

						$('#tutor_type').val(tutor_type);
						$('#visit_type').val(visit_type);
						$('#venue').val(venue);
						$('#session_type').val(session_type);

						$('#grade_id').val(grade_id);
						$('#subject_id').val(subject_id);
						$('#topic_id').val(topic_id);

						$('#grade_id_name').val(grade_name);
						$('#subject_id_name').val(subject_name);
						$('#topic_id_name').val(topic_name);

						$('#tutor').attr("readonly","true");
						$('#grade_id_name').attr("readonly","true");
						$('#subject_id_name').attr("readonly","true");
						$('#topic_id_name').attr("readonly","true");
						
						$('#booking_details_popups').modal('show');
					});
					$("#payment_getway_confirm").click(function(e){
						var grade_id = $("input[name=grade_id]").val();
						var subject_id = $("input[name=subject_id]").val();
						var topic_id = $("input[name=topic_id]").val();
						var start_date = $("input[name=start_date]").val();
						if(grade_id!=null)
						{
							if(subject_id!='' && topic_id!='' && start_date!='')
							{
								$('#payment_getway_popup').modal('show');
							}
							
							else
							{
								alert("Select Grade, Subject, Topic OR Start date");
							}
						}
						else
						{
							alert("Select Grade, Subject, Topic OR Start date");
						}
						
					});
					$("#confirm_payment").click(function(e){

						var grade_id = $("input[name=grade_id]").val();
						var subject_id = $("input[name=subject_id]").val();
						var topic_id = $("input[name=topic_id]").val();
						var start_date = $("input[name=start_date]").val();
						var start_time = $("input[name=start_time]").val();
						var end_date = $("input[name=start_date]").val();
						var end_time = $("input[name=end_time]").val();
						var tutor_type = $("input[name=tutor_type]").val();
						var visit_type = $("input[name=visit_type]").val(); 
						var venue = $("input[name=venue]").val();
						var session_type = $("input[name=session_type]").val();
						var tutor_id = $("input[name=tutor_id_fk]").val();
						
									$.ajax({
										type:'POST',
										url:'{{ url("BookSessionSave")}}',
										data:{grade_id:grade_id, subject_id:subject_id, topic_id:topic_id, start_date:start_date, start_time:start_time, end_date:end_date, end_time:end_time, tutor_type:tutor_type, visit_type:visit_type, session_type:session_type, venue:venue, tutor_id:tutor_id},
										success:function(data){
											//alert(data.success);
											console.log(data.success);
											if(data.success=='success')
											{
												//$('#confirm_booking_popups').modal('show');
											}
											else
											{
												//$('#confirm_booking_popups').modal('show');
											}
											$('#submit_success_popup').modal('show');
										}
									});
							
					});
					$(".open_account_session_details").click(function(e){

						var id = $(this).data('id');
						var tutor_name = $(this).data('tutor_name');
						var tutor_gender = $(this).data('tutor_gender');
						var grade_name = $(this).data('grade_name');
						var subject_name = $(this).data('subject_name');
						var topic_name = $(this).data('topic_name');
						var tutor_level = $(this).data('tutor_level');
						var session_type = $(this).data('session_type');
						var start_date = $(this).data('start_date');
						var start_time = $(this).data('start_time');
						var venue = $(this).data('venue');

						if(session_type==1)
						{
							var session_type_text ="Face 2 Face Session";
						}
						else if(session_type==2)
						{
							var session_type_text ="VIDEO 2 VIDEO Session";
						}
						else
						{
							var session_type_text ="";
						}
						
						
						$('#open_id').html("SESSION ID : "+id);
						$('#open_tutor_name').html(tutor_name);
						$('#open_tutor_gender').html(tutor_gender);
						$('#open_tutor_level').html("Level : "+tutor_level);
						$('#open_standerd').html(grade_name);
						$('#open_subject').html(subject_name);
						$('#open_topic').html(topic_name);
						$('#open_session_type').html(session_type_text);
						$('#open_date').html(start_date);
						$('#open_time').html(start_time);
						$('#open_location').html(venue);

						$('#open_subject2').html(subject_name);
						$('#open_topic2').html(topic_name);

						$('#account_session_details').modal('show');
					});
				});

			</script>

<script>
    $(function(){
      $('#getChildSession').on('change', function () {
		  var id = $(this).val(); // get selected value
		  var APP_URL = {!! json_encode(url('/getSessionByChild')) !!} + '/' + id;
		//   alert(APP_URL);
		  
          if (id) { 
              window.location = APP_URL; 
          }
        
      });
    });
</script>			

@include('Template.footer_parent')