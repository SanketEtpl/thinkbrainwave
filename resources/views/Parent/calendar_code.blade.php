<script>
				$(document).ready(function() {
						// page is now ready, initialize the calendar...
						$('#childCal').fullCalendar({
								// put your options and callbacks here
								header : {
										  left:   'today prev,next',
										  center: 'title',
										  right:  'prevYear,nextYear'
										},
								events : [
										@foreach($tasks as $task)
										{
												title : '{{ $task->start_time }}',
												start : '{{ $task->start_date }}',
												link : '{{  $task->id }}',
												backgroundColor: '#4c9cff',
												eventType : 'BookSession'
										},
										@endforeach
										@foreach($StudentCancletasks as $StudentCancletask)
										{
												title : '{{ $StudentCancletask->start_time }}',
												start : '{{ $StudentCancletask->start_date }}',
												link : '{{  $StudentCancletask->id }}',
												backgroundColor: '#b90415',
												eventType : 'CancleSession'
										},
										@endforeach
										@foreach($TutorCancletasks as $TutorCancletask)
										{
												title : '{{ $TutorCancletask->start_time }}',
												start : '{{ $TutorCancletask->start_date }}',
												link : '{{  $TutorCancletask->id }}',
												backgroundColor: '#b94704',
												eventType : 'CancleSession'
										},
										@endforeach
										@foreach($Completedtasks as $Completedtask)
										{
												title : '{{ $Completedtask->start_time }}',
												start : '{{ $Completedtask->start_date }}',
												link : '{{  $Completedtask->id }}',
												backgroundColor: '#3c763d',
												eventType : 'CompletedSession'
										},
										@endforeach
										@foreach($Booktasks as $Booktask)
										{
												title : '{{ $Booktask->start_time }}',
												start : '{{ $Booktask->start_date }}',
												link : '{{  $Booktask->id }}',
												backgroundColor: '#0023cc',
												eventType : 'CompletedSession'
										},
										@endforeach
								],								
						})
						$('#childCal').click(function(event){
										
							window.location.href="ParentMyPlan";
						});
				});
			</script>
			<script>
				$(document).ready(function() {
						// page is now ready, initialize the calendar...
						$('#editable_calendar').fullCalendar({
								// put your options and callbacks here
								header : {
										  left:   'today  prev, next',
										  center: 'title', 
										  right:  'prevYear,  nextYear'
										},
								events : [
										@foreach($tasks as $task)
										{
												title : '{{ $task->start_time }}',
												start : '{{ $task->start_date }}',
												id : '{{  $task->id }}',
												student_name : '{{  $task->student_name }}',
												student_gender : '{{  $task->student_gender }}',
												start_date : '{{  $task->start_date }}',
												start_time : '{{  $task->start_time }}',
												grade_name : '{{  $task->grade_name }}',
												subject_name : '{{  $task->subject_name }}',
												topic_name : '{{  $task->topic_name }}',
												venue : '{{  $task->venue }}',
												backgroundColor: '#4c9cff',
												eventType : 'BookSession',
												eventStatus : 'Pending',
												eventReason : '{{  $task->student_cancel_reason }}'
										},
										@endforeach
										@foreach($StudentCancletasks as $StudentCancletask)
										{
												title : '{{ $StudentCancletask->start_time }}',
												start : '{{ $StudentCancletask->start_date }}',
												id : '{{  $StudentCancletask->id }}',
												backgroundColor: '#b90415',
												eventType : 'CancleSession',
												eventReason : '{{  $StudentCancletask->student_cancel_reason }}'
										},
										@endforeach
										@foreach($TutorCancletasks as $TutorCancletask)
										{
												title : '{{ $TutorCancletask->start_time }}',
												start : '{{ $TutorCancletask->start_date }}',
												id : '{{  $TutorCancletask->id }}',
												backgroundColor: '#e9681c',
												eventType : 'CancleSession',
												eventReason : '{{  $TutorCancletask->tutor_cancel_reason }}'
										},
										@endforeach
										@foreach($Completedtasks as $Completedtask)
										{
												title : '{{ $Completedtask->start_time }}',
												start : '{{ $Completedtask->start_date }}',
												id : '{{  $Completedtask->id }}',
												student_name : '{{  $Completedtask->student_name }}',
												student_gender : '{{  $Completedtask->student_gender }}',
												start_date : '{{  $Completedtask->start_date }}',
												start_time : '{{  $Completedtask->start_time }}',
												grade_name : '{{  $Completedtask->grade_name }}',
												subject_name : '{{  $Completedtask->subject_name }}',
												topic_name : '{{  $Completedtask->topic_name }}',
												venue : '{{  $Completedtask->venue }}',
												backgroundColor: '#1c881e',
												eventType : 'BookSession',
												eventStatus : 'Complete',
												eventReason : '{{  $Completedtask->tutor_cancel_reason }}'
										},
										@endforeach
										@foreach($Booktasks as $Booktask)
										{
												title : '{{ $Booktask->start_time }}',
												start : '{{ $Booktask->start_date }}',
												id : '{{  $Booktask->id }}',
												student_name : '{{  $Booktask->student_name }}',
												student_gender : '{{  $Booktask->student_gender }}',
												start_date : '{{  $Booktask->start_date }}',
												start_time : '{{  $Booktask->start_time }}',
												grade_name : '{{  $Booktask->grade_name }}',
												subject_name : '{{  $Booktask->subject_name }}',
												topic_name : '{{  $Booktask->topic_name }}',
												venue : '{{  $Booktask->venue }}',
												backgroundColor: '#2a4cf1',
												eventType : 'BookSession',
												eventStatus : 'Pending',
												eventReason : '{{  $Booktask->tutor_cancel_reason }}'
										},
										@endforeach
								],
								
								eventClick: function(event){
										$('#modalTitle').html(event.title);
										$('#modalBody').html(event.title);
										$('#cancleId').val(event.id);
										$('#reason').val('');
										$('#cancel_reason').html(event.eventReason);

										$('#open_student_name').html(event.student_name);
										$('#open_date').html(event.start_date);
										$('#open_time').html(event.start_time);
										$('#open_location').html(event.venue);
										$('#open_grade').html(event.grade_name);
										$('#open_subject').html(event.subject_name);
										$('#open_topic').html(event.topic_name);
										//$('#cancel_reason').html('hello');
										//$('#fullCalModal').modal();
										if(event.eventType=='BookSession')
										{
											//console.log(event.eventStatus);
											if(event.eventStatus=='Complete')
											{
												$('#id_cancel').attr("data-target","#");
											}
											else
											{
												$('#id_cancel').attr("data-target","#cancel_session_popup");
											}


											$('#account_session_details').modal('show');
										}
										if(event.eventType=='CancleSession')
										{
											$('#cancel_display_session_popup').modal('show');
										}
								},
								
						})
						
						$("#cancleSave").click(function(e){

								var cancleId = $('#cancleId').val();
								var reason = $('#reason').val();
								window.location.replace("StudentSessionCancleSave/"+cancleId+"/"+reason);
						});

				});
			</script>