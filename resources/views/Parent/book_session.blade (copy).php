@include('Template.header_parent')

				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url(./img/mid-bg.jpg);">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>BOOK SESSION</h4>
									<div class="brainwav_plns_btn pull-right">
									<a href="{{url('ParentMyPlan')}}" class="btn plans_btn">Back</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!--account user details info start here-->
				<section class="my_account_all_us_details">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="my_account_data">
									<div class="row">
										<div class="col-md-5 col-sm-6 col-xs-6 dash_dta_border_right">
											<div class="dash_bora_profil_user_sec">
												<span class="dash_img_profile_img">
													<img src="{{ URL::to('/') }}/public/images/{{ $child[0]->profile_file_path}} " class="img-responsive" alt="profile"/>
												</span>
												<div class="dash_board_details_sec">
													<h4>{{ $child[0]->first_name}}</h4>
													<span>Child</span>
													<div class="star_ratting_table_views">
														<ul>
															<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
															<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
															<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
															<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
															<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
														</ul>
														<small>3.56/5</small>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-6">
											<div class="user-privat-details">
												<div class="user_prvi_det_lists">
													<span><img src="{{url('public/assets/img/gender.png')}}" class="img-responsive" alt="gender"/></span>
													<div>{{ $child[0]->gender}}</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{url('public/assets/img/age.png')}}" class="img-responsive" alt="age"/></span>
													<div>
													@php
														$dob = $child[0]->dob;
														$today = date('Y-m-d');
														$d1 = new DateTime($today);
														$d2 = new DateTime($dob);

														$diff = $d2->diff($d1);

														echo $diff->y;
													@endphp
														Years
													</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{url('public/assets/img/map-marker.png')}}" class="img-responsive" alt="gender"/></span>
													<div>{{$child[0]->address_line1}}</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--account user details info end here-->
				<!-- book session section start here-->
				<section class="common_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="common_sectiontabbin_session">
									<div class="common_main_tabs">
										<div class="tabing_nav_here">
											<ul class="nav nav-tabs">
											  <li class="active">
												 <a  href="#home" data-toggle="tab">
													<div class="tabs_navelink">
														<div class="tabs_nav_img">
															<span><img src="{{ url('public/assets/img/session_tab-icon-01.png')}}" class="img-responsive" alt="tabs"/></span>
														</div>
														<div class="tabs_heading">
															<h4>F2F <br>Face to Face Session</h4>
															<span>Sessions to be conducted in person.<br>
															Tutor may travel to Student or vice versa</span>
														</div>
													</div>
												 </a>
											  </li>
											  <li>
											     <a data-toggle="tab" href="#menu1">
													<div class="tabs_navelink">
														<div class="tabs_nav_img">
															<span><img src="{{ url('public/assets/img/session_tab-icon-02.png')}}" class="img-responsive" alt="tabs"/></span>
														</div>
														<div class="tabs_heading" >
															<h4>V2V <br> Video to Video Session</h4>
															<span>Sessions to be conducted using Online <br>Live Streaming engine. </span>
														</div>
													</div>
											     </a>
											  </li>
											</ul>
										</div>
										<div class="tabing_nav__content">
											<div class="tab-content">
											  <div id="home" class="tab-pane fade in active">
											  <form class="form1" method = "post" >
            									@csrf
													<div class="tabbing_content_book_session">
														<div class="tabscontent_heading" >
															<h4>FACE TO FACE SESSION</h4>
														</div>	
														<div class="tabs_content_form">
														<input type="hidden" id="tutor_type" name="tutor_type" value="{{ $child[0]->tutor_type }}">
														<input type="hidden" id="max_distance_travel_kms" name="max_distance_travel_kms" value="{{ $child[0]->max_distance_travel_kms}}">
															<div class="row">
																<div class="col-md-3">
																	<div class="form_set_sec">
																		<span>GRADE</span>
																		<select class="form-control select_tag_box required_check" name="grade_id" id ="grade_id">
																			<option value=""  selected disabled>Select Grade</option>
																			@foreach ($grades as $grade)
																					<option value="{{ $grade->id }}">{{ $grade->name }}</option>
																				@endforeach
																		</select>
																	</div>
																</div>
																<div class="col-md-3">
																	<div class="form_set_sec">
																		<span>SUBJECT</span>
																		<select class="form-control select_tag_box required_check" name="subject_id" id="subject_id">
																			<option value=""  selected disabled>Select Subject</option>
																		</select>
																	</div>
																</div>
																<div class="col-md-3">
																	<div class="form_set_sec">
																		<span>TOPIC</span>
																		<select class="form-control select_tag_box required_check" name="topic_id" id="topic_id">
																			<option value=""  selected disabled>Select Topic</option>
																			
																		</select>
																	</div>
																</div>
																<div class="col-md-3">
																	<div class="form_set_sec">
																		<span>START DATE</span>
																		<div class="input_boxes_set">
																			<div class="form-group calend_div_new">
																				<div class="input-group date form_date">
																					<input class="form-control calender_set required_check" type="datetime" name="start_date" id="start_date">
																					<div class="input-group-addon custom_input_calend"><i class="fa fa-calendar" placeholder="DD/MM/YYYY" aria-hidden="true"></i></div>
																				</div>
																				
																				<input type="hidden" id="dtp_input2" value="">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-md-3">
																	<div class="form_set_sec">
																		<span>START TIME</span>
																		<div class="input_boxes_set">
																			<div class="form-group calend_div_new">
																				<div class="input-group date form_time" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
																					<input class="form-control calender_set required_check" size="16" type="text"  name="start_time" id="start_time" placeholder="00:00">
																					<div class="input-group-addon custom_input_calend"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
																				</div>
																				<input type="hidden" id="dtp_input3" value="">
																			</div>
																		</div>
																	</div>
																</div>
																<!--<div class="col-md-4">
																	<div class="form_set_sec">
																		 <div class="profile_details_content_new">
																			  <div class="radio_button">
																				<input type="radio" id="test1" name="visit_type" data-toggle="modal" data-target="#visit_you_popups" value="2">
																				<label for="test1">TUTOR HOME</label>
																			  </div>
																			  <div class="radio_button">
																				<input type="radio" id="test2" name="visit_type" data-toggle="modal" data-target="#visit_me_popups" value="1">
																				<label for="test2">STUDENT HOME</label>
																			  </div>
																		 </div>
																	</div>
																</div>-->
																<div class="col-md-3">
																	<div class="form_set_sec">
																		<span>END DATE</span>
																		<div class="input_boxes_set">
																			<div class="form-group calend_div_new">
																				<div class="input-group date form_date">
																					<input class="form-control calender_set required_check" type="datetime" name="end_date" id="end_date">
																					<div class="input-group-addon custom_input_calend"><i class="fa fa-calendar" placeholder="DD/MM/YYYY" aria-hidden="true"></i></div>
																				</div>
																				
																				<input type="hidden" id="dtp_input2" value="">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-md-3">
																	<div class="form_set_sec">
																		<span>END TIME</span>
																		<div class="input_boxes_set">
																			<div class="form-group calend_div_new">
																				<div class="input-group date form_time" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
																					<input class="form-control calender_set required_check" size="16" type="text" name="end_time" id="end_time" placeholder="00:00">
																					<div class="input-group-addon custom_input_calend"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
																				</div>
																				<input type="hidden" id="dtp_input3" value="">
																			</div>
																		</div>
																	</div>
																</div>
																<!--<div class="col-md-4">
																	<div class="form_set_sec">
																		 <span>TUTOR</span>
																		 <div class="profile_details_content">
																			  <div class="radio_button">
																				<input type="radio" value="Any" id="test3" name="tutor_type" checked="">
																				<label for="test3">Any</label>
																			  </div>
																			  <div class="radio_button">
																				<input type="radio" value="Male" id="test4" name="tutor_type">
																				<label for="test4">Male</label>
																			  </div>
																			  <div class="radio_button">
																				<input type="radio" value="Female" id="test5" name="tutor_type">
																				<label for="test5">Female</label>
																			  </div>
																		 </div>
																	</div>
																</div>-->
																<div class="col-md-12">
																	<div class="save_adn_cancel_btn_grp_new">
																		<button type="button" class="btn common_btn_new" id="face_to_face">BOOK</button>
																		<button type="button" class="btn common_btn_new">RESET</button>
																	</div>
																</div>
															</div>
														</div>
													</div>
													</form>
											  </div>
												<div id="menu1" class="tab-pane fade">
													<form class="form2" method = "post" >
														<div class="tabbing_content_book_session">
															<div class="tabscontent_heading">
																<h4>VIDEO TO VIDEO SESSION</h4>
															</div>	
															<div class="tabs_content_form">
																<div class="row">
																	<div class="col-md-3">
																		<div class="form_set_sec">
																			<span>GRADE</span>
																			<select class="form-control select_tag_box required_check" name="grade_id2" id="grade_id2">
																					<option value=""  selected disabled>Select Grade</option>
																					@foreach ($grades as $grade)
																						<option value="{{ $grade->id }}">{{ $grade->name }}</option>
																					@endforeach
																			</select>
																		</div>
																	</div>
																	<div class="col-md-3">
																		<div class="form_set_sec">
																			<span>SUBJECT</span>
																			<select class="form-control select_tag_box required_check" name="subject_id2" id="subject_id2">
																				<option value=""  selected disabled>Select Subject</option>
																			</select>
																		</div>
																	</div>
																	<div class="col-md-3">
																		<div class="form_set_sec">
																			<span>TOPIC</span>
																			<select class="form-control select_tag_box required_check" name="topic_id2" id="topic_id2">
																			<option value=""  selected disabled>Select Subject</option>
																			</select>
																		</div>
																	</div>
																	<div class="col-md-3">
																		<div class="form_set_sec">
																			<span>DATE</span>
																			<div class="input_boxes_set">
																				<div class="form-group calend_div_new">
																					<div class="input-group date form_date">
																						<input class="form-control calender_set required_check" type="datetime" name="start_date2" id="start_date2">
																						<div class="input-group-addon custom_input_calend"><i class="fa fa-calendar" aria-hidden="true"></i></div>
																					</div>
																					<input type="hidden" id="dtp_input2" value="">
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col-md-3">
																		<div class="form_set_sec">
																			<span>TIME</span>
																			<div class="input_boxes_set">
																				<div class="form-group calend_div_new">
																					<div class="input-group date form_time" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
																						<input class="form-control calender_set required_check" size="16" type="text"  placeholder="00:00" name="start_time2" id="start_time2">
																						<div class="input-group-addon custom_input_calend"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
																					</div>
																					<input type="hidden" id="dtp_input3" value="">
																				</div>
																			</div>
																		</div>
																	</div>
																	<!--<div class="col-md-4">
																		<div class="form_set_sec">
																			<span>TUTOR</span>
																			<div class="profile_details_content">
																				<div class="radio_button">
																					<input type="radio" id="test6" value="Any" name="tutor_type2" checked="">
																					<label for="test6">Any</label>
																				</div>
																				<div class="radio_button">
																					<input type="radio" id="test7" value="Male"  name="tutor_type2">
																					<label for="test7">Male</label>
																				</div>
																				<div class="radio_button">
																					<input type="radio" id="test8" value="Female"  name="tutor_type2">
																					<label for="test8">Female</label>
																				</div>
																			</div>
																		</div>
																		<br><br>
																	</div>-->
																	<div class="col-md-3">
																		<div class="form_set_sec">
																			<span>DATE</span>
																			<div class="input_boxes_set">
																				<div class="form-group calend_div_new">
																					<div class="input-group date form_date">
																						<input class="form-control calender_set required_check" type="datetime" name="end_date2" id="end_date2">
																						<div class="input-group-addon custom_input_calend"><i class="fa fa-calendar" aria-hidden="true"></i></div>
																					</div>
																					<input type="hidden" id="dtp_input2" value="">
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col-md-3">
																		<div class="form_set_sec">
																			<span>TIME</span>
																			<div class="input_boxes_set">
																				<div class="form-group calend_div_new">
																					<div class="input-group date form_time" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
																						<input class="form-control calender_set required_check" size="16" type="text" placeholder="00:00" name="end_time2" id="end_time2">
																						<div class="input-group-addon custom_input_calend"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
																					</div>
																					<input type="hidden" id="dtp_input3" value="">
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col-md-3">
																	</div>
																	<div class="col-md-12">
																		<div class="save_adn_cancel_btn_grp_new">
																			<button type="button" class="btn common_btn_new" id="video_to_video">BOOK</button>
																			<button type="button" class="btn common_btn_new">RESET</button>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- book session section end here-->

				<!--popup start here-->
				
					
					  					 
					<!-- visit you popup Start Here -->
					<div class="modal fade" id="visit_you_popups" role="dialog">
							<div class="modal-dialog change_password_popup_width">
								<!-- Modal content-->
								<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
									<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
									</button>
								</div>
								<div class="modal-body">
									<div class="model_details">
										<div class="section_heading">
											<h4>VISIT YOU</h4>
										</div>
										<div class="model_content">
											<div class="visit_you_radio_btn">
												<div class="profile_details_content">
													<div class="radio_button">
														<input type="radio" id="test9" name="radio-group" checked="">
														<label for="test9">From Home Address</label>
													</div>
													<div class="radio_button">
														<input type="radio" id="test10" name="radio-group" checked="">
														<label for="test10">From Current Address</label>
													</div>
													
												</div>
												
											</div>
											<div class="main_profil_btn">
												<a href="#." class="btn save_cancel_btn margin-right-10px" data-dismiss="modal">CONFIRM</a>
												<a href="#." class="btn save_cancel_btn" data-dismiss="modal">CANCEL</a>
											</div>
										</div>
									</div>
								</div>
								</div>
							</div>
					  </div>
					  <!-- visit you popup end Here -->
					  
					  <!-- visit you popup Start Here -->
					  <div class="modal fade" id="visit_me_popups" role="dialog">
							<div class="modal-dialog change_password_popup_width">
								<!-- Modal content-->
								<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
									<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
									</button>
								</div>
								<div class="modal-body">
									<div class="model_details">
										<div class="section_heading">
											<h4>VISIT ME</h4>
										</div>
										<div class="model_content">
											<div class="visit_you_radio_btn">
												
												<div class="profile_details_content">
													<div class="radio_button">
														<input type="radio" id="visit_type3" value="1" name="visit_type">
														<label for="visit_type3">Visit Me</label>
													</div>
													<div class="radio_button">
														<input type="radio" id="visit_type2" value="2" name="visit_type">
														<label for="visit_type2">Visit You</label>
													</div>
													
												</div><br><br><br>
												<div class="profile_details_content">
													<div class="radio_button">
														<input type="radio" id="test11" name="radio-group">
														<label for="test11">To Home Address</label>
													</div>
													<div class="radio_button">
														<input type="radio" id="test12" name="radio-group">
														<label for="test12">To Current Address</label>
													</div>
												</div>
												<div class="profile_details_content">
													<br><br><br>
													<textarea name="venue" id="venue" class="form-control" > {{ $child[0]->address_line1 }}</textarea>
												</div>
											</div>
											<div class="main_profil_btn">
												<a href="#." class="btn save_cancel_btn margin-right-10px" data-dismiss="modal">CONFIRM</a>
												<a href="#." class="btn save_cancel_btn" data-dismiss="modal">CANCEL</a>
											</div>
										</div>
									</div>
								</div>
								</div>
							</div>
					  </div>
					  <!-- visit you popup end Here -->
					  
					  	<!-- visit you popup Start Here -->
						<div class="modal fade" id="confirm_booking_popups" role="dialog">
							<div class="modal-dialog change_password_popup_width">
								<!-- Modal content-->
								<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
									<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
									</button>
								</div>
								<div class="modal-body">
									<div class="model_details">
										<div class="section_heading">
											<h4>BOOKING CONFIRMATION</h4>
										</div>
										<div class="model_content">
											<div class="visit_you_radio_btn_sec">
													<ul>
														<li id="conf_not"></li>
													</ul>
											</div>
											<div class="visit_you_radio_btn_sec" id="face_available">
												
											</div>
											<div class="main_profil_btn">
												<a href="#." id="a_href_booking_confirm" class="btn save_cancel_btn margin-right-10px" data-dismiss="modal" data-toggle="modal" data-target="#payment_getway_popup">CONFIRM</a>
												<a href="#." class="btn save_cancel_btn" data-dismiss="modal">CANCEL</a>
											</div>
										</div>
									</div>
								</div>
								</div>
							</div>
					  </div>
					  <!-- visit you popup end Here -->
					  <!-- visit you popup Start Here -->
					  <div class="modal fade" id="confirm_booking_popups_video" role="dialog">
							<div class="modal-dialog change_password_popup_width">
								<!-- Modal content-->
								<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
									<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
									</button>
								</div>
								<div class="modal-body">
									<div class="model_details">
										<div class="section_heading">
											<h4>BOOKING CONFIRMATION</h4>
										</div>
										<div class="model_content">
                                            <div class="visit_you_radio_btn_sec">
													<ul>
														<li id="conf_not_vedio"></li>
													</ul>
											</div>
											<div class="visit_you_radio_btn_sec"  id="vedio_available">
												
											</div>
											<div class="main_profil_btn" >
												<a href="#." id="video_a_href_booking_confirm" class="btn save_cancel_btn margin-right-10px" data-dismiss="modal" data-toggle="modal" data-target="#payment_getway_popup2">CONFIRM</a>
												<a href="#." class="btn save_cancel_btn" data-dismiss="modal">CANCEL</a>
											</div>
										</div>
									</div>
								</div>
								</div>
							</div>
					  </div>
					  <!-- visit you popup end Here -->
					  
					  <!-- payment getway popup Start Here -->
					  <div class="modal fade" id="payment_getway_popup" role="dialog">
							<div class="modal-dialog model_forgot_pass_popup">
								<!-- Modal content-->
								<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
									<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
									</button>
								</div>
								<div class="modal-body">
									<div class="moel_details">
										<div class="section_heading">
											<h4>PAYMENT GETWAY</h4>
										</div>
										<div class="model_content">
											<span class="model_header">
												<img src="{{ url('public/assets/img/payment_getway.png')}}" class="img-responsive" alt="forgotpassword">
											</span>
											<div class="payment_gat_info">
												<h1>$12</h1>
											</div>
											<div class="pop_btn_sect">
												<a href="#." id="confirm_payment" data-dismiss="modal" class="btn popup_coom_btn">OK</a>
											</div>
										</div>
									</div>
								</div>
								</div>
							</div>
					  </div>
					  <!-- payment getway popup end Here -->
					  <!-- payment getway popup Start Here -->
					  <div class="modal fade" id="payment_getway_popup2" role="dialog">
							<div class="modal-dialog model_forgot_pass_popup">
								<!-- Modal content-->
								<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
									<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
									</button>
								</div>
								<div class="modal-body">
									<div class="moel_details">
										<div class="section_heading">
											<h4>PAYMENT GETWAY</h4>
										</div>
										<div class="model_content">
											<span class="model_header">
												<img src="{{ url('public/assets/img/payment_getway.png')}}" class="img-responsive" alt="forgotpassword">
											</span>
											<div class="payment_gat_info">
												<h1>$12</h1>
											</div>
											<div class="pop_btn_sect">
												<a href="#." id="confirm_payment_video" data-dismiss="modal" class="btn popup_coom_btn">OK</a>
											</div>
										</div>
									</div>
								</div>
								</div>
							</div>
					  </div>
					  <!-- payment getway popup end Here -->
					  
					  
				<!--popup end here-->

			<!--js link start here-->
			<script>
				 $(window).load(function(){        
				   		$('#visit_me_popups').modal('show');
				    });
			</script>
			<script>
				jQuery(document).ready(function(){
					jQuery('.scrollbar-inner').scrollbar();
				});
				$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
				$("#grade_id").click(function(e){
						var grade_id = $("select[name=grade_id]").val();
						// alert(grade_id);
						$.ajax({
								type:'POST',
								url:'{{ url("FetchSubjectForStudent")}}',
								data:{grade_id:grade_id},
								success:function(data){
									// console.log(data);
									$('select[name="subject_id"]').empty();
									$('select[name="subject_id"]').append('<option value=""  selected disabled>Select Subject</option>');
								$.each(data, function(key, value){

									$('select[name="subject_id"]').append('<option value="'+ key +'">' + value + '</option>');
									
								});
									
							}
						});
					});
					$("#subject_id").click(function(e){
						var grade_id = $("select[name=grade_id]").val();
						var subject_id = $("select[name=subject_id]").val();
						
						$.ajax({
								type:'POST',
								url:'{{ url("FetchTopicForStudent")}}',
								data:{grade_id:grade_id,subject_id:subject_id},
								success:function(data){
									// console.log(data);
									$('select[name="topic_id"]').empty();
									$('select[name="topic_id"]').append('<option value=""  selected disabled>Select Grade</option>');
                    $.each(data, function(key, value){

                         $('select[name="topic_id"]').append('<option value="'+ key +'">' + value + '</option>');
                    });
									
								}
							});
					});
					$("#grade_id2").click(function(e){
						var grade_id = $("select[name=grade_id2]").val();
						$.ajax({
								type:'POST',
								url:'{{ url("FetchSubjectForStudent")}}',
								data:{grade_id:grade_id},
								success:function(data){
									// console.log(data);
									$('select[name="subject_id2"]').empty();
									$('select[name="subject_id2"]').append('<option value=""  selected disabled>Select Grade</option>');
                    $.each(data, function(key, value){

                         $('select[name="subject_id2"]').append('<option value="'+ key +'">' + value + '</option>');
                    });
									
								}
							});
					});
					$("#subject_id2").click(function(e){
						var grade_id = $("select[name=grade_id2]").val();
						var subject_id = $("select[name=subject_id2]").val();
						
						$.ajax({
								type:'POST',
								url:'{{ url("FetchTopicForStudent")}}',
								data:{grade_id:grade_id,subject_id:subject_id},
								success:function(data){
									// console.log(data);
									$('select[name="topic_id2"]').empty();
									$('select[name="topic_id2"]').append('<option value=""  selected disabled>Select Grade</option>');
                    $.each(data, function(key, value){

                         $('select[name="topic_id2"]').append('<option value="'+ key +'">' + value + '</option>');
                    });
									
								}
							});
					});
					
					$("#face_to_face").click(function(e){
						var grade_id = $("select[name=grade_id]").val();
						var subject_id = $("select[name=subject_id]").val();
						var topic_id = $("select[name=topic_id]").val();
						var start_date = $("input[name=start_date]").val();
						var start_time = $("input[name=start_time]").val();
						var tutor_type = $("input[name=tutor_type]").val();
						var max_distance_travel_kms = $("input[name=max_distance_travel_kms]").val();
						var visit_type =  $('input[name=visit_type]:checked').val();

						var venue = $("textarea[name=venue]").val();

						var latitude = $("input[name=latitude]").val(); 
						var longitude = $("input[name=longitude]").val(); 
						var session_type = "1";
						
						if ($('.form1').valid()) 
						{

							$.ajax({
									type:'POST',
									url:'{{ url("BookStudentSessionConfirm")}}',
									data:{type : 1,grade_id:grade_id, subject_id:subject_id, topic_id:topic_id, start_date:start_date, start_time:start_time,  end_date:end_date, end_time:end_time,tutor_type:tutor_type, visit_type:visit_type, session_type:session_type, venue:venue},
									success:function(data){
										console.log(data);
										
										// if(data.success)
										// {
										// 	if(data.success!=null)
										// 	{
										// 		$("#face_available").html("");
										// 		$("#conf_not").html("");

										// 		var append_data = "<ul><li><span>Grade : </span><span>"+data.successArray.grade_name+"</span></li><li><span>Subject : </span><span>"+data.successArray.subject_name+"</span></li><li><span>Topic: </span><span>"+data.successArray.topic_name+"</span></li><li><span>Date : </span><span>"+data.successArray.start_date+" to "+data.successArray.end_date+"</span></li><li><span>Time : </span><span>"+data.successArray.start_time+" to "+data.successArray.end_time+"</span></li><li><span>Location : </span><span>"+venue+"</span></li><li>Notified 1 hour before the session</li><li><span>Tutor: </span><span>"+tutor_type+"</span></li>";

										// 		 $("#face_available").append(append_data);
												


										// 		$('#a_href_booking_confirm').attr({'data-dismiss':"modal", 'data-toggle':"modal",'data-target':"#payment_getway_popup"});
										// 	}
										// }
										// else if(data.error=='REQUEST_DENIED')
										// {
										// 	console.log(data.error);
										// 	$("#face_available").html("");
										// 	$("#conf_not").html("Somthing went wrong");

										// 	$('#a_href_booking_confirm').attr({'data-dismiss':"modal", 'data-toggle':"",'data-target':""});
										// }
										// else{
										// 	$("#face_available").html("");
										// 	$("#conf_not").html("Tutor is not available ");

										// 	$('#a_href_booking_confirm').attr({'data-dismiss':"modal", 'data-toggle':"",'data-target':""});
										// }


										// $('#confirm_booking_popups').modal('show');
										
									}
								});
						}
							
					});

					$("#video_to_video").click(function(e){
						var grade_id2 = $("select[name=grade_id2]").val();
						var subject_id2 = $("select[name=subject_id2]").val();
						var topic_id2 = $("select[name=topic_id2]").val();
						var start_date2 = $("input[name=start_date2]").val();
						var start_time2 = $("input[name=start_time2]").val();
						var tutor_type2 = $("input[name=tutor_type]").val();
						var session_type = "2";

						var venue = $("textarea[name=venue]").val();
						
						
						if ($('.form2').valid()) 
						{	
							$.ajax({
									type:'POST',
									url:'{{ url("BookStudentSessionConfirm")}}',
									data:{type : 2,grade_id:grade_id2, subject_id:subject_id2, topic_id:topic_id2, start_date:start_date2, start_time:start_time2,  end_date:end_date2, end_time:end_time2,tutor_type:tutor_type2},
									success:function(data){
										//alert(data.success);
										console.log(data.success);
										console.log(data.topic_name);
										if(data.success)
										{
											if(data.success!=null)
											{
												$("#vedio_available").html("");
												$("#conf_not_vedio").html("");

												var append_data = "<ul><li><span>Grade : </span><span>"+data.successArray.grade_name+"</span></li><li><span>Subject : </span><span>"+data.successArray.subject_name+"</span></li><li><span>Topic: </span><span>"+data.successArray.topic_name+"</span></li><li><span>Date : </span><span>"+data.successArray.start_date+" to "+data.successArray.end_date+"</span></li><li><span>Time : </span><span>"+data.successArray.start_time+" to "+data.successArray.end_time+"</span></li><li><span>Location : </span><span>"+venue+"</span></li><li>Notified 1 hour before the session</li><li><span>Tutor: </span><span>"+tutor_type2+"</span></li>";

												 $("#vedio_available").append(append_data);
												


												$('#video_a_href_booking_confirm').attr({'data-dismiss':"modal", 'data-toggle':"modal",'data-target':"#payment_getway_popup2"});
											}
										}
										else if(data.error=='REQUEST_DENIED')
										{
											console.log(data.error);
											$("#vedio_available").html("");
											$("#conf_not_vedio").html("Somthing went wrong");

											$('#video_a_href_booking_confirm').attr({'data-dismiss':"modal", 'data-toggle':"",'data-target':""});
										}
										else{
											$("#vedio_available").html("");
											$("#conf_not_vedio").html("Tutor is not available ");

											$('#video_a_href_booking_confirm').attr({'data-dismiss':"modal", 'data-toggle':"",'data-target':""});
										}

										$('#confirm_booking_popups_video').modal('show');
										
									}
								});
							}
							
						
					});

					$("#confirm_payment").click(function(e){
						var grade_id = $("select[name=grade_id]").val();
						var subject_id = $("select[name=subject_id]").val();
						var topic_id = $("select[name=topic_id]").val();
						var start_date = $("input[name=start_date]").val();
						var start_time = $("input[name=start_time]").val();
						var end_date = $("input[name=end_date]").val();
						var end_time = $("input[name=end_time]").val();
						var tutor_type = $("input[name=tutor_type]").val();
						var visit_type = $("input[name=visit_type]").val(); 
						var venue = $("textarea[name=venue]").val();
						var session_type = "1";
						if ($('.form1').valid()) 
						{
							$.ajax({
								type:'POST',
								url:'{{ url("BookStudentSessionSave")}}',
								data:{grade_id:grade_id, subject_id:subject_id, topic_id:topic_id, start_date:start_date, start_time:start_time, end_date:end_date, end_time:end_time, tutor_type:tutor_type, visit_type:visit_type, session_type:session_type, venue:venue},
								success:function(data){
									//alert(data.success);
									console.log(data.success);
									if(data.success)
									{
										window.location.replace("ParentMyPlan");
									}
									else if(data.error=='REQUEST_DENIED')
									{
										console.log(data.error);
										$("#face_available").html("");
										$("#conf_not").html("Somthing went wrong");

										$('#a_href_booking_confirm').attr({'data-dismiss':"modal", 'data-toggle':"",'data-target':""});
										$('#confirm_booking_popups').modal('show');
									}
									else
									{
										
									}
								}
							});
						}
					});
					$("#confirm_payment_video").click(function(e){
						var grade_id2 = $("select[name=grade_id2]").val();
						var subject_id2 = $("select[name=subject_id2]").val();
						var topic_id2 = $("select[name=topic_id2]").val();
						var start_date2 = $("input[name=start_date2]").val();
						var start_time2 = $("input[name=start_time2]").val();
						var end_date2 = $("input[name=end_date2]").val();
						var end_time2 = $("input[name=end_time2]").val();
						var tutor_type2 = $("input[name=tutor_type]").val();
						
						var session_type = "2";
						if ($('.form2').valid()) 
						{
							$.ajax({
								type:'POST',
								url:'{{ url("BookStudentSessionSave")}}',
								data:{grade_id:grade_id2, subject_id:subject_id2, topic_id:topic_id2, start_date:start_date2, start_time:start_time2, end_date:end_date2, end_time:end_time2, tutor_type:tutor_type2, session_type:session_type},
								success:function(data){
									//alert(data.success);
									console.log(data.success);
									if(data.success=='success')
									{
										//$('#confirm_booking_popups').modal('show');
									}
									else
									{
										//$('#confirm_booking_popups').modal('show');
									}
									//$('#submit_success_popup').modal('show');
									window.location.replace("ParentMyPlan");
								}
							});
						}
					});
					$("#test11").click(function(e){
						$('#venue').attr("type","text");
						$('#venue').val("{{ $child[0]->address_line1 }}");
					});
					$("#test12").click(function(e){
						$('#venue').attr("type","text");
						$('#venue').val("");
					});
		  </script>
			<!--js link end here-->

@include('Template.footer_parent')