@include('Template.header_parent')
				

				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url({{ url('public/assets/img/mid-bg.jpg')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>NOTIFICATION</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!-- notifiaction section start here-->
				<section class="web_section_common">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="section_details">
									<div class="notification_blog">
										@foreach ($AllNotificationData as $AllNotificationData)
											<div class="notifiaction_info_list_active">
												<span class="date_of_notification_active">
													
												
													{{ date('d-m-Y h:i A', strtotime($AllNotificationData->created_at)) }}
												</span>
												<div class="@if($AllNotificationData->status=='0') noti_list_pass @else noti_list_pass_read @endif">
													<h4>
														@if($AllNotificationData->notification_type=='AdminNotification') 
															{{ config('constants.AdminNotification') }} 
														
														@elseif($AllNotificationData->notification_type=='SessionBook') 
															{{ config('constants.SessionBook') }}

														@elseif($AllNotificationData->notification_type=='SessionAssign') 
															{{ config('constants.SessionAssign') }}
														
														@elseif($AllNotificationData->notification_type=='Session_1_Hour_Left') 
															{{ config('constants.Session_1_Hour_Left') }}
														
														@elseif($AllNotificationData->notification_type=='Session_5_Min_left') 
															{{ config('constants.Session_5_Min_left') }}
														
														@elseif($AllNotificationData->notification_type=='SessionStart') 
															{{ config('constants.SessionStart') }}
														
														@elseif($AllNotificationData->notification_type=='SessionStop') 
															{{ config('constants.SessionStop') }}
														
														@elseif($AllNotificationData->notification_type=='SessionComplete') 
															{{ config('constants.SessionComplete') }}
														
														@elseif($AllNotificationData->notification_type=='SessionTutorCancel') 
															{{ config('constants.SessionTutorCancel') }}
														
														@elseif($AllNotificationData->notification_type=='SessionStudentCancel') 
															{{ config('constants.SessionStudentCancel') }}
														
														@elseif($AllNotificationData->notification_type=='RatingByTutor') 
															{{ config('constants.RatingByTutor') }}
														
														@elseif($AllNotificationData->notification_type=='RatingByStudent') 
															{{ config('constants.RatingByStudent') }}
														
														@elseif($AllNotificationData->notification_type=='SessionParentCancel') 
														{{ config('constants.SessionParentCancel') }}
														
														@endif
													</h4>
													<p class="textLimit">{{ $AllNotificationData->message }}</p>
													<a href='{{ url("ViewNotification")}}/{{ $AllNotificationData->id }}' class="btn notif_link"><img src="{{ url('public/assets/img/right-arrow.png')}}" class="img-responsive" alt="noti_arrow"></a>
												</div>
											</div>
										@endforeach
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- notifiaction section end here-->
				

@include('Template.footer_parent') 				