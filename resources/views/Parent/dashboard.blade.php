@include('Template.header_parent')

				<!-- banner section start here-->
				<section class="banner_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>DASHBOARD</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!-- pie chart section start here-->
				<section class="piechart_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12 topinnersect">
								<div class="col-md-12 topsecthed">
									<div class="col-md-6 addchild">
										<div class="btnadd">
											<a href="#." class="dropdown">
												<button
													onclick="getUserInCode({{$profDash['user_id']}})" class="btn submit_btn_sign_in" >Add Child</button>

												<!-- <button type="button" class="btn submit_btn_sign_in" data-toggle="dropdown" data-target="childCode" alt="Add Child"/>Add Child</button>
												<div class="dropdown-menu search_box_set">  
													<div class="input-group">
														<input type="text" class="form-control" placeholder="Enter Code">
													</div>
												</div> -->
											</a>
										</div>
									</div>
									<div class="col-md-6 slctchild">
										<div class="row">
											<div class="col-md-10 slctchild-btm">
												<div class="input_form_set input_new_set_select">
												<span>CHILD :</span>

												<select class="form-control select_tag_new" id="dynamic_select">
													@if(count($child) > 0)
														@foreach($child as $childData)
															<option value="{{$childData->student_id}}" {{$childId == $childData->student_id? 'selected="selected"' : "" }}>{{$childData->first_name}}</option>
														@endforeach
													@else
													<option value="0">Select Child</option>
													@endif
												</select>
										
											</div>
											</div>
											<div class="col-md-2 srcfild">
												<!-- <a href="#." class="dropdown new_drop_search">
													<img src="{{url('public/assets/ParentAssets/img/search.png')}}" class="img-responsive" data-toggle="dropdown" alt="search"/>
													<div class="dropdown-menu search_box_set">  
														<div class="input-group">
															<input type="text" class="form-control" placeholder="Search">
															<div class="input-group-btn">
															<button class="btn search_btn_btn" type="submit">
																<i class="fa fa-search" aria-hidden="true"></i>
															</button>
															</div>
														</div>
													</div>
												</a> -->
											</div>
										</div>
									</div>
							</div>
						</div>
							<div class="col-md-12">
								<div class="piechart_details">
									<div class="row">
										<div class="col-sm-6">
											<div class="dash_profile_datils_account">
												<div class="dash_bora_profil_user">
													<div class="profile_details_img_with_btn">
														<span class="dash_img_profile_img">
															<img src="{{url($profDash['profile_file_path'])}}" class="img-responsive" alt="profile">
														</span>
														<a href="{{url ('ParentMyProfile')}}" class="btn small_comm_btn">View Profile</a>
													</div>
													<div class="dash_board_details">
														<h4>{{$profDash['name']}}</h4>
														<span>{{$profDash['gender']}}</span>
														<div class="star_ratting_table_views">
															<!-- <ul>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
															</ul>
															<small>3.56/5</small> -->
														</div>
													</div>
												</div>
												<div class="dash_bora_profil_user">
													<div class="profile_details_img_with_btn">
														<span class="dash_img_profile_img">
															<div class="account_img_set">
																<img src="{{url('public/assets/ParentAssets/img/my_account.png')}}" class="img-responsive" alt="profile">
															</div>
														</span>
														<a href="{{url('ParentMyAccount')}}" class="btn small_comm_btn">View Account</a>
													</div>
													<div class="dash_account_set">
														<h4>MY ACCOUNT </h4>
														<span></span>
													</div>
												</div>
											</div>
											
											</div>
											<div class="col-sm-6">
											<div class="pie_chart_details">
												<div class="pie_chart_heading">
													<h4>{{$childName}} PLAN</h4>
												</div>
												<div class="calender_form_set_new">
													<div class="row">
														<div class="col-md-8">
															<!-- <div class="input_new_set_search">
																<input type="text" class="form-control" placeholder="Search"/>
															</div> -->
														</div>
														<div class="col-md-4">
															<!-- <div class="input_new_set_select">
																<select class="form-control select_tag_new">
																	<option>Daily</option>
																	<option>Monthly</option>
																	<option>Yearly</option>
																</select>
															</div> -->
														</div>
													</div>
												</div>
												<div class="piechart-demo">
													<!-- <img src="{{ url('public/assets/img/calender-img.jpg')}}" class="img-responsive" alt="piechart"/> -->
												</div>
													<div class='calendar1'></div>
												</div>
												<div class="calnder-btn_set">
													<a href="{{url('ParentMyPlan')}}" class="btn common_btn">Add Plan</a>
												</div>
											</div>
										</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- pie chart section end here-->
				<!-- Calender script on -->
				@include('Student.calendar_code') 
				<!-- Calender script on -->

<script type="text/javascript">
		function getUserInCode(id){
			document.getElementById("parent_id").value=id;
			$('#childCode').modal('show');
		}
</script>

					  <!-- Add Child popup Start Here -->
					  <div class="modal fade" id="childCode" role="dialog">
							<div class="modal-dialog change_password_popup_width">
								<!-- Modal content-->
								<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
									<img src="{{url('public/assets/ParentAssets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
									</button>
								</div>
								<div class="modal-body">
									<div class="model_details">
										<div class="section_heading_sec">
											<h4>Add Child</h4>
										</div>
										<form class="form1" method="post" class="form1" enctype="multipart/form-data">
										@csrf
											<input type="hidden" name="parent_id" id="parent_id">

											<div class="model_content">
												<div class="add_topic_form">
													<div class="row">
														<div class="col-md-12">
															<div class="form_set">
																<span>Child Code :</span>
																<div class="input_box_set_popup required_check">
																	<input type="text"  name ="childcode" class="form-control required_check" placeholder="Enter Child Code" id="childcode"/>

																</div>
															</div>
														</div>
														
														<div class="col-md-12">
															<span class="text-danger">
																	<strong class="errormsg"></strong>
															</span>
														</div>
														<div class="col-md-12">
															<div class="main_profil_btn">
																<input type ="submit" class="btn save_cancel_btn margin-right-10px" value="Add" id="submitchild">
																<a href="#." class="btn save_cancel_btn" data-dismiss="modal">Cancel</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
								</div>
							</div>
							</div>
							<!--popup end here-->
						</div>
					  <!-- Add Child popup end Here -->

<script>
    $(function(){
      $('#dynamic_select').on('change', function () {
		  var id = $(this).val(); // get selected value
		  var APP_URL = {!! json_encode(url('/ChildCalender')) !!} + '/' + id;
		//   alert(APP_URL);
		  
          if (id) { 
              window.location = APP_URL; 
          }
        
      });
    });
</script>

<script>
	jQuery(document).ready(function(){
			// $('.form1').validate({
			// 	rules: {
			// 		childcode: {
			// 			required: true
			// 		}
			// 	}
			// });
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
					
			$("#submitchild").click(function(e){
				if ($('.form1').valid()) {
					e.preventDefault();
					var user_id = $("input[id=parent_id]").val();
					var childcode = $("input[id=childcode]").val();
					
					$.ajax({
							type:'POST',
							url:'AddChild',
							data:{user_id:user_id, childcode:childcode },
							success:function(data){
								// console.log(data);
								if(data.status == '1')
								{	
									if(data.role == '0') {
										$('#userRoleMsg').html('<a href="{{url("ParentDashboard")}}" class="btn popup_coom_btn">OK</a>');
									}
									$('#childCode').modal('hide');
									$('#pass-success-msg').modal('show');
									$('#showsuccessmsg').html(data.msg);
								}
								if(data.status == '0')
								{
									$('#pass-error-msg').modal('show');
									$('#showerrormsg').html(data.msg);
									document.getElementById('childcode').value = "";
								}
							}
					});
				}
			});
	});
</script>

@include('Template.footer_parent')