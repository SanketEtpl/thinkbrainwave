@include('Backend/header');
@include('Backend/sidebar');

        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="users">Courses</a>
                        </li>
                        <li class="crumb-icon">
                            <a href="{{url('admin/dashboard')}}">
                                <span class="glyphicon glyphicon-home"></span>
                            </a>
                        </li>
                        <li class="crumb-link">
                            <a href="{{url('admin/dashboard')}}">Home</a>
                        </li>
                        <li class="crumb-trail">Courses</li>
                    </ol>
                </div>
               
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <div id="content" class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">

                        <div class="panel">
                             @php
                                                             
                                    if($singleCourse!=NULL)
                                    {
                                        
                                        $status = $singleCourse['Status'];
                                        $action = 'Update';
                                    }
                                    else
                                    {
                                        
                                        $status = 3;
                                        $action = 'Add';
                                    }
                                    @endphp
                                   
                                     </br>
                                    <h3><center>{{$action}} Course Details</center></h3>
                           
                            <div class="panel-body">


                                <form class="form-horizontal form1" action = "{{url('admin/courses/addCourse')}}" id="form1"  method = "post">
                                    @csrf

                                    <input type="hidden" name="subject_id" id="subject_id" value="{{$singleCourse['subject_id']}}" >

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Grade</label>
                                        <div class="col-lg-4">
                                            <select name="grade" class="form-control required_check">
                                                <option value="">Select Option</option>
                                                @foreach ($grade as $grades)
                                                <option value="{{$grades->id}}" {{$singleCourse['grade'] == $grades->id? 'selected="selected"' : "" }}>{{$grades->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                   
                                        
                                    </div>



                                    <div class="form-group">
                                   
                                        <label class="col-lg-2 control-label">Name</label>
                                        <div class="col-lg-4">
                                             <input type="text" id="name" name="name" class="form-control required_letter" placeholder="Enter Name" value="{{$singleCourse['name']}}">
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Description</label>
                                        <div class="col-lg-8">
                                             <textarea class="form-control required_check" id="description" name="description" placeholder="Description" rows="3">{{$singleCourse['description']}}</textarea>
                                        </div>
                                       
                                        
                                    </div>

                                       <div class="form-group">
                                        <label class="col-lg-2 control-label" for="textArea3">Active</label>
                                        <div class="col-lg-4">
                                            <input type="radio"   name="status" @if($status==1) checked="" @elseif($status==3) checked="" @endif   value="1">Yes
                                            <input type="radio" name="status" @if($status==0) checked=""  @endif value="0">No
                                        </div>
                                    </div>

                                    

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="textArea3"></label>
                                        <div class="col-lg-2">
                                             <input type="submit" name="submit" value="Submit"  class="btn btn-success">
                                           
                                            @if($action == 'Update')
                                            <a href="{{url('admin/courses')}}"><button type="button" class="btn btn-warning">Cancel</button></a>
                                            @else
                                             <button type="button" class="btn btn-warning" onclick="resetformnew();">Reset</button>
                                            @endif

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    
                </div>


                

                    <div class="col-md-12">

                        <div class="panel">
                        </br>
                               <h3><center>Courses List</center></h3>
                            <div class="panel-body">
                             

                                <table class="table table-striped table-hover" id="datatable3" cellspacing="0" width="100%">
                                 <thead>
                                    <tr>

                                      <th>Id</th>
                                      <th>Course Name</th>
                                      <th>Grade</th>
                                      <th>Description</th>
                                      <th>Status</th>
                                      <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    @foreach ($couses as $index => $coursesData)
                                    <tr>
                                       <td>{{$index +1}}</td>
                                       <td>{{ $coursesData->Name }}</td>
                                       <td>{{ $coursesData->name }}</td>
                                       <td>{{ $coursesData->description }}</td>
                                        
                                        <td>
                                            @if($coursesData->status == 0)
                                            <a href="{{url('admin/courses/changeStatus')}}/{{$coursesData->Sid}}/{{$coursesData->status}}"><button class="btn btn-danger"'><i class="glyphicon glyphicon-ban-circle"  title="Dactive"></i></button></a>
                                            @elseif($coursesData->status == 1)
                                            <a href="{{url('admin/courses/changeStatus')}}/{{$coursesData->Sid}}/{{$coursesData->status}}"><button class="btn btn-primary" ><i class="glyphicon glyphicon-check" title="Active"></i></button></a>   
                                            @endif     
                                        </td>
                                        <td>
                                           <a href="{{url('admin/courses/editCourse')}}/{{$coursesData->Sid}}"><button class="btn btn-warning" ><i class="glyphicon glyphicon-pencil" title="Edit"></i>
                                          </button></a>
                                          <a href="{{url('admin/courses/deleteCourse')}}/{{$coursesData->Sid}}"><button class="btn btn-danger" ><i class="glyphicon glyphicon-remove" title="Delete"></i>
                                          </button></a>
                                       </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                              </table>
                            </div>
                        </div>

                    </div>

                    
   

            </div>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->


    </div>
    <!-- End: Main -->
@include('Backend/footer');