@include('Backend.header')

    <!-- Start: Main -->
    <div id="main" class="animated fadeIn">

        <!-- Start: Content-Wrapper -->
        <section class="no_left_margin" id="content_wrapper">

            <!-- begin canvas animation bg -->
            <div id="canvas-wrapper">
                <canvas id="demo-canvas"></canvas>
            </div>

            <!-- Begin: Content -->
            <section id="content">
<!--admin-form-->
                <div class="admin-form theme-info coustom_pop_width" id="login1">

                    <div class="row mb15 table-layout">

                        <div class="col-xs-6 va-m pln float_width no_padd">
                            <a href="dashboard.html" title="Return to Dashboard" class="float_width margin_img">
                                <img src="{{ url('public/assets/backend/img/logos/logo_white.png')}}" class="img-responsive w250 cet_margin " title="Admin Login" >
                            </a>
                        </div>

                        

                    </div>

                    <div class="panel panel-info mt10 br-n">
                        <div>

                        @if ($message = Session::get('success'))
                                            <div class="alert alert-success fade in alert-dismissible">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                                <p>{{ $message }}</p>
                                            </div>
                                        @endif
                                        @if ($message = Session::get('error'))
                                            <div class="alert alert-danger fade in alert-dismissible">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                                <p>{{ $message }}</p>
                                            </div>
                                        @endif
                                    </div>

                        <!-- end .form-header section -->
                        <form method="post" action="admin/adminlogin" >
                        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                            <div class="panel-body bg-light cust_box_padd">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="section">
                                            <label for="username" class="field-label text-muted fs18 mb10">Email Address</label>
                                            <label for="username" class="field prepend-icon">
                                                <input type="text" name="username" id="username" class="gui-input" placeholder="Enter username">
                                                <label for="username" class="field-icon"><i class="fa fa-user"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- end section -->

                                        <div class="section">
                                            <label for="username" class="field-label text-muted fs18 mb10">Password</label>
                                            <label for="password" class="field prepend-icon">
                                                <input type="password" name="password" id="password" class="gui-input" placeholder="Enter password">
                                                <label for="password" class="field-icon"><i class="fa fa-lock"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- end section -->

                                    </div>
                                    
                                </div>
                            </div>
                            <!-- end .form-body section -->
                            <div class="panel-footer clearfix p10 ph15">
                                 <label>
                                      <button type="submit"  class="button btn-primary mr10 pull-right cust_btn_set">Login</button>
                                </label> 

								<label class="switch block switch-primary pull-right input-align mt10 cout_forget_mar_hei">
                                    <span class="forgotpass"> <a href="#." class="" data-toggle="modal" data-target="#forgot_password_popup">Forgot Password ?</a></span>
								</label>		

                                <!-- <label class="switch block switch-primary pull-left input-align mt10">
                                    <input type="checkbox" name="remember" id="remember">
                                    <label for="remember"  data-on="YES" data-off="NO"></label>
                                    <span>Remember me</span>
                                </label> -->

                            </div>
                            <!-- end .form-footer section -->
                        </form>
                    </div>
                </div>


             <!-- forgot password popup Start Here -->
<div class="modal fade" id="forgot_password_popup" role="dialog" data-backdrop="static">
 <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><center>Forgot Password<center></h4>
		 
        </div>
        <div class="modal-body">
		<span><center class="Showerror error"></center></span>
		<span><center class="Showsuccess"></center></span>
        <form class="form1 form-horizontal" id="myForm"  method = "post">
                                    @csrf
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label"></label>
                                        <div class="col-lg-4">
                                     </div>
                                    </div>
                                   

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Email Address</label>
                                        <div class="col-lg-8">
                                             <input type="text" id="emailID" name="emailID" onkeypress="clearerrorN()"  class="form-control" placeholder="Enter email">
                                             <span class='error errors' id="showErrorN"></span>
										</div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label" for="textArea3"></label>
                                        <div class="col-lg-6">
                                            <input type="button" id="forgotpass" value="Reset Password"  class="btn btn-success">
                                            
                                        </div>
                                    </div>

                                </form>
                   </div>
           </div>
    </div>
  </div>

<!-- forgot password popup end Here -->



            </section>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->

    </div>
    <!-- End: Main -->
					



@include('Backend.footer')
<script>
jQuery(document).ready(function(){
				
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
					
					$("#forgotpass").click(function(){
                        var email = $("#emailID").val();
                        if(email=='')
						{
							$(".errors").html('This field is required.');
							return false;
						}
						else{
							$(".errors").html('');

						}
                        if ($('.form1').valid()) 
						{
                            $.ajax({
										type:'POST',
										url:'{{ url("forgotPassword")}}',
										data:{email:email},
										success:function(data){
											if(data.status==1)
											{ 
                                                $('#forgot_password_popup').modal('hide');
											}
											if(data.status==0)
											{
                                                $(".Showerror").html('Email is not exist.');
                                                $("#emailID").val('');
											}
											
										}
									});
                        }
                    });
                })
                function clearerrorN()
				{
                        
						$("#showErrorN").html('');
                        $(".Showerror").html('');
					
								
				}

</script>