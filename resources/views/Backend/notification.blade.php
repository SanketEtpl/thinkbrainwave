@include('Backend/header');
@include('Backend/sidebar');



<!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{url('AdminAllNotification')}}">Notifications</a>
                        </li>
                        <li class="crumb-icon">
                            <a href="{{url('admin/dashboard')}}">
                                <span class="glyphicon glyphicon-home"></span>
                            </a>
                        </li>
                        <li class="crumb-link">
                            <a href="{{url('admin/dashboard')}}">Home</a>
                        </li>
                        <li class="crumb-trail">Notifications</li>
                    </ol>
                </div>
               
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <div id="content" class="animated fadeIn">
			<div class="row">
							<div class="col-md-12">
								<div class="section_details">
									<div class="notification_blog">
										@foreach ($AllNotificationData as $AllNotificationData)
											<div class="notifiaction_info_list_active">
												<span class="date_of_notification_active">
													
												
													<!-- {{ date('d-m-Y h:i A', strtotime($AllNotificationData->created_at)) }} -->
												</span>
												<div class="@if($AllNotificationData->status=='0') noti_list_pass @else noti_list_pass_read @endif">
													<h4>
														@if($AllNotificationData->notification_type=='AdminNotification') 
															{{ config('constants.AdminNotification') }} 
														
														@elseif($AllNotificationData->notification_type=='SessionBook') 
															{{ config('constants.SessionBook') }}

														@elseif($AllNotificationData->notification_type=='SessionAssign') 
															{{ config('constants.SessionAssign') }}
														
														@elseif($AllNotificationData->notification_type=='Session_1_Hour_Left') 
															{{ config('constants.Session_1_Hour_Left') }}
														
														@elseif($AllNotificationData->notification_type=='Session_5_Min_left') 
															{{ config('constants.Session_5_Min_left') }}
														
														@elseif($AllNotificationData->notification_type=='SessionStart') 
															{{ config('constants.SessionStart') }}
														
														@elseif($AllNotificationData->notification_type=='SessionStop') 
															{{ config('constants.SessionStop') }}
														
														@elseif($AllNotificationData->notification_type=='SessionComplete') 
															{{ config('constants.SessionComplete') }}
														
														@elseif($AllNotificationData->notification_type=='SessionTutorCancel') 
															{{ config('constants.SessionTutorCancel') }}
														
														@elseif($AllNotificationData->notification_type=='SessionStudentCancel') 
															{{ config('constants.SessionStudentCancel') }}
														
														@elseif($AllNotificationData->notification_type=='RatingByTutor') 
															{{ config('constants.RatingByTutor') }}
														
														@elseif($AllNotificationData->notification_type=='RatingByStudent') 
															{{ config('constants.RatingByStudent') }}
														
														@elseif($AllNotificationData->notification_type=='SessionParentCancel') 
														{{ config('constants.SessionParentCancel') }}
														
														@endif
													</h4>
													<p class="textLimit">{{ $AllNotificationData->message }}</p>
													<a href='{{ url("ViewAdminNotification")}}/{{ $AllNotificationData->id }}' class="btn notif_link"><img src="{{ url('public/assets/img/right-arrow.png')}}" class="img-responsive" alt="noti_arrow"></a>
												</div>
											</div>
										@endforeach
										
									</div>
								</div>
							</div>
						</div>
            </div>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->


    </div>
    <!-- End: Main -->
@include('Backend/footer');
