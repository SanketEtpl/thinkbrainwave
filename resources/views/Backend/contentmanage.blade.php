@include('Backend/header');

@include('Backend/sidebar');




        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="users">Content Management</a>
                        </li>
                        <li class="crumb-icon">
                            <a href="dashboard">
                                <span class="glyphicon glyphicon-home"></span>
                            </a>
                        </li>
                        <li class="crumb-link">
                            <a href="dashboard">Home</a>
                        </li>
                        <li class="crumb-trail">Content Management</li>
                    </ol>
                </div>
               
            </header>
            <!-- End: Topbar -->
            @php
                                     
            if($ContentEditData!=NULL)
            {
                $title = $ContentEditData->title;
                $status = $ContentEditData->status;
                $id = $ContentEditData->id;
                $description = $ContentEditData->description;
                $action = 'Update';
            }
            else
            {
                $title = '';
                $status = 3;
                $id = '';
                $action = 'Add';
                $description = '';
            }
            @endphp

            <!-- Begin: Content -->
            <div id="content" class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="panel">
                            </br>
                                <h3 ><center>{{$action}} Content Management</center></h3>
                               
                            <div class="panel-body">
                                <form class="form-horizontal form1" action = "{{url('admin/contentmanagement/addContent')}}" id="form1" method = "post">
                                    @csrf


                                    <div class="form-group">
                                        <label class="col-lg-2 control-label"></label>
                                        <div class="col-lg-4">
                                    <input type="hidden" id="contentID" value="{{$id}}" name="contentID" class="form-control" >
                                     </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Title</label>
                                        <div class="col-lg-4">
                                    <input type="text" id="title" name="title" class="form-control required_check" value="{{$title}}" >
                                     </div>
                                    </div>

                                  
                                    
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Description</label>
                                        <div class="col-lg-8">
                                        <textarea class="form-control required_check" id="description" name="description"  placeholder="Description" rows="3" required>{{$description}}</textarea>
                                       </div>
                                    </div>
                                    

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="textArea3">Active</label>
                                        <div class="col-lg-4">
                                            <input type="radio"   name="status" @if($status==1) checked="" @elseif($status==3) checked="" @endif   value="1">Yes
                                            <input type="radio" name="status" @if($status==0) checked=""  @endif value="0">No
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="textArea3"></label>
                                        <div class="col-lg-2">
                                            <input type="submit" name="submit" value="Submit"  class="btn btn-success">
                                           
                                            @if($action == 'Update')
                                            <a href="{{url('admin/grade')}}"><button type="button" class="btn btn-warning">Cancel</button></a>
                                            @else
                                             <button type="button" class="btn btn-warning" onclick="resetformnew();">Reset</button>
                                            @endif

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    
                </div>

                <div class="col-md-13">
                            <div class="panel panel-visible" id="spy3">
                            </br>
                                <h3><center>Content List</center></h3>
                                 </br>
                                
                                <div class="panel-body pn">
                                    <table class="table table-striped table-hover" id="datatable3" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Content ID</th>
                                                <th>Title</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                           @php
                                            $i = 1;
                                            @endphp
                                            @if(isset($content)) 
                                            @foreach ($content as $contents)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$contents->title}}</td>
                                                <td>

                                                @if($contents->status == 0)
                                                <a href="{{url('admin/contentmanagement/changeStatus')}}/{{$contents->id}}/{{$contents->status}}"><button class="btn btn-danger"'><i class="glyphicon glyphicon-ban-circle"  title="Dactive"></i></button></a>
                                                @elseif($contents->status == 1)
                                                <a href="{{url('admin/contentmanagement/changeStatus')}}/{{$contents->id}}/{{$contents->status}}"><button class="btn btn-primary" ><i class="glyphicon glyphicon-check" title="Active"></i></button></a>   
                                                @endif   
                                             </td>
                                                <td>
                                                   <a href="{{url('admin/contentmanagement/editContent')}}/{{$contents->id}}"><button class="btn btn-warning" ><i class="glyphicon glyphicon-pencil"  title="Edit"></i>
                                                  </button></a>
                                                  <a href="{{url('admin/contentmanagement/deleteContent')}}/{{$contents->id}}"><button class="btn btn-danger" ><i class="glyphicon glyphicon-remove" title="Delete"></i>
                                                  </button>  </a>
                                                  
                                              </td>
                                                
                                            </tr>
                                            @endforeach
                                            @endif
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                </div>               

            </div>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->


    </div>
    <!-- End: Main -->
@include('Backend/footer');