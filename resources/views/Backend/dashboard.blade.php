@include('Backend/header');

@include('Backend/sidebar');



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <!-- <li class="crumb-active">
                            <a href="dashboard.html">Dashboard</a>
                        </li> -->
                        <li class="crumb-icon">
                            <a href="dashboard.html">
                                <span class="glyphicon glyphicon-home"></span>
                            </a>
                        </li>
                        <li class="crumb-link">
                            <a href="index.html">Home</a>
                        </li>
                        <li class="crumb-trail">Dashboard</li>
                    </ol>
                </div>
               
            </header>
            <!-- End: Topbar -->
			<!-- User -->
			<section class="my_account_all_us_details">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="my_account_data">
								<div class="row">
									<div class="col-md-4 col-sm-6 col-xs-6 dash_dta_border_right">
										<div class="dash_bora_profil_user_sec">
											<span class="dash_img_profile_img">
												<img style="width:100%; height:100%;" src="../public/assets/img/profile-photo.png" class="img-responsive" alt="profile">
											</span>
											<div class="dash_board_details_sec">
												<h4>John Joseph</h4>
												<span>Male</span>
												<span>03/08/1988</span>
											</div>
										</div>
									</div>
									<div class="col-md-8 col-sm-6 col-xs-6">
										<div class="col-md-12">
											<div class="col-md-8">
												<div class="float_width user_count_name_no">
													<div class="sub_user_name_title">Students</div>
													<div class="sub_user_name_count">3567</div>
												</div>
												<div class="float_width user_count_name_no">
													<div class="sub_user_name_title">Parent</div>
													<div class="sub_user_name_count">1034</div>
												</div>
												<div class="float_width user_count_name_no">
													<div class="sub_user_name_title">Tutors</div>
													<div class="sub_user_name_count">3567</div>
												</div>
											</div>
											<div class="col-md-4">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- User -->
			
			
            <!-- Begin: Content -->
			
			
           
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->

<!-- -->
 <div class="row mb10">
                    <div class="col-md-3">
                        <div class="panel bg-alert light of-h mb10">
                            <div class="pn pl20 p5">
                                <div class="icon-bg"> <i class="fa fa-comments-o"></i> </div>
                                <h2 class="mt15 lh15"> <b>523</b> </h2>
                                <h5 class="text-muted">Comments</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel bg-info light of-h mb10">
                            <div class="pn pl20 p5">
                                <div class="icon-bg"> <i class="fa fa-twitter"></i> </div>
                                <h2 class="mt15 lh15"> <b>348</b> </h2>
                                <h5 class="text-muted">Tweets</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel bg-danger light of-h mb10">
                            <div class="pn pl20 p5">
                                <div class="icon-bg"> <i class="fa fa-bar-chart-o"></i> </div>
                                <h2 class="mt15 lh15"> <b>267</b> </h2>
                                <h5 class="text-muted">Reach</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel bg-warning light of-h mb10">
                            <div class="pn pl20 p5">
                                <div class="icon-bg"> <i class="fa fa-envelope"></i> </div>
                                <h2 class="mt15 lh15"> <b>714</b> </h2>
                                <h5 class="text-muted">Comments</h5>
                            </div>
                        </div>
                    </div>
                </div>
<!-- -->
		
		
		
		
    </div>
    <!-- End: Main -->
@include('Backend/footer');