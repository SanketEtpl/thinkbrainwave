<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Admin</title>
    <meta name="keywords" content="HTML5 Bootstrap 3 Admin Template UI Theme" />
    <meta name="description" content="AdminDesigns - A Responsive HTML5 Admin UI Framework">
    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

<style>             
.help-block             
{               
color:red !important;           
}           
.error          
{               
color:red;          
}       
</style>

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ url('public/assets/backend/skin/default_skin/css/theme.css')}}">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="{{ url('public/assets/backend/admin-tools/admin-plugins/admin-panels/adminpanels.css')}}">

 <!-- Datatables CSS -->
    <link rel="stylesheet" type="text/css" href="{{ url('vendor/backend/plugins/datatables/media/css/dataTables.bootstrap.css')}}">

    <!-- Datatables Editor CSS -->
    <link rel="stylesheet" type="text/css" href="{{ url('vendor/backend/plugins/datatables/extensions/Editor/css/dataTables.editor.css')}}">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="{{ url('public/assets/backend/admin-tools/admin-forms/css/admin-forms.css')}}">
	<!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ url('public/assets/backend/css/style.css')}}">

    <!-- Favicon -->
    <link rel="icon" type="imge/png" sizes="16x16" href="{{ url('public/assets/img/favicon.png')}}"/>

    <script type="text/javascript" src="{{ url('vendor/backend/jquery/jquery-1.11.1.min.js')}}"></script>
    <style>				
    .help-block 			
    {				
        
     color:red !important;		
	}		
    .error		
    	{				
              color:red;			
          }		
</style>

</head>

<body class="external-page sb-r-c onload-check">

  <!-- Start: Main -->
    <div id="main">
@php
$val=Session::get('AdminloginSession');

	$id = '1';
    $role = '1';
	$CountUnreadNotificationData = \App\Http\Controllers\AllCommonDataController::NotificationUnreadCount($id,$role);	

@endphp
@if(isset($val))


        <!-- Start: Header -->
        <header class="navbar navbar-fixed-top bg-light">
            <div class="navbar-branding">
                <a class="navbar-brand" href="{{ url('')}}"> <img class="img-responsive header_logo_mar" src="../public/assets/img/logo_header120.png" alt="logo">
                </a>
                <span id="toggle_sidemenu_l" class="glyphicons glyphicons-show_lines"></span>
                <ul class="nav navbar-nav pull-right hidden">
                    <li>
                        <a href="#" class="sidebar-menu-toggle">
                            <span class="octicon octicon-ruby fs20 mr10 pull-right "></span>
                        </a>
                    </li>
                </ul>
            </div>
           
           
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown margin-right_10">
                    <a href="{{url ('AdminAllNotification')}}">
                        <div class="notifiaction_set">
                            <img src="{{url('public/assets/ParentAssets/img/nitification.png')}}" class="img-responsive icon_set notification_dis_cust" alt="nitification"/>
                            <span class="badge notification_dis_cust_ab">{{$CountUnreadNotificationData}}</span>
                        </div>
					</a>
                </li>
               
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle fw600 p15 cust_user_log" data-toggle="dropdown"> 
                        <span>@if(isset($val)) {{$val['admin_session_user_name']}} @endif</span>
                        <span class="caret caret-tp hidden-xs"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-persist pn w250 bg-white" role="menu">
                        
                        <li class="br-t of-h">
                            <a href="{{url('admin/logout')}}" class="fw600 p12 animated animated-short fadeInDown">
                                <span class="fa fa-power-off pr5"></span> Logout </a>
                        </li>
                    </ul>
                </li>

            </ul>

        </header>
        @endif
        <!-- End: Header -->