<div class="modal fade" id="myModal" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><center>Change Password<center></h4>
		 
        </div>
        <div class="modal-body">
		<span><center class="Showerror error"></center></span>
		<span><center class="Showsuccess"></center></span>
        <form class="form1 form-horizontal" id="myForm"  method = "post">
                                    @csrf
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label"></label>
                                        <div class="col-lg-4">
                                     </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Old Password</label>
                                        <div class="col-lg-8">
                                             <input type="password" id="old_password" onchange="clearerror()" name="old_password" class="form-control required_check" placeholder="Enter old password">
                                             <span class='error errors' id="showError"></span>
										</div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">New Password</label>
                                        <div class="col-lg-8">
                                             <input type="password" id="new_password" name="new_password" onchange="clearerrorN()"  class="form-control required_check" placeholder="Enter new password">
                                             <span class='error errors' id="showErrorN"></span>
										</div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Confirm Password</label>
                                        <div class="col-lg-8">
                                             <input type="password" id="retype_password" name="retype_password" onchange="clearerrorC()" class="form-control required_check" placeholder="Enter confirm password">
											 <span class='error errors' id="showErrorC"></span>
										</div>
                                    </div>
                                    

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="textArea3"></label>
                                        <div class="col-lg-4">
                                            <input type="button" id="AdminPassword" value="Update"  class="btn btn-success">
                                            <button type="button" class="btn btn-warning" onclick="close()" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </form>
        </div>
      </div>
      
    </div>
  </div>


              

  <script>
				jQuery(document).ready(function(){
					
				
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
					
					$("#AdminPassword").click(function(){

						var old = $("#old_password").val();
						var newpass = $("#new_password").val();
						var cnfrmpass = $("#retype_password").val();
						if(old=='')
						{
							$(".errors").html('This field is required.');
							return false;
						}
						else{
							$(".errors").html('');
						}
						if(newpass=='')
						{
							$(".errors").html('This field is required.');
							return false;
						}
						else{
							$(".errors").html('');
						}
						if(cnfrmpass=='')
						{
							$(".errors").html('This field is required.');
							return false;
						}
						else{
							$(".errors").html('');
						}
          
						if ($('.form1').valid()) 
						{
							
							var user_id = 1 ;
							var old_password = $("input[id=old_password]").val();
							var new_password = $("input[id=new_password]").val();
                            var retype_password = $("input[id=retype_password]").val();
              
							if(new_password==retype_password)
							{
								$.ajax({
										type:'POST',
										url:'{{ url("UpdatePassword")}}',
										data:{user_id:user_id, old_password:old_password,  new_password:new_password, retype_password:retype_password },
										success:function(data){
											console.log(data);
											//console.log(data.error);
											if(data.success)
											{
												   $(".Showsuccess").html('');
													$("#new_password").val('');
													$("#retype_password").val('');
													$("#old_password").val('');
													$('#myModal').modal('hide');

											}
											if(data.error)
											{
												    $(".Showerror").html('Old password does not exist.');
													$("#new_password").val('');
													$("#retype_password").val('');
													$("#old_password").val('');
													return false;
											}
											
										}
									});
								}
							else
							{
								$(".Showerror").html('New password and confirm password does not match.');
								$("#new_password").val('');
								$("#retype_password").val('');
							    return false;
							}
						}
					});
				});

				function clearerror()
				{
                        
						$("#showError").html('');
						$(".Showerror").html('');
				}
					function clearerrorN()
					{
							
							$("#showErrorN").html('');
							$(".Showerror").html('');
									
					}
				function clearerrorC()
				{
                        
						$("#showErrorC").html('');
				}
				function close()
				{
					alert(1);
					$(".Showsuccess").html('');
					$(".Showerror").html('');
					$("#new_password").val('');
					$("#retype_password").val('');
					$("#old_password").val('');
					$(".errors").html('');
               }
			</script>
			
  