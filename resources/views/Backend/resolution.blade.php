@include('Backend/header');

@include('Backend/sidebar');




        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="users">Resolution Center</a>
                        </li>
                        <li class="crumb-icon">
                            <a href="dashboard">
                                <span class="glyphicon glyphicon-home"></span>
                            </a>
                        </li>
                        <li class="crumb-link">
                            <a href="dashboard">Home</a>
                        </li>
                        <li class="crumb-trail">Resolution Center</li>
                    </ol>
                </div>
               
            </header>
            <!-- End: Topbar -->
          
            <!-- Begin: Content -->
            <div id="content" class="animated fadeIn">
                <div class="col-md-12">
                            <div class="panel panel-visible" id="spy3">
                            </br>
                                <h3><center>Resolution Center List</center></h3>
                                 </br>
                                
                                <div class="panel-body pn">
                                    <table class="table table-striped table-hover" id="datatable3" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Request ID</th>
                                                <th>Name</th>
                                                <th>Contact</th>
                                                <th>Email ID</th>
                                                <th>Country</th>
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                           @php
                                            $i = 1;
                                            @endphp
                                            @if(isset($resolution)) 
                                            @foreach ($resolution as $resolutions)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$resolutions->name}}</td>
                                                <td>{{$resolutions->contact}}</td>
                                                <td>{{$resolutions->email}}</td>
                                              <td>{{$resolutions->country_name}}</td>
                                                <td>

                                                  <button class="btn btn-warning showpopup" data-id="{{$resolutions->query_text}}" ><i class="glyphicon glyphicon-eye-open" title="View"></i>
                                                  </button> 

                                                  <a href="{{url('admin/resolution/deleteResolution')}}/{{$resolutions->id}}"><button class="btn btn-danger" ><i class="glyphicon glyphicon-remove" title="Delete"></i>
                                                  </button>  </a>

                                              

                                                </td>

                                                
                                            </tr>
                                            @endforeach
                                            @endif
                                            
                                        </tbody>
                                    </table>
                                </div>


                            </div>
                </div>               

            </div>
            <!-- End: Content -->

  
        </section>
        <!-- End: Content-Wrapper -->


    </div>


     <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Query</h4>
        </div>
        <div class="modal-body">
          <p id="showcontent"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

    <!-- End: Main -->
@include('Backend/footer');