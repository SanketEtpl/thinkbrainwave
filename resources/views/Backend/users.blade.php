@include('Backend/header');
@include('Backend/sidebar');

        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="users">Users</a>
                        </li>
                        <li class="crumb-icon">
                            <a href="{{url('admin/dashboard')}}">
                                <span class="glyphicon glyphicon-home"></span>
                            </a>
                        </li>
                        <li class="crumb-link">
                            <a href="{{url('admin/dashboard')}}">Home</a>
                        </li>
                        <li class="crumb-trail">Users</li>
                    </ol>
                </div>
               
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <div id="content" class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">

                        <div class="panel">

                             @php
                                                             
                                    if($singleData!=NULL)
                                    {
                                        
                                        $status = $singleData['status'];
                                        $action = 'Update';
                                    }
                                    else
                                    {
                                        
                                        $status = 3;
                                        $action = 'Add';
                                        
                                    }
                                    @endphp


                                   </br>
                                <h3 ><center>{{$action}} Users Details</center></h3> 
                                    
                           
                            <div class="panel-body">
                                 
                                <form class="form-horizontal form1" action = "{{url('admin/users/addUser')}}" id="form1"  method = "post">
                                    @csrf


                                    <input type="hidden" name="user_id" id="user_id" value="{{$singleData['user_id']}}">
                                   

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">User</label>
                                        <div class="col-lg-4">
                                            <select name="user" class="form-control required_check">
                                                <option value="">Select Option</option>
                                                <option value="2" {{ $singleData['user'] == 2? 'selected="selected"' : '' }}>Tutor</option>
                                                <option value="3" {{ $singleData['user'] == 3? 'selected="selected"' : '' }}>Student</option>
                                            </select>
                                        </div>

                                        <label class="col-lg-2 control-label">Name</label>
                                        <div class="col-lg-4">
                                             <input type="text" id="name" name="name" class="form-control required_letter" placeholder="Enter Name" value="{{$singleData['name']}}">
                                        </div>
                                       
                                    </div>



                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Contact Number</label>
                                        <div class="col-lg-4">
                                            <input type="text" id="contact" name="contact" class="form-control required_mobile" placeholder="Enter Contact Number" value="{{$singleData['contact']}}">
                                        </div>

                                        <label class="col-lg-2 control-label">Email Id</label>
                                        <div class="col-lg-4">
                                              <input type="text" id="email" name="email" class="form-control required_email" onchange="return emailvalid();" placeholder="Email address" value="{{$singleData['email']}}">
                                              <span id="error" class="error"></span>
                                          </div>
                                        

                                    </div>



                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Address</label>
                                        <div class="col-lg-4">
                                             <textarea class="form-control required_check" id="address" name="address" rows="3">{{$singleData['address']}}</textarea>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="textArea3">Active</label>
                                        <div class="col-lg-4">
                                            <input type="radio" name="status" value="1" {{ $status == 1? 'checked="checked"' : 'checked="checked"' }}>Yes
                                            <input type="radio" name="status" value="0" {{ $status == 0? 'checked="checked"' : '' }}>No
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="textArea3"></label>
                                        <div class="col-lg-2">
                                            <input type="submit" name="submit" value="Submit" class="btn btn-success">
                                              @if($action == 'Update')
                                            <a href="{{url('admin/users')}}"><button type="button" class="btn btn-warning">Cancel</button></a>
                                            @else
                                             <button type="button" class="btn btn-warning" onclick="resetformnew();">Reset</button>
                                            @endif
                                        </div>
                                    </div>


                                </form>
                            </div>
                        </div>

                    </div>

                    
                </div>


                <div class="row">

                    <div class="col-md-12">

                        <div class="panel">
                        </br>
                             <h3><center>Users List</center></h3>
                            <div class="panel-body">
                               

                                <table class="table table-striped table-hover" id="datatable3" cellspacing="0" width="100%">
                                 <thead>
                                    <tr>

                                      <th>User id</th>
                                      <th>Name</th>
                                      <th>Contact</th>
                                      <th>Email</th>
                                      <th>Address</th>
                                      <th>User</th>
                                      <th>Status</th>
                                      <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    @php
                                    $i =1;
                                    @endphp
                                    @if(isset($users))
                                    @foreach ($users as $userData)
                                    <tr>
                                       <td>{{ $i++ }}</td>
                                       <td>{{ $userData->Fname }}</td>
                                       <td>{{ $userData->Contact }}</td>
                                       <td>{{ $userData->Email }}</td>
                                       <td>{{ $userData->Address }}</td>
                                       <td>@if($userData->role_id == 2)  Tutor @elseif($userData->role_id == 3) Student @else  Parent @endif </td>
                                        <td>
                                            @if($userData->status == 0)
                                            <a href="{{url('admin/users/changeStatus')}}/{{$userData->id}}/{{$userData->status}}"><button class="btn btn-danger"'><i class="glyphicon glyphicon-ban-circle"  title="Dactive"></i></button></a>
                                            @elseif($userData->status == 1)
                                            <a href="{{url('admin/users/changeStatus')}}/{{$userData->id}}/{{$userData->status}}"><button class="btn btn-primary" ><i class="glyphicon glyphicon-check" title="Active"></i></button></a>   
                                            @endif     
                                        </td>
                                       <td>
                                       @if($userData->role_id != 4)
                                           <a href="{{url('admin/users/editUser')}}/{{$userData->id}}/{{$userData->role_id}}"><button class="btn btn-warning"><i class="glyphicon glyphicon-pencil"  title="Edit"></i>
                                          </button></a>
                                          @endif
                                          <a href="{{url('admin/users/deleteUser')}}/{{$userData->id}}"><button class="btn btn-danger" ><i class="glyphicon glyphicon-remove" title="Delete"></i>
                                          </button></a>
                                       </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                              </table>
                            </div>
                        </div>

                    </div>

                    
                </div>    

            </div>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->


    </div>
    <!-- End: Main -->
    <script>
    
    function emailvalid(){
        var oldemail = "{{$singleData['email']}}"; 
        var email = document.getElementById('email').value;
        if(email==oldemail)
        {
            document.getElementById('error').innerHTML = '';  
        }
        else
        {
            $.ajax({
                    type:'POST',
                    url:'{{ url("EmailValid") }}',
                    data:{email:email},
                    success:function(data){
                    
                    if( data.error!=0)
                    {
                    document.getElementById('email').value = '';
                    document.getElementById('error').innerHTML = data.msg;
                    }
                    else{
                        document.getElementById('error').innerHTML = '';
                    }
                        
                    }
                });
            }
       
    }
    
    </script>
@include('Backend/footer');