@include('Backend/header');
@include('Backend/sidebar');



<!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{url('AdminAllNotification')}}">Notifications</a>
                        </li>
                        <li class="crumb-icon">
                            <a href="{{url('admin/dashboard')}}">
                                <span class="glyphicon glyphicon-home"></span>
                            </a>
                        </li>
                        <li class="crumb-link">
                            <a href="{{url('admin/dashboard')}}">Home</a>
                        </li>
                        <li class="crumb-trail">Notifications details</li>
                    </ol>
                </div>
               
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <div id="content" class="animated fadeIn">
			<div class="row">
							<div class="col-md-12">
								<div class="section_details">
									<div class="notifiaction_info">
										<div class="notification_blog">
											<div class="stron_main_notifa">
												<p>

													@if($SingleNotificationData->notification_type=='AdminNotification') 
															{{ config('constants.AdminNotification') }} 
														
														@elseif($SingleNotificationData->notification_type=='SessionBook') 
															{{ config('constants.SessionBook') }}

														@elseif($SingleNotificationData->notification_type=='SessionAssign') 
															{{ config('constants.SessionAssign') }}
														
														@elseif($SingleNotificationData->notification_type=='Session_1_Hour_Left') 
															{{ config('constants.Session_1_Hour_Left') }}
														
														@elseif($SingleNotificationData->notification_type=='Session_5_Min_left') 
															{{ config('constants.Session_5_Min_left') }}
														
														@elseif($SingleNotificationData->notification_type=='SessionStart') 
															{{ config('constants.SessionStart') }}
														
														@elseif($SingleNotificationData->notification_type=='SessionStop') 
															{{ config('constants.SessionStop') }}
														
														@elseif($SingleNotificationData->notification_type=='SessionComplete') 
															{{ config('constants.SessionComplete') }}
														
														@elseif($SingleNotificationData->notification_type=='SessionTutorCancel') 
															{{ config('constants.SessionTutorCancel') }}
														
														@elseif($SingleNotificationData->notification_type=='SessionStudentCancel') 
															{{ config('constants.SessionStudentCancel') }}
														
														@elseif($SingleNotificationData->notification_type=='RatingByTutor') 
															{{ config('constants.RatingByTutor') }}
														
														@elseif($SingleNotificationData->notification_type=='RatingByStudent') 
															{{ config('constants.RatingByStudent') }}
														
														
														@endif
												</p>
											</div>
											<div class="view_notifaction_full_info">
												<span class="date_of_notification_sec">{{ date('d-m-Y h:i A', strtotime($SingleNotificationData->created_at)) }}</span>
												
												<p>{{ $SingleNotificationData->message }}</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->


    </div>
    <!-- End: Main -->
@include('Backend/footer');
