@include('Backend/header');

@include('Backend/sidebar');




        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="users">Subscription Plan</a>
                        </li>
                        <li class="crumb-icon">
                            <a href="dashboard">
                                <span class="glyphicon glyphicon-home"></span>
                            </a>
                        </li>
                        <li class="crumb-link">
                            <a href="dashboard">Home</a>
                        </li>
                        <li class="crumb-trail">Subscription Plan</li>
                    </ol>
                </div>
               
            </header>
            <!-- End: Topbar -->
            @php
                                     
            if($planEditData!=NULL)
            {
                $name = $planEditData[0]->plan_name;
                $status = $planEditData[0]->status;
                $priceyear = $planEditData[0]->price_per_year;
                $pricemonth = $planEditData[0]->price_per_month;
                $id = $planEditData[0]->Id;
                $usertype = $planEditData[0]->user_type;
                $description = $planEditData[0]->description;
                $action = 'Update';
                $period = $planEditData[0]->valid_period;
                $unit = $planEditData[0]->valid_period_unit;

            }
            else
            {
                $name = '';
                $status = 3;
                $priceyear = '';
                $pricemonth = '';
                $id = '';
                $usertype = '';
                $description = '';
                $action = 'Add';
                $period = '';
                $unit ='';

            }
            @endphp

            <!-- Begin: Content -->
            <div id="content" class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="panel">
                            </br>
                                <h3><center>{{$action}} Subscription Plan Details</center></h3>
                               
                            <div class="panel-body">
                                <form class="form-horizontal form1" action = "{{url('admin/subscription/addsubscription')}}" id="form1" method = "post">
                                    @csrf
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label"></label>
                                        <div class="col-lg-4">
                                    <input type="hidden" id="planid" value="{{$id}}" name="planid" class="form-control" >
                                     </div>
                                    </div>

                                    
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Plan Name</label>
                                        <div class="col-lg-4">
                                             <input type="text" id="plan" value ="{{$name}}" name="plan" class="form-control required_letter" placeholder="Enter Plan Name">
                                        </div>

                                        <label class="col-lg-2 control-label">Price per month</label>
                                        <div class="col-lg-4">
                                             <input type="text" id="price" value ="{{$pricemonth}}" name="price" class="form-control required_number" placeholder="Enter Price">
                                        </div>
                                    </div>
                                    


                                    <div class="form-group">

                                        <label class="col-lg-2 control-label">Price per year</label>
                                        <div class="col-lg-4">
                            <input type="text" id="priceyear" value ="{{$priceyear}}" name="priceyear" class="form-control required_number" placeholder="Enter Price">
                                        </div>

                                     <label class="col-lg-2 control-label">Valid Period</label>
                                        <div class="col-lg-2">

                                             <select name="period" class="form-control required_check">
                                                @if(isset($validperiod)) 
                                                @foreach($validperiod as $data)
                                                <option value="{{$data->id}}" {{ $data->id == $period? 'selected="selected"' : '' }}>{{$data->period}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>

                                        <div class="col-lg-2">
                                            
                                             <select name="unit" class="form-control required_check">
                                                <option value="Days"{{ 'Days' == $unit? 'selected="selected"' : '' }}>Days</option>
                                                <option value="Months"{{ 'Months' == $unit? 'selected="selected"' : '' }}>Months</option>
                                                <option value="Years"{{ 'Years' == $unit? 'selected="selected"' : '' }}>Years</option>
                                            </select>
                                        </div>

                                    </div>
                                     
                                     

                                    

                                     <div class="form-group">
                                        <label class="col-lg-2 control-label">User</label>
                                        <div class="col-lg-4">
                                            <select name="user" class="form-control required_check">
                                                @if(isset($userdata)) 
                                                @foreach($userdata as $user)
                                                <option value="{{$user->id}}" {{ $user->id == $usertype? 'selected="selected"' : '' }}>{{$user->role}}</option>
                                                @endforeach
                                                @endif
                                            </select>  
                                        </div>
                                    </div>


                                    <div class="form-group">
                                    <label class="col-lg-2 control-label">Plan Description</label>
                                        <div class="col-lg-8">
                                             <textarea class="form-control required_check"  placeholder="Enter Plan Description" id="description" name="description" rows="3">{{$description}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="textArea3">Active</label>
                                        <div class="col-lg-4">
                                            <input type="radio"   name="status" @if($status==1) checked="" @elseif($status==3) checked="" @endif   value="1">Yes
                                            <input type="radio" name="status" @if($status==0) checked=""  @endif value="0">No
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="textArea3"></label>
                                        <div class="col-lg-2">
                                            <input type="submit" name="submit" value="Submit" class="btn btn-success">
                                            @if($action == 'Update')
                                            <a href="{{url('admin/subscription')}}"><button type="button" class="btn btn-warning">Cancel</button></a>
                                            @else
                                             <button type="button" class="btn btn-warning" onclick="resetformnew();">Reset</button>
                                            @endif
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    
                </div>

                <div class="col-md-12">
                            <div class="panel panel-visible" id="spy3">
                            </br>
                                <h3><center>Subscription Plan List</center></h3>
                                 </br>
                                
                                <div class="panel-body pn">
                                    <table class="table table-striped table-hover" id="datatable3" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S No.</th>
                                                <th>Name</th>
                                                <th>Price (Month)</th>
                                                <th>Price (Year)</th>
                                                <th>Valid to</th>
                                                <th>User</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                           @php
                                            $i = 1;

                                            @endphp
                                            @if(isset($plandata)) 
                                            @foreach ($plandata as $plan)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$plan->plan_name}}</td>
                                                <td>${{$plan->price_per_month}}</td>
                                                <td>${{$plan->price_per_year}}</td>
                                                <td>{{$plan->period}} {{$plan->valid_period_unit}}</td>
                                                <td>{{$plan->role}}</td>
                                                 <td>{{$plan->description}}</td>
                                                <td>
                             
                                                @if($plan->status == 0)
                                                <a href="{{url('admin/subscription/changeStatus')}}/{{$plan->Id}}/{{$plan->status}}"><button class="btn btn-danger"'><i class="glyphicon glyphicon-ban-circle"  title="Dactive"></i></button></a>
                                                @elseif($plan->status == 1)
                                                <a href="{{url('admin/subscription/changeStatus')}}/{{$plan->Id}}/{{$plan->status}}"><button class="btn btn-primary"><i class="glyphicon glyphicon-check" title="Active"></i></button></a>   
                                                @endif   
                                             </td>
                                                <td>
                                                   <a href="{{url('admin/subscription/editsubscription')}}/{{$plan->Id}}"><button class="btn btn-warning" ><i class="glyphicon glyphicon-pencil" title="Edit"></i>
                                                  </button></a>
                                                  <a href="{{url('admin/subscription/deletesubscription')}}/{{$plan->Id}}"><button class="btn btn-danger" ><i class="glyphicon glyphicon-remove" title="Delete"></i>
                                                  </button>  </a>
                                              </td>
                                                
                                            </tr>
                                            @endforeach
                                            @endif
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                </div>               

            </div>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->


    </div>
    <!-- End: Main -->
@include('Backend/footer');