
<!-- Start: Sidebar -->
        <aside id="sidebar_left" class="nano nano-primary">
            <div class="nano-content">

                <!-- Start: Sidebar Header -->
                <header class="sidebar-header">
                    <div class="user-menu">
                        <div class="row text-center mbn">
                            <div class="col-xs-4">
                                <a href="dashboard.html" class="text-primary" data-toggle="tooltip" data-placement="top" title="Dashboard">
                                    <span class="glyphicons glyphicons-home"></span>
                                </a>
                            </div>
                            <div class="col-xs-4">
                                <a href="pages_messages.html" class="text-info" data-toggle="tooltip" data-placement="top" title="Messages">
                                    <span class="glyphicons glyphicons-inbox"></span>
                                </a>
                            </div>
                            <div class="col-xs-4">
                                <a href="pages_profile.html" class="text-alert" data-toggle="tooltip" data-placement="top" title="Tasks">
                                    <span class="glyphicons glyphicons-bell"></span>
                                </a>
                            </div>
                            <div class="col-xs-4">
                                <a href="pages_timeline.html" class="text-system" data-toggle="tooltip" data-placement="top" title="Activity">
                                    <span class="glyphicons glyphicons-imac"></span>
                                </a>
                            </div>
                            <div class="col-xs-4">
                                <a href="pages_profile.html" class="text-danger" data-toggle="tooltip" data-placement="top" title="Settings">
                                    <span class="glyphicons glyphicons-settings"></span>
                                </a>
                            </div>
                            <div class="col-xs-4">
                                <a href="pages_gallery.html" class="text-warning" data-toggle="tooltip" data-placement="top" title="Cron Jobs">
                                    <span class="glyphicons glyphicons-restart"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </header>
                <!-- End: Sidebar Header -->

<!-- sidebar menu -->
                <ul class="nav sidebar-menu" id="Link">
                    
                    <li class="">
                        <a href="{{url('admin/dashboard')}}">
                            <span class="glyphicons glyphicons-home"></span>
                            <span class="sidebar-title">Dashboard</span>
                        </a>
                    </li>

                    <li class="">
                        <a href="{{url('admin/myprofile')}}">
                            <span class="glyphicon glyphicon-user"></span>
                            <span class="sidebar-title">My Profile</span>
                        </a>
                    </li>

                    <li class="">
                        <a href="{{url('admin/users')}}">
                            <span class="glyphicon glyphicon-user"></span>
                            <span class="sidebar-title">Users</span>
                        </a>
                    </li>

                    <li class="">
                        <a href="{{url('admin/grade')}}">
                            <span class="glyphicons glyphicons-home"></span>
                            <span class="sidebar-title">Grades</span>
                        </a>
                    </li>

                    <li class="">
                        <!--{{url('admin/courses')}} -->
                            <a href="{{url('admin/courses')}}">
                            <span class="glyphicons glyphicons-home"></span>
                            <span class="sidebar-title">Courses</span>
                        </a>
                    </li>

                    <li class="">
                        <a href="{{url('admin/topics')}}">
                            <span class="glyphicons glyphicons-home"></span>
                            <span class="sidebar-title">Topic</span>
                        </a>
                    </li>

                    <!-- <li class="">
                            <a href="{{url('admin/price')}}">
                            <span class="glyphicons glyphicons-home"></span>
                            <span class="sidebar-title">Pricing</span>
                        </a>
                    </li> -->

                    <li class="">
                        <a href="{{url('admin/notify')}}">
                            <span class="glyphicons glyphicons-home"></span>
                            <span class="sidebar-title">Notify User</span>
                        </a>
                    </li>

                    <li class="">
                        <!--{{url('admin/resolution')}} -->
                        <a href="{{url('admin/resolution')}}">
                            <span class="glyphicons glyphicons-home"></span>
                            <span class="sidebar-title">Resolution Center</span>
                        </a>
                    </li>

                    <li class="">
                        <a href="{{url('admin/reports')}}">
                            <span class="glyphicons glyphicons-home"></span>
                            <span class="sidebar-title">Reports</span>
                        </a>
                    </li>

                    <li class="">
                        <a href="{{url('admin/subscription')}}">
                            <span class="glyphicons glyphicons-home"></span>
                            <span class="sidebar-title">Subscription Plan</span>
                        </a>
                    </li>

                     <li class="">
                        <a href="{{url('admin/contentmanagement')}}">
                            <span class="glyphicons glyphicons-home"></span>
                            <span class="sidebar-title">Content Management</span>
                        </a>
                    </li>
                    
                </ul>
                <div class="sidebar-toggle-mini">
                    <a href="#">
                        <span class="fa fa-sign-out"></span>
                    </a>
                </div>
            </div>
        </aside>
