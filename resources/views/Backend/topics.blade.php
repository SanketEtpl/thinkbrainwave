@include('Backend/header');

@include('Backend/sidebar');




        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="users">Topics</a>
                        </li>
                        <li class="crumb-icon">
                            <a href="dashboard">
                                <span class="glyphicon glyphicon-home"></span>
                            </a>
                        </li>
                        <li class="crumb-link">
                            <a href="dashboard">Home</a>
                        </li>
                        <li class="crumb-trail">Topics</li>
                    </ol>
                </div>
               
            </header>
            <!-- End: Topbar -->
            @php
                                     
            if($topiceditData!=NULL)
            {
                $name = $topiceditData->name;
                $description = $topiceditData->description;
                $status = $topiceditData->status;
                $id = $topiceditData->id;
                $gradeid = $topiceditData->grade_id;
                $courseid = $topiceditData->subject_id;
                $action = 'Update';
            }
            else
            {
                $name = '';
                $status = 3;
                $description = '';
                $id = '';
                $action = 'Add';
                $gradeid = '';
                $courseid = '';
            }
            @endphp

            <!-- Begin: Content -->
            <div id="content" class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="panel">
                            </br>
                                <h3><center>{{$action}} Topics Details</center></h3>

                            <div class="panel-body">
                                <form class="form-horizontal form1" action = "{{url('admin/topics/addTopic')}}" id="form1" method = "post">
                                    @csrf
                                   
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label"></label>
                                        <div class="col-lg-4">
                                    <input type="hidden" id="topicid" value="{{$id}}" name="topicid" class="form-control" >

                                     </div>
                                    </div>
                                    
                                    <div class="form-group">
                                     <input type="hidden" id="baseurl" value="{{url('admin/topics/FetchCourse')}}" name="baseurl" class="form-control" >
                                        <label class="col-lg-2 control-label">Grade</label>
                                        <div class="col-lg-4">
                                              <select name="grade" class="form-control required_check" id="grade_id">
                                                <option value="">Select Grade</option>
                                                @if(isset($grade))
                                                @foreach($grade as $grades)
                                                <option value="{{$grades->id}}" {{$gradeid == $grades->id? 'selected="selected"' : "" }}>{{$grades->name}}</option>
                                                
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>

                                    </div>


                                    <div class="form-group">
                                    <label class="col-lg-2 control-label">Course</label>
                                        <div class="col-lg-4">
                                            <select name="course" class="form-control required_check">
                                                <option value="">Select Course</option>
                                                @if(isset($course))
                                                @foreach($course as $courses)
                                                <option value="{{$courses->id}}" {{$courseid == $courses->id? 'selected="selected"' : "" }}>{{$courses->name}}</option>
                                                
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Topic</label>
                                        <div class="col-lg-4">
                                             <textarea class="form-control required_check" id="topic" name="topic" rows="3">{{$name}}</textarea>
                                        </div> 
                                    </div>


                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Description</label>
                                        <div class="col-lg-8">
                                             <textarea class="form-control required_check" id="description" name="description" rows="3">{{$description}}</textarea>
                                        </div>

                                    </div>


                                    

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="textArea3">Active</label>
                                        <div class="col-lg-4">
                                            <input type="radio"   name="status" @if($status==1) checked="" @elseif($status==3) checked="" @endif   value="1">Yes
                                            <input type="radio" name="status" @if($status==0) checked=""  @endif value="0">No
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="textArea3"></label>
                                        <div class="col-lg-2">
                                            <input type="submit" name="submit" value="Submit" class="btn btn-success">
                                            @if($action == 'Update')
                                            <a href="{{url('admin/topics')}}"><button type="button" class="btn btn-warning">Cancel</button></a>
                                            @else
                                             <button type="button" class="btn btn-warning" onclick="resetformnew();">Reset</button>
                                            @endif
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    
                </div>

                <div class="col-md-12">
                            <div class="panel panel-visible" id="spy3">
                            </br>
                                <h3><center>Topics List</center></h3>
                                 </br>
                                
                                <div class="panel-body pn">
                                    <table class="table table-striped table-hover" id="datatable3" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Topic ID</th>
                                                <th>Grade</th>
                                                <th>Course</th>
                                                <th>Topic</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            @php
                                            $i = 1;
                                            @endphp
                                            @if(isset($topic))
                                            @foreach ($topic as $topics)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$topics->grade_name}}</td>
                                                <td>{{$topics->subject_name}}</td>
                                                <td>{{$topics->name}}</td>
                                                <td>{{$topics->description}}</td>
                                                <td>
                                                @if($topics->status == 0)
                                                <a href="{{url('admin/topics/changeStatus')}}/{{$topics->id}}/{{$topics->status}}"><button class="btn btn-danger"'><i class="glyphicon glyphicon-ban-circle"  title="Dactive"></i></button></a>
                                                @elseif($topics->status == 1)
                                                <a href="{{url('admin/topics/changeStatus')}}/{{$topics->id}}/{{$topics->status}}"><button class="btn btn-primary" ><i class="glyphicon glyphicon-check" title="Active"></i></button></a>   
                                                @endif     

                                                </td>
                                                <td>
                                                   <a href="{{url('admin/topics/editTopic')}}/{{$topics->id}}"><button class="btn btn-warning" ><i class="glyphicon glyphicon-pencil" title="Edit"></i>
                                                  </button></a>
                                                  <a href="{{url('admin/topics/deleteTopic')}}/{{$topics->id}}"><button class="btn btn-danger" ><i class="glyphicon glyphicon-remove" title="Delete"></i></button>
                                                  </a>
                                                  
                                              </td>
                                              
                                                
                                            </tr>
                                            @endforeach
                                            @endif
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                </div>               

            </div>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->


    </div>
    <!-- End: Main -->
@include('Backend/footer');