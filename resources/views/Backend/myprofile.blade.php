@include('Backend/header');

 @include('Backend/sidebar');



        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="users">My Prfoile</a>
                        </li>
                        <li class="crumb-icon">
                            <a href="dashboard">
                                <span class="glyphicon glyphicon-home"></span>
                            </a>
                        </li>
                        <li class="crumb-link">
                            <a href="dashboard">Home</a>
                        </li>
                        <li class="crumb-trail">My Profile</li>
                    </ol>
                </div>
                
               
            </header>
            <!-- End: Topbar -->

            <!-- Begin: Content -->
            <div id="content" class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">

                        <div class="panel">
                            </br>
                            <h3><center>Edit My Profile Details</center>	
                            </h3>				
                            <div class="panel-body">

                                <form class="form-horizontal form1" action = "{{url('admin/myprofile/updateprofile')}}" id="form1" method = "post">

                                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Name</label>
                                        <div class="col-lg-4">
                                             <input type="text" value="{{$profileData->name}}" id="name" name="name" class="form-control required_letter" placeholder="Enter Name">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                    	 <label class="col-lg-2 control-label">Contact Number</label>
                                        <div class="col-lg-4">
                                            <input type="text" id="contact" value="{{$profileData->contact_number}}" name="contact" class="form-control required_mobile" placeholder="Enter Contact Number">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Email Id</label>
                                        <div class="col-lg-4">
                                              <input type="text" id="email" value="{{$profileData->email}}" name="email" class="form-control required_email" placeholder="Email address">
                                        </div>
                                    </div>


                                     <div class="form-group">
                                        <label class="col-lg-2 control-label">Country</label>
                                        <div class="col-lg-4">
                                            <select class="form-control select_tag_box required_check" id="country_id" name="country_id">
												@php
												$country_id = $profileData->country_id;
												@endphp
												<option value="" >Select Country</option>
													@foreach ($countries as $coun)
														<option value="{{ $coun->id }}" @if ($country_id == $coun->id ) selected @endif >{{ $coun->name }}</option>
													@endforeach
											</select>
                                        </div>
                                    </div>



                                      <div class="form-group">
                                        <label class="col-lg-2 control-label">State</label>
                                        <div class="col-lg-4">
                                            <select class="form-control select_tag_box required_check" id="state_id" name="state_id">
												@php
												$state_id = $profileData->state_id;
												@endphp
												<option value=""  >Select State</option>
													@foreach ($state as $st)
														<option value="{{ $st->id }}" @if ($state_id == $st->id ) selected @endif >{{ $st->name }}</option>
													@endforeach
											</select>
                                        </div>
                                    </div>


                                     <div class="form-group">
                                        <label class="col-lg-2 control-label">City</label>
                                        <div class="col-lg-4">
                                             <select class="form-control select_tag_box required_check" id="city_id" name="city_id">
												@php
												$city_id = $profileData->city_id;
												@endphp
												<option value=""  >Select City</option>
													@foreach ($city as $ci)
														<option value="{{ $ci->id }}" @if ($city_id == $ci->id ) selected @endif >{{ $ci->name }}</option>
													@endforeach
											</select>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                    	 <label class="col-lg-2 control-label">Address</label>
                                        <div class="col-lg-4">
                                             <textarea class="form-control required_check"  id="address" name="address" rows="3">{{$profileData->address}}</textarea>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="textArea3"></label>
                                        <div class="col-lg-5">
                                            <input type="submit" name="submit" value="Submit" class="btn btn-success">
                                             <button type="button" class="btn btn-warning" onclick="resetform();">Reset</button>
                                             <a href="#." data-toggle="modal" data-target="#myModal"><button type="button" class="btn btn-primary" >Change Password</button>
                                             </a>
							
                                        </div>
                                    </div>
                                    

                                </form>
                            </div>




                        </div>

                    </div>

                    
                </div>     

            </div>
            <!-- End: Content -->



            <!-- Begin: Content -->
            <!-- <div id="content" class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">

                        <div class="panel">
                           </br>
                                <h3><center>Edit Bank Details</center></h3>
                            <div class="panel-body">
                                <form class="form-horizontal form2" action = "{{url('admin/myprofile/updatebankdetails')}}" id="form2" method = "post">
                                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

                                    
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Bank Name</label>
                                        <div class="col-lg-4">
                                             <input type="text" id="bankname" value="{{$profileData->bank_name}}" name="bankname" class="form-control required_letter" placeholder="Enter Bank Name">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                    	 <label class="col-lg-2 control-label">Branch Name</label>
                                        <div class="col-lg-4">
                                            <input type="text" id="branchname" value="{{$profileData->branch_name}}" name="branchname" class="form-control required_letter" placeholder="Enter Branch Name">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Account Number</label>
                                        <div class="col-lg-4">
                                              <input type="text" id="accountnumber" value="{{$profileData->account_no}}" name="accountnumber" class="form-control required_number" placeholder="Enter Account Number">
                                        </div>
                                    </div>


                                    <div class="form-group">

                                    	 <label class="col-lg-2 control-label">IFSC Code</label>
                                        <div class="col-lg-4">
                                              <input type="text" id="ifsc" name="ifsc" value="{{$profileData->Ifsc}}" class="form-control required_check" placeholder="Enter IFSC Code">
                                        </div>
                                    </div>



                                     <div class="form-group">
                                        <label class="col-lg-2 control-label">Swift Code</label>
                                        <div class="col-lg-4">
                                              <input type="text" id="swiftcode" value="{{$profileData->swift_code}}" name="swiftcode" class="form-control required_check" placeholder="Enter Swift Code">
                                        </div>
                                    </div>


                                 
                                     <div class="form-group">
                                     <label class="col-lg-2 control-label">MICR Code</label>
                                        <div class="col-lg-4">
                                              <input type="text" id="micr" name="micr" value="{{$profileData->micr_code}}" class="form-control required_number" placeholder="Enter MICR Code">
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="textArea3"></label>
                                        <div class="col-lg-2">
                                            <input type="submit" name="submit" value="Submit" class="btn btn-success">
                                             <button type="button" class="btn btn-warning" onclick="resetform();">Reset</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            
                        </div>

                    </div>

                    
                </div>     

            </div>
            End: Content -->

            

        </section>
        <!-- End: Content-Wrapper -->


    </div>
    <!-- End: Main -->


        	<!-- Change password script on -->
			@include('Backend.change_pass') 
			<!-- Change password script on -->

        @include('Backend/footer');