@include('Backend.header')
@php
<!-- $val=Session::get('loginSession')
@endphp -->
				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url({{ url('public/assets/img/mid-bg.jpg')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>My Profile</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!--account user details info start here-->
				<section class="my_account_all_us_details">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="my_account_data">
									<div class="row">
										<div class="col-md-5 col-sm-6 col-xs-6 dash_dta_border_right">
											<div class="dash_bora_profil_user_sec">
											<input type="hidden" name="user_id" id="user_id" value ="{{ $users['user_id'] }}">
					
												@php
													if($users['profile_file_path']!='')
													{
														$image_path = $users['profile_file_path'];
													}
													else{
														$image_path = "no-image.png";
													}
												@endphp
												<span class="dash_img_profile_img">
													<img style="width:100%; height:100%;" src="@if($val['session_user_type']=='1') {{ URL::to('/') }}/public/images/{{ $val['session_user_profile_file_path'] }} @else {{ $val['session_user_profile_file_path'] }} @endif" class="img-responsive" alt="profile"/>
												</span>
												<div class="dash_board_details_sec">
												
													<h4>{{ $users['first_name'] }}</h4>
													<span>@if($val) {{ $val['session_user_role'] }} @endif</span>
													<div class="star_ratting_table_views">
														<ul>
															<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
															<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
															<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
															<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
															<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
														</ul>
														<small>3.56/5</small>
													</div>
													<div class="refer_frds_btn_new">
														<a href="EditProfile" class="btn small_comm_btn">Edit Profile</a>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-6">
											<div class="user-privat-details">
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/img/gender.png')}}" class="img-responsive" alt="gender"/></span>
													<div>{{ $users['gender'] }}</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/img/age.png')}}" class="img-responsive" alt="age"/></span>
													<div>

														@php
														$dob = $users['dob'];
														$today = date('Y-m-d');
														$d1 = new DateTime($today);
														$d2 = new DateTime($dob);

														$diff = $d2->diff($d1);

														echo $diff->y;
														@endphp
														Years
													</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/img/map-marker.png')}}" class="img-responsive" alt="gender"/></span>
													<div>{{ $users['address_line1'] }}</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--account user details info end here-->
				<!-- profile details section start here-->
				<section class="common_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="common_section_details_here">
									<div class="common_section_heading">
										<h4>PROFILE DETAILS</h4>
									</div>
									<div class="common_section_content">
										<div class="row">
											<div class="col-md-7">
												<div class="profile_user_details">
													<div class="profile_personal_details">
														<ul>
															<li><span>GRADE:</span><div class="profile_details_content">{{ $users['grade_name'] }}</div></li>
															<li><span>SUBJECTS/COURSES:</span><div class="profile_details_content">{{ $users['subject_name'] }}</div></li>
															<li><span>DATE OF BIRTH:</span><div class="profile_details_content">{{ $users['dob'] }}</div></li>
															<li><span>GENDER</span><div class="profile_details_content">{{ $users['gender'] }}</div></li>
															<li><span>ADDRESS</span><div class="profile_details_content">{{ $users['address_line1'] }}</div></li>
															<li><span>COUNTRY</span><div class="profile_details_content">{{ $users['country_name'] }}</div></li>
															<li><span>MAX DISTANCE TRAVEL:</span><div class="profile_details_content_link ">{{ $users['max_distance_travel_kms'] }}</div><a href="#." class="info-link-details tooltip_new"><span class="tooltiptext_new">Maximum Distance You Can Travel to Tutor</span><i class="fa fa-info-circle" aria-hidden="true"></i></a></li>
															<li><span>PHONE NUMBER:</span><div class="profile_details_content">{{ $users['primary_phone'] }}</div></li>
															@php
															$val=Session::get('loginSession')
															@endphp
															<li><span>EMAIL ID:</span><div class="profile_details_content">@if($val) {{ $val['session_user_username'] }} @endif</div></li>
															<!-- <li><span>PASSWORD:</span><div class="profile_details_content_link">**********</div><a href="#." class="info-link-details"><i class="fa fa-eye" aria-hidden="true"></i></a></li> -->
														</ul>
													</div>
													<div class="common_btn_div">
														<a href="#." class="btn small_comm_btn" data-toggle="modal" data-target="#change_password_popup">Change Password</a>
													</div>
												</div>
											</div>
											<div class="col-md-5">
												<div class="profile_details_shar_pa_div">
													<div class="profile_calender">
														<div class="calender_form_set_new">
															<div class="input_new_set_search">
																<input type="text" class="form-control" placeholder="Search">
															</div>
														</div>
														<div class="pie_chart_heading">
															<h4>MY PLAN</h4>
														</div>
														<div class="piechart-demo">
															<img src="{{ url('public/assets/img/calender-img.jpg')}}" class="img-responsive" alt="piechart">
														</div>
													</div>
													<div class="profile_shar_refer_add_links">
														<div class="profile_share_add_link_here">
															<span><img src="{{ url('public/assets/img/share-freind-icon.png')}}" class="img-responsive" alt="share-friend"/></span>
															<div class="blog_btn_set">
																<button type="button" class="btn common_btn" data-toggle="modal" data-target="#share_profile_parent_set_popup">Share With Parents</button>
															</div>
														</div>
														<div class="profile_share_add_link_here">
															<span><img src="{{ url('public/assets/img/refer-freind-icon.png')}}" class="img-responsive" alt="refer-friend"/></span>
															<div class="blog_btn_set">
																<a href="BookSession" class="btn common_btn">Add Session</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- profile details section end here-->
				
				
				<!--popup start here-->
				
					
					  
					  <!-- change password popup Start Here -->
					  <div class="modal fade" id="change_password_popup" role="dialog">
						<div class="modal-dialog change_password_popup_width">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="model_details">
									<div class="section_heading_sec">
										<h4>CHANGE PASSWORD</h4>
									</div>
												
									<div class="model_content">
										<div class="add_topic_form">
											<form  id="form1" method = "post" enctype="multipart/form-data">
            									<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
											
												<div class="row">
													<div class="col-md-12">
														<div class="form_set">
															<span>OLD PASSWORD :</span>
															<div class="input_box_set_popup"><input type="password" class="form-control" id="old_password"/></div>
														</div>
													</div>
													<div class="col-md-12">
														<div class="form_set">
															<span>NEW PASSWORD :</span>
															<div class="input_box_set_popup"><input type="password" class="form-control" id="new_password"/></div>
														</div>
													</div>
													<div class="col-md-12">
														<div class="form_set">
															<span>CONFIRM PASSWORD :</span>
															<div class="input_box_set_popup"><input type="password" class="form-control" id="retype_password"/></div>
														</div>
													</div>
													<div class="col-md-12">
														<div class="main_profil_btn">


														<!-- 	<a href="#." class="btn save_cancel_btn margin-right-10px" data-dismiss="modal" data-toggle="modal" data-target="#success_changes_popup">Update</a> -->

															<button  type="button" id="UpdatePassword" class="btn save_cancel_btn margin-right-10px">Update</button>

															<a href="#." class="btn save_cancel_btn" data-dismiss="modal">Cancel</a>
														</div>
													</div>
												</div>
											</form>
										</div>
									 </div>
							      
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- change password popup end Here -->
					  
					  <!--share parents profile popup start here-->
					  <div class="modal fade" id="share_profile_parent_set_popup" role="dialog">
						<div class="modal-dialog refer_friend_width_popup">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="moel_details_new">
									<div class="section_heading_sec">
										<h4>SHARE PROFILE WITH PARENTS</h4>
									</div>
									<div class="model_content">
										<div class="refer_friend_popup_details">
											<ul class="nav nav-tabs">
											  <li class="active">
												  <a data-toggle="tab" href="#refer_frd_01">
													  <span>
														  <img src="{{ url('public/assets/img/popup-call-icon.png')}}" class="img-responsive unactive" alt="call"/>
														  <img src="{{ url('public/assets/img/popup-call-icon-active.png')}}" class="img-responsive active alt="call"/>
													  </span>
													  <div>PHONE NUMBER</div>
												  </a>
											  </li>
											  <li>
												  <a data-toggle="tab" href="#refer_frd_02">
													  <span>
														  <img src="{{ url('public/assets/img/popup-email-icon.png')}}" class="img-responsive unactive" alt="call"/>
														  <img src="{{ url('public/assets/img/popup-email-icon-active.png')}}" class="img-responsive active" alt="call"/>
													  </span>
													  <div>EMAIL ID</div>
												  </a>
											  </li>
											</ul>

											<div class="tab-content">
											  <div id="refer_frd_01" class="tab-pane fade in active">
													<div class="refe_frd_tab_content">
														<div class="form_set_n_h">
															<label>Enter Phone Number</label>
															<div class="input-group">
																<input class="form-control" id="phone_number"/>
																<button type="button" class="btn comm-btn" data-dismiss="modal" data-toggle="modal" data-target="#sent_success_popup">Share</button>
															</div>
														</div>
													</div>
											  </div>
											  <div id="refer_frd_02" class="tab-pane fade">
													<div class="refe_frd_tab_content">
														<div class="form_set_n_h">
															<label>Enter Email ID</label>
															<div class="input-group">
																<input class="form-control" id="email_id"/>
																<button type="button" class="btn comm-btn" data-dismiss="modal" data-toggle="modal" data-target="#sent_success_popup">Share</button>
															</div>
														</div>
													</div>											 
											  </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!--share parents profile popup end here-->
					  <script>
				jQuery(document).ready(function(){
					$('#form1').validate({
					rules: {

						old_password: {
							required: true
						},

						new_password: {
							required: true
						},
						retype_password: {
								required: true,
								equalTo: "#new_password"
						}
					}
				});
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
					
					$("#UpdatePassword").click(function(e){
						if ($('#form1').valid()) {
							e.preventDefault();
							var user_id = $("input[id=user_id]").val();
							var old_password = $("input[id=old_password]").val();
							var new_password = $("input[id=new_password]").val();
							var retype_password = $("input[id=retype_password]").val();
							if(new_password==retype_password)
							{
								$.ajax({
										type:'POST',
										url:'UpdatePassword',
										data:{user_id:user_id, old_password:old_password,  new_password:new_password, retype_password:retype_password },
										success:function(data){
											console.log(data.success);
											//console.log(data.error);
											if(data.success)
											{
												$('#password_save_popup').modal('show');
												
											}
											if(data.error)
											{
												$('#not_password_popup').modal('show');
												
											}
											
										}
									});
								}
							else
							{
								$('#passwod_not_match_popup').modal('show');
							}
						}
					});
				});
			</script>
					  
				<!--popup end here-->
@include('StudentTemplate.footer') 	