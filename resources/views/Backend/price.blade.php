@include('Backend/header');
@include('Backend/sidebar');

        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Start: Topbar -->
            <header id="topbar">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="users">Pricing</a>
                        </li>
                        <li class="crumb-icon">
                            <a href="{{url('admin/dashboard')}}">
                                <span class="glyphicon glyphicon-home"></span>
                            </a>
                        </li>
                        <li class="crumb-link">
                            <a href="{{url('admin/dashboard')}}">Home</a>
                        </li>
                        <li class="crumb-trail">Pricing</li>
                    </ol>
                </div>
               
            </header>
            <!-- End: Topbar -->
@php
                                     
            if($singlePrice!=NULL)
            {
                
                $action = 'Update';
                 $status = $singlePrice['status'];
            }
            else
            {
                
                $status = 3;
                $action = 'Add';
                
            }
            @endphp
            <!-- Begin: Content -->
            <div id="content" class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">

                        <div class="panel">
                            </br>
                                <h3><center>{{$action}} Pricing Details</center></h3>
                            <div class="panel-body">

                                <form class="form-horizontal form1" action = "{{url('admin/price/addPrice')}}" id="form1" method = "post">
                                    @csrf

                                    <input type="hidden" name="price_id" id="price_id" value="{{$singlePrice['price_id']}}" >

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Session Type</label>
                                        <div class="col-lg-4">
                                            <select name="session_type " class="form-control required_check">
                                                <option value="">Select Option</option>
                                                <option value="1" {{$singlePrice['session_type'] == 1? 'selected="selected"' : "" }}>F2F Session</option>
                                                <option value="2" {{$singlePrice['session_type'] == 2? 'selected="selected"' : "" }}>V2V Session</option>
                                            </select>
                                        </div>
                                   
                                    </div>


                                        <div class="form-group">
                                        <label class="col-lg-2 control-label">Duration</label>
                                        <div class="col-lg-4">
                                            <select name="duration" class="form-control required_check">
                                                <option value="">Select Option</option>
                                                <option value="0.5" {{$singlePrice['duration'] == 0.5? 'selected="selected"' : "" }}>0.5</option>
                                                <option value="1" {{$singlePrice['session_type'] == 1? 'selected="selected"' : "" }}>1</option>
                                                <option value="1.5" {{$singlePrice['session_type'] == 1.5? 'selected="selected"' : "" }}>1.5</option>
                                                <option value="2" {{$singlePrice['session_type'] == 2? 'selected="selected"' : "" }}>2</option>
                                                <option value="2.5" {{$singlePrice['session_type'] == 2.5? 'selected="selected"' : "" }}>2.5</option>
                                                <option value="3" {{$singlePrice['session_type'] == 3? 'selected="selected"' : "" }}>3</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Price</label>
                                        <div class="col-lg-4">
                                             <input type="text" class="form-control required_number" id="price" name="price" placeholder="Enter Price" value="{{$singlePrice['price']}}">
                                        </div>

                                        
                                    </div>
                                   

                                    <div class="form-group">
                                    <label class="col-lg-2 control-label" for="textArea3">Active</label>
                                        <div class="col-lg-4">
                                            <input type="radio"   name="status" @if($status==1) checked="" @elseif($status==3) checked="" @endif   value="1">Yes
                                            <input type="radio" name="status" @if($status==0) checked=""  @endif value="0">No
                                        </div>
                                    </div>
                                    

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="textArea3"></label>
                                        <div class="col-lg-2">
                                            <input type="submit" name="submit" value="Submit" class="btn btn-success">
                                             @if($action == 'Update')
                                            <a href="{{url('admin/price')}}"><button type="button" class="btn btn-warning">Cancel</button></a>
                                            @else
                                             <button type="button" class="btn btn-warning" onclick="resetformnew();">Reset</button>
                                            @endif
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    
                </div>


                <div class="row">

                    <div class="col-md-12">

                        <div class="panel">
                             </br>
                                <h3><center>Pricing List</center></h3>
                                
                            <div class="panel-body">
                              
                                <table class="table table-striped table-hover" id="datatable3" cellspacing="0" width="100%">
                                 <thead>
                                    <tr>

                                      <th>Id</th>
                                      <th>Session Type</th>
                                      <th>Duration</th>
                                      <th>Price</th>
                                      <th>Status</th>
                                      <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    @foreach ($price as $index => $priceData)
                                    <tr>
                                       <td>{{$index +1}}</td>
                                       @if($priceData->session_type == 1)
                                       <td>F2F Session</td>
                                       @else
                                       <td>V2V Session</td>
                                       @endif
                                       <td>{{ $priceData->duration }}</td>
                                       <td>{{ $priceData->price }}</td>
                                        
                                        <td>
                                            @if($priceData->status == 0)
                                            <a href="{{url('admin/price/changeStatus')}}/{{$priceData->id}}/{{$priceData->status}}"><button class="btn btn-danger"'><i class="glyphicon glyphicon-ban-circle"  title="Dactive"></i></button></a>
                                            @elseif($priceData->status == 1)
                                            <a href="{{url('admin/price/changeStatus')}}/{{$priceData->id}}/{{$priceData->status}}"><button class="btn btn-primary" ><i class="glyphicon glyphicon-check" title="Active"></i></button></a>   
                                            @endif     
                                        </td>
                                        <td>
                                           <a href="{{url('admin/price/editPrice')}}/{{$priceData->id}}"><button class="btn btn-warning" ><i class="glyphicon glyphicon-pencil" title="Edit"></i>
                                          </button></a>
                                          <a href="{{url('admin/price/deletePrice')}}/{{$priceData->id}}"><button class="btn btn-danger" ><i class="glyphicon glyphicon-remove" title="Delete"></i>
                                          </button></a>
                                       </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                              </table>
                            </div>
                        </div>

                    </div>

                    
                </div>    

            </div>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->


    </div>
    <!-- End: Main -->
@include('Backend/footer');