		@include('Template.header')


		<!-- Page wrapper Start here-->
			<div class="wrapper">
			    <!-- signin Page Start Here -->
				<section class="login_section" style="background-image:url({{ url('public/assets/img/sign-siup-bannner-img.png')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="login_details">
									<div class="login_form">
										<div class="login_form_logo">
											<a href='{{ url("Home")}}'><img src="{{ url('public/assets/img/logo_new.png')}}" class="img-responsive" alt="logo"/></a>
										</div>
										<div class="login_form_set_details">
										@if ($message = Session::get('success'))
											<div class="alert alert-success fade in alert-dismissible">
												<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
												<p>{{ $message }}</p>
											</div>
										@endif
										@if ($message = Session::get('error'))
											<div class="alert alert-danger fade in alert-dismissible">
												<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
												<p>{{ $message }}</p>
											</div>
										@endif
											<div class="login_head">
												<h3>SIGN IN</h3>
											</div>
											<form action = "SignInSave" id="form1" class="form1" method = "post" enctype="multipart/form-data">
            									<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
												
												<div class="login_form_details">
													<div class="input_form_set">
														<span>EMAIL ADDRESS</span>
														<input type="text" class="form-control required_email" id="email" name="email" placeholder="Enter Email Address"/>
													</div>
													<div class="input_form_set">
														<span>PASSSWORD</span>
														<input type="password" class="form-control required_check" id="password" name="password" placeholder="Enter password"/>
													</div>
													<div class="remember_forgot_password">
														<div class="check_inner">
															<input class="filled-in" id="check1" name="check1" type="checkbox">
															<label for="check1">Remember Me</label>
															<span class="forgotpass"> <a href="#." class="" data-toggle="modal" data-target="#forgot_password_popup">Forgot Password ?</a></span>
														</div>
													</div>
													<div class="login_sign_up_btns_group">
														<!-- <a href="Dashboard" class="btn sign_in_up_btn">SIGN IN</a> -->
														<button type="submit" class="btn sign_in_up_btn">SIGN IN</button>
														<a href='{{ url("Signup")}}' class="btn sign_in_up_btn">SIGN UP</a>
													</div>
													<span class="or_text">OR</span>
													<div class="login_sign_social_link">
														<a href="#." class="btn fac_book_link"  data-toggle="modal" data-target="#sign_in_facebook_popup">
															<span><i class="fa fa-facebook" aria-hidden="true"></i></span>
															<div class="text_set">Continue With Facebook</div>
														</a>
														<a href="#." class="btn google_link" data-toggle="modal" data-target="#sign_in_google_popup">
															<span><i class="fa fa-google-plus" aria-hidden="true"></i></span>
															<div class="text_set">Continue With Google</div>
														</a>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- signin Page end Here -->
				<!-- select role for facebook popup end Here -->
				<div class="modal fade" id="sign_in_facebook_popup" role="dialog">
						<form class="form2" method = "post" >
            								
							<div class="modal-dialog model_forgot_pass_popup">

								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
										</button>
									</div>
									<div class="modal-body">
										<div class="moel_details">
											<span class="model_header">
											<span>SIGN IN AS</span>
																<br><br><br>	
												<select class="form-control required_check" name="role" id="role">
														
													<option value="">Select User Type</option>
													@foreach ($roles as $role)
														<option value="{{ $role->id }}" >{{ $role->role }}</option>
													@endforeach
												</select>
											</span>
											<div class="model_content">
												<div class="pop_btn_sect">
													 <button type="button" id="login" class="btn popup_coom_btn">Continue With Facebook</button> 
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
						  <!-- select role for facebook popup end Here -->
						  <!-- select role for google popup end Here -->
					<div class="modal fade" id="sign_in_google_popup" role="dialog">
						<form class="form3" method = "post" >
							<div class="modal-dialog model_forgot_pass_popup">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
										</button>
									</div>
									<div class="modal-body">
										<div class="moel_details">
											<span class="model_header">
											<span>SIGN IN AS</span>
																<br><br><br>	
												<select class="form-control required_check" name="role2" id="role2">
														
													<option value="">Select User Type</option>
													@foreach ($roles as $role)
														<option value="{{ $role->id }}" >{{ $role->role }}</option>
													@endforeach
												</select>
											</span>
											<div class="model_content">
												<div class="pop_btn_sect">
													 <button type="button" id="login_google" class="btn popup_coom_btn">Continue With Google</button> 
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
						  <!-- select role for google popup end Here -->
						  <script type="text/javascript">
								$("#login").click(function(e){
									
									if ($('.form2').valid()) {
										var role = $("select[name=role]").val();
										var url = '{{ url("redirectFB")}}/'+role;
										if(role=='')
										{
											alert("Plese select role");
										}
										else{
											//window.location.replace("LoginWithFacebook/"+role);
											window.location.replace(url);
										}
									}
								});
								$("#login_google").click(function(e){
									if ($('.form3').valid()) {	
										var role = $("select[name=role2]").val();
										var url = '{{ url("redirect")}}/'+role;
										if(role=='')
										{
											alert("Plese select role");
										}
										else{
											//window.location.replace("LoginWithFacebook/"+role);
											window.location.replace(url);
										}
									}
								});
						  </script>
				
		@include('Template.footer')

