@include('Template.header')
				<!-- banner section start here-->
				<section class="banner_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>CONTACT US</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!-- contact us section start here-->
				<section class="web_section_common">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="section_details">
									<div class="section_heading_new_temp">
										<h4>THE CONTACT US PAGE IS ONE OF THE MOST VISITED PAGES ON ANY WEBSITE. :-</h4>
									</div>
									<div class="inn_cols_main_div">
										<div class="faq_set_info">
											<div class="row">
												<div class="col-md-5">
													<div class="about_us" style="background-image:url({{ url('public/assets/img/contact_us.jpg')}});">
														<div class="overlay"></div>
													</div>
												</div>
												<div class="col-md-7">
													<div class="faq_details">
														<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
															<br><br>
														   It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, <br>as opposed to using 'Content here, content here', making it look like readable English.There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.
														</p>
													</div>
												</div>
											</div>
										</div>
										<div class="contact_us_info">
											<div class="contact_us_add_web_details">
												<div class="row">
													<div class="col-md-4 col-sm-4 col-xs-4">
														<div class="cont_links_set">
															<div class="set_contact_data">
																<span><img src="{{ url('public/assets/img/address.png')}}" class="img-responsive" alt="address"/></span>
																<div class="set_con_deta_det">
																	<h5>Michael I. Days 3756 Preston Street Wichita, KS 67213 </h5>
																</div>
															</div>
														</div>
													</div>
													<div class="col-md-4 col-sm-4 col-xs-4">
														<div class="cont_links_set">
															<div class="set_contact_data">
																<a href="mailto:info@thinkbrainwave.com;">
																	<span><img src="{{ url('public/assets/img/mail.png')}}" class="img-responsive" alt="mail"/></span>
																	<div class="set_con_deta_det_sec">
																		<h5>info@thinkbrainwave.com</h5>
																	</div>
																</a>
															</div>
														</div>
													</div>
													<div class="col-md-4 col-sm-4 col-xs-4">
														<div class="cont_links_set">
															<div class="set_contact_data">
																<a href="tel:9876543210;">
																	<span><img src="{{ url('public/assets/img/contact.png')}}" class="img-responsive" alt="contact"/></span>
																	<div class="set_con_deta_det_sec">
																		<h5>987 - 654 - 3210</h5>
																	</div>
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="contact_us_form_set">
												<div class="row">
													<div class="col-md-4">
														<div class="form_set">
															<label>Enter Your First Name</label>
															<input type="text" class="form-control" id="first_name">
														</div>
													</div>
													<div class="col-md-4">
														<div class="form_set">
															<label>Enter Your Last Name</label>
															<input type="text" class="form-control" id="first_name">
														</div>
													</div>
													<div class="col-md-4">
														<div class="form_set">
															<label>Enter Your Email</label>
															<input type="text" class="form-control" id="first_name">
														</div>
													</div>
													<div class="col-md-12">
														<div class="conatct_form_set">
															<label>Enter Your Massage</label>
															<textarea type="text" class="form-control" id="first_name"></textarea>
														</div>
													</div>
													<div class="col-md-12">
														<div class="contact_btn_div">
															<button type="button" class="btn contact_us_send_msg_btn">
																<i class="fa fa-paper-plane-o" aria-hidden="true"></i>
																SEND MASSAGE
															</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- contact us section end here-->
					  
				
				

@include('Template.footer') 
			