@include('Template.header')
				<!-- banner section start here-->
				<section class="banner_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>FREQUENTLY ASKED QUESTIONS</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!-- faq section start here-->
				<section class="web_section_common">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="section_details_se_new">
									<div class="section_heading_new_temp">
										<h4>FREQUENTLY ASKED QUESTIONS</h4>
									</div>
									<div class="about_details">
										<div class="row">
											<div class="col-md-5">
												<div class="about_us" style="background-image:url({{ url('public/assets/img/faq.jpg')}});">
													<div class="overlay"></div>
												</div>
											</div>
											<div class="col-md-7 col-sm-7">
												<div class="faq_panels_set">
													<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
														<div class="panel">
															<div class="panel-heading" role="tab" id="headingOne">
																<h4 class="panel-title">
																	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
																		<i class="more-less glyphicon glyphicon-plus"></i>
																		Lorem Ipsum is simply dummy text of the printing and typesetting industry.
																	</a>
																</h4>
															</div>
															<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
																<div class="panel-body">
																	  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
																</div>
															</div>
														</div>
														<div class="panel">
															<div class="panel-heading" role="tab" id="headingTwo">
																<h4 class="panel-title">
																	<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
																		<i class="more-less glyphicon glyphicon-plus"></i>
																		It was popularised in the 1960s with the release of Letraset sheets
																	</a>
																</h4>
															</div>
															<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
																<div class="panel-body">
																	 It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
																</div>
															</div>
														</div>
														<div class="panel">
															<div class="panel-heading" role="tab" id="headingThree">
																<h4 class="panel-title">
																	<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
																		<i class="more-less glyphicon glyphicon-plus"></i>
																		Contrary to popular belief, Lorem Ipsum is not simply random text.
																	</a>
																</h4>
															</div>
															<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
																<div class="panel-body">
																	Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.
																</div>
															</div>
														</div>
														<div class="panel">
															<div class="panel-heading" role="tab" id="headingfour">
																<h4 class="panel-title">
																	<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
																		<i class="more-less glyphicon glyphicon-plus"></i>
																		Lorem Ipsum comes from sections
																	</a>
																</h4>
															</div>
															<div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
																<div class="panel-body">
																	Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
																</div>
															</div>
														</div>
														<div class="panel">
															<div class="panel-heading" role="tab" id="headingfive">
																<h4 class="panel-title">
																	<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
																		<i class="more-less glyphicon glyphicon-plus"></i>
																		The standard chunk of Lorem Ipsum used since the 1500s
																	</a>
																</h4>
															</div>
															<div id="collapsefive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfive">
																<div class="panel-body">
																	The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
																</div>
															</div>
														</div>
														<div class="panel">
															<div class="panel-heading" role="tab" id="headingsix">
																<h4 class="panel-title">
																	<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsesix" aria-expanded="false" aria-controls="collapsesix">
																		<i class="more-less glyphicon glyphicon-plus"></i>
																		It is a long established fact that a reader
																	</a>
																</h4>
															</div>
															<div id="collapsesix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingsix">
																<div class="panel-body">
																	It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
																</div>
															</div>
														</div>
														<div class="panel">
															<div class="panel-heading" role="tab" id="headingseven">
																<h4 class="panel-title">
																	<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseseven" aria-expanded="false" aria-controls="collapseseven">
																		<i class="more-less glyphicon glyphicon-plus"></i>
																		Many desktop publishing packages
																	</a>
																</h4>
															</div>
															<div id="collapseseven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingseven">
																<div class="panel-body">
																	Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
																</div>
															</div>
														</div>
														<div class="panel">
															<div class="panel-heading" role="tab" id="headingeight">
																<h4 class="panel-title">
																	<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseight" aria-expanded="false" aria-controls="collapseight">
																		<i class="more-less glyphicon glyphicon-plus"></i>
																		There are many variations of passages of Lorem Ipsum available
																	</a>
																</h4>
															</div>
															<div id="collapseight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingeight">
																<div class="panel-body">
																	There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.
																</div>
															</div>
														</div>
														<div class="panel">
															<div class="panel-heading" role="tab" id="headingnine">
																<h4 class="panel-title">
																	<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsenine" aria-expanded="false" aria-controls="collapsenine">
																		<i class="more-less glyphicon glyphicon-plus"></i>
																		If you are going to use a passage of Lorem Ipsum.
																	</a>
																</h4>
															</div>
															<div id="collapsenine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingnine">
																<div class="panel-body">
																	 If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.
																</div>
															</div>
														</div>
														<div class="panel">
															<div class="panel-heading" role="tab" id="headingten">
																<h4 class="panel-title">
																	<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseten" aria-expanded="false" aria-controls="collapseten">
																		<i class="more-less glyphicon glyphicon-plus"></i>
																		It uses a dictionary of over 200 Latin words.
																	</a>
																</h4>
															</div>
															<div id="collapseten" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingten">
																<div class="panel-body">
																	It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- faq section end here-->
					

				
			
@include('Template.footer') 