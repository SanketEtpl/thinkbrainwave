@include('Template.header')
				<!-- banner section start here-->
				<section class="banner_section" >
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>TERMS AND SERVICE</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!-- terms and conditions section start here-->
				<section class="web_section_common">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="section_details_se_new">
									<div class="privacy_policy_content_heading">
										<h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
										<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
									</div>
									<div class="privacy_policy_main_group">
										<div class="privacy_policy_content">
											<h4>1. Lorem Ipsum is not simply random text.</h4>
											<p>It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
										</div>
										<div class="privacy_policy_content_sec">
											<h4>a) Lorem Ipsum comes from sections.</h4>
											<p>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
										</div>
										<div class="privacy_policy_content_sec">
											<h4>b) It is a long established fact that a reader</h4>
											<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
												<br><br>
											   Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
											</p>
										</div>
										<div class="privacy_policy_content_sec">
											<h4>c) There are many variations of passages</h4>
											<p>Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
										</div>
									</div>
									<div class="privacy_policy_main_group">
										<div class="privacy_policy_content">
											<h4>2. All the Lorem Ipsum generators on the Internet</h4>
											<p>It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.</p>
											<div class="our_strory_list_sec">
												<ul>
													<li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</li>
													<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
													<li>sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</li>
													<li>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit</li>
													<li>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat</li>
												</ul>
											</div>
											<p>Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances.</p>
										</div>
									</div>
									<div class="privacy_policy_main_group">
										<div class="privacy_policy_content">
											<h4>3. similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</h4>
											<p>similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus</p>
										</div>
									</div>
									<div class="privacy_policy_main_group">
										<div class="privacy_policy_content">
											<h4>4. saepe eveniet ut et voluptates repudiandae sint et molestiae </h4>
											<p>non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>
										</div>
									</div>
									<div class="privacy_policy_main_group">
										<div class="privacy_policy_content">
											<h4>5. Same as saying through shrinking</h4>
											<p>demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue</p>
										</div>
									</div>
									<div class="privacy_policy_main_group">
										<div class="privacy_policy_content">
											<h4>6.  But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. </h4>
											<p>But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- terms and conditions section end here-->
					 

				

				
@include('Template.footer') 
			