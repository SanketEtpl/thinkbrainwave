		@include('Template.header')


		<!-- Page wrapper Start here-->
			<div class="wrapper">
			    <!-- reset password Page Start Here -->
				<section class="login_section" style="background-image:url({{ url('public/assets/img/sign-siup-bannner-img.png')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="login_details">
									<div class="login_form">
										<div class="login_form_logo">
											<a href='{{ url("Home")}}'><img src="{{ url('public/assets/img/logo_new.png')}}" class="img-responsive" alt="logo"/></a>
										</div>
										<div class="login_form_set_details">
										
											<div class="login_head">
												<h3>RESET PASSWORD</h3>
											</div>
											<form action = "ResetNewPassword" id="form1" class="form1" method = "post" enctype="multipart/form-data">
            									@csrf
												
												<div class="login_form_details">
													<div class="input_form_set">
														<span>EMAIL ADDRESS</span>
														<input type="password" class="form-control required_check" id="password" name="password" placeholder="Enter password"/>
													</div>
													<div class="input_form_set">
														<span>PASSSWORD</span>
														<input type="password" class="form-control required_check" id="confirmpass" name="confirmpass" placeholder="Enter password"/>
													</div>
													
													<div class="login_sign_up_btns_group">
														<input type="submit" class="btn sign_in_up_btn">Reset</button>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- reset password Page end Here -->
				
		@include('Template.footer')

