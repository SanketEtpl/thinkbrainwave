
		@include('Template.header')
			<!-- Page wrapper Start here-->
			<div class="wrapper">
			  <!-- signup Page Start Here -->
				<section class="login_section" style="background-image:url({{ url('public/assets/img/sign-siup-bannner-img.png')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="login_details">
									<div class="signup_form">
										<div class="login_form_logo">
											<a href='{{ url("Home")}}'><img src="{{ url('public/assets/img/logo_new.png')}}" class="img-responsive" alt="logo"/></a>
										</div>
										<div class="login_form_set_details">
											<div class="login_head">
												<h3>SIGN UP</h3>
											</div>
											<form action = "SignupSave" id="form1" class="form1" method = "post" enctype="multipart/form-data">
            									<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
												<div class="login_form_details">
													<div class="sign_in_up_new_form">
														<div class="row">
															<div class="col-md-6">
																<div class="form_set_sec">
																	<span>SIGN UP AS</span>
																	
																	<select type="select" class="form-control required_check" name="role" id="demandChannel">
																		
																		<option value="">Select User Type</option>
																				@foreach ($roles as $role)
																					<option value="{{ $role->id }}" >{{ $role->role }}</option>
																				@endforeach
																	</select>
																	
																</div>
															</div>
															<div class="col-md-6">
																<div class="input_form_set">
																	<span>FULL NAME</span>
																	<input type="text" class="form-control required_letter" id="name" name="name" placeholder="Enter Full Name" />
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-6">
																<div class="input_form_set">
																	<span>EMAIL ADDRESS</span>
																	<input type="text" class="form-control required_email" id="email" name="email" placeholder="Enter Email Address"/>
																</div>
															</div>
															<div class="col-md-6">
																<div class="input_form_set">
																	<span>CONTACT NUMBERS</span>
																	<input type="text" class="form-control required_mobile" id="contact" name="contact" placeholder="Enter Contact Numbers"/>
																</div>
															</div>
														</div>
														
														<div class="row">
															<div class="col-md-6">
																<div class="input_form_set">
																	<span>PASSWORD</span>
																	<input type="password" class="form-control required_check" id="password" name="password" placeholder="Enter Password"/>
																</div>
															</div>
															<div class="col-md-6">
																<div class="input_form_set">
																	<span>CONFIRM PASSWORD</span>
																	<input type="password" class="form-control required_confirm_password" id="confirm_password" name="confirm_password" placeholder="Enter Confirm Password"/>
																</div>
															</div>
														</div>
													</div>
													<div class="remember_forgot_password">
														<div class="check_inner">
															<input class="filled-in" id="check1" name="check1" type="checkbox">
															<label for="check1">I Accept <a href="#.">Terms & Conditions</a></label>
															<span id="errNm2" style="color: red;">This field is required.</span>
														</div>
													</div>
													<div class="login_sign_up_btns_group_new">
														<a href='{{ url("Signin")}}' class="btn cancel_btn_sign_in">CANCEL</a>
														<button type="button" class="btn submit_btn_sign_in btn-submit">Submit</button>
														
													</div>
												</div>
											</form>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- signup Page end Here -->
				@include('Template.footer')

				<script type="text/javascript">
				$(document).ready(function() {
					$("#errNm2").hide();
				});
				

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
			
				$(".btn-submit").click(function(e){


					if ($('.form1').valid()) {
						e.preventDefault();
				
						var name = $("input[name=name]").val();
						var password = $("input[name=password]").val();
						var confirm_password = $("input[name=confirm_password]").val();
						var email = $("input[name=email]").val();
						var role = $("select[name=role]").val();
						var contact = $("input[name=contact]").val();
						if($("#check1").prop('checked') == true){
							
							$("#errNm2").hide();
							
								$.ajax({
									type:'POST',
									url:'{{ url("SignupSave")}}',
									data:{name:name, password:password, email:email, role:role, contact:contact},
									success:function(data){
										//alert(data.success);
										console.log(data);
										if(data.success=='success')
										{
											$('#sent_success_popup').modal('show');
										}
										else
										{
											$('#sent_error_popup').modal('show');
										}
									}
								});
						}
						else{
							$("#errNm2").show();
						}
					}
				});
				$("#check1").change(function() {
					if(this.checked) {
						$("#errNm2").hide();
					}
					else{
						$("#errNm2").show();
					}
				});
			</script>