@include('Template.header_tutor')
@php
$val=Session::get('loginSessionTutor')
@endphp
@php
	if($users['profile_file_path']!='')
	{
		$image_path = $users['profile_file_path'];
	}
	else{
		$image_path = "no-image.png";
	}
@endphp
			<!-- banner section start here-->
			<section class="banner_section">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="banner_content">
								<h4>DASHBOARD</h4>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- banner section end here-->
			<!-- pie chart section start here-->
			<section class="piechart_section">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="piechart_details">
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12 zero_padding">
										<div class="dash_profile_datils_account">
											<div class="col-md-6 col-sm-6 col-xs-12">
												<div class="dash_bora_profil_user">
													<div class="profile_details_img_with_btn">
														<span class="dash_img_profile_img">
															<div class="dash_borad_bg_color" style="background-image:url(@if($val['session_tutor_type']=='1') {{ URL::to('/') }}/public/images/{{ $image_path }} @else {{ $image_path }} @endif);"></div>
														</span>
														<a href='{{ url("TutorMyProfile")}}' class="btn small_comm_btn">View Profile</a>
													</div>
													<div class="dash_board_details">
														<h4>{{ $users['name'] }}</h4>
														<span>@if($val) {{ $val['session_tutor_role'] }} @endif</span>
														<div class="star_ratting_table_views">
															<ul>
																
																<!-- <li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
															 -->
																
																@php $rating = $TutorRatingCount;  @endphp
																@foreach(range(1,5) as $i)
																		@if($rating >0)
																			@if($rating >0.5)
																				<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																			@elseif($rating <=0.5)
																				<li><a href="#."><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>
																			@endif	
																		@else
																			<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																		@endif
																		@php $rating--; @endphp
																	
																@endforeach
															</ul>
															<small>{{ $TutorRatingCount }}/5</small>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<div class="dash_bora_profil_user">
													<div class="profile_details_img_with_btn">
														<span class="dash_img_profile_img">
															<div class="account_img_set">
																<img src="{{ url('public/assets/TutorAssets/img/my_account.png')}}" class="img-responsive" alt="profile"/>
															</div>
														</span>
														<a href='{{ url("TutorAccount")}}' class="btn small_comm_btn">View Account</a>
													</div>
													<div class="dash_account_set">
														<h4>{{  $TutorSessionCount }}</h4>
														<span>MY SESSIONS</span>
													</div>
												</div>
											</div>	
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12 zero_padding">
										<div class="margin_top"></div>
										<div class="col-md-6">
											<div class="pie_chart_details">
												<div class="pie_chart_heading">
													<h4>SESSION TRACKER</h4>
													<div class="session_track_btn">
														<a href="#." class="btn small_comm_btn session_track_btn_width">View All</a>
													</div>
												</div>
												<div class="calender_form_set_new">
													<div class="row">
														<div class="col-md-12">
															<div class="input_right_align">	
																<div class="input_new_set_select">
																	<select id="chartSelect" class="form-control select_tag_new">
																		<option value="W">Weekly</option>
																		<option value="M">Monthly</option>
																		<option value="Y">Yearly</option>
																	</select>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="piechart-demo">
													<!-- <img src="{{ url('public/assets/TutorAssets/img/barchrt.jpg')}}" class="img-responsive" alt="piechart"/> -->

													<div id="chartContainer" style="height: 300px; width: 100%;"></div>


												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="pie_chart_details">
												<div class="pie_chart_heading">
													<h4>MY PLAN</h4>
												</div>
												<div class="calender_form_set_new">
													<div class="row">
														<!-- <div class="col-md-8">
															<div class="input_new_set_search">
																<input type="text" class="form-control" placeholder="Search"/>
															</div>
														</div>
														<div class="col-md-4">
															<div class="input_new_set_select">
																<select class="form-control select_tag_new">
																	<option>Daily</option>
																	<option>Monthly</option>
																	<option>Yearly</option>
																</select>
															</div>
														</div> -->
													</div>
												</div>
												<div class="calender_paln_set_here">
													<div class="responsive_calender">
														<div class='calendar1 padd-zero'></div>
													</div>
												</div>
												<div class="calnder-btn_set">
													<a href="#." class="btn common_btn" data-toggle="modal" data-target="#availability_add_user">Add AVAILABILITY</a>
												</div>
											</div>
										</div>	
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- pie chart section end here-->
				
				<!-- Successfully session popup Start Here -->
				<div class="modal fade" id="successfilly_session_popup" role="dialog">
						<div class="modal-dialog success_session_popup">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/TutorAssets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="moel_details">
									<div class="model_content model_head_new_set">
										<div class="successful_check_img">
											<img class="img-responsive" src="{{ url('public/assets/TutorAssets/img/Shape.png')}}" alt="successfully" />
										</div>		
										<div class="success_session_text">
											You have successfully updated your Calender
										</div>
										<div class="pop_btn_sect">
											<a href="#." data-dismiss="modal" class="btn save_cancel_btn ">Ok</a>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					</div>
				 <!-- Successfully session popup end Here -->	
			<!-- display calendar -->
				@include('Tutor.calendar_code') 
				<script>
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
					window.onload = function () {
							chart = new CanvasJS.Chart("chartContainer", {
								theme: "light1",
								title:{
									text: "Weekly"              
								},
								data: [              
								{
									type: "column",
									dataPoints: [
										@foreach($TutorSessionData as $TutorSessionData)
											{ label: '{{ $TutorSessionData->day_d }}', y: {{ $TutorSessionData->total_click }}  },
										@endforeach
									]
								}
								]
							});
							chart.render();
											
					
					
							$("#chartSelect").click(function(e){
								var selectStatus = $("select[id=chartSelect]").val();
								var LABLE;
								var URL;
								var TUOTORID = "{{  $val['session_tutor_id'] }}";
								if(selectStatus=='W')
								{
									URL ='{{ url("TutorSessionDataLastweek")}}/'+TUOTORID;
									LABLE = 'Weekly';
								}
								if(selectStatus=='M')
								{
									URL ='{{ url("TutorSessionDataLastMonth")}}/'+TUOTORID;
									LABLE = 'Monthly';
								}
								if(selectStatus=='Y')
								{
									URL ='{{ url("TutorSessionDataLastYear")}}/'+TUOTORID;
									LABLE = 'Yearly';
								}
								console.log(URL);
										$.ajax({
											type:'GET',
											url:URL,
											data:{id:TUOTORID},
											success:function(data){
												
												console.log(data);
												var v;
												v = [];
												var success = data;
												$.each (data, function (key) {
														console.log (success[key].start_date);
														v.push({label: success[key].day_d, y: success[key].total_click});
												});
												
												
												console.log (v);

												chart.render(); 
												chart.destroy();
												
												chart = new CanvasJS.Chart("chartContainer", {
													theme: "light1",
													title:{
														text: LABLE             
													},
													data: [              
													{
														type: "column",
														dataPoints: v
													}
													]
												});
												chart.render();
													
											}
										});
							});		
				
						}
				</script>
				<!-- end display calendar -->

			<!-- tutur availablity code -->
			@include('Tutor.tutor_availability_code')
			<!-- tutur availablity code -->	


			
@include('Template.footer_tutor') 