@include('Template.header_tutor')
@php
$val=Session::get('loginSessionTutor')
@endphp
@php
	if($users['profile_file_path']!='')
	{
		$image_path = $users['profile_file_path'];
	}
	else{
		$image_path = "no-image.png";
	}
@endphp
				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url(./img/mid-bg.jpg')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>Edit Profile</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!--edit profile start here-->
				<form action = '{{ url("TurorProfileSave")}}' class="form1" method = "post" enctype="multipart/form-data">
					<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

					<input type="hidden" name="user_id" id="user_id" value ="{{ $users['user_id'] }}">
					<input type="hidden" name="old_image" value ="{{ $image_path }}">
					<input type="hidden" name="role" id="role" value ="@if($val) {{ $val['session_tutor_role_id'] }} @endif">

				<section class="my_account_all_us_details">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="my_account_data">
									<div class="row">
										<div class="col-md-4 my_account_profile_width_set dash_dta_border_right">
											<div class="dash_bora_profil_user_sec">
												<span class="dash_img_profile_img">
													<img id="profile-img-tag"  src="@if($val['session_tutor_type']=='1') {{ URL::to('/') }}/public/images/{{ $image_path }} @else {{ $image_path }} @endif" class="img-responsive live_streaming_img_width" alt="profile"/>
													<div class="file btn edit_profile_photo">
														<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
														<input type="file" id="profile-img" name="profile_file_path"/>
													</div>
												</span>
												<div class="dash_board_details_sec zero_margin">
													<h4>{{ $users['name'] }}</h4>
													<span>@if($val) {{ $val['session_tutor_role'] }} @endif</span>
													<div class="star_ratting_table_views zero_margin">
														<ul>
																
																<!-- <li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
															 -->
																
																@php $rating = $TutorRatingCount;  @endphp
																@foreach(range(1,5) as $i)
																		@if($rating >0)
																			@if($rating >0.5)
																				<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																			@elseif($rating <=0.5)
																				<li><a href="#."><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>
																			@endif	
																		@else
																			<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																		@endif
																		@php $rating--; @endphp
																	
																@endforeach
															</ul>
															<small>{{ $TutorRatingCount }}/5</small>
													</div>
													<!-- <div class="streaming_tutor_level">Tutor Level : {{ $users['level'] }}</div> -->
												</div>
												<div class="refer_frds_btn_new">
													<a href='{{ url("TutorMyProfile")}}' class="btn small_comm_btn">View Profile</a>
												</div>
											</div>
										</div>
										<div class="col-md-3 dash_dta_border_right">
											<div class="user-privat-details">
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/TutorAssets/img/gender.png')}}" class="img-responsive" alt="gender"/></span>
													<div>{{ $users['gender'] }}</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/TutorAssets/img/age.png')}}" class="img-responsive" alt="age"/></span>
													<div>

														@php
															$dob = $users['dob'];
															$today = date('Y-m-d');
															$d1 = new DateTime($today);
															$d2 = new DateTime($dob);

															$diff = $d2->diff($d1);

															echo $diff->y;
														@endphp
														Years
													</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/TutorAssets/img/map-marker.png')}}" class="img-responsive" alt="gender"/></span>
													<div>{{ $users['address'] }}</div>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="user-privat-details">
												<div class="followers_section">
													<!-- <div class="followers_img">
														<img class="img-responsive" src="{{ url('public/assets/TutorAssets/img/follow.png')}}" alt="follows">
													</div>
													<div class="followers_text_count">
														<div class="following_tutor_name">Followers</div>
														<div class="following_tutor_count">334</div>
													</div> -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--edit profile details end here-->
				<!-- profile details section start here-->
				<section class="common_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="common_section_details_here">
									<div class="common_section_heading">
										<h4>PROFILE DETAILS</h4>
									</div>
									<div class="common_section_content">
										<div class="row">
											<div class="col-md-7">
												<div class="profile_user_details">
													<div class="profile_personal_details">
														<ul>
															<li><span>Name:</span><div class="input_boxes_set"><input type="text" class="form-control required_check" id="name" name="name" value="{{ $users['name'] }}"/></div></li>
															<li><span>TEACHING GRADE:</span> 
																		@php
																			$grade_from = $users['grade_from'];
																			$grade_to = $users['grade_to'];
																		@endphp
																		
																<div class="input_boxes_set input_box_right_padding">
																	<select class="form-control select_tag_box required_check" name="grade_id" id="grade_id">
																		<option value="">------select-------</option>
																		@foreach ($grades as $grade)
																			<option value="{{ $grade->id }}" @if ($grade_from == $grade->id ) selected @endif>{{ $grade->name }}</option>
																		@endforeach
																	</select>
																</div>
																<div class="input_boxes_set input_box_left_padding">
																	<select class="form-control select_tag_box " name="grade_id2" id="grade_id2">
																		<option value="">------select-------</option>
																		@foreach ($grades as $grade)
																			<option value="{{ $grade->id }}" @if ($grade_to == $grade->id ) selected @endif>{{ $grade->name }}</option>
																		@endforeach
																	</select>
																</div>
															</li>	
															<li><span>TEACHING SPECIALITIES:</span>
																<div class="input_boxes_set">
																	@php
																		$subjectArray = explode(',', $users['subject_id']);
																	
																		$count_subject_id = count($subjectArray);
																		$i=0;
																	@endphp
																	<select class="form-control select_tag_box" multiple="multiple" name="subject_id[]" id="subject_id">
																		
																		@foreach ($subjects as $subject)
																			<option value="{{ $subject->grade_id }}-{{ $subject->subject_id }}" @if($i<=$count_subject_id-1) @if (in_array($subject->subject_id, $subjectArray)) selected @php $i++; @endphp @endif  @endif>{{ $subject->grade_name }} - {{ $subject->subject_name }}</option>
																			@php
																				
																			@endphp
																		@endforeach
																	</select>
																	
																</div>
															</li>
															<li><span>TUTOR SINCE:</span>
																<div class="input_boxes_set">
																	<div class="form-group calend_div_new">
																		<div class="input-group date selectDate">
																			<input class="form-control calender_set required_date_check" type="text" name="tutor_since" id="tutor_since" value="{{ $users['tutor_since'] }}">
																			<div class="input-group-addon custom_input_calend"><i class="fa fa-calendar" aria-hidden="true"></i></div>
																		</div>
																		
																	</div>
																</div>
															</li>
															<li><span>EXPERIENCE:</span><div class="input_boxes_set"><input type="text" class="form-control required_check" id="year_exp" name="year_exp" value="{{ $users['year_exp'] }}"/></div>
															</li>
															<li><span>QUALIFICATION:</span><div class="input_boxes_set"><input type="text" class="form-control required_check" id="qualification" name="qualification" value="{{ $users['qualification'] }}"/></div>
															</li>
															<li>
																<span>DATE OF BIRTH:</span>
																<div class="input_boxes_set">
																	<div class="form-group calend_div_new">
																		<div class="input-group date tutorDOB">
																			<input class="form-control calender_set required_date_check" type="text" name="dob" id="dob" value="{{ $users['dob'] }}">
																			<div class="input-group-addon custom_input_calend"><i class="fa fa-calendar" aria-hidden="true"></i></div>
																		</div>
																		<input type="hidden" id="dtp_input2" value="">
																	</div>
																</div>
																<a href="#." class="info-link-details_sec tooltip_new"><span class="tooltiptext_new">Tutor age should be greater than or equal to 20 year.</span><i class="fa fa-info-circle" aria-hidden="true"></i></a>
															</li>
															<li>
																<span>GENDER</span>
																@php
																	$gender = $users['gender'];
																@endphp
																<div class="profile_details_content">
																  <div class="radio_button">
																	<input type="radio" id="test1" name="gender" value="Male"  @if ($gender == "Male") checked @endif >
																	<label for="test1">Male</label>
																  </div>
																  <div class="radio_button">
																	<input type="radio" id="test2" name="gender" value="Female" @if ($gender == "Female") checked @endif >
																	<label for="test2">Female</label>
																  </div>
																</div>
															</li>
															<li><span>MAX DISTANCE TRAVEL:</span><div class="input_boxes_set"><input type="text" class="form-control" id="max_distance_travel" name="max_distance_travel" value="{{ $users['max_distance_travel'] }}"/></div><a href="#." class="info-link-details_sec tooltip_new"><span class="tooltiptext_new">Maximum Distance You Can Travel to Studant</span><i class="fa fa-info-circle" aria-hidden="true"></i></a></li>
															<li><span>ADDRESS</span><div class="input_boxes_set"><input type="text" class="form-control required_check" id="address" name="address" value="{{ $users['address'] }}"/></div></li>
															<li>
																<span>COUNTRY</span>
																<div class="input_boxes_set">
																	<select class="form-control select_tag_box required_check" name="country_id" id="country_id">
																		@php
																			$country_id = $users['country_id'];
																		@endphp
																		<option value=""  selected disabled>------select-------</option>
																		@foreach ($countries as $countrie)
																			<option value="{{ $countrie->id }}" @if ($country_id == $countrie->id ) selected @endif >{{ $countrie->name }}</option>
																		@endforeach
																	</select>
																</div>
															</li>
															<!-- <li><span>PASSWORD:</span><div class="input_boxes_set"><input type="text" class="form-control" id="primary_phone" name="primary_phone/></div><a href="#." class="info-link-details_sec"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
															 -->
															<li><span>PHONE NUMBER:</span><div class="input_boxes_set"><input type="text" class="form-control required_mobile" id="primary_phone" name="primary_phone" value="{{ $users['primary_phone'] }}"/></div></li>
															<li><span>EMAIL ID:</span><div class="input_boxes_set"><input type="text" class="form-control required_email" id="email_id" name="email_id" value="{{ $users['email_id'] }}"/></div></li>
														</ul>
													</div>
													<div class="common_btn_div">
															<a href="#." class="btn small_comm_btn"  data-toggle="modal" data-target="#change_password_popup">Change Password</a>
													</div>
												</div>
											</div>
											<div class="col-md-5">
												<div class="profile_details_shar_pa_div">
													<div class="profile_calender">
														<!-- <div class="Session_rate_start session_rate_bg_color">
															<div class="session_rate_img">
																<img class="img-responsive" src="{{ url('public/assets/TutorAssets/img/session_rate.png')}}" alt="Rate">
															</div>
															<div class="session_rate_text">
																<span class="session_rate_text_head">Session Rate Status At</span>
																<span class="Session_rate_start_amount">$12 USD </span>
															</div>
															
														</div> -->
														<!-- <div class="calender_form_set_new">
															<div class="input_new_set_search input_search_margin">
																<input type="text" class="form-control" placeholder="Search">
															</div>
														</div> -->
														<div class="pie_chart_heading add_repeat_section">
															<h4>MY PLAN</h4>
														</div>
														<!-- <div class="piechart-demo">
															<a href="TutorMyPlan"><img src="{{ url('public/assets/TutorAssets/img/calender-img.jpg')}}" class="img-responsive" alt="piechart"></a>
														</div> -->
														<div class="calender_paln_set_here">
													<div class="responsive_calender">
															<div class='calendar1 profile-calender-set padd-zero calender-set-margin-top'></div>
													</div>
												</div>
													</div>
												</div>
											</div>
											<!-- <div class="col-md-12">
												<div class="tutor_bank_details">
													<h4>BANK DETAILS</h4>
													<div class="profile_personal_details bank_bg_color">
														<ul>
															<li><span>BANK:</span><div class="input_boxes_set"><input type="text" class="form-control" id="bank_name" name="bank_name" value="{{ $users['bank_name'] }}"/></div>
															</li>
															<li><span>ACCOUNT NO:</span><div class="input_boxes_set"><input type="text" class="form-control" id="account_no" name="account_no" value="{{ $users['account_no'] }}"/></div>
															</li>
															<li><span>BRANCH:</span><div class="input_boxes_set"><input type="text" class="form-control" id="branch_name" name="branch_name" value="{{ $users['branch_name'] }}"/></div>
															</li>
															<li><span>IFSC CODE:</span><div class="input_boxes_set"><input type="text" class="form-control" id="Ifsc" name="Ifsc" value="{{ $users['Ifsc'] }}"/></div>
															</li>
															<li><span>SWIFT CODE:</span><div class="input_boxes_set"><input type="text" class="form-control" id="swift_code" name="swift_code" value="{{ $users['swift_code'] }}"/></div>
															</li>
														</ul>
													</div>	
												</div>
											</div> -->
											
											
											<div class="col-md-12">
												<div class="save_adn_cancel_btn_grp">
													<!-- <button type="button" class="btn common_btn_new" data-dismiss="modal" data-toggle="modal" data-target="#successfilly_session_popup">Save</button>
													 -->
													<button type="submit" class="btn common_btn_new" >Save</button>
													
													<button type="button" class="btn common_btn_new">Cancel</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				</form>
				<!-- profile details section end here-->
				
				
				<!--popup start here-->
				
					
					  <!-- Successfully session popup Start Here -->
						<div class="modal fade" id="successfilly_session_popup" role="dialog">
							<div class="modal-dialog success_session_popup">
							  <!-- Modal content-->
							  <div class="modal-content">
								<div class="modal-header">
								  <button type="button" class="close" data-dismiss="modal">
									 <img src="{{ url('public/assets/TutorAssets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
								  </button>
								</div>
								<div class="modal-body">
									<div class="moel_details">
										<div class="model_content model_head_new_set_profile">
											<!-- <div class="successful_check_img"> -->
												<!-- <img class="img-responsive" src="{{ url('public/assets/TutorAssets/img/Shape.png')}}" alt="successfully" /> -->
											<!-- </div>		 -->
											<div class="success_session_text wish_bg_color">
												DO YOU WISH TO<br>UPDATE YOUR PROFILE PICTURE ?
											</div>
											<div class="pop_btn_sect">
												<a href="#." data-dismiss="modal" class="btn save_cancel_btn">Upload</a>
											</div>
										</div>
									</div>
								</div>
							  </div>
							</div>
						</div>
						 <!-- Successfully session popup end Here -->	
					<!--popup end here-->
					<script type="text/javascript">
						function readURL(input) {
							if (input.files && input.files[0]) {
							var reader = new FileReader();

							reader.onload = function (e) {
								$('#profile-img-tag').attr('src', e.target.result);
							}
							reader.readAsDataURL(input.files[0]);
							}
						}
						
						
						$("#profile-img").change(function(){
							readURL(this);
						});
						
					</script>
					@include('Tutor.calendar_code') 

					<!-- Change password script on -->
					@include('Student.change_password') 
					<!-- Change password script on -->
			
@include('Template.footer_tutor') 