<script>
				$(document).ready(function() {
						// page is now ready, initialize the calendar...
						$('.calendar1').fullCalendar({
								// put your options and callbacks here
								header : {
										  left:   'today prev , next',
										  center: 'title',
										  right:  'prevYear , nextYear , agendaDay,agendaWeek,month'
										},
								events : [
										@foreach($Tasks as $Task)
										{
												title : '',
												start : '{{ $Task->start_date }}T{{ $Task->start_time }}',
												link : '{{  $Task->id }}',
												backgroundColor: '#4c9cff',
												eventType : 'BookSession'
										},
										@endforeach
										@foreach($StudentCancletasks as $StudentCancletask)
										{
												title : '',
												start : '{{ $StudentCancletask->start_date }}T{{ $StudentCancletask->start_time }}',
												link : '{{  $StudentCancletask->id }}',
												backgroundColor: '#b90415',
												eventType : 'CancleSession'
										},
										@endforeach
										@foreach($TutorCancletasks as $TutorCancletask)
										{
												title : '',
												start : '{{ $TutorCancletask->start_date }}T{{ $TutorCancletask->start_time }}',
												link : '{{  $TutorCancletask->id }}',
												backgroundColor: '#b94704',
												eventType : 'CancleSession'
										},
										@endforeach
										@foreach($Completedtasks as $Completedtask)
										{
												title : '',
												start : '{{ $Completedtask->start_date }}T{{ $Completedtask->start_time }}',
												link : '{{  $Completedtask->id }}',
												backgroundColor: '#3c763d',
												eventType : 'CompletedSession'
										},
										@endforeach
										@foreach($Booktasks as $Booktask)
										{
												title : '',
												start : '{{ $Booktask->start_date }}T{{ $Booktask->start_time }}',
												link : '{{  $Booktask->id }}',
												backgroundColor: '#0023cc',
												eventType : 'CompletedSession'
										},
										@endforeach
										
								],								
						})
						$('.fc-view-container').dblclick(function(event){
										
							window.location.href="TutorMyPlan";
						});
				});
			</script>
<script>
				$(document).ready(function() {
						// page is now ready, initialize the calendar...
						$('#editable_calendar').fullCalendar({
								// put your options and callbacks here
								header : {
										  left:   'today prev , next',
										  center: 'title',
										  right:  'prevYear , nextYear, agendaDay,agendaWeek,month'
										}, 
								events : [
										@foreach($Tasks as $Task)
										{
												title : '',
												start : '{{ $Task->start_date }}T{{ $Task->start_time }}',
												id : '{{  $Task->id }}',
												student_name : '{{  $Task->student_name }}',
												student_gender : '{{  $Task->student_gender }}',
												start_date : '{{  $Task->start_date }}',
												start_time : '{{  $Task->start_time }}',
												grade_name : '{{  $Task->grade_name }}',
												subject_name : '{{  $Task->subject_name }}',
												topic_name : '{{  $Task->topic_name }}',
												venue : '{{  $Task->venue }}',
												backgroundColor: '#4c9cff',
												eventType : 'BookSession',
												eventStatus : 'Pending',
												eventReason : '{{  $Task->student_cancel_reason }}'
										},
										@endforeach
										@foreach($StudentCancletasks as $StudentCancletask)
										{
												title : '',
												start : '{{ $StudentCancletask->start_date }}T{{ $StudentCancletask->start_time }}',
												id : '{{  $StudentCancletask->id }}',
												student_name : '{{  $StudentCancletask->student_name }}',
												student_gender : '{{  $StudentCancletask->student_gender }}',
												start_date : '{{  $StudentCancletask->start_date }}',
												start_time : '{{  $StudentCancletask->start_time }}',
												grade_name : '{{  $StudentCancletask->grade_name }}',
												subject_name : '{{  $StudentCancletask->subject_name }}',
												topic_name : '{{  $StudentCancletask->topic_name }}',
												venue : '{{  $StudentCancletask->venue }}',
												backgroundColor: '#b90415',
												eventType : 'CancleSession',
												eventStatus : 'Pending',
												eventReason : '{{  $StudentCancletask->student_cancel_reason }}'
										},
										@endforeach
										@foreach($TutorCancletasks as $TutorCancletask)
										{
												title : '',
												start : '{{ $TutorCancletask->start_date }}T{{ $TutorCancletask->start_time }}',
												id : '{{  $TutorCancletask->id }}',
												student_name : '{{  $TutorCancletask->student_name }}',
												student_gender : '{{  $TutorCancletask->student_gender }}',
												start_date : '{{  $TutorCancletask->start_date }}',
												start_time : '{{  $TutorCancletask->start_time }}',
												grade_name : '{{  $TutorCancletask->grade_name }}',
												subject_name : '{{  $TutorCancletask->subject_name }}',
												topic_name : '{{  $TutorCancletask->topic_name }}',
												venue : '{{  $TutorCancletask->venue }}',
												backgroundColor: '#e9681c',
												eventType : 'CancleSession',
												eventStatus : 'Pending',
												eventReason : '{{  $TutorCancletask->tutor_cancel_reason }}'
										},
										@endforeach
										@foreach($Completedtasks as $Completedtask)
										{
												title : '',
												start : '{{ $Completedtask->start_date }}T{{ $Completedtask->start_time }}',
												id : '{{  $Completedtask->id }}',
												student_name : '{{  $Completedtask->student_name }}',
												student_gender : '{{  $Completedtask->student_gender }}',
												start_date : '{{  $Completedtask->start_date }}',
												start_time : '{{  $Completedtask->start_time }}',
												grade_name : '{{  $Completedtask->grade_name }}',
												subject_name : '{{  $Completedtask->subject_name }}',
												topic_name : '{{  $Completedtask->topic_name }}',
												venue : '{{  $Completedtask->venue }}',
												backgroundColor: '#1c881e',
												eventType : 'BookSession',
												eventStatus : 'Complete',
												eventReason : '{{  $Completedtask->tutor_cancel_reason }}'
										},
										@endforeach
										@foreach($Booktasks as $Booktask)
										{
												title : '',
												start : '{{ $Booktask->start_date }}T{{ $Booktask->start_time }}',
												id : '{{  $Booktask->id }}',
												student_name : '{{  $Booktask->student_name }}',
												student_gender : '{{  $Booktask->student_gender }}',
												start_date : '{{  $Booktask->start_date }}',
												start_time : '{{  $Booktask->start_time }}',
												grade_name : '{{  $Booktask->grade_name }}',
												subject_name : '{{  $Booktask->subject_name }}',
												topic_name : '{{  $Booktask->topic_name }}',
												venue : '{{  $Booktask->venue }}',
												backgroundColor: '#2a4cf1',
												eventType : 'BookSession',
												eventStatus : 'Pending',
												eventReason : '{{  $Booktask->tutor_cancel_reason }}'
										},
										@endforeach
										
								],
								
								eventClick: function(event){
										$('#modalTitle').html(event.title);
										$('#modalBody').html(event.title);
										$('#cancleId').val(event.id);
										$('#reason').val('');
										$('#cancel_reason').html(event.eventReason);

										$('#open_student_name').html(event.student_name);
										$('#open_date').html(event.start_date);
										$('#open_time').html(event.start_time);
										$('#open_location').html(event.venue);
										$('#open_grade').html(event.grade_name);
										$('#open_subject').html(event.subject_name);
										$('#open_topic').html(event.topic_name);
										//$('#cancel_reason').html('hello');
										//$('#fullCalModal').modal();
										if(event.eventType=='BookSession')
										{
											if(event.eventStatus=='Complete')
											{
												$('#id_cancel').attr("data-target","#");
												$("#id_cancel").text("Cancel");
											}
											else
											{
												$('#id_cancel').attr("data-target","#cancel_session_popup");
												$("#id_cancel").text("Cancel Session");
											}


											$('#account_session_details').modal('show');
										
										}
										if(event.eventType=='CancleSession')
										{
											$('#cancel_reason_session_popup').modal('show');
										}
								},
								
						})
						
						$("#cancleSave").click(function(e){
								
								var cancleId = $('#cancleId').val();
								var reason = $('#reason').val();
								if(reason=='')
								{
									reason_val="-";
								}
								else
								{
									reason_val=reason;
								}
								var url ='{{ url("TutorSessionCancleSave")}}/'+cancleId+"/"+reason_val;
								window.location.replace(url);
						});

				});
			</script>