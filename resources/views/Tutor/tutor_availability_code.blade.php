<!-- Add session popup Start Here -->
<div class="modal fade" id="availability_add_user" role="dialog">
						<div class="modal-dialog add_event_popup_width">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/TutorAssets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<form class="form1" method = "post" >
									<div class="model_details">
										<div class="section_heading">
											<h4>ADD NEW AVAILABILITY</h4>
										</div>
										<div class="model_content">
											<div class="my_session_info_sec">
												<div class="row">
													<div class="col-md-4 margin_bottom_new_event">
														<div class="add_event_title">ADD TITLE</div>
														<div class="input_boxes_set"><input type="text" id="add_title"  name="add_title" class="form-control required_check" placeholder="Enter Title"/></div>
													</div>
													<div class="col-md-4 margin_bottom_new_event">
														<div class="add_event_title">DESCRIPTION</div>
														<div class="input_boxes_set"><input type="text" id="description"  name="description" class="form-control required_check" placeholder="Enter Text"/></div>
													</div>
													
													<div class="col-md-4 margin_bottom_new_event">
														<div class="add_event_title">REPEAT</div>
														<div class="input_boxes_set">
															<div class="input_boxes_set">
																<select class="form-control select_tag_box required_check" id="repeat"  name="repeat" >
																	<option value=""  selected disabled>Select</option>
																	<option value="Never">Never</option>
																	<option value="EveryDay">Every Day</option>
																	<option value="EveryWeek">Every Week</option>
																	<option value="EveryMonth">Every Month</option>
																</select>
															</div>
														</div>
													</div>
													<div class="col-md-4 margin_bottom_new_event" id="div_from_date">
														<div class="add_event_title">FROM DATE</div>
														<div class="input_boxes_set">
															<div class="form-group calend_div_new">
																<div class="input-group date newDate">
																	<input class="form-control calender_set" type="text" name="from_date" id="from_date" placeholder="Select Date">
																	<div class="input-group-addon custom_input_calend calender_bg_color"><i class="fa fa-calendar" aria-hidden="true"></i></div>
																</div>
																<input type="hidden" id="dtp_input2" value="">
															</div>
														</div>
													</div>
													<div class="col-md-4 margin_bottom_new_event" id="div_to_date">
														<div class="add_event_title">TO DATE</div>
														<div class="input_boxes_set">
															<div class="form-group calend_div_new">
																<div class="input-group date newDate">
																	<input class="form-control calender_set" type="text" name="to_date" id="to_date" placeholder="Select Date">
																	<div class="input-group-addon custom_input_calend calender_bg_color"><i class="fa fa-calendar" aria-hidden="true"></i></div>
																</div>
																<input type="hidden" id="dtp_input2" value="">
															</div>
														</div>
													</div>
													
													<div class="col-md-4 margin_bottom_new_event" id="div_from_month">
														<div class="add_event_title">From Month</div>
														<div class="input_boxes_set">
															<div class="form-group calend_div_new">
																<div class="input-group date newMonth">
																	<input class="form-control calender_set" type="text" name="from_month" id="from_month" placeholder="Select Month">
																	<div class="input-group-addon custom_input_calend calender_bg_color"><i class="fa fa-calendar" aria-hidden="true"></i></div>
																</div>
																<input type="hidden" id="from_month" value="">
															</div>
														</div>
													</div>
													<div class="col-md-4 margin_bottom_new_event" id="div_to_month">
														<div class="add_event_title">To Month</div>
														<div class="input_boxes_set">
															<div class="form-group calend_div_new">
																<div class="input-group date newMonth">
																	<input class="form-control calender_set" type="text" name="to_month" id="to_month" placeholder="Select Month">
																	<div class="input-group-addon custom_input_calend calender_bg_color"><i class="fa fa-calendar" aria-hidden="true"></i></div>
																</div>
																<input type="hidden" id="to_month" value="">
															</div>
														</div>
													</div>
													<div class="col-md-4 margin_bottom_new_event" id="div_availability_date">
														<div class="add_event_title">AVAILABILITY DATE</div>
														<div class="input_boxes_set">
															<div class="form-group calend_div_new">
																<div class="input-group date newDate">
																	<input class="form-control calender_set" type="text" name="availability_date" id="availability_date" placeholder="Select Date">
																	<div class="input-group-addon custom_input_calend calender_bg_color"><i class="fa fa-calendar" aria-hidden="true"></i></div>
																</div>
																<input type="hidden" id="dtp_input2" value="">
															</div>
														</div>
													</div>
													<div class="col-md-4 margin_bottom_new_event" id="div_availability_from_time">
														<div class="add_event_title"> FROM TIME</div>
														
														<div class="input_boxes_set">
															<div class="form-group calend_div_new">
																<div class="input-group date newTime" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
																	<input class="form-control calender_set required_time_check" size="16" type="text" placeholder="00:00" name="availability_from_time" id="availability_from_time">
																	<div class="input-group-addon custom_input_calend"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
																</div>
																<input type="hidden" id="dtp_input3" value="">
															</div>
														</div>
													</div>
													<div class="col-md-4 margin_bottom_new_event" id="div_availability_to_time">
														<div class="add_event_title"> TO TIME</div>
														
														<div class="input_boxes_set">
															<div class="form-group calend_div_new">
																<div class="input-group date newTime" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
																	<input class="form-control calender_set required_time_check" size="16" type="text" placeholder="00:00" name="availability_to_time" id="availability_to_time">
																	<div class="input-group-addon custom_input_calend"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
																</div>
																<input type="hidden" id="dtp_input3" value="">
															</div>
														</div>
													</div>
													<div class="col-md-12 margin_bottom_new_event"  id="div_availability_week">
														<div class="add_event_title">AVAILABILITY DAY'S</div>
														<div class="input_boxes_set"><br><br>
																
															<div class="weekDays-selector">
																<input type="checkbox" id="weekday-mon" name="availability_days" value="Monday" class="weekday" />
																<label for="weekday-mon">Monday</label>
																<input type="checkbox" id="weekday-tue" name="availability_days"  value="Tuesday" class="weekday" />
																<label for="weekday-tue">Tuesday</label>
																<input type="checkbox" id="weekday-wed" name="availability_days"  value="Wednesday" class="weekday" />
																<label for="weekday-wed">Wednesday</label>
																<input type="checkbox" id="weekday-thu" name="availability_days"  value="Thursday" class="weekday" />
																<label for="weekday-thu">Thursday</label>
																<input type="checkbox" id="weekday-fri" name="availability_days"  value="Friday" class="weekday" />
																<label for="weekday-fri">Friday</label>
																<input type="checkbox" id="weekday-sat" name="availability_days"  value="Saturday" class="weekday" />
																<label for="weekday-sat">Saturday</label>
																<input type="checkbox" id="weekday-sun" name="availability_days"  value="Sunday" class="weekday" />
																<label for="weekday-sun">Sunday</label>
															</div>

														</div>
													</div>
													
													<div class="col-md-12 margin_bottom_new_event"  id="div_availability_month">
														<div class="add_event_title">AVAILABILITY DATE</div>
														<div class="input_boxes_set"><br><br>
																
															<div class="monthDays-selector">
																<input type="checkbox" id="monthDay-1" name="availability_month" value="1" class="monthDay" />
																<label for="monthDay-1">1</label>
																<input type="checkbox" id="monthDay-2" name="availability_month"  value="2" class="monthDay" />
																<label for="monthDay-2">2</label>
																<input type="checkbox" id="monthDay-3" name="availability_month"  value="3" class="monthDay" />
																<label for="monthDay-3">3</label>
																<input type="checkbox" id="monthDay-4" name="availability_month"  value="4" class="monthDay" />
																<label for="monthDay-4">4</label>
																<input type="checkbox" id="monthDay-5" name="availability_month"  value="5" class="monthDay" />
																<label for="monthDay-5">5</label>
																<input type="checkbox" id="monthDay-6" name="availability_month"  value="6" class="monthDay" />
																<label for="monthDay-6">6</label>
																<input type="checkbox" id="monthDay-7" name="availability_month"  value="7" class="monthDay" />
																<label for="monthDay-7">7</label>
																<input type="checkbox" id="monthDay-8" name="availability_month"  value="8" class="monthDay" />
																<label for="monthDay-8">8</label>
																<input type="checkbox" id="monthDay-9" name="availability_month"  value="9" class="monthDay" />
																<label for="monthDay-9">9</label>
																<input type="checkbox" id="monthDay-10" name="availability_month"  value="10" class="monthDay" />
																<label for="monthDay-10">10</label>

																<input type="checkbox" id="monthDay-11" name="availability_month" value="11" class="monthDay" />
																<label for="monthDay-11">11</label>
																<input type="checkbox" id="monthDay-12" name="availability_month"  value="12" class="monthDay" />
																<label for="monthDay-12">12</label>
																<input type="checkbox" id="monthDay-31" name="availability_month"  value="13" class="monthDay" />
																<label for="monthDay-13">13</label>
																<input type="checkbox" id="monthDay-14" name="availability_month"  value="14" class="monthDay" />
																<label for="monthDay-14">14</label>
																<input type="checkbox" id="monthDay-15" name="availability_month"  value="15" class="monthDay" />
																<label for="monthDay-15">15</label>
																<input type="checkbox" id="monthDay-16" name="availability_month"  value="16" class="monthDay" />
																<label for="monthDay-16">16</label>
																<input type="checkbox" id="monthDay-17" name="availability_month"  value="17" class="monthDay" />
																<label for="monthDay-17">17</label>
																<input type="checkbox" id="monthDay-18" name="availability_month"  value="18" class="monthDay" />
																<label for="monthDay-18">18</label>
																<input type="checkbox" id="monthDay-19" name="availability_month"  value="19" class="monthDay" />
																<label for="monthDay-19">19</label>
																<input type="checkbox" id="monthDay-20" name="availability_month"  value="20" class="monthDay" />
																<label for="monthDay-20">20</label>

																<input type="checkbox" id="monthDay-21" name="availability_month" value="21" class="monthDay" />
																<label for="monthDay-21">21</label>
																<input type="checkbox" id="monthDay-22" name="availability_month"  value="22" class="monthDay" />
																<label for="monthDay-22">22</label>
																<input type="checkbox" id="monthDay-23" name="availability_month"  value="23" class="monthDay" />
																<label for="monthDay-23">23</label>
																<input type="checkbox" id="monthDay-24" name="availability_month"  value="24" class="monthDay" />
																<label for="monthDay-24">24</label>
																<input type="checkbox" id="monthDay-25" name="availability_month"  value="25" class="monthDay" />
																<label for="monthDay-25">25</label>
																<input type="checkbox" id="monthDay-26" name="availability_month"  value="26" class="monthDay" />
																<label for="monthDay-26">26</label>
																<input type="checkbox" id="monthDay-27" name="availability_month"  value="27" class="monthDay" />
																<label for="monthDay-27">27</label>
																<input type="checkbox" id="monthDay-28" name="availability_month"  value="28" class="monthDay" />
																<label for="monthDay-28">28</label>
																<input type="checkbox" id="monthDay-29" name="availability_month"  value="29" class="monthDay" />
																<label for="monthDay-29">29</label>
																<input type="checkbox" id="monthDay-30" name="availability_month"  value="30" class="monthDay" />
																<label for="monthDay-30">30</label>
																<input type="checkbox" id="monthDay-31" name="availability_month"  value="31" class="monthDay" />
																<label for="monthDay-31">31</label>
															</div>

														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="pop_btn_sect pop_right_location">
											<a href="#." class="btn save_cancel_btn " data-dismiss="" id="saveAvailability">Submit</a>
											<a href="#." data-dismiss="modal" class="btn save_cancel_btn ">Reset</a>
										</div>
									</div>
								</form>
							</div>
						  </div>
						</div>
					</div>
				<!-- Add session popup end Here -->


<script>
jQuery(document).ready(function(){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});


		jQuery('.scrollbar-inner').scrollbar();

		$("#div_from_date").hide();
		$("#div_to_date").hide();
		$("#div_availability_week").hide();
		
		$("#div_from_month").hide();
		$("#div_to_month").hide();
		$("#div_availability_month").hide();

		$("#repeat").click(function(){

				var repeat = $("select[name=repeat]").val();

				if(repeat=='Never')
				{
					$("#div_from_date").hide();
					$("#div_to_date").hide();
					$("#div_availability_date").show();
					$("#div_availability_week").hide();
					$("#div_from_month").hide();
					$("#div_to_month").hide();
					$("#div_availability_month").hide();

				}
				if(repeat=='EveryDay')
				{
					$("#div_from_date").show();
					$("#div_to_date").show();
					$("#div_availability_date").hide();
					$("#div_availability_week").hide();

					$("#div_from_month").hide();
					$("#div_to_month").hide();
					$("#div_availability_month").hide();

				}
				if(repeat=='EveryWeek')
				{
					$("#div_from_date").show();
					$("#div_to_date").show();
					$("#div_availability_date").hide();
					$("#div_availability_week").show();

					$("#div_from_month").hide();
					$("#div_to_month").hide();
					$("#div_availability_month").hide();

				}
				if(repeat=='EveryMonth')
				{
					$("#div_from_date").hide();
					$("#div_to_date").hide();
					$("#div_availability_date").hide();
					$("#div_availability_week").hide();

					$("#div_from_month").show();
					$("#div_to_month").show();
					$("#div_availability_month").show();

				}
		});
		
		$("#saveAvailability").click(function(){
			var add_title = $("input[name=add_title]").val();
			var description = $("input[name=description]").val();
			var repeat = $("select[name=repeat]").val();
			var from_date = $("input[name=from_date]").val();
			var to_date = $("input[name=to_date]").val();
			var availability_date = $("input[name=availability_date]").val();
			var availability_from_time = $("input[name=availability_from_time]").val();
			var availability_to_time = $("input[name=availability_to_time]").val();

			var from_month = $("input[name=from_month]").val();
			var to_month = $("input[name=to_month]").val();
		
			var availability_days= [];

			$("input:checkbox[name=availability_days]:checked").each(function(){
			availability_days.push($(this).val());
			});

			availability_days_json= JSON.stringify(availability_days);


			var availability_month= [];

			$("input:checkbox[name=availability_month]:checked").each(function(){
			availability_month.push($(this).val());
			});

			availability_month_json= JSON.stringify(availability_month);
			
					
				if (repeat == 'Never')
				{
					$("#availability_date").addClass("required_date_check");
				}
				else if (repeat == 'EveryDay')
				{
					$("#from_date").addClass("required_date_check");
					$("#to_date").addClass("required_date_check");
				}
				else if (repeat == 'EveryWeek')
				{
					$("#from_date").addClass("required_date_check");
					$("#to_date").addClass("required_date_check");
				}
				else if (repeat == 'EveryMonth')
				{
					$("#from_month").addClass("required_date_check");
					$("#to_month").addClass("required_date_check");
				}
				else
				{
					
					$("#availability_date").addClass("required_date_check");
				}
			if ($('.form1').valid()) 
			{
				
				var URL = '{{ url("TutorAvailabilitySave")}}';	
				$.ajax({
					type:'POST',
					url:URL,
					data:{add_title:add_title, description:description, repeat:repeat, from_date:from_date, to_date:to_date, availability_date:availability_date, availability_from_time:availability_from_time, availability_to_time:availability_to_time,availability_days:availability_days_json,  from_month:from_month,to_month:to_month, availability_month:availability_month_json},
					success:function(data){
						//alert(data.success);
						console.log(data);
						if(data.success=='success')
						{
							$('#availability_add_user').modal('hide');
							$('#successfilly_session_popup').modal('show');
						}
						else
						{
						}
					}
				});
			}

			//$('#successfilly_session_popup').modal('show');
		});
});
</script>