@include('Template.header_tutor')
				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url(./img/mid-bg.jpg);">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>NOTIFICATION</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!-- notifiaction section start here-->
				<section class="web_section_common">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="section_details">
									<div class="Notification_list_heading">
										NOTIFICATION LIST
									</div>
									<div class="notification_blog">
										@foreach ($AllNotificationData as $AllNotificationData)
											<div class="notifiaction_info_list_active">
												<span class="date_of_notification_active">{{ date('d-m-Y h:i A', strtotime($AllNotificationData->created_at)) }}</span>
												<div class="noti_list_pass">
													<div class="@if($AllNotificationData->status=='0') noti_titel @else noti_titel_read @endif"><div class="textLimit">{{ $AllNotificationData->message }}</div></div>
													<div class="noti_titel_left_arrow">
													<a href='{{ url("TutorViewNotificationDetails")}}/{{ $AllNotificationData->id }}'><i class="fa fa-angle-right" aria-hidden="true"></i></a>
													</div>
												</div>
											</div>
										@endforeach
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- notifiaction section end here-->
				
@include('Template.footer_tutor') 
