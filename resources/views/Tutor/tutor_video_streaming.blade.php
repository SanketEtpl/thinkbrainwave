@include('Template.header_tutor')
				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url(./img/mid-bg.jpg);">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>VIDEO STREAMING</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!-- video streaming info start here-->
				<section class="my_account_all_us_details">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="my_account_data">
									<div class="row">
										<div class="col-md-4 my_account_profile_width_set dash_dta_border_right">
											<div class="dash_bora_profil_user_sec">
												<span class="dash_img_profile_img">
													<img src="{{ url('public/assets/TutorAssets/img/followers-06.jpg')}}" class="img-responsive live_streaming_img_width" alt="profile"/>
												</span>
												<div class="dash_board_details_sec zero_margin">
													<h4>John Joseph</h4>
													<span>Tutor</span>
													<div class="star_ratting_table_views zero_margin">
														<ul>
															<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
															<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
															<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
															<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
															<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
														</ul>
														<small>3.56/5</small>
													</div>
													<div class="streaming_tutor_level">Tutor Level : 3</div>
												</div>
											</div>
										</div>
										<div class="col-md-3 dash_dta_border_right">
											<div class="user-privat-details">
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/TutorAssets/img/gender.png')}}" class="img-responsive" alt="gender"/></span>
													<div>Male</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/TutorAssets/img/age.png')}}" class="img-responsive" alt="age"/></span>
													<div>52 Years</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/TutorAssets/img/map-marker.png')}}" class="img-responsive" alt="gender"/></span>
													<div>B3/101, Near South Main Road</div>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="user-privat-details">
												<div class="followers_section">
													<div class="followers_img">
														<img class="img-responsive" src="{{ url('public/assets/TutorAssets/img/follow.png')}}" alt="follows">
													</div>
													<div class="followers_text_count">
														<div class="following_tutor_name">Followers</div>
														<div class="following_tutor_count">334</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--video streaming info end here-->
				<!-- live video section start here-->
				<section class="common_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="common_section_details_here">
									<div class="common_section_heading">
										<h4>LIVE VIDEO STREAMING</h4>
									</div>
									<div class="common_section_content">
										<div class="video_note">
											<span>Note: All video content is monitored for quality assurance purposes</span>
										</div>
										<div class="live_video_sectioin">
											<div class="live_video_heading">
												<div class="liv_video_left_side">
													<i class="fa fa-video-camera" aria-hidden="true"></i>
													<h4>Live Video</h4>
												</div>
												<div class="live_session_time">
													<h4>Session Starting in : <span>  02:54 mins</span></h4>
												</div>
											</div>
											<div class="video_live_here">
												<iframe src="https://www.youtube.com/embed/l5XTwf1gk4E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
											</div>
										</div>
									</div>
									<div class="new_commn_grop_btn">
										<button type="button" class="btn common_btn" data-toggle="modal" data-target="#report_issue_popups">Report Issue</button>
										<button type="button" class="btn common_btn end_session_color">End Session</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- live video section end here-->
				<!-- footer section start Here -->
				
					<!-- report issue popup Start Here -->
						<div class="modal fade" id="report_issue_popups" role="dialog">
							<div class="modal-dialog change_password_popup_width">
								<!-- Modal content-->
								<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
									<img src="{{ url('public/assets/TutorAssets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
									</button>
								</div>
								<div class="modal-body">
									<div class="model_details">
										<div class="section_heading_new">
											<h4>REPORT ISSUE</h4>
										</div>
										<div class="model_content">
											<div class="report_tutor_issue_radio_option">
												<div class="radio_button">
													<input type="radio" id="test9" name="radio-group" checked="">
													<label for="test9">STUDENT DID NOT JOIN SESSION</label>
												</div>
												<div class="radio_button">
													<input type="radio" id="test10" name="radio-group" checked="">
													<label for="test10">STUDENT CONNECTION ERROR</label>
												</div>
												<div class="radio_button">
													<input type="radio" id="test11" name="radio-group" checked="">
													<label for="test11">STUDENT NOT INTERESTED IN THE SESSION</label>
												</div>
											</div>
											<div class="report_issue_form">
												<h4>COMMENTS</h4>
												<textarea class="form-control" placeholder="Enter Comments.."></textarea>
											</div>
											<div class="main_profil_btn">
												<a href="#." class="btn save_cancel_btn margin-right-10px" data-dismiss="modal" data-toggle="modal" data-target="#thnk_bran_stream_video_popups">SUBMIT</a>
												<a href="#." class="btn save_cancel_btn" data-dismiss="modal">CANCEL</a>
											</div>
										</div>
									</div>
								</div>
								</div>
							</div>
					  </div>
					  <!-- report issue popup end Here -->
					  <!-- report issue popup Start Here -->
					  <div class="modal fade" id="thnk_bran_stream_video_popups" role="dialog">
							<div class="modal-dialog change_password_popup_width">
								<!-- Modal content-->
								<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
									<img src="{{ url('public/assets/TutorAssets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
									</button>
								</div>
								<div class="modal-body">
									<div class="model_details">
										<div class="section_heading_new">
											<h4>THANK YOU FOR USING <br>BRAINWAVE LIVE STREAMING ENGINE</h4>
											<p>Please rate us and provide feedback to serv you better</p>
										</div>
										<div class="model_content">
											<div class="report_issue_star_rate new_star_rat_issu_rate">
												<h4>RATINGS</h4>
												<div class="star_ratting_table_views_new">
													<ul>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="main_profil_btn btn_popups_center">
												<a href="#." class="btn save_cancel_btn margin-right-10px" data-dismiss="modal" data-toggle="modal" data-target="#thnk_bran_stream_edit_popups">SUBMIT</a>
												<a href="#." class="btn save_cancel_btn" data-dismiss="modal">CANCEL</a>
											</div>
										</div>
									</div>
								</div>
								</div>
							</div>
							</div>
							<!-- report issue popup end Here -->
						<!-- report issue popup Start Here -->
						<div class="modal fade" id="thnk_bran_stream_edit_popups" role="dialog">
							<div class="modal-dialog change_password_popup_width">
								<!-- Modal content-->
								<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
									<img src="{{ url('public/assets/TutorAssets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
									</button>
								</div>
								<div class="modal-body">
									<div class="model_details">
										<div class="section_heading_new">
											<h4>THANK YOU FOR USING <br>BRAINWAVE LIVE STREAMING ENGINE</h4>
											<p>Please rate us and provide feedback to serv you better</p>
										</div>
										<div class="model_content">
											<div class="report_issue_star_rate">
												<h4>RATINGS</h4>
												<div class="star_ratting_table_views_new">
													<ul>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="report_issue_form">
												<h4>COMMENTS</h4>
												<textarea class="form-control" placeholder="Enter Comments.."></textarea>
											</div>
											<div class="main_profil_btn">
												<a href="#." class="btn save_cancel_btn margin-right-10px" >SUBMIT</a>
												<a href="#." class="btn save_cancel_btn" data-dismiss="modal">CANCEL</a>
											</div>
										</div>
									</div>
								</div>
								</div>
							</div>
					  </div>
					  <!-- report issue popup end Here -->
				<!--popup end here-->
@include('Template.footer_tutor') 			