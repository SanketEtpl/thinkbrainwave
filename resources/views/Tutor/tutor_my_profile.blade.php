@include('Template.header_tutor')
@php
$val=Session::get('loginSessionTutor')
@endphp
@php
	if($users['profile_file_path']!='')
	{
		$image_path = $users['profile_file_path'];
	}
	else{
		$image_path = "no-image.png";
	}
@endphp
				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url({{ url('public/assets/TutorAssets/img/mid-bg.jpg)')}};">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>My Profile</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!--account user details info start here-->
				<input type="hidden" name="user_id" id="user_id" value ="{{ $users['user_id'] }}">
					
				<section class="my_account_all_us_details">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="my_account_data">
									<div class="row">
										<div class="col-md-4 my_account_profile_width_set dash_dta_border_right">
											<div class="dash_bora_profil_user_sec">
												<span class="dash_img_profile_img">
													<img src="@if($val['session_tutor_type']=='1') {{ URL::to('/') }}/public/images/{{ $image_path }} @else {{ $image_path }} @endif" class="img-responsive live_streaming_img_width" alt="profile"/>
												</span>
												<div class="dash_board_details_sec zero_margin">
													<h4>{{ $users['name'] }}</h4>
													<span>@if($val) {{ $val['session_tutor_role'] }} @endif</span>
													<div class="star_ratting_table_views zero_margin">
															<ul>
																
																<!-- <li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
															 -->
																
																@php $rating = $TutorRatingCount;  @endphp
																@foreach(range(1,5) as $i)
																		@if($rating >0)
																			@if($rating >0.5)
																				<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																			@elseif($rating <=0.5)
																				<li><a href="#."><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>
																			@endif	
																		@else
																			<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																		@endif
																		@php $rating--; @endphp
																	
																@endforeach
															</ul>
															<small>{{ $TutorRatingCount }}/5</small>
													</div>
													<!-- <div class="streaming_tutor_level">Tutor Level : {{ $users['level'] }}</div> -->
												</div>
												<div class="refer_frds_btn_new">
													<a href='{{ url("TutorEditProfile")}}' class="btn small_comm_btn">Edit Profile</a>
												</div>
											</div>
										</div>
										<div class="col-md-3 dash_dta_border_right">
											<div class="user-privat-details">
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/TutorAssets/img/gender.png')}}" class="img-responsive" alt="gender"/></span>
													<div>{{ $users['gender'] }}</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/TutorAssets/img/age.png')}}" class="img-responsive" alt="age"/></span>
													<div>

														@php
															$dob = $users['dob'];
															$today = date('Y-m-d');
															$d1 = new DateTime($today);
															$d2 = new DateTime($dob);

															$diff = $d2->diff($d1);

															echo $diff->y;
														@endphp
														Years
													</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/TutorAssets/img/map-marker.png')}}" class="img-responsive" alt="gender"/></span>
													<div>{{ $users['address'] }}</div>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="user-privat-details">
												<div class="followers_section">
													<!-- <div class="followers_img">
														<img class="img-responsive" src="{{ url('public/assets/TutorAssets/img/follow.png')}}" alt="follows">
													</div>
													<div class="followers_text_count">
														<div class="following_tutor_name">Followers</div>
														<div class="following_tutor_count">334</div>
													</div> -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--account user details info end here-->
				<!-- profile details section start here-->
				<section class="common_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="common_section_details_here">
									<div class="common_section_heading">
										<h4>PROFILE DETAILS</h4>
									</div>
									<div class="common_section_content">
										<div class="row">
											<div class="col-md-7">
												<div class="profile_user_details">
													<div class="profile_personal_details">
														<ul>
															<li><span>TEACHING GRADE:</span><div class="profile_details_content">{{ $users['grade_from_name'] }} To {{ $users['grade_to_name'] }} </div></li>
															<li><span>TEACHING SPECIALITIES:</span><div class="profile_details_content">{{ $users['subject_name'] }}</div></li>
															<li><span>TUTOR SINCE:</span><div class="profile_details_content">{{ $users['tutor_since'] }}</div></li>
															<li><span>EXPERIENCE:</span><div class="profile_details_content">{{ $users['year_exp'] }}</div></li>
															<li><span>QUALIFICATION:</span><div class="profile_details_content"></div>{{ $users['qualification'] }}</li>
															<li><span>DATE OF BIRTH:</span><div class="profile_details_content">{{ $users['dob'] }}</div></li>
															<li><span>GENDER:</span><div class="profile_details_content">{{ $users['gender'] }}</div></li>
															<li><span>MAX DISTANCE TRAVEL:</span><div class="profile_details_content_link ">{{ $users['max_distance_travel'] }}</div><a href="#." class="info-link-details tooltip_new"><span class="tooltiptext_new">Maximum Distance You Can Travel to Studant</span><i class="fa fa-info-circle" aria-hidden="true"></i></a></li>
															<li><span>ADDRESS:</span><div class="profile_details_content">{{ $users['address'] }}</div></li>
															<li><span>COUNTRY:</span><div class="profile_details_content">{{ $users['country_name'] }}</div></li>
															<li><span>PASSWORD:</span><div class="profile_details_content_link">**********</div><a href="#." class="info-link-details"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
															<li><span>PHONE NUMBER:</span><div class="profile_details_content">{{ $users['primary_phone'] }}</div></li>
															<li><span>EMAIL ID:</span><div class="profile_details_content">{{ $users['email_id'] }}</div></li>
														</ul>
													</div>
													<div class="common_btn_div">
															<a href="#." class="btn small_comm_btn"  data-toggle="modal" data-target="#change_password_popup">Change Password</a>
														</div>		
												</div>
											</div>
											<div class="col-md-5">
												<div class="profile_details_shar_pa_div">
													<div class="profile_calender">
														<!-- <div class="Session_rate_start session_rate_bg_color">
															<div class="session_rate_img">
																<img class="img-responsive" src="{{ url('public/assets/TutorAssets/img/session_rate.png')}}" alt="Rate">
															</div>
															<div class="session_rate_text">
																<span class="session_rate_text_head">Session Rate Status At</span>
																<span class="Session_rate_start_amount">$12 USD </span>
															</div>
															
														</div> -->
														<div class="calender_form_set_new">
																<!-- <div class="input_new_set_search input_search_margin">
																	<input type="text" class="form-control" placeholder="Search">
																</div> -->
														</div>
														<div class="pie_chart_heading add_repeat_section">
															<h4>MY PLAN</h4>
														</div>
														<div class="piechart-demo">
															<!-- <a href="TutorMyPlan"><img src="{{ url('public/assets/TutorAssets/img/calender-img.jpg')}}" class="img-responsive" alt="piechart"></a> -->
															<div class="calender_paln_set_here">
															<div class="responsive_calender">
																<div class='calendar1  profile-calender-set padd-zero calender-set-margin-top '></div>
															</div>
												</div>
														</div>
													</div>
												</div>
											</div>
											<!-- <div class="col-md-12">
												<div class="tutor_bank_details">
													<h4>BANK DETAILS</h4>
													<div class="profile_personal_details bank_bg_color">
														<ul>
															<li><span>BANK:</span><div class="profile_details_content">{{ $users['bank_name'] }}</div></li>
															<li><span>BRANCH:</span><div class="profile_details_content">{{ $users['branch_name'] }}</div></li>
															<li><span>IFSC CODE:</span><div class="profile_details_content">{{ $users['Ifsc'] }}</div></li>
															<li><span>SWIFT CODE:</span><div class="profile_details_content">{{ $users['swift_code'] }}</div></li>
														</ul>
													</div>	
												</div>
											</div> -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- profile details section end here-->
				@include('Tutor.calendar_code') 
				<!-- Change password script on -->
				@include('Student.change_password') 
				<!-- Change password script on -->
@include('Template.footer_tutor') 