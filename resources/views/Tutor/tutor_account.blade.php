@include('Template.header_tutor')

			<!-- banner section start here-->
		<section class="banner_section">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner_content">
							<h4>My Account</h4>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- banner section end here -->
		<!-- my account section start here-->
		<section class="my_accoun_section account_bg_color">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="account_top_head_balance_money">
							<div class="account_curancy_img">
								<img class="img-responsive" src="{{ url('public/assets/TutorAssets/img/account_cur.png')}}" alt="curancy">
							</div>
							<div class="total_balance_count_name">
								<div class="total_balance_count">1778</div><br>
								<div class="total_balance_name">Total Balance</div>
							</div>	
							<!-- <div class="withdraw_money">
								<a href="#." data-toggle="modal" data-target="#transactions_wallet_popup"><img class="img-responsive" src="{{ url('public/assets/TutorAssets/img/wallet.png')}}" alt="wallet">
								<span class="withdrawmoney_name">Withdraw Money</span></a>
							</div> -->
							
						</div>
						<div class="account_section_deta">
							<div class="acc_secti_tabing">
								<ul class="nav nav-tabs">
								  <li class="active"><a data-toggle="tab" href="#home">UPCOMING</a></li>
								  <li><a data-toggle="tab" href="#menu1">PAST</a></li>
								</ul>
								<div class="session_track_btn">
									<!-- <a href="#." class="btn small_comm_btn session_track_btn_width add_session_font">Add Session</a> -->
								</div>
							</div>
							<div class="tab_account_content">
								<div class="tab-content">
									<div id="home" class="tab-pane fade in active">
										<div class="tab_content_account_deta">
											<div class="common_section_heading_new">
												<span class="plan_bootsing_tutor past_mysession no_border">
													<h4>MY SESSIONS</h4>
													<span class="float_right">
														<h4>Boosting Plans</h4>
														<a href='{{ url("TutorBoostingPlans")}}' class="btn comm_btn upcoming_tutor_plan_btn" type="button">Plans</a>	
													</span>	
												</span>
											</div>
											<div class="may_account_session_info">
												@foreach ($FutureSessions as $FutureSessions)
													<div class="my_accoundetails">
														<div class="my_sesion_id_head">
															<div class="my_session_id_details">
																<a href="#."id="open_account_session_details" class="open_account_session_details" data-id ="{{ $FutureSessions->id }}" data-tutor_name ="{{ $FutureSessions->tutor_name }}" data-grade_name ="{{ $FutureSessions->grade_name }}" data-subject_name ="{{ $FutureSessions->subject_name }}" data-topic_name ="{{ $FutureSessions->topic_name }}" data-tutor_level ="{{ $FutureSessions->tutor_level }}" data-session_type ="{{ $FutureSessions->session_type }}" data-start_date ="{{ $FutureSessions->start_date }}" data-start_time ="{{ $FutureSessions->start_time }}" data-venue ="{{ $FutureSessions->venue }}">
																	<span><img src="{{ url('public/assets/TutorAssets/img/session-icon.png')}}" class="img-responsive" alt="session"/></span>
																	<h4>SESSION ID: {{ $FutureSessions->id }}</h4>
																</a>
															</div>
															<div class="my_session_date">
																{{ date('d M Y', strtotime($FutureSessions->start_date)) }}, {{ date('h:i A ', strtotime($FutureSessions->start_time)) }}
															</div>
														</div>
														<div class="my_session_info">
															<div class="my_session_info_icon">
																<span><img src="@if($FutureSessions->session_type==1) {{ url('public/assets/img/session_symonds_icon.png')}} @else {{ url('public/assets/img/session_video_icon.png')}} @endif" class="img-responsive" alt="session"/></span>
															</div>
															<div class="my_session_details_here">
																<div class="my_session_deta_set my_session_new_data">
																	<ul>
																		<li><label>Student Name :</label><span>{{ $FutureSessions->student_name }} </span></li>
																		<!-- <li><label>Tutor Name : </label><span>@if($FutureSessions->tutor_id=='') Tutor not assigned yet @else {{ $FutureSessions->tutor_name }} @endif</span>	</li> -->
																		<li><label>Tutor Subject :</label><span>{{ $FutureSessions->subject_name }} </span></li>
																		<li><label>Standard : </label><span>{{ $FutureSessions->grade_name }} </span></li>
																		<li><label>Topic Name : </label><span>{{ $FutureSessions->topic_name }} </span></li>
																		<li><label>Session Name :  </label><small>@if($FutureSessions->session_type==1) F2F Session @else V2V Session @endif </small></li>
																	</ul>
																</div>
																<div class="my_session_btn">
																	@if($FutureSessions->id==$SessionsStartFirst->session_start_first_id && $FutureSessions->status!=5)
																		@if($FutureSessions->session_type==1)
																			<a href="#" class="btn session_comm_btn start_ahref"  data-id ="{{ $FutureSessions->id }}" class="btn session_comm_btn">Start</a>
																		@else
																			<a href="{{ url('TutorVideoStreaming')}}" class="btn session_comm_btn">Start</a>
																		@endif
																	@elseif($FutureSessions->id==$SessionsStartFirst->session_start_first_id && $FutureSessions->status==5)
																		<a href="@if($FutureSessions->session_type==1) {{ url('TutorStopSessionSave/'.$FutureSessions->id)}}  @else '{{ url('TutorVideoStreaming')}} @endif" class="btn session_comm_btn">Stop</a>
																	@endif
																</div>
															</div>
															<div class="my_session_status">
																@if($FutureSessions->status==5) <h2>LIVE</h2> @else  @endif
																<span>Booked Hours: 
																	@php
																		$d1 = new DateTime($FutureSessions->start_time);
																		$d2 = new DateTime($FutureSessions->end_time);

																		$diff = $d2->diff($d1);

																		echo $diff->h;
																		echo ".".$diff->i;
																	@endphp
																</span>
																
															</div>
														</div>
													</div>
												@endforeach
												
											</div>
										</div>
									</div>
									<div id="menu1" class="tab-pane fade">
											<div class="plan_bootsing_tutor past_mysession">
												<h4>MY SESSIONS</h4>
											</div>
										<div class="tab_content_account_deta">
											<div class="may_account_session_info">
												@foreach ($PastSessions as $PastSessions)
													<div class="my_accoundetails">
														<div class="my_sesion_id_head">
															<div class="my_session_id_details">
																<a href="#." data-toggle="modal" data-target="#account_past_session_details"> 
																	<h4>SESSION ID: {{ $PastSessions->id }}</h4>
																</a>
															</div>
															<div class="my_session_date">
																{{ date('d M Y', strtotime($PastSessions->start_date)) }}, {{ date('h:i A ', strtotime($PastSessions->start_time)) }}
															</div>
														</div>
														<div class="my_session_info">
															<div class="my_session_info_icon">
																<span><img src="@if($PastSessions->session_type==1) {{ url('public/assets/img/session_symonds_icon.png')}} @else {{ url('public/assets/img/session_video_icon.png')}} @endif" class="img-responsive" alt="session"/></span>
															</div>
															<div class="my_session_details_here">
																<div class="my_session_deta_set my_session_new_data">
																	<ul>
																		<li><label>Student Name :</label><span>{{ $PastSessions->student_name }}</span></li>
																		<li><label>Subject Name :</label><span>{{ $PastSessions->subject_name }}</span></li>
																		<li><label>Standard :</label><span>{{ $PastSessions->grade_name }}</span></li>
																		<li><label>opic Name :</label><span>{{ $PastSessions->topic_name }}</span></li>
																		<li><label>Session Name :</label><small>@if($PastSessions->session_type==1) F2F Session @else V2V Session @endif</small></li>
																		<li><div class="star_ratting_table_views star-rat-new-set-plan">
																				<input type="hidden" name="rating" id="rating" value="{{  $PastSessions->student_rating }}" />
																					@if($PastSessions->student_rating=='0.0')
																						<div id="tutorial-{{  $PastSessions->id }}">
																							<ul onMouseOut="resetRating({{  $PastSessions->id }});">
																								
																							@php $rating = $PastSessions->student_rating;  @endphp
																							@for($i=1;$i<=5;$i++)
																								@php 
																									$selectedClass = "fa fa-star-o";
																									$removeClass = "yes";

																									if(!empty($PastSessions->student_rating) && $i<=$PastSessions->student_rating) {
																										$selectedClass = "fa fa-star";
																										$removeClass = "no";
																									}
																								@endphp
																								<li id="{{ $PastSessions->id }}{{ $i }}{{ $removeClass }}" onmouseover="highlightStar(event,this,{{  $PastSessions->id }},{{ $i }},{{ $PastSessions->id }}{{ $i }},'{{ $removeClass }}');" onmouseout="highlightStar(event,this,{{  $PastSessions->id }},{{ $i }},{{ $PastSessions->id }}{{ $i }},'{{ $removeClass }}');" onClick="addRating(this,{{  $PastSessions->id }},{{ $i }},{{ $PastSessions->id }}{{ $i }});"><a href="#."><i id="{{ $PastSessions->id }}{{ $i }}" class="{{ $selectedClass }}" aria-hidden="true"></i></a></li>  
										
																							@endfor
																							</ul>
																						</div>
																						<div id="ratingComplete-{{  $PastSessions->id }}">
																							
																						</div>
																					@else
																						<div id="ratingComplete-{{  $PastSessions->id }}">
																							<ul onMouseOut="resetRating({{  $PastSessions->id }});">
																								
																							@php $rating = $PastSessions->student_rating;  @endphp
																							@for($i=1;$i<=5;$i++)
																								@php 
																									$selectedClass = "fa fa-star-o";
																									$removeClass = "yes";

																									if(!empty($PastSessions->student_rating) && $i<=$PastSessions->student_rating) {
																										$selectedClass = "fa fa-star";
																										$removeClass = "no";
																									}
																								@endphp
																								<li id="{{ $PastSessions->id }}{{ $i }}{{ $removeClass }}"><a ><i id="{{ $PastSessions->id }}{{ $i }}ratingComplete" class="{{ $selectedClass }}" aria-hidden="true"></i></a></li>  
										
																							@endfor
																							</ul>
																						</div>
																					@endif
																			</div></li>
																	</ul>
																</div>
															</div>
															<div class="my_session_status">
																<h2>$ 20</h2>
																<span>Session time: @php
																				$d1 = new DateTime($PastSessions->start_time);
																				$d2 = new DateTime($PastSessions->end_time);

																				$diff = $d2->diff($d1);

																				echo $diff->h;
																				echo ".".$diff->i;
																			@endphp hours
																			
																			
																</span>
															</div>
														</div>
													</div>
												@endforeach
											</div>
									   </div>
								  </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
			
			<!-- payment getway popup Start Here -->
				<div class="modal fade" id="transactions_wallet_popup" role="dialog">
					<div class="modal-dialog session_details_popup_width">
					  <!-- Modal content-->
					  <div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">
							 <img src="{{ url('public/assets/TutorAssets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
						  </button>
						</div>
						<div class="modal-body">
							<div class="model_details">
								<div class="section_heading">
									<h4>Transactions</h4>
								</div>
								<div class="model_content">
									<div class="trasaction_bg_img">
										<div class="trasaction_img_logo_width"> 
											<div class="trasaction_img_logo">
												<img class="img-responsive" src="{{ url('public/assets/TutorAssets/img/account_cur.png')}}" alt="curancy">
											</div>
											<div class="available_balance_show tra_available_balance_text_center"> 
												Available balance<br>
												1778
											</div>
											<form>
												<div class="form-group">
													<input type="text" class="form-control trasaction_text_fild" id="" placeholder="Enter Amount">
												</div>
											</form>
										</div>
									</div>
									<div class="my_sessiondetails">
										<div class="my_session_heading">
											<div class="bank_details_width">BANK DETAILS</div> &nbsp;
										</div>
										<div class="session_details_list_new session_details_list_new_margin">
											<ul>
												<li><span>Bank:</span>Yes Bank</li>
												<li><span>Branch:</span>Deccan Gymkhana, Pune</li>
												<li><span>IFSC Code:</span>153400938432</li>
												<li><span>Swift Code:</span>560457</li>
											</ul>
										</div>
										<button class="btn transactions_process_btn">Proceed to Withdraw</button>
									</div>
								</div>
							</div>
						</div>
					  </div>
					</div>
				  </div>
				  <!-- payment getway popup end Here -->
				  
					  <!-- view session details Start Here -->
			  <div class="modal fade" id="session_id_pop" role="dialog">
				<div class="modal-dialog session_details_popup_width">
				  <!-- Modal content-->
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">
						 <img src="{{ url('public/assets/TutorAssets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
					  </button>
					</div>
					<div class="modal-body">
						<div class="model_details">
							<div class="section_heading">
								<h4 id="open_id"></h4>
							</div>
							<div class="model_content">
								<div class="my_session_info_sec">
									<div class="my_session_info_icon_popups">
										<div><span><img src="{{ url('public/assets/TutorAssets/img/session_symonds_icon.png')}}" class="img-responsive" alt="session"></span></div>
									</div>
									<div class="my_session_details_here_popups">
										<div class="my_session_deta_set">
												<!-- <h4 id="open_tutor_name"></h4>
												<span>Grade : </span>
												<span id="open_standerd">7</span>
												<span id="open_topic"></span> -->
											
											<ul>
												<li><label>Tutor Name :</label><span id="open_tutor_name">dfsdfsef</span></li>
												<li><label>Grade :</label><span>b</span></li>
												<li><label>Standard:</label><span id="open_standerd">12 th</span></li>
												<li><label>Topic Name:</label><span id="open_topic">PPPPQQQ</span></li>
											</ul>
										</div>
										<div class="my_session_btn">
											<a href="#." class="btn session_comm_btn">Start</a>
										</div>
										<div class="calls_icon_popups">
											<!-- <a href="#.">
												<span>
													<img src="{{ url('public/assets/TutorAssets/img/popup-call-icon-active.png')}}" class="img-responsive" alt="call"/>
												</span>
											</a> -->
										</div>
									</div>
								</div>
								<div class="my_sessiondetails">
									<div class="my_session_heading">
									<h4>SESSION DETAILS</h4>
								</div>
								<div class="my_session_deta_set new-width-set-pop-deta">
									<ul>
										<li><label>Session Type:</label><span id="open_session_type"></span></li>
										<li><label>Date:</label><span id="open_date"></span></li>
										<li><label>Time:</label><span id="open_time"></span></li>
										<li><label>Location:</label><span id="open_location"></span></li>
										<li><label>Topic:</label><span id="open_topic2"></span></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			  </div>
			</div>
		  </div>
		<!-- view session details popup end Here -->
		<!-- otp popup Start Here -->
		<div class="modal fade" id="start_session_pop" role="dialog">
					<div class="modal-dialog session_details_popup_width">
					  <!-- Modal content-->
					  <div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">
							 <img src="{{ url('public/assets/TutorAssets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
						  </button>
						</div>
						<div class="modal-body">
							<div class="model_details">
								<div class="section_heading">
									<h4>Session Start</h4>
								</div>
								<div class="model_content">
									
									<div class="my_sessiondetails">
										<form>
										<span id="errNm2" style="color: red;">OTP not match, please try again.</span>
														
											<div class="session_details_list_new session_details_list_new_margin">
												
												<div class="form-group">
													<input type="hidden" id="session_id">
													<input type="text" class="form-control trasaction_text_fild" id="session_otp" placeholder="Enter OTP">
												</div>
												
											</div>
											<button id="startSessionWithOtp" class="btn transactions_process_btn">Start Session</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					  </div>
					</div>
				  </div>
				  <!-- otp popup end Here -->
					  
				<!--popup end here-->
			
			
			<script>
				
				$(document).ready(function () {
					$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
					$("#errNm2").hide();
					$('#horizontalTab').easyResponsiveTabs({
							type: 'default', //Types: default, vertical, accordion           
							width: 'auto', //auto or any width like 600px
							fit: true,   // 100% fit in a container
						closed: 'accordion', // Start closed if in accordion view
					});
					$('#verticalTab').easyResponsiveTabs({
						type: 'vertical',
						width: 'auto',
						fit: true
					});
					$(".open_account_session_details").click(function(e){

						var id = $(this).data('id');
						var tutor_name = $(this).data('tutor_name');
						var tutor_gender = $(this).data('tutor_gender');
						var grade_name = $(this).data('grade_name');
						var subject_name = $(this).data('subject_name');
						var topic_name = $(this).data('topic_name');
						var tutor_level = $(this).data('tutor_level');
						var session_type = $(this).data('session_type');
						var start_date = $(this).data('start_date');
						var start_time = $(this).data('start_time');
						var venue = $(this).data('venue');

						if(session_type==1)
						{
							var session_type_text ="Face 2 Face Session";
						}
						else if(session_type==2)
						{
							var session_type_text ="VIDEO 2 VIDEO Session";
						}
						else
						{
							var session_type_text ="";
						}
						
						
						$('#open_id').html("SESSION ID : "+id);
						$('#open_tutor_name').html(tutor_name);
						$('#open_tutor_gender').html(tutor_gender);
						$('#open_tutor_level').html("Level : "+tutor_level);
						$('#open_standerd').html(grade_name);
						$('#open_subject').html(subject_name);
						$('#open_topic').html(topic_name);
						$('#open_session_type').html(session_type_text);
						$('#open_date').html(start_date);
						$('#open_time').html(start_time);
						$('#open_location').html(venue);

						$('#open_subject2').html(subject_name);
						$('#open_topic2').html(topic_name);

						$('#session_id_pop').modal('show');
					});

					$(".start_ahref").click(function(e){
						$("#errNm2").hide();
						var id = $(this).data('id');

						$('#session_id').val(id);

						$('#session_otp').val('');

						$('#start_session_pop').modal('show');
					});
					$("#startSessionWithOtp").click(function(e){

						var session_id = $('#session_id').val();

						var session_otp = $('#session_otp').val();
						
						$.ajax({
							type:'POST',
							url:'{{ url("startSessionUsingOtp")}}',
							data:{session_id:session_id, session_otp:session_otp},
							success:function(data){
								
								console.log(data);
								if(data.success=='success')
								{
									location.reload();
								}
								else
								{
									$("#errNm2").show();
								}
							}
						});
						
					});
				});
			</script>
			<script>
			
				
			function highlightStar(event,obj,id,ratingVal,iVal,removeClass) {
				
				
					if(event.type=='mouseover')
					{
						var i;
						for (i = 5; i >= 1; i--) { 
							if(i>ratingVal)
							{
								document.getElementById(id+""+i).className ="fa fa-star-o";
							}
							else
							{
								document.getElementById(id+""+i).className ="fa fa-star";
							}
						}
					}
					if(event.type=='mouseout')
					{
						// var i;
						// for (i = 5; i >= 1; i--) { 							
						// 	if(removeClass=='yes')
						// 	{
						// 		var liId = document.getElementById(id+""+i+""+'yes');
						// 		var liId2 = document.getElementById(id+""+i+""+'no');	
						// 		if(liId){
						// 			document.getElementById(id+""+i).className ="fa fa-star-o";
						// 		}
						// 		if(liId2){
						// 			document.getElementById(id+""+i).className ="fa fa-star";
						// 		}
						// 	}
						//}
					}						
				}

				

				function addRating(obj,id,ratingVal,iVal) {
					
					var i;
					for (i = 5; i >= 1; i--) { 
						if(i>ratingVal)
						{
							document.getElementById(id+""+i).className ="fa fa-star-o";
						}
						else
						{
							document.getElementById(id+""+i).className ="fa fa-star";
						}
					}
					
    
					$.ajax({
							type:'POST',
							url:'{{ url("TutorAddRating")}}',
							data:{id:id, rating:ratingVal},
							success:function(data){
								
								console.log(data);
								var x = document.getElementById("tutorial-"+id);
       							x.style.display = "none";
								
								var main_li="<ul>";
								  var i;
								for (i = 1; i <= 5; i++) { 
									if(i>ratingVal)
									{
										main_li += "<li ><a ><i  class='fa fa-star-o' aria-hidden='true'></i></a></li>";
									}
									else
									{
										main_li += "<li ><a ><i  class='fa fa-star' aria-hidden='true'></i></a></li>";
									
									}
								}
								var div = document.getElementById("ratingComplete-"+id);
								div.innerHTML += main_li+"</ul>";
							}
						});
				}

				function resetRating(id) {
					if($('#tutorial-'+id+' #rating').val() != 0) {
						$('.demo-table #tutorial-'+id+' li').each(function(index) {
							$(this).addClass('selected');
							if((index+1) == $('#tutorial-'+id+' #rating').val()) {
								return false;	
							}
						});
					}
				} </script>
	@include('Template.footer_tutor') 
