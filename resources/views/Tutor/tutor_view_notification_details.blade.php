@include('Template.header_tutor')

				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url({{ url('public/assets/img/mid-bg.jpg')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>NOTIFICATION DETAILS</h4>
									<a href='{{ url("TutorNotification")}}' class="back_page"><img src="{{ url('public/assets/img/back_arrow.png')}}" class="img-responsive" alt="back"></a>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!-- view notifiaction details section start here-->
				<section class="web_section_common">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="section_details">
									<div class="notifiaction_info">
										<div class="notification_blog">
											<div class="stron_main_notifa">
												<p>

													@if($SingleNotificationData->notification_type=='AdminNotification') 
															{{ config('constants.AdminNotification') }} 
														
														@elseif($SingleNotificationData->notification_type=='SessionBook') 
															{{ config('constants.SessionBook') }}

														@elseif($SingleNotificationData->notification_type=='SessionAssign') 
															{{ config('constants.SessionAssign') }}
														
														@elseif($SingleNotificationData->notification_type=='Session_1_Hour_Left') 
															{{ config('constants.Session_1_Hour_Left') }}
														
														@elseif($SingleNotificationData->notification_type=='Session_5_Min_left') 
															{{ config('constants.Session_5_Min_left') }}
														
														@elseif($SingleNotificationData->notification_type=='SessionStart') 
															{{ config('constants.SessionStart') }}
														
														@elseif($SingleNotificationData->notification_type=='SessionStop') 
															{{ config('constants.SessionStop') }}
														
														@elseif($SingleNotificationData->notification_type=='SessionComplete') 
															{{ config('constants.SessionComplete') }}
														
														@elseif($SingleNotificationData->notification_type=='SessionTutorCancel') 
															{{ config('constants.SessionTutorCancel') }}
														
														@elseif($SingleNotificationData->notification_type=='SessionStudentCancel') 
															{{ config('constants.SessionStudentCancel') }}
														
														@elseif($SingleNotificationData->notification_type=='RatingByTutor') 
															{{ config('constants.RatingByTutor') }}
														
														@elseif($SingleNotificationData->notification_type=='RatingByStudent') 
															{{ config('constants.RatingByStudent') }}
														
														
														@endif
												</p>
											</div>
											<div class="view_notifaction_full_info">
												<span class="date_of_notification_sec">{{ date('d-m-Y h:i A', strtotime($SingleNotificationData->created_at)) }}</span>
												
												<p>{{ $SingleNotificationData->message }}</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- view notifiaction details section end here-->
				

@include('Template.footer_tutor') 				