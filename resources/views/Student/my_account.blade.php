@include('Template.header_student')

				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url({{ url('public/assets/img/mid-bg.jpg')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>MY ACCOUNT</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!-- my account section start here-->
				<section class="my_accoun_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="account_section_deta">
									<div class="acc_secti_tabing">
										<ul class="nav nav-tabs">
										  <li class="active"><a data-toggle="tab" href="#home">UPCOMING</a></li>
										  <li><a data-toggle="tab" href="#menu1">PAST</a></li>
										</ul>
										<div class="accoun_ad_session_btn">
											<a href='{{ url("BookSession")}}' class="btn common_btn">Add Session</a>
										</div>
									</div>
									<div class="tab_account_content">
										<div class="tab-content">
										  <div id="home" class="tab-pane fade in active">
											   <div class="tab_content_account_deta">
													<div class="common_section_heading_new">
														<h4>MY SESSIONS</h4>
														<div class="my_session_account_head_btns">
															<span class="my_session_plan_label">My Plan</span>
															<a href='{{ url("BoostingPlans")}}' class="btn comm_btn_sec" >Gold</a>
															<a href="#." class="btn comm_btn" data-toggle="modal" data-target="#balance_details_popups">Balance Details</a>
														</div>
													</div>
													<div class="may_account_session_info">
														@foreach ($FutureSessions as $FutureSessions)
															<div class="my_accoundetails">
																<div class="my_sesion_id_head">
																	<div class="my_session_id_details">
																		<a href="#." id="open_account_session_details" class="open_account_session_details" data-id ="{{ $FutureSessions->id }}" data-tutor_name ="{{ $FutureSessions->tutor_name }}" data-grade_name ="{{ $FutureSessions->grade_name }}" data-subject_name ="{{ $FutureSessions->subject_name }}" data-topic_name ="{{ $FutureSessions->topic_name }}" data-tutor_level ="{{ $FutureSessions->tutor_level }}" data-session_type ="{{ $FutureSessions->session_type }}" data-start_date ="{{ $FutureSessions->start_date }}" data-start_time ="{{ $FutureSessions->start_time }}" data-venue ="{{ $FutureSessions->venue }}" >
																		  
																			<span><img src="{{ url('public/assets/img/session-icon.png')}}" class="img-responsive" alt="session"/></span>
																			<h4>SESSION ID: {{ $FutureSessions->id }}</h4>
																		</a>
																	</div>
																	<div class="my_session_date">
																	{{ date('d M Y', strtotime($FutureSessions->start_date)) }}, {{ date('h:i A ', strtotime($FutureSessions->start_time)) }}
																	</div>
																</div>
																<div class="my_session_info">
																	<div class="my_session_info_icon">
																		<span><img src="@if($FutureSessions->session_type==1) {{ url('public/assets/img/session_symonds_icon.png')}} @else {{ url('public/assets/img/session_video_icon.png')}} @endif" class="img-responsive" alt="session"/></span>
																	</div>
																	<div class="my_session_details_here">
																		<div class="my_session_deta_set my_session_new_data">
																			<ul>
																				<li>
																					<label>Tutor Name :</label>
																					<span><a href="#.">@if($FutureSessions->tutor_id=='') Tutor not assigned yet @else {{ $FutureSessions->tutor_name }} @endif </a></span>
																				</li>
																				<li>
																					<label>Tutor Subject :</label>
																					<span>{{ $FutureSessions->subject_name }}</span>
																				</li>
																				<li>
																					<label>Standard :</label>
																					<span>{{ $FutureSessions->grade_name }}</span>
																				</li>
																				<li>
																					<label>Topic Name :</label>
																					<span>{{ $FutureSessions->topic_name }}</span>
																				</li>
																				
																				<li>
																					<label>Session Name :</label>
																					<small>@if($FutureSessions->session_type==1) F2F Session @else V2V Session @endif</small>
																				</li>
																				<li>
																					<label>OTP :</label>
																					<span>{{ $FutureSessions->otp }}</span>
																				</li>
																			</ul>
																		</div>
																		<div class="my_session_btn">
																			@if($FutureSessions->id==$SessionsStartFirst->session_start_first_id && $FutureSessions->status!=5)
																				<!-- <a href="@if($FutureSessions->session_type==1) {{ url('StartSessionSave/'.$FutureSessions->id )}} @else {{ url('VideoStreaming')}}  @endif" class="btn session_comm_btn">Start</a>
																			 -->
																			 	<a href="@if($FutureSessions->session_type==1) # @else {{ url('VideoStreaming')}}  @endif" class="btn session_comm_btn">Start</a>
																			
																			@elseif($FutureSessions->id==$SessionsStartFirst->session_start_first_id && $FutureSessions->status==5)
																				<!-- <a href="@if($FutureSessions->session_type==1) {{ url('StopSessionSave/'.$FutureSessions->id )}} @else {{ url('VideoStreaming')}} @endif" class="btn session_comm_btn">Stop</a>
																			 -->
																			 <a href="@if($FutureSessions->session_type==1) # @else {{ url('VideoStreaming')}} @endif" class="btn session_comm_btn">Stop</a>
																			
																			@endif
																		</div>
																	</div>
																	<div class="my_session_status">
																		@if($FutureSessions->status==5) <h2>LIVE</h2> @else  @endif
																		<span>Booked Hours: 
																		
																			@php
																				$d1 = new DateTime($FutureSessions->start_time);
																				$d2 = new DateTime($FutureSessions->end_time);

																				$diff = $d2->diff($d1);

																				echo $diff->h;
																				echo ".".$diff->i;
																			@endphp
																		</span>
																	</div>
																</div>
															</div>
														@endforeach
														
														
													</div>
											   </div>
										  </div>
										  <div id="menu1" class="tab-pane fade">
											   <div class="tab_content_account_deta">
													<div class="common_section_heading_new">
														<h4>MY SESSIONS</h4>
													</div>
													<div class="may_account_session_info">
														@foreach ($PastSessions as $PastSessions)
															<div class="my_accoundetails">
																<div class="my_sesion_id_head">
																	<div class="my_session_id_details">
																		<a href="#." data-toggle="modal" data-target="#account_past_session_details"> 
																			<h4>SESSION ID: {{ $PastSessions->id }}</h4>
																		</a>
																	</div>
																	<div class="my_session_date">
																	{{ date('d M Y', strtotime($PastSessions->start_date)) }}, {{ date('h:i A ', strtotime($PastSessions->start_time)) }}
																	</div>
																</div>
																<div class="my_session_info">
																	<div class="my_session_info_icon">
																		<span><img src="@if($PastSessions->session_type==1) {{ url('public/assets/img/session_symonds_icon.png')}} @else {{ url('public/assets/img/session_video_icon.png')}} @endif" class="img-responsive" alt="session"/></span>
																	</div>
																	<div class="my_session_details_here">
																		<div class="my_session_deta_set my_session_new_data">
																			<ul>
																				<li>
																					<label>Tutor Name :</label>
																					<span><a>{{ $PastSessions->tutor_name }}</a></span>
																				</li>
																				<li>
																					<label>Subject Name :</label>
																					<span>{{ $PastSessions->subject_name }}</span>
																				</li>
																				<li>
																					<label>Standard :</label>
																					<span>{{ $PastSessions->grade_name }} </span>
																				</li>
																				<li>
																					<label>Topic Name :</label>
																					<span>{{ $PastSessions->topic_name }}</span>
																				</li>
																				<li>
																					<label>Session Name :</label>
																					<small>@if($PastSessions->session_type==1) F2F Session @else V2V Session @endif</small>
																				</li>
																			</ul>
																		</div>
																		<div class="my_session_btn">
																			<a href="#." class="btn session_comm_btn rebook" id="rebook"  data-id ="{{ $PastSessions->id }}" data-tutor_id ="{{ $PastSessions->tutor_id }}"  data-tutor_name ="{{ $PastSessions->tutor_name }}"  data-tutor_gender ="{{ $PastSessions->tutor_gender }}" data-student_id ="{{ $PastSessions->student_id }}" data-session_type ="{{ $PastSessions->session_type }}" data-venue ="{{ $PastSessions->venue }}" data-visit_type ="{{ $PastSessions->visit_type }}" data-tutor_type ="{{ $PastSessions->tutor_type }}" data-grade_name ="{{ $PastSessions->grade_name }}" data-subject_name ="{{ $PastSessions->subject_name }}" data-topic_name ="{{ $PastSessions->topic_name }}" data-grade_id ="{{ $PastSessions->grade_id }}" data-subject_id ="{{ $PastSessions->subject_id }}" data-topic_id ="{{ $PastSessions->topic_id }}" data-latitude ="{{ $PastSessions->latitude }}" data-longitude ="{{ $PastSessions->longitude }}">Rebook Tutor</a>
																		</div>
																	</div>
																	<div class="my_session_status">
																		<h2> @php
																				$d1 = new DateTime($PastSessions->start_time);
																				$d2 = new DateTime($PastSessions->end_time);

																				$diff = $d2->diff($d1);

																				echo $diff->h;
																				echo ".".$diff->i;
																			@endphp</h2>
																		<span>Available Hours: 23</span>

																		<div class="star_ratting_table_views">
																				<input type="hidden" name="rating" id="rating" value="{{  $PastSessions->tutor_rating }}" />
																					@if($PastSessions->tutor_rating=='0.0')
																						<div id="tutorial-{{  $PastSessions->id }}">
																							<ul onMouseOut="resetRating({{  $PastSessions->id }});">
																								
																							@php $rating = $PastSessions->tutor_rating;  @endphp
																							@for($i=1;$i<=5;$i++)
																								@php 
																									$selectedClass = "fa fa-star-o";
																									$removeClass = "yes";

																									if(!empty($PastSessions->tutor_rating) && $i<=$PastSessions->tutor_rating) {
																										$selectedClass = "fa fa-star";
																										$removeClass = "no";
																									}
																								@endphp
																								<li id="{{ $PastSessions->id }}{{ $i }}{{ $removeClass }}" onmouseover="highlightStar(event,this,{{  $PastSessions->id }},{{ $i }},{{ $PastSessions->id }}{{ $i }},'{{ $removeClass }}');" onmouseout="highlightStar(event,this,{{  $PastSessions->id }},{{ $i }},{{ $PastSessions->id }}{{ $i }},'{{ $removeClass }}');" onClick="addRating(this,{{  $PastSessions->id }},{{ $i }},{{ $PastSessions->id }}{{ $i }});"><a href="#."><i id="{{ $PastSessions->id }}{{ $i }}" class="{{ $selectedClass }}" aria-hidden="true"></i></a></li>  
										
																							@endfor
																							</ul>
																						</div>
																						<div id="ratingComplete-{{  $PastSessions->id }}">
																							
																						</div>
																					@else
																						<div id="ratingComplete-{{  $PastSessions->id }}">
																							<ul onMouseOut="resetRating({{  $PastSessions->id }});">
																								
																							@php $rating = $PastSessions->tutor_rating;  @endphp
																							@for($i=1;$i<=5;$i++)
																								@php 
																									$selectedClass = "fa fa-star-o";
																									$removeClass = "yes";

																									if(!empty($PastSessions->tutor_rating) && $i<=$PastSessions->tutor_rating) {
																										$selectedClass = "fa fa-star";
																										$removeClass = "no";
																									}
																								@endphp
																								<li id="{{ $PastSessions->id }}{{ $i }}{{ $removeClass }}"><a ><i id="{{ $PastSessions->id }}{{ $i }}ratingComplete" class="{{ $selectedClass }}" aria-hidden="true"></i></a></li>  
										
																							@endfor
																							</ul>
																						</div>
																					@endif
																			</div>
																			
																	</div>
																</div>
															</div>
														@endforeach
														
														
													</div>
											   </div>
										  </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- my account section end here-->
				<!-- profile details section end here-->
				
				
				<!--popup start here-->
				
					
					  
					<!-- view session details Start Here -->
					  <div class="modal fade" id="account_session_details" role="dialog">
						<div class="modal-dialog session_details_popup_width">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="model_details">
									<div class="section_heading">
										<h4 id="open_id"></h4>
									</div>
									<div class="model_content">
										<div class="my_session_info_sec">
											<div class="my_session_info_icon_popups">
												<div><span><img src="{{ url('public/assets/img/session_symonds_icon.png')}}" class="img-responsive" alt="session"></span></div>
											</div>
											<div class="my_session_details_here_popups">
												<div class="my_session_deta_set">
													<ul>
														<li>
															<label>Tutor Name:</label>
															<span id="open_tutor_name"></span>
														</li>
														<li>
															<label>Gender:</label>
															<span id="open_tutor_gender">Male</span>
														</li>
														<li>
															<label>Tutor Level:</label>
															<span id="open_tutor_level"></span>
														</li>
														<li>
															<label>Standard:</label>
															<span id="open_standerd"></span>
														</li>
														<li>
															<label>Subject:</label>
															<span id="open_subject"></span>
														</li>
														<li>
															<label>Topic Name:</label>
															<span id="open_topic"></span>
														</li>
													</ul>
												</div>
												<div class="my_session_btn_new">
													<a href="#." id="open_start" class="btn session_comm_btn">Start</a>
												</div>
												<div class="calls_icon_popups">
													<!-- <a href="#.">
														<span>
															<img src="{{ url('public/assets/img/popup-call-icon-active.png')}}" class="img-responsive" alt="call"/>
														</span>
													</a> -->
												</div>
											</div>
										</div>
										<div class="my_sessiondetails">
											<div class="my_session_heading">
												<h4>SESSION DETAILS</h4>
											</div>
											<div class="session_details_list_new">
												<ul>
													<li><label >Session Type:</label><span id="open_session_type">Face 2 Face Session</span></li>
													<li><label id="">Date:</label><span id="open_date"></span></li>
													<li><label>Time:</label><span  id="open_time"></span></li>
													<li><label>Location:</label><span  id="open_location"></span></li>
													<li><label>Suject:</label><span  id="open_subject2"></span></li>
													<li><label>Topic:</label><span  id="open_topic2"></span></li>
												<ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- view session details popup end Here -->
					  
					  
					  
					  <!-- view session details Start Here -->
					  <div class="modal fade" id="balance_details_popups" role="dialog">
						<div class="modal-dialog session_details_popup_width_new">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="model_details">
									<div class="section_heading">
										<h4>BALANCE DETAILS</h4>
									</div>
									<div class="model_content">
										<div class="my_session_info_sec">
											<div class="my_session_info_icon_popups_sec ">
												<div><span><img src="{{ url('public/assets/img/balance_detail_icon_pop.png')}}" class="img-responsive" alt="session"></span></div>
											</div>
											<div class="my_session_details_here_popups my_session_details_here_popups_new">
												<div class="my_session_deta_set set_past_sesio_user">
													<h2>Plan : GOLD</h2>
												</div>
												<div class="past_user_session_hrs_new ">
													<h4>Upto 09-09-2019</h4>
												</div>
											</div>
										</div>
										<div class="my_sessiondetails">
											<div class="my_session_heading">
												<h4>SUBSCRIPTION BALANCE :</h4>
											</div>
											<div class="session_details_list_new_set">
												<ul>
													<li><span>F2F Sessions:</span><div class="session_det_no">56</div></li>
													<li><span>V2V Sessions:</span><div class="session_det_no">66</div></li>
												</ul>
											</div>
										</div>
										<div class="my_sessiondetails">
											<div class="my_session_heading">
												<h4>OTHER BALANCE (FREE) :</h4>
											</div>
											<div class="session_details_list_new_set session_details_listup">
												<ul>
													<li><span>F2F Sessions (15 mins) :</span><div class="session_det_no">1</div></li>
													<li><span>V2V Sessions (30 mins) :</span><div class="session_det_no">1</div></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- view session details popup end Here -->
					  
					  <!-- booking details Start Here -->
					  <div class="modal fade" id="booking_details_popups" role="dialog">
						<div class="modal-dialog booking_details_popup_width">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="model_details">
									<div class="section_heading">
										<h4>BOOKING DETAILS</h4>
									</div>
									<div class="model_content">
										<div class="my_session_info_sec">
											<form class="form1" method = "post" >
												<div class="booking_info_form">
													<div class="row">
														<div class="col-md-4">
															<div class="form_set">
																<label>TUTOR</label>
																<input type="text" class="form-control" id="tutor"/>
																<input type="hidden" id="tutor_id_fk" name="tutor_id_fk"/>
																<input type="hidden" id="student_id_fk" name="student_id_fk"/>
																<input type="hidden" id="tutor_type" name="tutor_type"/>
																<input type="hidden" id="visit_type" name="visit_type"/>
																<input type="hidden" id="venue" name="venue"/>
																<input type="hidden" id="session_type" name="session_type"/>

																<input type="hidden" id="grade_id" name="grade_id"/>

																<input type="hidden" id="subject_id" name="subject_id"/>

																<input type="hidden" id="topic_id" name="topic_id"/>
																
																<input type="hidden" id="latitude" name="latitude"/>
																<input type="hidden" id="longitude" name="longitude"/>
															</div>
														</div>
														<div class="col-md-4">
															<div class="form_set">
																<label>DATE</label>
																<div class="input_boxes_set">
																	<div class="form-group calend_div_new">
																		<div class="input-group date newDate">
																			<input class="form-control calender_set required_date_check" type="text" name="start_date" id="start_date">
																			<div class="input-group-addon custom_input_calend"><i class="fa fa-calendar" aria-hidden="true"></i></div>
																		</div>
																		<input type="hidden" id="dtp_input2" value="">
																	</div>
																</div>
															</div>
														</div>
														
														<div class="col-md-4">
															<div class="form_set_sec">
																<span>START TIME</span>
																<div class="input_boxes_set">
																	<div class="form-group calend_div_new">
																		<div class="input-group date newTime" data-date="" data-date-format="hh:ii" data-link-field="start_time" data-link-format="hh:ii">
																			<input class="form-control calender_set required_time_check" size="16" type="text"  placeholder="00:00" name="start_time" id="start_time">
																			<div class="input-group-addon custom_input_calend"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														
													</div>
													<div class="row">
														<div class="col-md-4">
															<div class="form_set">
																<label>GRADE</label>
																<div class="input_boxes_set">
																<input type="text" class="form-control" id="grade_id_name"/>
																</div>
															</div>
														</div>
														<div class="col-md-4">
															<div class="form_set">
																<label>SUBJECT</label>
																<div class="input_boxes_set">
																	
																<input type="text" class="form-control" id="subject_id_name"/>
																</div>
															</div>
														</div>
														<div class="col-md-4">
															<div class="form_set">
																<label>TOPIC</label>
																<div class="input_boxes_set">
																<input type="text" class="form-control" id="topic_id_name"/>
																</div>
															</div>
														</div>
													</div>
												</div>
											</form>
										</div>
										<div class="main_profil_btn">
											<a href="#." class="btn save_cancel_btn" ID="payment_getway_confirm">BOOK</a>
											<a href="#." class="btn save_cancel_btn" data-dismiss="modal">RESET</a>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- booking details popup end Here -->
					  
					  <!-- payment getway popup Start Here -->
					  <div class="modal fade" id="payment_getway_popup" role="dialog">
						<div class="modal-dialog model_forgot_pass_popup">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="moel_details">
									<div class="section_heading">
										<h4>PAYMENT GETWAY</h4>
									</div>
									<div class="model_content">
										<span class="model_header">
											<img src="{{ url('public/assets/img/payment_getway.png')}}" class="img-responsive" alt="forgotpassword">
										</span>
										<div class="payment_gat_info">
											<h1>$9</h1>
										</div>
										<div class="pop_btn_sect">
											<a href="#." data-dismiss="modal" id="confirm_payment" class="btn popup_coom_btn">OK</a>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- payment getway popup end Here -->
					  
				<!--popup end here-->
			<script>
				$(document).ready(function() {
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
				
				
					$(".rebook").click(function(e){

						var id = $(this).data('id');
						var tutor_name = $(this).data('tutor_name');
						var tutor_id = $(this).data('tutor_id');
						var student_id = $(this).data('student_id');

						var tutor_type = $(this).data('tutor_type');
						var visit_type = $(this).data('visit_type');
						var venue = $(this).data('venue');
						var session_type = $(this).data('session_type');

						var grade_name = $(this).data('grade_name');
						var subject_name = $(this).data('subject_name');
						var topic_name = $(this).data('topic_name');
						var grade_id = $(this).data('grade_id');
						var subject_id = $(this).data('subject_id');
						var topic_id = $(this).data('topic_id');
						var latitude = $(this).data('latitude');
						var longitude = $(this).data('longitude');

						$('#tutor').val(tutor_name);
						$('#tutor_id_fk').val(tutor_id);

						$('#latitude').val(latitude);
						$('#longitude').val(longitude);

						$('#student_id_fk').val(student_id);

						$('#tutor_type').val(tutor_type);
						$('#visit_type').val(visit_type);
						$('#venue').val(venue);
						$('#session_type').val(session_type);

						$('#grade_id').val(grade_id);
						$('#subject_id').val(subject_id);
						$('#topic_id').val(topic_id);

						$('#grade_id_name').val(grade_name);
						$('#subject_id_name').val(subject_name);
						$('#topic_id_name').val(topic_name);

						$('#tutor').attr("readonly","true");
						$('#grade_id_name').attr("readonly","true");
						$('#subject_id_name').attr("readonly","true");
						$('#topic_id_name').attr("readonly","true");
						
						$('#booking_details_popups').modal('show');
					});
					$("#payment_getway_confirm").click(function(e){
						var grade_id = $("input[name=grade_id]").val();
						var subject_id = $("input[name=subject_id]").val();
						var topic_id = $("input[name=topic_id]").val();
						var start_date = $("input[name=start_date]").val();
						var start_time = $("input[name=start_time]").val();
						
						if ($('.form1').valid()) 
						{
							$('#payment_getway_popup').modal('show');
						}
						
					});
					$("#confirm_payment").click(function(e){

						var grade_id = $("input[name=grade_id]").val();
						var subject_id = $("input[name=subject_id]").val();
						var topic_id = $("input[name=topic_id]").val();
						var start_date = $("input[name=start_date]").val();
						var start_time = $("input[name=start_time]").val();
						var tutor_type = $("input[name=tutor_type]").val();
						var visit_type = $("input[name=visit_type]").val(); 

						var latitude = $("input[name=latitude]").val(); 
						var longitude = $("input[name=longitude]").val(); 

						var venue = $("input[name=venue]").val();
						var session_type = $("input[name=session_type]").val();
						var tutor_id = $("input[name=tutor_id_fk]").val();
						if ($('.form1').valid()) 
						{
							$.ajax({
								type:'POST',
								url:'{{ url("BookSessionSave")}}',
								data:{grade_id:grade_id, subject_id:subject_id, topic_id:topic_id, start_date:start_date, start_time:start_time,  tutor_type:tutor_type, visit_type:visit_type, session_type:session_type, venue:venue, tutor_id:tutor_id, latitude:latitude, longitude:longitude},
								success:function(data){
									//alert(data.success);
									console.log(data.success);
									if(data.success=='success')
									{
										//$('#confirm_booking_popups').modal('show');
									}
									else
									{
										//$('#confirm_booking_popups').modal('show');
									}
									$('#submit_success_popup').modal('show');
								}
							});
						}
							
					});
					$(".open_account_session_details").click(function(e){

						var id = $(this).data('id');
						var tutor_name = $(this).data('tutor_name');
						var tutor_gender = $(this).data('tutor_gender');
						var grade_name = $(this).data('grade_name');
						var subject_name = $(this).data('subject_name');
						var topic_name = $(this).data('topic_name');
						var tutor_level = $(this).data('tutor_level');
						var session_type = $(this).data('session_type');
						var start_date = $(this).data('start_date');
						var start_time = $(this).data('start_time');
						var venue = $(this).data('venue');

						if(session_type==1)
						{
							var session_type_text ="Face 2 Face Session";
						}
						else if(session_type==2)
						{
							var session_type_text ="VIDEO 2 VIDEO Session";
						}
						else
						{
							var session_type_text ="";
						}
						
						
						$('#open_id').html("SESSION ID : "+id);
						$('#open_tutor_name').html(tutor_name);
						$('#open_tutor_gender').html(tutor_gender);
						$('#open_tutor_level').html("Level : "+tutor_level);
						$('#open_standerd').html(grade_name);
						$('#open_subject').html(subject_name);
						$('#open_topic').html(topic_name);
						$('#open_session_type').html(session_type_text);
						$('#open_date').html(start_date);
						$('#open_time').html(start_time);
						$('#open_location').html(venue);

						$('#open_subject2').html(subject_name);
						$('#open_topic2').html(topic_name);

						$('#account_session_details').modal('show');
					});
				});

			</script>
			<script>
			
				
			function highlightStar(event,obj,id,ratingVal,iVal,removeClass) {
				
				
					if(event.type=='mouseover')
					{
						var i;
						for (i = 5; i >= 1; i--) { 
							if(i>ratingVal)
							{
								document.getElementById(id+""+i).className ="fa fa-star-o";
							}
							else
							{
								document.getElementById(id+""+i).className ="fa fa-star";
							}
						}
					}
					if(event.type=='mouseout')
					{
						// var i;
						// for (i = 5; i >= 1; i--) { 							
						// 	if(removeClass=='yes')
						// 	{
						// 		var liId = document.getElementById(id+""+i+""+'yes');
						// 		var liId2 = document.getElementById(id+""+i+""+'no');	
						// 		if(liId){
						// 			document.getElementById(id+""+i).className ="fa fa-star-o";
						// 		}
						// 		if(liId2){
						// 			document.getElementById(id+""+i).className ="fa fa-star";
						// 		}
						// 	}
						//}
					}						
				}

				

				function addRating(obj,id,ratingVal,iVal) {
					
					var i;
					for (i = 5; i >= 1; i--) { 
						if(i>ratingVal)
						{
							document.getElementById(id+""+i).className ="fa fa-star-o";
						}
						else
						{
							document.getElementById(id+""+i).className ="fa fa-star";
						}
					}
					
    
					$.ajax({
							type:'POST',
							url:'{{ url("StudentAddRating")}}',
							data:{id:id, rating:ratingVal},
							success:function(data){
								
								console.log(data);
								var x = document.getElementById("tutorial-"+id);
       							x.style.display = "none";
								
								var main_li="<ul>";
								  var i;
								for (i = 1; i <= 5; i++) { 
									if(i>ratingVal)
									{
										main_li += "<li ><a ><i  class='fa fa-star-o' aria-hidden='true'></i></a></li>";
									}
									else
									{
										main_li += "<li ><a ><i  class='fa fa-star' aria-hidden='true'></i></a></li>";
									
									}
								}
								var div = document.getElementById("ratingComplete-"+id);
								div.innerHTML += main_li+"</ul>";
							}
						});
				}

				function resetRating(id) {
					if($('#tutorial-'+id+' #rating').val() != 0) {
						$('.demo-table #tutorial-'+id+' li').each(function(index) {
							$(this).addClass('selected');
							if((index+1) == $('#tutorial-'+id+' #rating').val()) {
								return false;	
							}
						});
					}
				} </script>

@include('Template.footer_student') 	