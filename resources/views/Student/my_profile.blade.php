@include('Template.header_student')
@php
$val=Session::get('loginSession')
@endphp
@php
	if($users['profile_file_path']!='')
	{
		$image_path = $users['profile_file_path'];
	}
	else{
		$image_path = "no-image.png";
	}
@endphp
				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url({{ url('public/assets/img/mid-bg.jpg')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>My Profile</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!--account user details info start here-->
				<section class="my_account_all_us_details">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="my_account_data">
									<div class="row">
										<div class="col-md-5 col-sm-6 col-xs-6 dash_dta_border_right">
											<div class="dash_bora_profil_user_sec">
											<input type="hidden" name="user_id" id="user_id" value ="{{ $users['user_id'] }}">
											<input type="hidden" name="child_code" id="child_code" value ="{{ $users['child_code'] }}">
					
												
												<span class="dash_img_profile_img">
													<img style="width:100%; height:100%;" src="@if($val['session_user_type']=='1') {{ URL::to('/') }}/public/images/{{ $image_path }} @else {{ $image_path }} @endif" class="img-responsive" alt="profile"/>
												</span>
												<div class="dash_board_details_sec">
												
													<h4>{{ $users['first_name'] }}</h4>
													<span>@if($val) {{ $val['session_user_role'] }} @endif</span>
													<div class="star_ratting_table_views">
															<ul>
																@php $rating = $StudentRatingCount;  @endphp
																@foreach(range(1,5) as $i)
																		@if($rating >0)
																			@if($rating >0.5)
																				<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																			@elseif($rating <=0.5)
																				<li><a href="#."><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>
																			@endif	
																		@else
																			<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																		@endif
																		@php $rating--; @endphp
																	
																@endforeach
															</ul>
															<small>{{ $StudentRatingCount }}/5</small>
													</div>
													<div class="refer_frds_btn_new">
														<a href='{{ url("EditProfile")}}' class="btn small_comm_btn">Edit Profile</a>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-6">
											<div class="user-privat-details">
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/img/gender.png')}}" class="img-responsive" alt="gender"/></span>
													<div>{{ $users['gender'] }}</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/img/age.png')}}" class="img-responsive" alt="age"/></span>
													<div>

														@php
														$dob = $users['dob'];
														$today = date('Y-m-d');
														$d1 = new DateTime($today);
														$d2 = new DateTime($dob);

														$diff = $d2->diff($d1);

														echo $diff->y;
														@endphp
														Years
													</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/img/map-marker.png')}}" class="img-responsive" alt="gender"/></span>
													<div>{{ $users['address_line1'] }}</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--account user details info end here-->
				<!-- profile details section start here-->
				<section class="common_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="common_section_details_here">
									<div class="common_section_heading">
										<h4>PROFILE DETAILS</h4>
									</div>
									<div class="common_section_content">
										<div class="row">
											<div class="col-md-7">
												<div class="profile_user_details">
													<div class="profile_personal_details">
														<ul>
															<li><span>GRADE:</span><div class="profile_details_content">{{ $users['grade_name'] }}</div></li>
															<li><span>SUBJECTS/COURSES:</span><div class="profile_details_content">{{ $users['subject_name'] }}</div></li>
															<li><span>DATE OF BIRTH:</span><div class="profile_details_content">{{ $users['dob'] }}</div></li>
															<li><span>GENDER</span><div class="profile_details_content">{{ $users['gender'] }}</div></li>
															<li><span>TUTOR TYPE</span><div class="profile_details_content">{{ $users['tutor_type'] }}</div></li>
															<li><span>ADDRESS</span><div class="profile_details_content">{{ $users['address_line1'] }}</div></li>
															<li><span>COUNTRY</span><div class="profile_details_content">{{ $users['country_name'] }}</div></li>
															<li><span>MAX DISTANCE TRAVEL:</span><div class="profile_details_content_link ">{{ $users['max_distance_travel_kms'] }}</div><a href="#." class="info-link-details tooltip_new"><span class="tooltiptext_new">Maximum Distance You Can Travel to Tutor</span><i class="fa fa-info-circle" aria-hidden="true"></i></a></li>
															<li><span>PHONE NUMBER:</span><div class="profile_details_content">{{ $users['primary_phone'] }}</div></li>
															@php
															$val=Session::get('loginSession')
															@endphp
															<li><span>EMAIL ID:</span><div class="profile_details_content">{{ $users['email_id'] }}</div></li>
															<!-- <li><span>PASSWORD:</span><div class="profile_details_content_link">**********</div><a href="#." class="info-link-details"><i class="fa fa-eye" aria-hidden="true"></i></a></li> -->
														</ul>
													</div>
													<div class="common_btn_div">
														<a href="#." class="btn small_comm_btn" data-toggle="modal" data-target="#change_password_popup">Change Password</a>
													</div>
												</div>
											</div>
											<div class="col-md-5">
												<div class="profile_details_shar_pa_div">
													<div class="profile_calender">
														<div class="calender_form_set_new">
															<!-- <div class="input_new_set_search">
																<input type="text" class="form-control" placeholder="Search">
															</div> -->
														</div>
														<div class="pie_chart_heading">
															<h4>MY PLAN</h4>
														</div>
														<div class="piechart-demo">
															<!-- <img src="{{ url('public/assets/img/calender-img.jpg')}}" class="img-responsive" alt="piechart"> -->

															<div class='calendar1 profile-calender-set padd-zero calender-set-margin-top'></div>

														</div>
													</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class="profile_shar_refer_add_links">
													<div class="profile_share_add_link_here">
														<span><img src="{{ url('public/assets/img/share-freind-icon.png')}}" class="img-responsive" alt="share-friend"/></span>
														<div class="blog_btn_set">
															<button type="button" class="btn common_btn" data-toggle="modal" data-target="#share_profile_parent_set_popup">Share With Parents</button>
														</div>
													</div>
													<div class="profile_share_add_link_here">
														<span><img src="{{ url('public/assets/img/refer-freind-icon.png')}}" class="img-responsive" alt="refer-friend"/></span>
														<div class="blog_btn_set">
															<a href='{{ url("BookSession")}}' class="btn common_btn">Add Session</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- profile details section end here-->
				
				
				<!--popup start here-->
				
					
					  
					  
					  
					  
					  
			<!-- Claender script on -->
				@include('Student.calendar_code') 
			<!-- Claender script on -->

			<!-- Change password script on -->
			@include('Student.change_password') 
			<!-- Change password script on -->

			<!-- Share with parent script on -->
			@include('Student.share_with_parent') 
			<!-- Share with parent script on -->

				<!--popup end here-->
@include('Template.footer_student') 	