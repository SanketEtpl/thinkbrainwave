@include('Template.header_student')

@php
$val=Session::get('loginSession')
@endphp
@php
	if($users['profile_file_path']!='')
	{
		$image_path = $users['profile_file_path'];
	}
	else{
		$image_path = "no-image.png";
	}
@endphp
				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url({{ url('public/assets/img/mid-bg.jpg')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>Edit Profile</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!--edit profile start here-->
				<form action = '{{ url("ProfileSave")}}' id="formEdit" class="form1" method = "post" enctype="multipart/form-data">
					<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
					
					<input type="hidden" name="user_id" id="user_id" value ="{{ $users['user_id'] }}">
					<input type="hidden" name="child_code" id="child_code" value ="{{ $users['child_code'] }}">
					<input type="hidden" name="old_image" value ="{{ $image_path }}">
					<input type="hidden" name="role" id="role" value ="@if($val) {{ $val['session_user_role_id'] }} @endif">

					
				<section class="my_account_all_us_details">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="my_account_data">
									<div class="row">
										<div class="col-md-5 col-sm-6 col-xs-6 dash_dta_border_right">
											<div class="dash_bora_profil_user_sec">
												
												<span class="dash_img_profile_img">
													<img id="profile-img-tag" 	style="width:100%; height:100%;" src="@if($val['session_user_type']=='1') {{ URL::to('/') }}/public/images/{{ $image_path }} @else {{ $image_path }} @endif" class="img-responsive" alt="profile"/>
													<div class="file btn edit_profile_photo">
														<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
														<input type="file" id="profile-img" name="profile_file_path"/>
													</div>
												</span>
												<div class="dash_board_details_sec">
												
													<h4>{{ $users['first_name'] }}</h4>
													<span>@if($val) {{ $val['session_user_role'] }} @endif</span>
													<div class="star_ratting_table_views">
															<ul>
																@php $rating = $StudentRatingCount;  @endphp
																@foreach(range(1,5) as $i)
																		@if($rating >0)
																			@if($rating >0.5)
																				<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																			@elseif($rating <=0.5)
																				<li><a href="#."><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>
																			@endif	
																		@else
																			<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																		@endif
																		@php $rating--; @endphp
																	
																@endforeach
															</ul>
															<small>{{ $StudentRatingCount }}/5</small>
													</div>
													<div class="refer_frds_btn_new">
														<!-- <a href="MyProfile" class="btn small_comm_btn same_width_btn">Save</a>
														<a href="#." class="btn small_comm_btn same_width_btn">Cancel</a> -->
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-6">
											<div class="user-privat-details">
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/img/gender.png')}}" class="img-responsive" alt="gender"/></span>
													<div>{{ $users['gender'] }}</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/img/age.png')}}" class="img-responsive" alt="age"/></span>
													<div>
													@php
														$dob = $users['dob'];
														$today = date('Y-m-d');
														$d1 = new DateTime($today);
														$d2 = new DateTime($dob);

														$diff = $d2->diff($d1);

														echo $diff->y;
														@endphp
														Years
													</div>
												</div>
												<div class="user_prvi_det_lists">
													<span><img src="{{ url('public/assets/img/map-marker.png')}}" class="img-responsive" alt="gender"/></span>
													<div>{{ $users['address_line1'] }}</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--edit profile details end here-->
				<!-- profile details section start here-->
				<section class="common_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="common_section_details_here">
									<div class="common_section_heading">
										<h4>PROFILE DETAILS</h4>
									</div>
									<div class="common_section_content">
										<div class="row">
												<div class="col-md-7">
													<div class="profile_user_details">
														<div class="profile_personal_details">
															
																<ul>
																	<li><span>Name:</span><div class="input_boxes_set "><input value="{{ $users['first_name'] }}" type="text" class="form-control required_letter" id="name" name="name"/></div></li>
																	<li><span>GRADE:</span>
																		<div class="input_boxes_set">
																			<select class="form-control select_tag_box required_check" id="grade" name="grade">
																				@php
																					$grade_id = $users['grade_id'];
																				@endphp
																				<option value="">------select-------</option>
																					@foreach ($grades as $grade)
																						<option value="{{ $grade->id }}" @if ($grade_id == $grade->id ) selected @endif>{{ $grade->name }}</option>
																					@endforeach
																			</select>
																		</div>	
																	<li><span>SUBJECTS/COURSES:</span><div class="input_boxes_set"><input type="text" value="{{ $users['subject_name'] }}" class="form-control" id="subj_course" name="subject"/></div></li>
																	<li>
																		<span>DATE OF BIRTH:</span>
																		<div class="input_boxes_set">
																			<div class="form-group calend_div_new">
																				<div class="input-group date studentDOB">
																					<input class="form-control calender_set required_date_check" value="{{ $users['dob'] }}"  type="text" name="dob" id="dob">
																					<div class="input-group-addon custom_input_calend"><i class="fa fa-calendar" aria-hidden="true"></i></div>
																				</div>
																				<input type="hidden" id="dtp_input2" value="">
																			</div>
																			
																		</div>
																		<a href="#." class="info-link-details_sec tooltip_new"><span class="tooltiptext_new">Student age should be greater than or equal to 5 year.</span><i class="fa fa-info-circle" aria-hidden="true"></i></a>
														
																	</li>
																	<li>
																		<span>GENDER</span>
																		@php
																			$gender = $users['gender'];
																		@endphp
																		<div class="profile_details_content">
																		<div class="radio_button">
																			<input type="radio" id="test1" value="Male" name="gender" @if ($gender == "Male") checked @endif >
																			<label for="test1">Male</label>
																		</div>
																		<div class="radio_button">
																			<input type="radio" id="test2" value="Female" name="gender" @if ($gender == "Female") checked @endif >
																			<label for="test2">Female</label>
																		</div>
																		</div>
																	</li>
																	<li>
																		<span>VISITE TYPE</span>
																		@php
																			$tutor_type = $users['tutor_type'];
																		@endphp
																		<div class="profile_details_content">
																		<div class="radio_button">
																			<input type="radio" id="test3" value="Any" name="tutor_type" @if ($tutor_type == "Any") checked @endif >
																			<label for="test3">Any</label>
																		</div>
																		<div class="radio_button">
																			<input type="radio" id="test4" value="Male" name="tutor_type" @if ($tutor_type == "Male") checked @endif >
																			<label for="test4">Male</label>
																		</div>
																		<div class="radio_button">
																			<input type="radio" id="test5" value="Female" name="tutor_type" @if ($tutor_type == "Female") checked @endif >
																			<label for="test5">Female</label>
																		</div>
																		</div>
																	</li>
																	<li><span>ADDRESS</span><div class="input_boxes_set"><input type="text"  name="address_line1" value="{{ $users['address_line1'] }}"  class="form-control required_check" id="address" placeholder="ex. plot no, colony name, area name, city name" /></div></li>
																	<li>
																		<span>COUNTRY</span>
																		<div class="input_boxes_set">
																			<select class="form-control select_tag_box required_check" id="country_id" name="country_id">
																			@php
																				$country_id = $users['country_id'];
																			@endphp
																			<option value=""  selected disabled>------select-------</option>
																				@foreach ($countries as $countrie)
																					<option value="{{ $countrie->id }}" @if ($country_id == $countrie->id ) selected @endif >{{ $countrie->name }}</option>
																				@endforeach
																			</select>
																		</div>
																	</li>
																	<li><span>MAX DISTANCE TRAVEL:</span><div class="input_boxes_set"><input type="text"  name="max_distance_travel_kms" value="{{ $users['max_distance_travel_kms'] }}"  class="form-control required_check" id="distan_travel"/></div><a href="#." class="info-link-details_sec tooltip_new"><span class="tooltiptext_new">Maximum Distance You Can Travel to Tutor</span><i class="fa fa-info-circle" aria-hidden="true"></i></a></li>
																	<li><span>PHONE NUMBER:</span><div class="input_boxes_set"><input type="text" class="form-control required_mobile"  id="primary_phone" name="primary_phone" value="{{ $users['primary_phone'] }}" /></div></li>
																	<li><span>EMAIL ID:</span><div class="input_boxes_set"><input type="text" class="form-control required_check"  id="email" name="email" value="{{ $users['email_id'] }}" /></div></li>
																	<!-- <li><span>PASSWORD:</span><div class="input_boxes_set"><input type="text" class="form-control"  name="password" id="phon_number"/></div><a href="#." class="info-link-details_sec"><i class="fa fa-eye" aria-hidden="true"></i></a></li> -->
																</ul>
															
														</div>
														<div class="common_btn_div">
															<a href="#." class="btn small_comm_btn"  data-toggle="modal" data-target="#change_password_popup">Change Password</a>
														</div>
													</div>
												</div>
												<div class="col-md-5">
													<div class="profile_details_shar_pa_div">
														<div class="profile_calender">
															<div class="calender_form_set_new">
																<!-- <div class="input_new_set_search">
																	<input type="text" class="form-control" placeholder="Search">
																</div> -->
															</div>
															<div class="pie_chart_heading">
																<h4>MY PLAN</h4>
															</div>
															<div class="piechart-demo">
																<!-- <img src="{{ url('public/assets/img/calender-img.jpg')}}" class="img-responsive" alt="piechart"> -->
																<div class='calendar1'></div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-12">
													<div class="profile_shar_refer_add_links">
															<div class="profile_share_add_link_here" >
																<span><img src="{{ url('public/assets/img/share-freind-icon.png')}}" class="img-responsive" alt="share-friend"/></span>
																<div class="blog_btn_set">
																	<button type="button" class="btn common_btn" data-toggle="modal" data-target="#share_profile_parent_set_popup">Share With Parents</button>
																</div>
															</div>
															<div class="profile_share_add_link_here">
																<span><img src="{{ url('public/assets/img/refer-freind-icon.png')}}" class="img-responsive" alt="refer-friend"/></span>
																<div class="blog_btn_set">
																	<a href='{{ url("BookSession")}}' class="btn common_btn">Add Session</a>
																</div>
															</div>
														</div>
												</div>
												<div class="col-md-12">
													<div class="save_adn_cancel_btn_grp">
														<button type="submit" class="btn common_btn_new">Save</button>
														<button type="button" class="btn common_btn_new">Cancel</button>
													</div>
												</div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				</form>
				<!-- profile details section end here-->
				
				
				<!--popup start here-->
				
					 
				<!--popup end here-->
			
			<!--js link start here-->
			
			<script>
				jQuery(document).ready(function(){
					jQuery('.scrollbar-inner').scrollbar();
					
					$('#form1').validate({
						rules: {

							old_password: {
								required: true
							},

							new_password: {
								required: true
							},
							retype_password: {
									required: true,
									equalTo: "#new_password"
							}
						}
					});
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
					$("#grade").click(function(e){
						var grade = $("select[name=grade]").val();
						//alert(grade);
						$.ajax({
								type:'POST',
								url:'GradeFetch',
								data:{grade:grade},
								success:function(data){
									//alert(data.success);
									console.log(data.success);
									console.log(data.success);
									$("input[name=subject]").val(data.success);
									
								}
							});
					});
					
				});
			</script>
			<script type="text/javascript">
				function readURL(input) {
					if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function (e) {
						$('#profile-img-tag').attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
					}
				}
				$("#profile-img").change(function(){
					readURL(this);
				});
				</script>
			<!--js link end here-->
			<!-- Claender script on -->
				@include('Student.calendar_code') 
			<!-- Claender script on -->

			<!-- Change password script on -->
			@include('Student.change_password') 
			<!-- Change password script on -->

			<!-- Share with parent script on -->
			@include('Student.share_with_parent') 
			<!-- Share with parent script on -->

@include('Template.footer_student') 				