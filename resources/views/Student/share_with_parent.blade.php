<!--share parents profile popup start here-->
<div class="modal fade" id="share_profile_parent_set_popup" role="dialog">
						<div class="modal-dialog refer_friend_width_popup">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="moel_details_new">
									<div class="section_heading_sec">
										<h4>SHARE PROFILE WITH PARENTS</h4>
									</div>
									<div class="model_content">
										<div class="refer_friend_popup_details">
											<ul class="nav nav-tabs">
											  <li class="active">
												  <a data-toggle="tab" href="#refer_frd_01">
													  <span>
														  <img src="{{ url('public/assets/img/popup-call-icon.png')}}" class="img-responsive unactive" alt="call"/>
														  <img src="{{ url('public/assets/img/popup-call-icon-active.png')}}" class="img-responsive active alt="call"/>
													  </span>
													  <div>PHONE NUMBER</div>
												  </a>
											  </li>
											  <li>
												  <a data-toggle="tab" href="#refer_frd_02">
													  <span>
														  <img src="{{ url('public/assets/img/popup-email-icon.png')}}" class="img-responsive unactive" alt="call"/>
														  <img src="{{ url('public/assets/img/popup-email-icon-active.png')}}" class="img-responsive active" alt="call"/>
													  </span>
													  <div>EMAIL ID</div>
												  </a>
											  </li>
											</ul>

											<div class="tab-content">
											  <div id="refer_frd_01" class="tab-pane fade in active">
													<form class="form2" method = "post" >
														<div class="refe_frd_tab_content">
															<div class="form_set_n_h">
																<label>Enter Phone Number</label>
																<div class="input-group">
																	<input class="form-control required_mobile" id="phone_number" name="phone_number"/>
																	<button type="button" class="btn comm-btn" id="SmsShareWithParent">Share</button>
																</div>
															</div>
														</div>
													</form>
											  </div>
											  <div id="refer_frd_02" class="tab-pane fade">
													<form class="form3" method = "post" >
														<div class="refe_frd_tab_content">
															<div class="form_set_n_h">
																<label>Enter Email ID</label>
																<div class="input-group">
																	<input class="form-control required_email" id="email_id" name="email_id"/>
																	<button type="button" class="btn comm-btn" id="EmailShareWithParent">Share</button>
																</div>
															</div>
														</div>		
													</form>									 
											  </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!--share parents profile popup end here-->
				<script>
				
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
					
					$("#SmsShareWithParent").click(function(e){
						if ($('.form2').valid()) {
							e.preventDefault();
							var user_id = $("input[id=user_id]").val();
							var child_code = $("input[id=child_code]").val();
							var phone_number = $("input[id=phone_number]").val();
							
							$.ajax({
									type:'POST',
									url:'{{ url("CodeShareWithParentSms")}}',
									data:{user_id:user_id, child_code:child_code, phone_number:phone_number},
									success:function(data){
										console.log(data.success);
										//console.log(data.error);
										if(data.success)
										{
											$('#share_profile_parent_set_popup').modal('hide');
											$('#sent_success_popup').modal('show');
										}
										if(data.error)
										{
											$('#share_profile_parent_set_popup').modal('show');
											
										}
										
									}
								});
							}
						
					});

					$("#EmailShareWithParent").click(function(e){
						if ($('.form3').valid()) {
							e.preventDefault();
							var user_id = $("input[id=user_id]").val();
							var child_code = $("input[id=child_code]").val();
							var email_id = $("input[id=email_id]").val();
							
							$.ajax({
									type:'POST',
									url:'{{ url("CodeShareWithParentEmail")}}',
									data:{user_id:user_id, child_code:child_code, email_id:email_id},
									success:function(data){
										console.log(data.success);
										//console.log(data.error);
										if(data.success)
										{
											$('#share_profile_parent_set_popup').modal('hide');
											$('#sent_success_popup').modal('show');
										}
										if(data.error)
										{
											$('#share_profile_parent_set_popup').modal('show');
											
										}
										
									}
								});
							}
						
					});
			</script>
			