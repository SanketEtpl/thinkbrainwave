@include('Template.header_student')
@php
	$val=Session::get('loginSession')
@endphp
@php
	if($users['profile_file_path']!='')
	{
		$image_path = $users['profile_file_path'];
	}
	else{
		$image_path = "no-image.png";
	}
@endphp
				<!-- banner section start here-->
				<section class="banner_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>DASHBOARD</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!-- pie chart section start here-->
				<section class="piechart_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="piechart_details">
									<div class="row">
										<div class="col-sm-6">
											<div class="dash_profile_datils_account">
											
												<div class="dash_bora_profil_user">
													<div class="profile_details_img_with_btn">
														<span class="dash_img_profile_img">
															<img src="@if($val['session_user_type']=='1') {{ URL::to('/') }}/public/images/{{ $image_path }} @else {{ $image_path }} @endif" class="img-responsive" alt="profile"/>
														</span>
														<a href='{{ url("MyProfile")}}' class="btn small_comm_btn">View Profile</a>
													</div>
													<div class="dash_board_details">
														<h4>
														{{ $users['first_name'] }}</h4>
														<span>@if($val) {{ $val['session_user_role'] }} @endif</span>
														<div class="star_ratting_table_views">
															<ul>
																@php $rating = $StudentRatingCount;  @endphp
																@foreach(range(1,5) as $i)
																		@if($rating >0)
																			@if($rating >0.5)
																				<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																			@elseif($rating <=0.5)
																				<li><a href="#."><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>
																			@endif	
																		@else
																			<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																		@endif
																		@php $rating--; @endphp
																	
																@endforeach
															</ul>
															<small>{{ $StudentRatingCount }}/5</small>
														</div>
													</div>
												</div>
												
												<div class="dash_bora_profil_user">
													<div class="profile_details_img_with_btn">
														<span class="dash_img_profile_img">
															<div class="account_img_set new_account_pro_img">
																<img src="{{ url('public/assets/img/my_account.png')}}" class="img-responsive" alt="profile"/>
															</div>
														</span>
														<a href='{{ url("MyAccount")}}' class="btn small_comm_btn">View Account</a>
													</div>
													<div class="dash_account_set">
														<!--<h4>846</h4>-->
														<span>MY ACCOUNT</span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="pie_chart_details">
												<div class="pie_chart_heading">
													<h4>MY PLAN</h4>
												</div>
												<div class="calender_form_set_new">
													<div class="row">
														<div class="col-md-8">
															<!-- <div class="input_new_set_search">
																<input type="text" class="form-control" placeholder="Search"/>
															</div> -->
														</div>
														<div class="col-md-4">
															<!-- <div class="input_new_set_select">
																<select class="form-control select_tag_new">
																	<option>Daily</option>
																	<option>Monthly</option>
																	<option>Yearly</option>
																</select>
															</div> -->
														</div>
													</div>
												</div>
												<div class="piechart-demo">
													<!-- <img src="{{ url('public/assets/img/calender-img.jpg')}}" class="img-responsive" alt="piechart"/> -->
													<div class='calendar1 padd-zero'></div>
												</div>
												<div class="calnder-btn_set">
													<a href='{{ url("MyPlan") }}' class="btn common_btn">Add Session</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- pie chart section end here-->
			<!-- Claender script on -->
				@include('Student.calendar_code') 
			<!-- Claender script on -->

@include('Template.footer_student') 				
