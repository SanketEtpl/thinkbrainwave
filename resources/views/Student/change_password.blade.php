<!-- change password popup Start Here -->
<div class="modal fade" id="change_password_popup" role="dialog">
						<div class="modal-dialog change_password_popup_width">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="model_details">
									<div class="section_heading_sec">
										<h4>CHANGE PASSWORD</h4>
									</div>
												
									<div class="model_content">
										<div class="add_topic_form">
											<form class="form1" method = "post" >
            									<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
											
												<div class="row">
													<div class="col-md-12">
														<div class="form_set">
															<span>OLD PASSWORD :</span>
															<div class="input_box_set_popup"><input type="password" class="form-control required_check" id="old_password" name="old_password"/></div>
														</div>
													</div>
													<div class="col-md-12">
														<div class="form_set">
															<span>NEW PASSWORD :</span>
															<div class="input_box_set_popup"><input type="password" class="form-control required_check" id="new_password" name="new_password"/></div>
														</div>
													</div>
													<div class="col-md-12">
														<div class="form_set">
															<span>CONFIRM PASSWORD :</span>
															<div class="input_box_set_popup"><input type="password" class="form-control required_check" id="retype_password" name="retype_password"/></div>
														</div>
													</div>
													<div class="col-md-12">
														<div class="main_profil_btn">


														<!-- 	<a href="#." class="btn save_cancel_btn margin-right-10px" data-dismiss="modal" data-toggle="modal" data-target="#success_changes_popup">Update</a> -->

															<button  type="button" id="UpdatePassword" class="btn save_cancel_btn margin-right-10px">Update</button>

															<a href="#." class="btn save_cancel_btn" data-dismiss="modal">Cancel</a>
														</div>
													</div>
												</div>
											</form>
										</div>
									 </div>
							      
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- change password popup end Here -->
					  <!-- password save popup end Here -->
						<div class="modal fade" id="password_save_popup" role="dialog">
							<div class="modal-dialog model_forgot_pass_popup">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
										</button>
									</div>
									<div class="modal-body">
										<div class="moel_details">
											<span class="model_header">
												<h5>Success</h5>
											</span>
											<div class="model_content">
												<p>Password save successfully.</p>
												<div class="pop_btn_sect">
													<lable class="btn popup_coom_btn" data-dismiss="modal">ok</lable>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						  <!-- password save popup end Here -->

						  <!-- not_password_popup end Here -->
						<div class="modal fade" id="not_password_popup" role="dialog">
							<div class="modal-dialog model_forgot_pass_popup">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
										</button>
									</div>
									<div class="modal-body">
										<div class="moel_details">
											<span class="model_header">
												<h5>Error</h5>
											</span>
											<div class="model_content">
												<p>Old password does not exist.</p>
												<div class="pop_btn_sect">
													<!-- <a href="" class="btn popup_coom_btn">OK</a> -->
													<lable class="btn popup_coom_btn" data-dismiss="modal">ok</lable>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
							<!-- not_password_popup end Here -->
			<script>
				jQuery(document).ready(function(){
					
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
					
					$("#UpdatePassword").click(function(e){
						if ($('.form1').valid()) 
						{
							e.preventDefault();
							var user_id = $("input[id=user_id]").val();
							var old_password = $("input[id=old_password]").val();
							var new_password = $("input[id=new_password]").val();
							var retype_password = $("input[id=retype_password]").val();
							if(new_password==retype_password)
							{
								$.ajax({
										type:'POST',
										url:'{{ url("UpdatePassword")}}',
										data:{user_id:user_id, old_password:old_password,  new_password:new_password, retype_password:retype_password },
										success:function(data){
											console.log(data.success);
											//console.log(data.error);
											if(data.success)
											{
												$('#change_password_popup').modal('hide');
												$('#password_save_popup').modal('show');
												
											}
											if(data.error)
											{
												$('#not_password_popup').modal('show');
												
											}
											
										}
									});
								}
							else
							{
								$('#passwod_not_match_popup').modal('show');
							}
						}
					});
				});
			</script>
			