@include('Template.header_student')

				<!-- banner section start here-->
				<section class="banner_section" style="background-image:url({{ url('public/assets/img/mid-bg.jpg')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="banner_content">
									<h4>Boosting Plans</h4>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!-- profile details section start here-->
				<section class="common_section_deta">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="common_section_details_here">
									<div class="common_section_heading">
										<h4>SUBSCRIBE NOW TO AVAIL BRAINWAVE SERVICES</h4>
									</div>
									<div class="common_section_content_div">
										<div class="avail_brainwave_cols">
											<div class="row">
												<div class="col-md-3">
													<div class="boosting_plan_first" style="background-image:url({{ url('public/assets/img/avail_brainwave_back_img.png')}});">
														<div class="boostin_pla_head">
															<h4>FREEMIUM</h4>
														</div>
														<div class="boostin_pla_first_content">
															<div class="boosting_price">
																<span>$0</span>
															</div>
															<div class="featurs_content_details">
																<div class="featurs_content_heading">Features</div>
																<div class="feturers_cont_hrs">
																	<h4>2 HOURS V2V</h4>
																	<h4>2 HOURS F2F</h4>
																</div>
																<div class="brainwav_plns_btn">
																	<button type="button" class="btn plans_btn_sec">Sign up</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="boosting_plan_sec" style="background-image:url({{ url('public/assets/img/avail_brainwave_back_img.png')}});">
														<div class="boostin_pla_head">
															<h4>BRONZE</h4>
														</div>
														<div class="boostin_pla_first_content">
															<div class="boosting_price">
																<span>$200</span>
															</div>
															<div class="featurs_content_details">
																<div class="featurs_content_heading">Features</div>
																<div class="feturers_cont_hrs">
																	<h4>10 HOURS</h4>
																	<h4>V2V/ F2F</h4>
																</div>
																<div class="brainwav_plns_btn">
																	<button type="button" class="btn plans_btn">Proceed to Checkout</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="boosting_plan_third" style="background-image:url({{ url('public/assets/img/avail_brainwave_back_img.png')}});">
														<div class="boostin_pla_head">
															<h4>SILVER</h4>
														</div>
														<div class="boostin_pla_first_content">
															<div class="boosting_price">
																<span>$400</span>
															</div>
															<div class="featurs_content_details">
																<div class="featurs_content_heading">Features</div>
																<div class="feturers_cont_hrs">
																	<h4>20 HOURS</h4>
																	<h4>V2V/ F2F</h4>
																</div>
																<div class="brainwav_plns_btn">
																	<button type="button" class="btn plans_btn">Proceed to Checkout</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="boosting_plan_four" style="background-image:url({{ url('public/assets/img/avail_brainwave_back_img.png')}});">
														<div class="boostin_pla_head">
															<h4>GOLD</h4>
														</div>
														<div class="boostin_pla_first_content">
															<div class="boosting_price">
																<span>$600</span>
															</div>
															<div class="featurs_content_details">
																<div class="featurs_content_heading">Features</div>
																<div class="feturers_cont_hrs_sec">
																	<h4>30 HOURS</h4>
																	<h4>V2V/ F2F</h4>
																</div>
																<span class="current_blog_icon"><img src="{{ url('public/assets/img/bootin-pln_correct_icon.png')}}" class="img-responsive" alt="correct"/></span>
																<div class="brainwav_plns_btn">
																	<button type="button" class="btn plans_btn_sec">Current Plan</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="avail_brainwave_cols_new">
											<div class="row">
												<div class="col-md-3"></div>
												<div class="col-md-3">
													<div class="boosting_plan_five" style="background-image:url({{ url('public/assets/img/avail_brainwave_back_img.png')}});">
														<div class="boostin_pla_head">
															<h4>PLATINUM</h4>
														</div>
														<div class="boostin_pla_first_content">
															<div class="boosting_price">
																<span>$800</span>
															</div>
															<div class="featurs_content_details">
																<div class="featurs_content_heading">Features</div>
																<div class="feturers_cont_hrs">
																	<h4>40 HOURS</h4>
																	<h4>V2V/F2V</h4>
																</div>
																<div class="brainwav_plns_btn">
																	<button type="button" class="btn plans_btn_sec">Sign up</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="boosting_plan_six" style="background-image:url({{ url('public/assets/img/avail_brainwave_back_img.png')}});">
														<div class="boostin_pla_head">
															<h4>BRONZE</h4>
														</div>
														<div class="boostin_pla_first_content">
															<div class="boosting_price">
																<span>$1000</span>
															</div>
															<div class="featurs_content_details">
																<div class="featurs_content_heading">Features</div>
																<div class="feturers_cont_hrs">
																	<h4>50 HOURS</h4>
																	<h4>V2V/F2V</h4>
																</div>
																<div class="brainwav_plns_btn">
																	<button type="button" class="btn plans_btn">Proceed to Checkout</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-3"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- profile details section end here-->
				
				
				<!--popup start here-->
				
					
					  
					  <!-- change password popup Start Here -->
					  <div class="modal fade" id="change_password_popup" role="dialog">
						<div class="modal-dialog change_password_popup_width">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="model_details">
									<div class="section_heading_sec">
										<h4>CHANGE PASSWORD</h4>
									</div>
									<div class="model_content">
										<div class="add_topic_form">
											<div class="row">
												<div class="col-md-12">
													<div class="form_set">
														<span>OLD PASSWORD :</span>
														<div class="input_box_set_popup"><input type="password" class="form-control" id="old_password"/></div>
													</div>
												</div>
												<div class="col-md-12">
													<div class="form_set">
														<span>NEW PASSWORD :</span>
														<div class="input_box_set_popup"><input type="password" class="form-control" id="new_password"/></div>
													</div>
												</div>
												<div class="col-md-12">
													<div class="form_set">
														<span>CONFIRM PASSWORD :</span>
														<div class="input_box_set_popup"><input type="password" class="form-control" id="retype_password"/></div>
													</div>
												</div>
												<div class="col-md-12">
													<div class="main_profil_btn">
														<a href="#." class="btn save_cancel_btn margin-right-10px" data-dismiss="modal" data-toggle="modal" data-target="#success_changes_popup">Update</a>
														<a href="#." class="btn save_cancel_btn" data-dismiss="modal">Cancel</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- change password popup end Here -->
					  
					  <!--share parents profile popup start here-->
					  <div class="modal fade" id="share_profile_parent_set_popup" role="dialog">
						<div class="modal-dialog refer_friend_width_popup">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="moel_details_new">
									<div class="section_heading_sec">
										<h4>SHARE PROFILE WITH PARENTS</h4>
									</div>
									<div class="model_content">
										<div class="refer_friend_popup_details">
											<ul class="nav nav-tabs">
											  <li class="active">
												  <a data-toggle="tab" href="#refer_frd_01">
													  <span>
														  <img src="{{ url('public/assets/img/popup-call-icon.png')}}" class="img-responsive unactive" alt="call"/>
														  <img src="{{ url('public/assets/img/popup-call-icon-active.png')}}" class="img-responsive active alt="call"/>
													  </span>
													  <div>PHONE NUMBER</div>
												  </a>
											  </li>
											  <li>
												  <a data-toggle="tab" href="#refer_frd_02">
													  <span>
														  <img src="{{ url('public/assets/img/popup-email-icon.png')}}" class="img-responsive unactive" alt="call"/>
														  <img src="{{ url('public/assets/img/popup-email-icon-active.png')}}" class="img-responsive active" alt="call"/>
													  </span>
													  <div>EMAIL ID</div>
												  </a>
											  </li>
											</ul>

											<div class="tab-content">
											  <div id="refer_frd_01" class="tab-pane fade in active">
													<div class="refe_frd_tab_content">
														<div class="form_set_n_h">
															<label>Enter Phone Number</label>
															<div class="input-group">
																<input class="form-control" id="phone_number"/>
																<button type="button" class="btn comm-btn" data-dismiss="modal" data-toggle="modal" data-target="#sent_success_popup">Share</button>
															</div>
														</div>
													</div>
											  </div>
											  <div id="refer_frd_02" class="tab-pane fade">
													<div class="refe_frd_tab_content">
														<div class="form_set_n_h">
															<label>Enter Email ID</label>
															<div class="input-group">
																<input class="form-control" id="email_id"/>
																<button type="button" class="btn comm-btn" data-dismiss="modal" data-toggle="modal" data-target="#sent_success_popup">Share</button>
															</div>
														</div>
													</div>											 
											  </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!--share parents profile popup end here-->
					  
				<!--popup end here-->
			


@include('Template.footer_student') 				