<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Homepage</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<!--css link start here-->
		<link rel="icon" type="imge/png" sizes="16x16" href="{{ url('public/assets/img/favicon.png')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/bootstrap.min.css') }}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/font-awesome-4.7.0/css/font-awesome.min.css') }}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/style.css') }}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/media.css') }}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/fonts/stylesheet.css') }}"/>
		<link rel="stylesheet"  href="{{ url('public/assets/css/owl.carousel.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/jquery.scrollbar.css')}}"/>

		<link rel="stylesheet" type="text/css" href="{{ url('public/datepicker/bootstrap-datepicker.css')}}"/>
		
		<link rel='stylesheet' href="{{ url('public/datepicker/bootstrap-datepicker.min.css')}}" />

		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/bootstrap-datetimepicker.min.css')}}"/>

		<link rel="stylesheet"  href="{{ url('public/assets/css/owl.carousel.min.css') }}"/>
		<!--css link end here-->
		<!-- <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
    	<meta name="csrf-token" content="{{ csrf_token() }}" />

		<style>	
			.help-block
			{
				color:red;
			}
		</style>

		<script src="{{ url('public/assets/js/jquery.min.js')}}"></script>

		<script src="{{ url('public/datepicker/bootstrap-datepicker.js')}}"></script>
		<script src="{{ url('public/js/validation.js')}}"></script>
		
	</head>
		<body>
			<!-- Page wrapper Start here-->
			<div class="wrapper">
				<!-- header section Start here-->
				@if(Route::currentRouteName() != 'SigninSignup')
				<header class="header_section_new">
					<div class="col-md-12">
						<div class="head_navbar">
							 <nav class="navbar">
								<div class="navbar-header">
								  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								  </button>
								  <a class="navbar-brand nav_logo" href='{{ url("Home")}}'>
									 <img src="{{ url('public/assets/img/logo_header.png') }}" class="img-responsive" alt="logo"/>
								  </a>
								</div>
								<div class="navbar_set">
								@php
									$ses1 = Session::get('loginSession');
									$ses2 = Session::get('loginSessionTutor');
									$ses3 = Session::get('loginSessionParent');
									$session_set = '';
									$var2 = '';
									if($ses1)
									{
										$session_set = 'yes';
									}
									if($ses2)
									{
										$session_set = 'yes';
									}
									if($ses3)
									{
										$session_set = 'yes';
									}
								@endphp
								  <ul class="nav navbar-nav collapse navbar-collapse first_navbar" id="myNavbar">
									<li class="{{ Route::currentRouteName() == 'home' ? 'active' : '' }}"><a href='{{ url("Home")}}'>Home</a></li>
									<li class="{{ Route::currentRouteName() == 'about' ? 'active' : '' }}"><a href='{{ url("About")}}'>About Us</a></li>
									<li class="{{ Route::currentRouteName() == 'contact_us' ? 'active' : '' }}"><a href='{{ url("ContactUs")}}'>Contact Us</a></li>
									@if($session_set=='')
										<li class="{{ Route::currentRouteName() == 'SigninSignup' ? 'active' : '' }}"><a href='{{ url("Signin")}}'>Sign In</a></li>
										<li class="{{ Route::currentRouteName() == 'SigninSignup' ? 'active' : '' }}"><a href='{{ url("Signup")}}'>Sign Up</a></li>
									
									@endif
									</ul>
								  <ul class="nav navbar-nav second_navbar">
									@if($session_set!='')
										<li class="dropdown navbar_pro_dropdown_set">
										<a class="dropdown-toggle" data-toggle="dropdown" href="#.">
											<div class="header_profile_drop_down">  
												@if($ses1) 
													<img src="@if($ses1['session_user_type']=='1') {{ URL::to('/') }}/public/images/{{ $ses1['session_user_profile_file_path'] }} @else {{ $ses1['session_user_profile_file_path'] }} @endif" class="img-responsive img-circle profil_header_img" alt="profile">
												@endif
												@if($ses2) 
													<img src="@if($ses2['session_tutor_type']=='1') {{ URL::to('/') }}/public/images/{{ $ses2['session_tutor_profile_file_path'] }} @else {{ $ses2['session_tutor_profile_file_path'] }} @endif" class="img-responsive img-circle profil_header_img" alt="profile">
												@endif
												@if($ses3) 
													<img src="{{ URL::to('/') }}/{{ $ses3['sessImage'] }} "
													class="img-responsive img-circle profil_header_img" alt="profile">
												@endif
												<div class="profil_details">
													<span>
														@if($ses1) {{ $ses1['session_user_name'] }} @endif
														@if($ses2) {{ $ses2['session_tutor_name'] }} @endif
														@if($ses3) {{ $ses3['sessName'] }} @endif
													</span>
													<small>
														@if($ses1) {{ $ses1['session_user_role'] }} @endif
														@if($ses2) {{ $ses2['session_tutor_role'] }} @endif
														@if($ses3) {{ $ses3['sessRole'] }} @endif
													</small>
												</div>
												<i class="fa fa-caret-down" aria-hidden="true"></i>
											</div>
										</a>
										<ul class="dropdown-menu">
											<li>
												@if($ses1) <a href='{{ url("Dashboard")}}'>Dashboard</a>@endif
												@if($ses2) <a href='{{ url("TutorDashboard")}}'>Dashboard</a>@endif
												@if($ses3) <a href='{{ url("ParentDashboard")}}'>Dashboard</a>@endif
											</li>
											<li><a href="#.">Settings</a></li>
											<li class="logout_list"><a href="#." class="btn logout_btn" data-toggle="modal" data-target="#logout_popup">LOGOUT</a></li>
										</ul>
										</li>
									@endif
								  </ul>
								</div>
							 </nav> 
						</div>
					</div>
				</header>
				<!-- header section end here-->
				@endif
				