				<!-- footer section start Here -->
				<footer class="footer-section" style="background-image:url(./img/footer-img.jpg);">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="footer_details">
									<div class="row">
										<div class="col-md-4 col-sm-6">
											<div class="foot_blogs">
												<span class="foot-logo">
													<a href="{{url ('ParentDashboard')}}">
														<img src="{{ url('public/assets/ParentAssets/img/footer-logo.png')}}" class="img-responsive" alt="logo"/>
													</a>
												</span>
												<div class="foot_web_info">
													<p>
														Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,<br><br> when an unknown printer took a galley of type and scrambled it to make a type specimen book.
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-4 col-sm-6">
											<div class="foot_blogs">
												<div class="fot_blo_head">
													<h4>Contact</h4>
												</div>
												<div class="foot_web_info_sec">
													<div class="cont_us_link">
														<span>Email : </span>
														<a href="mailto:info@thinkbrainwave.com">info@thinkbrainwave.com</a>
													</div>
													<p>
														Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,<br><br> when an unknown printer took a galley of type and scrambled it to make a type specimen book.
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-3 pull-right col-sm-6">
											<div class="foot_blogs">
												<div class="fot_blo_head">
													<h4>Useful Link</h4>
												</div>
												<div class="foot_web_info_sec">
													<ul>
														<li><a href="{{url ('Home')}}">Home</a></li>
														<li><a href="{{url ('About')}}">About Us</a></li>
														<li><a href="{{url ('ContactUs')}}">Contact Us</a></li>
														<li><a href="{{url ('Faq')}}">FAQs</a></li>
														<li><a href="{{url ('PrivacyPolicy')}}">Privacy Policy</a></li>
														<li><a href="{{url ('TermsAndService')}}">Terms and Services</a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="soacial-footer-links">
									<ul>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Facebook</span><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Google-plus</span><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Twitter</span><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Snapchat</span><i class="fa fa-snapchat" aria-hidden="true"></i></a></li>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Instagram</span><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
									 </ul>
								</div>
							</div>
						</div>
					</div>
					<div class="foot_not_info">
						<p>Copyright &copy; 2018 Thinkbrainwave. All rights reserved.</p>
					</div>
				</footer>
				<!-- footer section end Here -->
				
				<!--popup start here-->
					  
					  <!-- logout popup Start Here -->
					  <div class="modal fade" id="logout_popup" role="dialog">
						<div class="modal-dialog model_forgot_pass_popup">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/ParentAssets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="moel_details">
									<span class="model_header">
										<h3>LOGOUT</h3>
									</span>
									<div class="model_content">
										<p>Are you sure you want to logout.</p>
										<div class="pop_btn_sect">
											<a href="{{url('logout')}}" class="btn popup_coom_btn">Logout</a>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- logout popup end Here -->
					   <!-- view past session details Start Here -->
					  <div class="modal fade" id="plan_session_details" role="dialog">
						<div class="modal-dialog session_details_popup_width">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/ParentAssets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="model_details">
									<div class="section_heading">
										<h4>Roland Claven - Maths - Grade 5</h4>
									</div>
									<div class="model_content">
										
										<div class="my_sessiondetails">
											<div class="my_session_heading">
												<h4>SESSION DETAILS</h4>
											</div>
											<div class="session_details_list_new">
												<ul>
													<li><span>Session Type:</span>Face 2 Face Session</li>
													<li><span>Date:</span>06.11.2018</li>
													<li><span>Time:</span>03.30 pm to 04.30 pm</li>
													<li><span>Location:</span>Royal homes, A2 Wing, Johannesberg</li>
													<li><span>Topic:</span>Math - Algebra</li>
												<ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- view past session details popup end Here -->
					  
				<!--popup end here-->
			</div>
			<!--js link start here-->
			
			<script>
				//full calender width js here

					var calender = document.querySelector(".calender"),//container of calender
					topDiv = document.querySelector('.month'),
					monthDiv = calender.querySelector("h1"),//h1 of monthes
					yearDiv = calender.querySelector('h2'),//h2 for years
					weekDiv = calender.querySelector(".weeks"),//week container
					dayNames = weekDiv.querySelectorAll("li"),//dayes name
					dayItems = calender.querySelector(".days"),//date of day container
					prev = calender.querySelector(".prev"),
					next = calender.querySelector(".next"),

					// date variables
					years = new Date().getFullYear(),
					monthes = new Date(new Date().setFullYear(years)).getMonth(),
					lastDayOfMonth = new Date(new Date(new Date().setMonth(monthes + 1)).setDate(0)).getDate(),
					dayOfFirstDateOfMonth = new Date(new Date(new Date().setMonth(monthes)).setDate(1)).getDay(),

					// array to define name of monthes
					monthNames = ["January", "February", "March", "April", "May", "June",
								  "July", "August", "September", "October", "November", "December"],
					colors = ['#FFA549', '#ABABAB', '#1DABB8', '#953163', '#E7DF86', '#E01931', '#92F22A', '#FEC606', '#563D28', '#9E58DC', '#48AD01', '#0EBB9F'],
					i,//counter for day before month first day in week
					x,//counter for prev , next
					counter;//counter for day of month  days;


				//display dayes of month in items
				function days(x) {
				  'use strict';
				  dayItems.innerHTML = "";
				  monthes = monthes + x;

				  /////////////////////////////////////////////////
				  //test for last month useful while prev ,max prevent go over array
				  if (monthes > 11) {
					years = years + 1;
					monthes = new Date(new Date(new Date().setFullYear(years)).setMonth(0)).getMonth();//ترجع الشهر لاول شهر فى السنه الجديده
				  }
				  if (monthes < 0) {
					years = years - 1;
					monthes = new Date(new Date(new Date().setFullYear(years)).setMonth(11)).getMonth();//ترجع الشهر لاخر شهر فى السنه اللى فاتت
				  }
				  //هعرف اخر يوم واول يوم فى الشهر تانى علشان الشهر بيتغير مع next,prev
				  lastDayOfMonth = new Date(new Date(new Date(new Date().setFullYear(years)).setMonth(monthes + 1)).setDate(0)).getDate();//اخر يوم فى الشهر
				  dayOfFirstDateOfMonth = new Date(new Date(new Date(new Date().setFullYear(years)).setMonth(monthes)).setDate(1)).getDay();//بداية الشهر فى اى يوم من ايام الاسبوع؟
				  /////////////////////////////////////////////////
				  yearDiv.innerHTML = years;
				  monthDiv.innerHTML = monthNames[monthes];
				  for (i = 0; i <= dayOfFirstDateOfMonth; i = i + 1) {
					if (dayOfFirstDateOfMonth === 6) { break; }
					dayItems.innerHTML += "<li> - </li>";
				  }
				  for (counter = 1; counter <= lastDayOfMonth; counter = counter + 1) {
					dayItems.innerHTML += "<li>" + (counter) + "</li>";


				  }
				  topDiv.style.background = colors[monthes];
				  dayItems.style.background = colors[monthes];
				  //تمييز اليوم الحالى
				  if (monthes === new Date().getMonth() && years === new Date().getFullYear()) {
					//فى حالة التاريخ الحالى =الخلفيه خضراء
					dayItems.children[new Date().getDate() + dayOfFirstDateOfMonth].style.background = "#faa0a0";
				  }
				}
				prev.onclick = function () {
				  'use strict';
				  days(-1);//decrement monthes
				};
				next.onclick = function () {
				  'use strict';
				  days(1);//increment monthes
				};
				days(0);
					
			</script>

<script type="text/javascript">
		function getUserIdModal(id){
		
			document.getElementById("user_id").value=id;
				 $('#change_password_popup').modal('show');
		}
</script>

					  <!-- change password popup Start Here -->
					  <div class="modal fade" id="change_password_popup" role="dialog">
							<div class="modal-dialog change_password_popup_width">
								<!-- Modal content-->
								<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
									<img src="{{url('public/assets/ParentAssets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
									</button>
								</div>
								<div class="modal-body">
									<div class="model_details">
										<div class="section_heading_sec">
											<h4>CHANGE PASSWORD</h4>
										</div>
										<form id="passwordForm" method="post" class="form1" enctype="multipart/form-data">
										@csrf

											<input type="hidden" name="user_id" id="user_id">

											<div class="model_content">
												<div class="add_topic_form">
													<div class="row">
														<div class="col-md-12">
															<div class="form_set">
																<span>OLD PASSWORD :</span>
																<div class="input_box_set_popup">
																	<input type="password"  name ="old_pass" class="form-control required_check" placeholder="Enter Old Password" id="old_password"/>

																</div>
															</div>
														</div>
														<div class="col-md-12">
															<div class="form_set">
																<span>NEW PASSWORD :</span>
																<div class="input_box_set_popup"><input type="password" name ="new_pass" class="form-control required_check" id="new_password" placeholder="Enter New Password"/>
																
																</div>
															</div>
														</div>
														<div class="col-md-12">
															<div class="form_set">
																<span>CONFIRM PASSWORD :</span>
																<div class="input_box_set_popup"><input type="password" name ="confirm_pass" class="form-control required_check" id="retype_password" placeholder="Enter Confirm Password"/>
																
																</div>
															</div>
														</div>
														
														<div class="col-md-12">
															<div class="main_profil_btn">
																<input type ="submit" class="btn save_cancel_btn margin-right-10px" value="Update" id="submitPass">
																<a href="#." class="btn save_cancel_btn" data-dismiss="modal">Cancel</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
								</div>
							</div>
							</div>
							<!--popup end here-->
						</div>
					  <!-- change password popup end Here -->

 <script>
		jQuery(document).ready(function(){
			$('#passwordForm').validate({
			rules: {

				old_password: {
					required: true
				},

				new_password: {
					required: true
				},
				retype_password: {
						required: true,
						equalTo: "#new_password"
				}
			}
		});
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			
			$("#submitPass").click(function(e){
				if ($('#passwordForm').valid()) {
					e.preventDefault();
					var user_id = $("input[id=user_id]").val();
					var old_password = $("input[id=old_password]").val();
					var new_password = $("input[id=new_password]").val();
					var retype_password = $("input[id=retype_password]").val();
					if(new_password==retype_password)
					{
						$.ajax({
								type:'POST',
								url:'SaveNewPass',
								data:{user_id:user_id, old_password:old_password,  new_password:new_password, retype_password:retype_password },
								success:function(data){
									
									if(data.status == '1')
									{	
										if(data.role == '4') {
											$('#userRoleMsg').html('<a href="{{url("Signin")}}" class="btn popup_coom_btn">OK</a>');
										} else {
											$('#userRoleMsg').html('<a href="{{url("ParentStudentProfile")}}" class="btn popup_coom_btn">OK</a>');
										}
										$('#change_password_popup').modal('hide');
										$('#pass-success-msg').modal('show');
										$('#showsuccessmsg').html(data.msg);
										
										
									}
									if(data.status == '0')
									{
										$('#pass-error-msg').modal('show');
										$('#showerrormsg').html(data.msg);
										
									}
									
								}
							});
						}
					else
					{
						$('#pass-error-msg').modal('show');
						$( '#showerrormsg' ).html('New password and confirm password should be same.');
					}
				}
			});
		});
</script>						

			<!-- password error popup Start Here -->
				<div class="modal fade" id="pass-error-msg" role="dialog">
					<div class="modal-dialog model_forgot_pass_popup">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="moel_details">
									<span class="model_header">
										<h3>Error</h3>
									</span>
									<div class="model_content">
										<p id="showerrormsg"></p>
										<div class="pop_btn_sect">
											<button data-dismiss="modal" class="btn popup_coom_btn">OK</button>
										</div>
									</div>
								</div>
							</div>
						  </div>
					</div>
				</div>
			<!-- password error popup end Here -->

			<!-- password success popup Start Here -->
			<div class="modal fade" id="pass-success-msg" role="dialog">
					<div class="modal-dialog model_forgot_pass_popup">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="moel_details">
									<span class="model_header">
										<h3>Success</h3>
									</span>
									<div class="model_content">
										<p id="showsuccessmsg"></p>
										<div class="pop_btn_sect">
											<span id="userRoleMsg">
												
											</span
										</div>
									</div>
								</div>
							</div>
						  </div>
					</div>
				</div>
			<!-- password success popup end Here -->			


<script>
		function getChildUserIdModal(id){
			// alert(id);
			// document.getElementById("user_id").value=id;
			$('#demoModal').modal('show');
		}

		var date = new Date();
    	var pastDate = date.getFullYear()-20 + "-" + (date.getMonth()+1) + "-" + date.getDate();
		$('.pastDateParent').datepicker({	
			format: 'yyyy-mm-dd',		
			autoclose: 1,		
			weekStart: 1,		
			todayBtn:  1,		
			todayHighlight: 1,
			endDate: pastDate
		});
		var paststudent = date.getFullYear()-5 + "-" + (date.getMonth()+1) + "-" + date.getDate();
		$('.pastDateStudent').datepicker({	
			format: 'yyyy-mm-dd',		
			autoclose: 1,		
			weekStart: 1,		
			todayBtn:  1,		
			todayHighlight: 1,
			endDate: paststudent
		});
</script>

			<!-- password error popup Start Here -->
			<div class="modal fade" id="demoModal" role="dialog">
				<div class="modal-dialog model_forgot_pass_popup">
						<!-- Modal content-->
						<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							</button>
						</div>
						<div class="modal-body">
							<div class="moel_details">
								<span class="model_header">
									<h3>Error</h3>
								</span>
								<div class="model_content">
									
								</div>
							</div>
						</div>
						</div>
				</div>
			</div>
			<!-- password error popup end Here -->			

			<!--start js  for calender-->
			<!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script> -->
			<script src="{{ url('public/assets/fullcalendar/moment.min.js')}}"></script>

			
			<!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script> -->
			<script src="{{ url('public/assets/fullcalendar/fullcalendar.min.js')}}"></script>


			<!-- <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script> -->

			<!--end js for calender-->
			
			<!--js link start here-->
			
			<script src="{{ url('public/assets/js/owl.carousel.min.js')}}"></script>
			<script src="{{ url('public/assets/js/bootstrap.min.js')}}"></script>
			<script src="{{ url('public/assets/js/bootstrap-datetimepicker.min.js')}}"></script>
			<script src="{{ url('public/assets/js/custom.js')}}"></script>
			<script src="{{ url('public/assets/js/jquery.scrollbar.js')}}"></script>

			<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.js"></script>
			
			<!--js link end here-->
			<!--js link end here-->

			<!-- validation -->
			<script src="{{ url('public/js/jquery.validate.js')}}"></script>
 			<!-- end validation -->

		</body>
</html>
