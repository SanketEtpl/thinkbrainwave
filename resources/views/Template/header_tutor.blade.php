<!DOCTYPE html>
<html lang="en">
@php
	$val=Session::get('loginSessionTutor');
	$id = $val['session_tutor_id'];
	$role = $val['session_tutor_role_id'];
	$CountUnreadNotificationData = \App\Http\Controllers\AllCommonDataController::NotificationUnreadCount($id,$role);	
			
@endphp
	<head>
		<title>Dashboard</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<!--css link start here-->
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		
		<link rel="icon" type="imge/png" sizes="16x16" href="{{ url('public/assets/img/favicon.png')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/TutorAssets/css/bootstrap.min.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/TutorAssets/css/font-awesome-4.7.0/css/font-awesome.min.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/TutorAssets/css/style.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/TutorAssets/css/media.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/TutorAssets/fonts/stylesheet.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/TutorAssets/css/jquery.scrollbar.css')}}"/>

		<link rel="stylesheet" type="text/css" href="{{ url('public/datepicker/bootstrap-datepicker.css')}}"/>
		
		<link rel='stylesheet' href="{{ url('public/datepicker/bootstrap-datepicker.min.css')}}" />

		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/TutorAssets/css/bootstrap-datetimepicker.min.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/TutorAssets/css/easy-responsive-tabs.css')}}"/>
		<!--css link end here-->
		<!--start css for calender-->

		  <link rel='stylesheet' href="{{ url('public/assets/fullcalendar/fullcalendar.min.css')}}" />
		<!--end  css for calender-->
		<script src="{{ url('public/assets/TutorAssets/js/jquery.min.js') }}"></script>
		
		<script src="{{ url('public/datepicker/bootstrap-datepicker.js')}}"></script>
		<style>	
			.help-block 
			{
				color:red !important;
			}
			.error
			{
				color:red;
			}
		</style>
		
	</head>
	<body>
		<!-- Page wrapper Start here-->
		<div class="wrapper">
			<!-- header section Start here-->
			<header class="header_section" id="my-navbar-two-set">
				<div class="col-md-12">
					<div class="head_navbar">
						<nav class="navbar">
							<div class="navbar-header">
							  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							  </button>
							  <a class="navbar-brand nav_logo" href='{{ url("Home")}}'>
								<img src="{{ url('public/assets/TutorAssets/img/logo_header.png')}}" class="img-responsive" alt="logo"/>
							  </a>
							</div>
							<div class="navbar_set">
							  <ul class="nav navbar-nav collapse navbar-collapse first_navbar" id="myNavbar">
								<li class="{{ Route::currentRouteName() == '' ? 'active' : '' }}"><a href='{{ url("TutorDashboard")}}'>Dashboard</a></li>
								<li class="{{ Route::currentRouteName() == 'tutor_my_plan' ? 'active' : '' }}"><a href='{{ url("TutorMyPlan")}}'>My Plan</a></li>
								<li class="{{ Route::currentRouteName() == 'tutor_account' ? 'active' : '' }}"><a href='{{ url("TutorAccount")}}'>My Account</a></li>
							  </ul>
							  <ul class="nav navbar-nav second_navbar">
								<!-- <li>
									<a href="#." class="dropdown">
										<img src="{{ url('public/assets/TutorAssets/img/search.png')}}" class="img-responsive" data-toggle="dropdown" alt="search"/>
										<div class="dropdown-menu search_box_set">  
											<div class="input-group">
												<input type="text" class="form-control" placeholder="Search">
												<div class="input-group-btn">
												<button class="btn search_btn_btn" type="submit">
													<i class="fa fa-search" aria-hidden="true"></i>
												</button>
												</div>
											</div>
										</div>
									</a>
								</li> -->
								<li>
									<a href='{{ url("TutorNotification")}}'>
										<div class="notifiaction_set">
											<img src="{{ url('public/assets/TutorAssets/img/nitification.png')}}" class="img-responsive icon_set" alt="nitification"/>
											<img src="{{ url('public/assets/TutorAssets/img/notifiation-active.png')}}" class="img-responsive active_icon_set" alt="nitification"/>
											<span class="badge">{{ $CountUnreadNotificationData }}</span>
										</div>
									</a>
								</li>
								@php
								 	
								 	
									$result_tu = \App\Http\Controllers\AllProfileDataController::ShowProfileData($id,$role);
									if($result_tu['profile_file_path']=='')
										{
										
											$session_image = "no-image.png";
										}
										else{
											$session_image = $result_tu['profile_file_path'];
										}
								@endphp
								<li class="dropdown navbar_pro_dropdown_set">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#.">
										<div class="header_profile_drop_down">
											<img src="@if($val['session_tutor_type']=='1') {{ URL::to('/') }}/public/images/{{ $session_image }} @else {{ $session_image }} @endif" class="img-responsive img-circle profil_header_img" alt="profile"/>
											<div class="profil_details">
											
												<span>{{ $result_tu['name'] }}</span>
												<small>Tutor</small>
											</div>
											<i class="fa fa-caret-down" aria-hidden="true"></i>
										</div>
									</a>
									<ul class="dropdown-menu">
									@if($val)
									  <li><a href='{{ url("TutorMyProfile")}}'>My Profile</a></li>
									  <li><a href="#.">Settings</a></li>
									  <li class="logout_list"><a href="#." class="btn logout_btn" data-toggle="modal" data-target="#logout_popup">LOGOUT</a></li>
									
										@else
												<li class="logout_list"><a href='{{ url("Signin")}}' class="btn logout_btn">SignIn</a></li>
											
									@endif	
									</ul>
								</li>
							  </ul>
							</div>
						 </nav> 
					</div>
				</div>
			</header>
			<!-- header section end here-->