	<!-- footer section start Here -->
	<footer class="footer-section" style="background-image:url(./img/footer-img.jpg')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="footer_details">
									<div class="row">
										<div class="col-md-4 col-sm-6">
											<div class="foot_blogs">
												<span class="foot-logo">
													<a href='{{ url("Home")}}'>
														<img src="{{ url('public/assets/TutorAssets/img/footer-logo.png')}}" class="img-responsive" alt="logo"/>
													</a>
												</span>
												<div class="foot_web_info">
													<p>
														Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,<br><br> when an unknown printer took a galley of type and scrambled it to make a type specimen book.
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-4 col-sm-6">
											<div class="foot_blogs">
												<div class="fot_blo_head">
													<h4>Contact</h4>
												</div>
												<div class="foot_web_info_sec">
													<div class="cont_us_link">
														<span>Email : </span>
														<a href="mailto:info@thinkbrainwave.com">info@thinkbrainwave.com</a>
													</div>
													<p>
														Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,<br><br> when an unknown printer took a galley of type and scrambled it to make a type specimen book.
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-3 pull-right col-sm-6">
											<div class="foot_blogs">
												<div class="fot_blo_head">
													<h4>Useful Link</h4>
												</div>
												<div class="foot_web_info_sec">
													<ul>
														<li><a href='{{ url("Home")}}'>Home</a></li>
														<li><a href='{{ url("About")}}'>About Us</a></li>
														<li><a href='{{ url("ContactUs")}}'>Contact Us</a></li>
														<li><a href='{{ url("Faq")}}'>FAQs</a></li>
														<li><a href='{{ url("PrivacyPolicy")}}'>Privacy Policy</a></li>
														<li><a href='{{ url("TermsAndService")}}'>Terms and Services</a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="soacial-footer-links">
									<ul>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Facebook</span><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Google-plus</span><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
										<!-- <li><a href="#." class="tooltip"><span class="tooltiptext">Twitter</span><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Snapchat</span><i class="fa fa-snapchat" aria-hidden="true"></i></a></li>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Instagram</span><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
									 -->
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="foot_not_info">
						<p>Copyright &copy; 2018 Thinkbrainwave. All rights reserved.</p>
					</div>
				</footer>
				<!-- footer section end Here -->
				<!--popup start here-->
				<!-- logout popup Start Here -->
					<div class="modal fade" id="logout_popup" role="dialog">
						<div class="modal-dialog model_forgot_pass_popup">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/TutorAssets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="moel_details">
									<span class="model_header">
										<h3>LOGOUT</h3>
									</span>
									<div class="model_content">
										<p>Are you sure you want to logout.</p>
										<div class="pop_btn_sect">
											<a href='{{ url("logout")}}' class="btn popup_coom_btn">Logout</a>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					</div>
				<!-- logout popup end Here -->
				<!-- view session details Start Here -->
					<div class="modal fade" id="date_sestion" role="dialog">
						<div class="modal-dialog session_details_popup_width">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/TutorAssets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="model_details">
									<div class="section_heading">
										<h4>Rohan Sharma - Maths - Grade 5</h4>
									</div>
									<div class="model_content">
										
										<div class="my_sessiondetails zero_margin">
											<div class="session_details_list_new">
												<ul>
													<li><span>Topic:</span>English - FOS</li>
													<li><span>Session Type:</span>Face to Face Session</li>
													<li><span>Date:</span>Mon, 03/06/2018</li>
													<li><span>Time:</span>03.30 pm to 04.30 pm</li>
													<li><span>Location:</span>Royal homes, A2 Wing, Johannesberg</li>
												</ul>
											</div>
											<button class="btn right_top_btn small_comm_btn" data-dismiss="modal" data-toggle="modal" data-target="#cancel_session_popup">Cancel Session</button>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					</div>
				<!-- view session details popup end Here -->
			
				<!-- cancel session popup Start Here -->
					  <div class="modal fade" id="cancel_session_popup" role="dialog">
						<div class="modal-dialog session_cancel_pass_popup">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/TutorAssets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="moel_details">
									<span class="model_header model_head_new_set">
										<h4>ARE YOU SURE <br> YOU WANT TO CANCEL SESSION ?</h4>
									</span>
									<div class="model_content">
										<div class="pop_btn_sect">
											<a href="#." class="btn save_cancel_btn" data-dismiss="modal" data-toggle="modal" data-target="#reason_comment_popup">Yes</a>
											<a href="#." data-dismiss="modal" class="btn save_cancel_btn ">No</a>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- cancel session popup end Here -->
					  
					  <!-- reason comment popup Start Here -->
					  <div class="modal fade" id="reason_comment_popup" role="dialog">
						<div class="modal-dialog session_cancel_pass_popup">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/TutorAssets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="moel_details">
									<div class="section_heading_sec">
										<h4>REASON</h4>
									</div>
									<div class="model_content">
										<div class="model_comment_box">
											<textarea class="form-control" placeholder="Enter Comments"></textarea>
										</div>
										<div class="pop_btn_sect">
											<a href="#." class="btn save_cancel_btn" data-dismiss="modal" data-toggle="modal" data-target="#add_new_event">Submit</a>
											<a href="#." data-dismiss="modal" class="btn save_cancel_btn">Cancel</a>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- reason comment session popup end Here -->
					
				
				
				
				<!--popup end here-->
			</div>
			<!--js link start here-->
			<!--start js  for calender-->
			<script src="{{ url('public/assets/fullcalendar/moment.min.js')}}"></script>
			<script src="{{ url('public/assets/fullcalendar/fullcalendar.min.js')}}"></script>
			<!--end js for calender-->
			<script src="{{ url('public/assets/TutorAssets/js/easy-responsive-tabs.js') }}"></script>
			<script src="{{ url('public/assets/TutorAssets/js/bootstrap.min.js') }}"></script>
			
			<script src="{{ url('public/assets/TutorAssets/js/bootstrap-datetimepicker.min.js') }}"></script>
			<script src="{{ url('public/assets/TutorAssets/js/custom.js') }}"></script>
			<script src="{{ url('public/assets/TutorAssets/js/owl.carousel.min.js') }}"></script>
			<script src="{{ url('public/assets/TutorAssets/js/jquery.scrollbar.js') }}"></script>
			<!-- validation -->
			<script src="{{ url('public/js/jquery.validate.js')}}"></script>
			<script src="{{ url('public/js/validation.js')}}"></script>
			<script src="{{ url('public/js/canvasJs/jquery.canvasjs.min.js')}}"></script>

 			<!-- end validation -->
			
			<!--js link end here-->
			<!-- <script> -->
				<!-- jQuery(document).ready(function(){ -->
					<!-- jQuery('.scrollbar-inner').scrollbar(); -->
				<!-- }); -->
				<!-- </script> -->
			<script>
				jQuery(document).ready(function(){
					jQuery('.scrollbar-inner').scrollbar();

					$(".modal").on("hidden.bs.modal", function(){
						$(this)
						.find("input,textarea,select")
							.val('')
							.end()
						.find("input[type=checkbox], input[type=radio]")
							.prop("checked", "")
							.end();
					});
				});
				
		    </script>
		    
		
			<!--js link end here-->
		</body>
</html>
