<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Dashboard</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<!--css link start here-->
		<link rel="icon" type="imge/png" sizes="16x16" href="{{ url('public/assets/img/favicon.png')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/bootstrap.min.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/font-awesome-4.7.0/css/font-awesome.min.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/ParentAssets/css/style.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/media.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/fonts/stylesheet.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/jquery.scrollbar.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/bootstrap-datetimepicker.min.css')}}"/>

		
		<!--start css for calender-->
		 <!-- <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' /> -->

		 <link rel='stylesheet' href="{{ url('public/assets/fullcalendar/fullcalendar.min.css')}}" />
		<!--end  css for calender-->
	
		<script src="{{ url('public/assets/js/jquery.min.js')}}"></script>
		
		<!-- <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->

		<script src="{{ url('public/js/validation.js')}}"></script>

		<!-- datepicker -->
		<script src="{{ url('public/datepicker/bootstrap-datepicker.js')}}"></script>
		<link rel="stylesheet" type="text/css" href="{{ url('public/datepicker/bootstrap-datepicker.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{ url('public/datepicker/bootstrap-datepicker.min.css')}}"/>

		<meta name="csrf-token" content="{{ csrf_token() }}" />

		<style>             
			.help-block {               
				color:red !important;           
			}           
			.error {               
				color:red;          
			}       
		</style>
	</head>
	@php
		$val=Session::get('loginSessionParent');
		$id = $val['sessId'];
    $role = $val['sessRoleId'];
		$CountUnreadNotificationData = \App\Http\Controllers\AllCommonDataController::NotificationUnreadCount($id,$role);	
												
	@endphp
		<body>
			<!-- Page wrapper Start here-->
			<div class="wrapper">
				<!-- header section Start here-->
				<header class="header_section" id="my-navbar-two-set">
					<div class="col-md-12">
						<div class="head_navbar">
							 <nav class="navbar">
								<div class="navbar-header">
								  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								  </button>
								  <a class="navbar-brand nav_logo" href="{{url ('Home')}}">
									 <img src="{{url('public/assets/ParentAssets/img/logo_header.png')}}" class="img-responsive" alt="logo"/>
								  </a>
								</div>
								<div class="navbar_set">
								  <ul class="nav navbar-nav collapse navbar-collapse first_navbar" id="myNavbar">
									<li class="{{ Route::currentRouteName() == '' ? 'active' : '' }}"><a href="{{url ('ParentDashboard')}}">Dashboard</a></li>
									<li class="{{ Route::currentRouteName() == 'my_plan' ? 'active' : '' }}"><a href="{{url ('ParentMyPlan')}}">My Plan</a></li>
									<li class="{{ Route::currentRouteName() == 'my_account' ? 'active' : '' }}"><a href="{{url ('ParentMyAccount')}}">My Account</a></li>
								  </ul>
								  <ul class="nav navbar-nav second_navbar">
									<li>
										<a href="#." class="dropdown">
											<img src="{{url('public/assets/ParentAssets/img/search.png')}}" class="img-responsive" data-toggle="dropdown" alt="search"/>
											<div class="dropdown-menu search_box_set">  
												<div class="input-group">
													<input type="text" class="form-control" placeholder="Search">
													<div class="input-group-btn">
													<button class="btn search_btn_btn" type="submit">
														<i class="fa fa-search" aria-hidden="true"></i>
													</button>
													</div>
												</div>
											</div>
										</a>
									</li>
									<li>
										<a href="{{url ('AllNotification')}}">
											<div class="notifiaction_set">
												<img src="{{url('public/assets/ParentAssets/img/nitification.png')}}" class="img-responsive icon_set" alt="nitification"/>
												<img src="{{url('public/assets/ParentAssets/img/notifiation-active.png')}}" class="img-responsive active_icon_set" alt="nitification"/>
												<span class="badge">{{ $CountUnreadNotificationData }}</span>
											</div>
										</a>
									</li> 
									<li class="dropdown navbar_pro_dropdown_set">
										<a class="dropdown-toggle" data-toggle="dropdown" href="#.">
											<div class="header_profile_drop_down">
												@php
													$parentVal=Session::get('loginSessionParent');
												@endphp
												
												<img src="{{url($parentVal['sessImage'])}}" class="img-responsive img-circle profil_header_img" alt="profile"/>
												<div class="profil_details">
													<span>{{$parentVal['sessName']}}</span>
													<small>Parent</small>
												</div>
												<i class="fa fa-caret-down" aria-hidden="true"></i>
											</div>
										</a>
										<ul class="dropdown-menu">
										  <li><a href="{{url ('ParentMyProfile')}}">My Profile</a></li>
										  <li><a href="#.">Settings</a></li>
										  <li class="logout_list"><a href="#." class="btn logout_btn" data-toggle="modal" data-target="#logout_popup">LOGOUT</a></li>
										</ul>
									</li>
								  </ul>
								</div>
							 </nav> 
						</div>
					</div>
				</header>
				<!-- header section end here-->