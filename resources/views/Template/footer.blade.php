<!-- footer section start Here -->
				<footer class="footer-section" style="background-image:url({{ url('public/assets/img/footer-img.jpg')}});">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="footer_details">
									<div class="row">
										<div class="col-md-4 col-sm-6">
											<div class="foot_blogs">
												<span class="foot-logo">
													<a href='{{ url("Home")}}'>
														<img src="{{ url('public/assets/img/footer-logo.png')}}" class="img-responsive" alt="logo"/>
													</a>
												</span>
												<div class="foot_web_info">
													<p>
														Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,<br><br> when an unknown printer took a galley of type and scrambled it to make a type specimen book.
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-4 col-sm-6">
											<div class="foot_blogs">
												<div class="fot_blo_head">
													<h4>Contact</h4>
												</div>
												<div class="foot_web_info_sec">
													<div class="cont_us_link">
														<span>Email : </span>
														<a href="mailto:info@thinkbrainwave.com">info@thinkbrainwave.com</a>
													</div>
													<p>
														Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,<br><br> when an unknown printer took a galley of type and scrambled it to make a type specimen book.
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-3 pull-right col-sm-6">
											<div class="foot_blogs">
												<div class="fot_blo_head">
													<h4>Useful Link</h4>
												</div>
												<div class="foot_web_info_sec">
													<ul>
														<li><a href='{{ url("Home")}}'>Home</a></li>
														<li><a href='{{ url("About")}}'>About Us</a></li>
														<li><a href='{{ url("ContactUs")}}'>Contact Us</a></li>
														<li><a href='{{ url("Faq")}}'>FAQs</a></li>
														<li><a href='{{ url("PrivacyPolicy")}}'>Privacy Policy</a></li>
														<li><a href='{{ url("TermsAndService")}}'>Terms and Services</a></li>
													
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="soacial-footer-links">
									<ul>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Facebook</span><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Google-plus</span><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
										<!-- <li><a href="#." class="tooltip"><span class="tooltiptext">Twitter</span><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Snapchat</span><i class="fa fa-snapchat" aria-hidden="true"></i></a></li>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Instagram</span><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
									  -->
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="foot_not_info">
						<p>Copyright &copy; 2018 Thinkbrainwave. All rights reserved.</p>
					</div>
				</footer>
				<!-- popups start Here -->
				
					<!-- logout popup Start Here -->
					<div class="modal fade" id="logout_popup" role="dialog">
						<div class="modal-dialog model_forgot_pass_popup">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="moel_details">
									<span class="model_header">
										<h3>LOGOUT</h3>
									</span>
									<div class="model_content">
										<p>Are you sure you want to logout.</p>
										<div class="pop_btn_sect">
											<a href='{{ url("logout")}}' class="btn popup_coom_btn">Logout</a>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- logout popup end Here -->
					  <!-- sent errot popup end Here -->
					  <div class="modal fade" id="passwod_not_match_popup" role="dialog">
							<div class="modal-dialog model_forgot_pass_popup">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
										</button>
									</div>
									<div class="modal-body">
										<div class="moel_details">
											<span class="model_header">
												<h5>Error</h5>
											</span>
											<div class="model_content">
												<p>Password Not Match.</p>
												<div class="pop_btn_sect">
													<!-- <a href="" class="btn popup_coom_btn">OK</a> -->
													<lable class="btn popup_coom_btn" data-dismiss="modal">ok</lable>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- password not match popup end Here -->
						<!-- sent succesfully popup end Here -->
						<div class="modal fade" id="sent_error_popup" role="dialog">
							<div class="modal-dialog model_forgot_pass_popup">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
										</button>
									</div>
									<div class="modal-body">
										<div class="moel_details">
											<span class="model_header">
												<h5>Error</h5>
											</span>
											<div class="model_content">
												<p>Your email alredy registerd. <br>Please signup with another email.</p>
												<div class="pop_btn_sect">
													<!-- <a href="" class="btn popup_coom_btn">OK</a> -->
													<lable class="btn popup_coom_btn" data-dismiss="modal">ok</lable>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- sent errot popup end Here -->
						<!-- sent succesfully popup Start Here -->
						<div class="modal fade" id="sent_success_popup" role="dialog">
							<div class="modal-dialog model_forgot_pass_popup">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">
									<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
								</button>
								</div>
								<div class="modal-body">
									<div class="moel_details">
										<span class="model_header">
											<img src="{{ url('public/assets/img/sent.png')}}" class="img-responsive" alt="forgotpassword"/>
											<h5>Sent Successfully</h5>
										</span>
										<div class="model_content">
											<p id="resetsuccessmsg"></p>
											<div class="pop_btn_sect">
											<a href='{{ url("Signin")}}' class="btn popup_coom_btn">OK</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							</div>
						</div>
						<!-- sent succesfully popup end Here -->
						<!-- forgot password popup Start Here -->
						<div class="modal fade" id="forgot_password_popup" role="dialog">
							<div class="modal-dialog model_forgot_pass_popup">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">
									<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
								</button>
								</div>
								<div class="modal-body">
									<div class="moel_details">
										<span class="model_header">
											<img src="{{ url('public/assets/img/forgot_password.png')}}" class="img-responsive" alt="forgotpassword"/>
											<h5>Forgot Password</h5>
										</span>
										<form id="forgotPassForm" method="post" class="form1">
											@csrf
											<div class="model_content">
												<p>Please enter the email address you signed up with and <br>we'll send you a password reset Link.</p>
												<div class="pop_form">
													<span>EMAIL ADDRESS</span>
													<input id="email_address" type="text" name="email_address" class="form-control forgot_ps_input" />
												</div>
												<div class="pop_btn_sect">
													<input type="submit" class="btn popup_coom_btn" id="resetPass" value="RESET PASSWORD"></button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
							</div>
						</div>
						<!-- forgot password popup end Here -->

						<!-- reset password error popup Start Here -->
						<div class="modal fade" id="reset-error-msg" role="dialog">
							<div class="modal-dialog model_forgot_pass_popup">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
									</button>
									</div>
									<div class="modal-body">
										<div class="moel_details">
											<span class="model_header">
												<h3>Error</h3>
											</span>
											<div class="model_content">
												<p id="reseterrormsg"></p>
												<div class="pop_btn_sect">
													<button data-dismiss="modal" class="btn popup_coom_btn">OK</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<!-- reset password error popup end Here -->

						<!-- submit succesfully popup Start Here -->
						<div class="modal fade" id="submit_success_popup" role="dialog">
						<div class="modal-dialog model_forgot_pass_popup">
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">
								 <img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
							  </button>
							</div>
							<div class="modal-body">
								<div class="moel_details">
									<span class="model_header">
										<h5>Session Book Successfully</h5>
									</span>
									<div class="model_content">
										<div class="pop_btn_sect">
										<a href="" class="btn popup_coom_btn">OK</a>
										</div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					  </div>
						<!-- submit succesfully popup end Here -->
						<!-- password save popup end Here -->
						<div class="modal fade" id="password_save_popup" role="dialog">
							<div class="modal-dialog model_forgot_pass_popup">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
										</button>
									</div>
									<div class="modal-body">
										<div class="moel_details">
											<span class="model_header">
												<h5>Success</h5>
											</span>
											<div class="model_content">
												<p>Password save successfully.</p>
												<div class="pop_btn_sect">
													<lable class="btn popup_coom_btn" data-dismiss="modal">ok</lable>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						  <!-- password save popup end Here -->
						  <!-- not_password_popup end Here -->
						<div class="modal fade" id="not_password_popup" role="dialog">
							<div class="modal-dialog model_forgot_pass_popup">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<img src="{{ url('public/assets/img/pop_up_close.png')}}" class="img-responsive" alt="close"/>
										</button>
									</div>
									<div class="modal-body">
										<div class="moel_details">
											<span class="model_header">
												<h5>Error</h5>
											</span>
											<div class="model_content">
												<p>Old password does not exist.</p>
												<div class="pop_btn_sect">
													<!-- <a href="" class="btn popup_coom_btn">OK</a> -->
													<lable class="btn popup_coom_btn" data-dismiss="modal">ok</lable>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- not_password_popup end Here -->
						
				
					  
				<!-- popups end Here -->
				<!-- footer section end Here -->
				<!--bottom to top link start here-->
				<div id="stop" class="scrollTop">
					<span><i class="fa fa-angle-double-up"></i></span>
				</div>
				<!--bottom to top link end here-->
			</div>
			<!--js link start here-->
			<!--start js  for calender-->
			<!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script> -->
			<script src="{{ url('public/assets/fullcalendar/moment.min.js')}}"></script>

			
			<!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script> -->
			<script src="{{ url('public/assets/fullcalendar/fullcalendar.min.js')}}"></script>


			<!-- <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script> -->

			<!--end js for calender-->
			
			<script src="{{ url('public/assets/js/bootstrap.min.js')}}"></script>
			<script src="{{ url('public/assets/js/bootstrap-datetimepicker.min.js')}}"></script>
			
			<script src="{{ url('public/assets/js/owl.carousel.min.js')}}"></script>
			<script src="{{ url('public/assets/js/custom.js')}}"></script>
			<!-- <script src="{{ url('public/assets/js/materialize.min.js')}}"></script> -->

			<script src="{{ url('public/assets/js/jquery.scrollbar.js')}}"></script>
			<!-- validation -->
			<script src="{{ url('public/js/jquery.validate.js')}}"></script>
 			<!-- end validation -->
			<!--js link end here-->
			<script>
				jQuery(document).ready(function(){
					

					$(".modal").on("hidden.bs.modal", function(){
						$(this)
						.find("input,textarea,select")
							.val('')
							.end()
						.find("input[type=checkbox], input[type=radio]")
							.prop("checked", "")
							.end();
					});
				});
				
		    </script>

<script>
		jQuery(document).ready(function(){
			$('#forgotPassForm').validate({
			rules: {
 				email_address: {
					required: true
				}
			}
		});
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			
			$("#resetPass").click(function(e){
				if ($('#forgotPassForm').valid()) {
					e.preventDefault();
					var email_address = $("input[id=email_address]").val();
					$.ajax({
							type:'POST',
							url:'ResetPassLink',
							data:{email_address:email_address },
							success:function(data){
								
								if(data.status == '1')
								{	
									$('#forgot_password_popup').modal('hide');
									$('#sent_success_popup').modal('show');
									$('#resetsuccessmsg').html(data.msg);
									
								}
								if(data.status == '0')
								{
									$('#reset-error-msg').modal('show');
									$('#reseterrormsg').html(data.msg);
									
								}
								
							}
						});
					}
			});
		});
</script>	
		</body>
</html>
