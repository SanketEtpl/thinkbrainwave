<?php
return [
	
	'EMAIL_FROM' =>'exceptionaire123@gmail.com',
	'EMAIL_NAME' =>'Think BrainWave',
	'CONSTANTS_EXAMPLE' =>' Yogita',
	'AdminNotification' =>'Admin Notification',
	'SessionBook' =>'Session Book',
	'SessionAssign' =>'SessionAssign',
	'Session_1_Hour_Left' =>'Session 1 Hour Left',
	'Session_5_Min_left' =>'Session 5 Min left',
	'SessionStart' =>'Session Start',
	'SessionStop' =>'Session Stop',
	'SessionComplete' =>'Session Complete',
	'SessionTutorCancel' =>'Session Tutor Cancel',
	'SessionStudentCancel' =>'Session Student Cancel',
	'RatingByTutor' =>'Rating By Tutor',
	'RatingByStudent' =>'Rating By Student',
	'SessionParentCancel' =>'Session Parent Cancel',
];
?>

